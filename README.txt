Bachelor thesis "Country-dependant analysis of the LinkedIn-Leak" for the University of Innsbruck. 

Dir:
./code -> Code-Snipplets used at the beginning of the thesis (extract data from file/SQL, do some analysis over it
./database -> Results of the analysis, split by mostly their TLD
./full_experiment -> The "real" experiment at the end of my thesis.
./old_stuff -> OLD
./resources -> Paper i needed for my thesis
./writing material -> All writing stuff, Exposé, Init and Final.

Hidden:
55 GB SQL Data with ~118 mio. users/pw for obvious reasons
12 GB Raw SQL Dump of the LinkedIn Leak
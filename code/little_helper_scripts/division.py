import sys

for line in sys.stdin:
	a,b = map(int,line.split())
	print(str(format(round(((a/b))*100,2), '.2f')))
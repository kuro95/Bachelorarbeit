list_tld = ['fulllist', 'com', 'net', 'uk', 'fr', 'edu', 'br', 'it', 'in', 'org', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'gov', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'mil', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'eu', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'biz', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'info', 'ua', 'lv', 'Pk', 'sk', 'uy', 'sa', 'tv', 'rs', 'th', 'lt', 'cc', 'cat', 'ma', 'ee', 'fm', 'ke', 'si', 'zw', 'is', 've', 'cr', 'om', 'nu', 'lu', 'lk', 'tn', 'ec', 'cy', 'int', 'jo', 'lb', 'mk', 'na', 'eg', 'me', 'qa', 'ws', 'cm', 'bw', 'do', 'mu', 'coop']
path_tld = "D:\\Bachelorarbeit\\database\\hashes_per_tld\\"
path_new = "D:\\Bachelorarbeit\\database\\hashes_per_tld_cleartext\\"
path_pot = "D:\\Bachelorarbeit\\code\\hashcat\\"
path_stats = "D:\\Bachelorarbeit\\database\\"
file_pot = open(path_pot+'hashcat.potfile','r',encoding="utf8")
dict = {}
for line in file_pot:
  dict[line.split(':', 1)[0]] = line.split(':', 1)[1][:-1]
print("Done reading pot-file")

cracked_100 = []
cracked_500 = [] 
cracked_1000 = [] 
cracked_5000 = [] 
pw_rate = [] 
account_rate = [] 
tld_i = [] 
total_i = []

for tld in list_tld:
	file_tld = open(path_tld+tld+'.txt','r')
	file_new = open(path_new+tld+'.txt','w',encoding="utf8")
	cracked = 0
	i = -3
	amount = 0
	total = 0
	for line in file_tld:
		if(i >= 0):
			string = ""
			if(line.split(' ', 1)[0] in dict):
				string = dict[line.split(' ', 1)[0]]
				amount += int(line.split(' ', 1)[1]) 
				cracked += 1
			file_new.write(line[:-1]+"\t"+string+"\n")
			total += int(line.split(' ', 1)[1]) 
			i += 1
			if(i == 100):
				cracked_100.append(cracked)
			if(i == 500):
				cracked_500.append(cracked)
			if(i == 1000):
				cracked_1000.append(cracked)
			if(i == 5000):
				cracked_5000.append(cracked)
		else:
			if(i == -1):
				file_new.write(line[:-1]+" \tKlartext\n")
			else:
				file_new.write(line)
			i += 1
	if(i < 5000):
		cracked_5000.append(cracked)
	if (i < 1000):
		cracked_1000.append(cracked)
	if (i < 500):
		cracked_500.append(cracked)
	if (i < 100):
		cracked_100.append(cracked)
	file_new.write("\nCleartext PWs: "+str(cracked)+"/"+str(i)+" ("+str(round(((cracked/i))*100,2))+"%)")
	file_new.write("\nAccounts: "+str(amount)+"/"+str(total)+" ("+str(round(((amount/total))*100,2))+"%)")
	file_new.write("\n100-Rate: "+str(cracked_100[-1])+"/100 ("+str(round(((cracked_100[-1]/100))*100,2))+"%)")
	file_new.write("\n500-Rate: "+str(cracked_500[-1])+"/500 ("+str(round(((cracked_500[-1]/500))*100,2))+"%)")
	file_new.write("\n1000-Rate: "+str(cracked_1000[-1])+"/1000 ("+str(round(((cracked_1000[-1]/1000))*100,2))+"%)")
	file_new.write("\n5000-Rate: "+str(cracked_5000[-1])+"/5000 ("+str(round(((cracked_5000[-1]/5000))*100,2))+"%)")
	pw_rate.append(cracked)
	account_rate.append(amount)
	tld_i.append(i)
	total_i.append(total)
	file_tld.close()
	file_new.close()
	print(tld,i,cracked,amount)
file_pot.close()
file_stats = open(path_stats+'cracked_stats.txt','w')
file_stats.write("Cracking stats per TLD \n\n")
data_tld = "TLD 100 500 1000 5000 pw% acc% crack_pw Passwords crack_acc Accounts"
file_stats.write('{0[0]:<7}{0[1]:>7}{0[2]:>10}{0[3]:>10}{0[4]:>10}{0[5]:>10}{0[6]:>10}{0[7]:>20}{0[8]:>13}{0[9]:>13}{0[10]:>13}'.format(data_tld.split())+"\n\n")
i = 0
for tld in list_tld:
	data_tld=tld+" "+str(cracked_100[i])+" "+str(cracked_500[i])+" "+str(cracked_1000[i])+" "+str(cracked_5000[i])+" "+str(format(round(((pw_rate[i]/tld_i[i]))*100,2), '.2f'))+" "+str(format(round(((account_rate[i]/total_i[i]))*100,2), '.2f'))+" "+str(pw_rate[i])+" "+str(tld_i[i])+" "+str(account_rate[i])+" "+str(total_i[i])
	data_temp = data_tld.split()
	file_stats.write('{0[0]:<7}{0[1]:>7}{0[2]:>10}{0[3]:>10}{0[4]:>10}{0[5]:>10}{0[6]:>10}{0[7]:>20}{0[8]:>13}{0[9]:>13}{0[10]:>13}'.format(data_temp)+"\n")
	print(tld+" done!")
	i += 1
file_stats.close()
print("Finito!")
import mysql.connector
from mysql.connector import errorcode

try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
                                database='bachelorarbeit')
  cursor = cnx.cursor()
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
   
query = ("SELECT MEMBER_HASH AS hashed, count(*) AS amount FROM bachelorarbeit.net group by hashed order by amount desc LIMIT 20")
cursor.execute(query)
list_at = []
for(hashed, amount) in cursor:
	list_at.append((hashed, amount))
for item in list_at:
	print(str(item[0]) + " , " + str(item[1]))
cnx.close()
import sys

path_default = "G:\\Bachelorarbeit\\code\\little_helper_scripts\\" # Local path to the output folder

file_input = open(path_default+sys.argv[1],'r')
file_output = open(path_default+'output.txt','w')


for line in file_input:
	splitted = line.split()
	if(len(splitted) != 0):
		output_str = splitted[0]
		for word in splitted[1:]:
			output_str += " & "+word
		output_str += " \\\ \n"
		file_output.write(output_str)
	

file_input.close()
file_output.close()



import math

path_input = "D:\\Bachelorarbeit\\database\\hashes_per_tld\\"
path = "D:\\Bachelorarbeit\\database\\big\\"
list_tld = ['fulllist', 'com', 'net', 'uk', 'fr', 'edu', 'br', 'it', 'in', 'org', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'gov', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'mil', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'eu', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'biz', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'info', 'ua', 'lv', 'Pk', 'sk', 'uy', 'sa', 'tv', 'rs', 'th', 'lt', 'cc', 'cat', 'ma', 'ee', 'fm', 'ke', 'si', 'zw', 'is', 've', 'cr', 'om', 'nu', 'lu', 'lk', 'tn', 'ec', 'cy', 'int', 'jo', 'lb', 'mk', 'na', 'eg', 'me', 'qa', 'ws', 'cm', 'bw', 'do', 'mu', 'coop']
list_alpha_workfactor = [0.01,0.05,0.10,0.2,0.50]
list_alpha_guesswork = [0.01,0.05,0.10,0.25,0.50]
list_beta = [1,10,100,1000]

def workfactor(wanted_results_workfactor,hashes,counter,users,j):
	while(j < len(wanted_results_workfactor) and wanted_results_workfactor[j] <= counter):
		workfactor_results.append(hashes)
		workfactor_entropy.append(round(math.log2((hashes/(counter/users))),2))
		j += 1
	return j

def guesswork(wanted_results_guesswork,hashes,counter,users,k):
	while(k < len(wanted_results_guesswork) and wanted_results_guesswork[k] <= counter):
		beta_temp = float(counter)/users
		prob = 0.0
		for counter in range(1,hashes):
			prob += ((float(loaded_list[(counter-1)][1]))/users)*counter
		guesswork_results.append(round((((1-beta_temp)*hashes)+prob),2))
		guesswork_entropy.append(round(math.log2((((2*guesswork_results[k])/beta_temp)-1))+math.log2(1/(2-beta_temp)),2))
		k += 1
	return k

file_guesswork = open(path+'alpha_guesswork.txt','w')
file_guesswork_entropy = open(path+'alpha_guesswork_entropy.txt','w')
file_workfactor = open(path+'alpha_work_factor.txt','w')
file_workfactor_entropy = open(path+'alpha_work_factor_entropy.txt','w')
file_successrate = open(path+'beta_success_rate.txt','w')
file_successrate_entropy = open(path+'beta_success_rate_entropy.txt','w')
file_successrate_percentage = open(path+'beta_success_rate_percentage.txt','w')

file_guesswork.write("Alpha-guesswork per country\n\n")
file_guesswork_entropy.write("Alpha-guesswork entropy per country\n\n")
temp_str = "TLD "+' '.join(str(alpha) for alpha in list_alpha_guesswork)
data_metric, data_entropy = [temp_str.split()]*2 
file_guesswork.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_metric)+"\n\n")
file_guesswork_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_entropy)+"\n\n")

file_workfactor.write("Alpha-work-factor per TLD\n\n")
file_workfactor_entropy.write("Alpha-work-factor-entropy per TLD\n\n")
temp_str = "TLD "+' '.join(str(alpha) for alpha in list_alpha_workfactor)
data_metric, data_entropy = [temp_str.split()]*2
file_workfactor.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_metric)+"\n\n")
file_workfactor_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_entropy)+"\n\n")

file_successrate.write("Beta-success-rate per country\n\n")
file_successrate_entropy.write("Beta-success-rate entropy per country\n\n")
file_successrate_percentage.write("Beta-success-rate Percentage per country\n\n")
temp_str = "TLD "+' '.join(str(top) for top in list_beta)
data_metric, data_entropy = [temp_str.split()]*2 
file_successrate.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_metric)+"\n\n")
file_successrate_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_entropy)+"\n\n")
file_successrate_percentage.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_metric)+"\n\n")

temp_str_metric, temp_str_entropy = [""]*2
temp_str_percentage = ""
wanted_results_workfactor = [0]*len(list_alpha_workfactor)
wanted_results_guesswork = [0]*len(list_alpha_guesswork)

for tld in list_tld:
	beta_success_results = []
	beta_success_entropy = []
	beta_success_percentage = []
	workfactor_results = []
	workfactor_entropy = []
	guesswork_results = []
	guesswork_entropy = []
	loaded_list = []
	hashes = 0
	users = 0
	file_tld = open(path_input+tld+'.txt','r')
	i = -3
	for line in file_tld:
		if i >= 0:
			temp_line = line.split()
			loaded_list.append((temp_line[0],int(temp_line[1])))
			users += int(temp_line[1])
			hashes += 1
		else:
			i +=1
	for a in range(0,(len(list_alpha_workfactor))):
		wanted_results_workfactor[a] = float(users)*list_alpha_workfactor[a]
	for b in range(0,(len(list_alpha_guesswork))):
		wanted_results_guesswork[b] = float(users)*list_alpha_guesswork[b]
	i = 0
	j = 0
	k = 0
	counter = 0
	for (hash,amount) in loaded_list:
		counter += amount
		i += 1
		if i in list_beta:
			beta_success_results.append(round((float(counter)/users),4))
			beta_success_entropy.append(round(math.log2(i/(float(counter)/users)),2))
			beta_success_percentage.append(round(((float(counter)/users)*100),2))
		j = workfactor(wanted_results_workfactor,i,counter,users,j)
		k = guesswork(wanted_results_guesswork,i,counter,users,k)

	temp_str_metric = tld + " " + ' '.join(map(str, beta_success_results))
	temp_str_entropy = tld + " " + ' '.join(map(str, beta_success_entropy))
	temp_str_percentage = tld + " " + ' '.join(map(str, beta_success_percentage))
	file_successrate.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format((temp_str_metric.split()))+"\n")
	file_successrate_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format((temp_str_entropy.split()))+"\n")
	file_successrate_percentage.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format((temp_str_percentage.split()))+"\n")

	temp_str_metric = tld + " " + ' '.join(map(str, workfactor_results))
	temp_str_entropy = tld + " " + ' '.join(map(str, workfactor_entropy))
	file_workfactor.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format((temp_str_metric.split()))+"\n")
	file_workfactor_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format((temp_str_entropy.split()))+"\n")

	temp_str_metric = tld + " " + ' '.join(map(str, guesswork_results))
	temp_str_entropy = tld + " " + ' '.join(map(str, guesswork_entropy))
	file_guesswork.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format((temp_str_metric.split()))+"\n")
	file_guesswork_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format((temp_str_entropy.split()))+"\n")

	print(tld,i,counter)
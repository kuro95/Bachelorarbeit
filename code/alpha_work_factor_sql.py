import mysql.connector
from mysql.connector import errorcode
import math

path = "D:\\Bachelorarbeit\\database\\"
list_tld = ['uk', 'fr', 'edu', 'br', 'it', 'in', 'org', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'gov', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'mil', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'eu', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'biz', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'info', 'ua', 'lv', 'Pk', 'sk', 'uy', 'sa', 'tv', 'rs', 'th', 'lt', 'cc', 'cat', 'ma', 'ee', 'fm', 'ke', 'si', 'zw', 'is', 've', 'cr', 'om', 'nu', 'lu', 'lk', 'tn', 'ec', 'con', 'cy', 'int', 'jo', 'lb', 'mk', 'na', 'eg', 'me', 'qa', 'ws', 'cm', 'bw', 'do', 'mu', 'coop']
list_alpha = [0.01,0.05,0.10,0.20,0.50]
try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
                                database='bachelorarbeit')
  cursor = cnx.cursor(buffered=True)
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

file_metric = open(path+'alpha_work_factor.txt','w')
file_entropy = open(path+'alpha_work_factor_entropy.txt','w')
file_metric.write("Alpha-work-factor per TLD\n\n")
file_entropy.write("Alpha-work-factor-entropy per TLD\n\n")
temp_str = "TLD "+' '.join(str(alpha) for alpha in list_alpha)
data_metric, data_entropy = [temp_str.split()]*2
file_metric.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_metric)+"\n")
file_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_entropy)+"\n")
temp_str_metric, temp_str_entropy = [""]*2
wanted_results = [0]*len(list_alpha)
for tld in list_tld:
  temp_str_metric, temp_str_entropy = [tld]*2
  total = 0
  unique_pw = [0]*len(list_alpha)
  metric = [float(0)]*len(list_alpha)
  entropy = [float(0)]*len(list_alpha)
  temp_sum = [0]*len(list_alpha)
  query = ("SELECT count(*) FROM bachelorarbeit."+tld+"")
  cursor.execute(query)
  stored_query = cursor.fetchall()
  for count in stored_query:
    total = count[0]
  for i in range(0,(len(list_alpha))):
    wanted_results[i] = total*list_alpha[i]
  counter = 0
  query = ("SELECT MEMBER_HASH AS hashed, count(*) AS amount FROM bachelorarbeit."+tld+" group by hashed order by amount desc")
  cursor.execute(query)
  stored_query = cursor.fetchall()
  for (hash,amount) in stored_query:
    counter += amount
    for i in range(0,len(list_alpha)):
      if(wanted_results[i] > counter):
        unique_pw[i] = unique_pw[i] + 1
    if(wanted_results[len(list_alpha)-1] < counter):
      break
  unique_pw = [int(x) + 1 for x in unique_pw]
  counter = 0
  for (hash,amount) in stored_query:
    counter += 1
    for i in range(0,len(list_alpha)):
      if(unique_pw[i] >= counter):
        temp_sum[i]+=amount
    if(unique_pw[len(list_alpha)-1] <= counter):
      break
  for i in range(0,len(list_alpha)):
    metric[i] = unique_pw[i]
    temp_str_metric += " "+str(round(metric[i],2))
    print(unique_pw,total,temp_sum[i])
    entropy[i] = math.log2(unique_pw[i]/(temp_sum[i]/total))
    temp_str_entropy += " "+str(round(entropy[i],2))      
  data_metric = temp_str_metric.split()
  data_entropy = temp_str_entropy.split()    
  file_metric.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_metric)+"\n")
  file_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}{0[5]:<10}'.format(data_entropy)+"\n")
cnx.close()
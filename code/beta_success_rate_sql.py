import mysql.connector
from mysql.connector import errorcode
import math

path = "D:\\Bachelorarbeit\\database\\"
list_tld = ['uk', 'fr', 'edu', 'br', 'it', 'in', 'org', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'gov', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'mil', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'eu', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'biz', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'info', 'ua', 'lv', 'Pk', 'sk', 'uy', 'sa', 'tv', 'rs', 'th', 'lt', 'cc', 'cat', 'ma', 'ee', 'fm', 'ke', 'si', 'zw', 'is', 've', 'cr', 'om', 'nu', 'lu', 'lk', 'tn', 'ec', 'con', 'cy', 'int', 'jo', 'lb', 'mk', 'na', 'eg', 'me', 'qa', 'ws', 'cm', 'bw', 'do', 'mu', 'coop']
list_top = [1,10,100,1000]
try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
    database='bachelorarbeit')
  cursor = cnx.cursor()
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

file_metric = open(path+'beta_success_rate.txt','w')
file_entropy = open(path+'beta_success_rate_entropy.txt','w')
file_metric.write("Beta-success-rate per country\n\n")
file_entropy.write("Beta-success-rate entropy per country\n\n")
temp_str = "TLD "+' '.join(str(top) for top in list_top)
data_metric, data_entropy = [temp_str.split()]*2 
file_metric.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_metric)+"\n")
file_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_entropy)+"\n")
temp_str_metric, temp_str_entropy = [""]*2
for tld in list_tld:
  temp_str_metric, temp_str_entropy = [tld]*2
  for top in list_top:
    query = ("SELECT sum(pw_tbl.amount)/(SELECT count(*) FROM bachelorarbeit."+tld+") as valid FROM (SELECT MEMBER_HASH AS hashed, count(*) AS amount FROM bachelorarbeit."+tld+" group by hashed order by amount desc LIMIT "+str(top)+") AS pw_tbl ")
    cursor.execute(query)
    for prob in cursor:
      temp_str_metric += " " + str(prob[0])
      temp_str_entropy += " " + str(round(math.log2(top/prob[0]),2))
      data_metric = temp_str_metric.split() 
      data_entropy = temp_str_entropy.split()    
  file_metric.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_metric)+"\n")
  file_entropy.write('{0[0]:<10}{0[1]:<10}{0[2]:<10}{0[3]:<10}{0[4]:<10}'.format(data_entropy)+"\n")
cnx.close()
import mysql.connector
from mysql.connector import errorcode

try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
                                database='bachelorarbeit')
  cursor = cnx.cursor()
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)
 
query = ("SELECT SUBSTRING_INDEX(SUBSTRING_INDEX(MEMBER_PRIMARY_EMAIL, '@', -1), '.', -1) as tld, count(*) as amount FROM bachelorarbeit.fulllist group by tld order by amount desc LIMIT 100")
cursor.execute(query)
list = []
for(tld, amount) in cursor:
  list.append((tld, amount))
for item in list:
  print(item[0],item[1])
i = 0
for item in list:
  query = ("CREATE TABLE bachelorarbeit."+item[0]+" LIKE bachelorarbeit.fulllist;")
  cursor.execute(query)
  query = ("INSERT INTO bachelorarbeit."+item[0]+" SELECT * FROM bachelorarbeit.fulllist WHERE SUBSTRING_INDEX(SUBSTRING_INDEX(MEMBER_PRIMARY_EMAIL, '@', -1), '.', -1) = '"+ item[0] + "';")
  cursor.execute(query)
cnx.close()
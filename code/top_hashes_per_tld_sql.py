import mysql.connector
from mysql.connector import errorcode
import math

path = "D:\\Bachelorarbeit\\database\\hashes_per_tld\\"
list_tld = ['at']
try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
                                database='bachelorarbeit')
  cursor = cnx.cursor(buffered=True)
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

for tld in list_tld:
  file_tld = open(path+tld+'.txt','w')
  file_tld.write("Top Hashes for ."+tld+"\n\n")
  temp_str = "Hash Amount" 
  file_tld.write('{0[0]:<44}{0[1]:<6}'.format(temp_str.split())+"\n")
  query = ("SELECT MEMBER_HASH AS hashed, count(*) AS amount FROM bachelorarbeit."+tld+" group by hashed order by amount desc")
  cursor.execute(query)
  stored_query = cursor.fetchall()
  for (hash,amount) in stored_query:
    file_tld.write('{0[0]:<35}{0[1]:>10}'.format((hash+" "+str(amount)).split())+"\n")
  file_tld.close()
cnx.close()
import mysql.connector
from mysql.connector import errorcode

try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
                                database='bachelorarbeit')
  cursor = cnx.cursor()
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

f = open('D:\\Bachelorarbeit\\database\\Top100HashByTLD.txt','w')
list_tld = ['net', 'uk', 'fr', 'edu', 'br', 'it', 'in', 'org', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'gov', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'mil', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'eu', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'biz', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'info', 'ua', 'lv', 'Pk', 'sk', 'uy', 'sa', 'tv', 'rs', 'th', 'lt', 'cc', 'cat', 'ma', 'ee', 'fm', 'ke', 'si', 'zw', 'is', 've', 'cr', 'om', 'nu', 'lu', 'lk', 'tn', 'ec', 'con', 'cy', 'int', 'jo', 'lb', 'mk', 'na', 'eg', 'me', 'qa', 'ws', 'cm', 'bw', 'do', 'mu', 'coop']
for item in list_tld:
  query = ("SELECT MEMBER_HASH AS hashed, count(*) AS amount FROM bachelorarbeit."+item+" group by hashed order by amount desc LIMIT 100")
  cursor.execute(query)
  f.write("---------------------- ."+item+" -----------------------\n")
  f.write("\t\t\t\tHashes\t\t\t\t\t   Amount\n")
  for(hashed, amount) in cursor:
    f.write(hashed+" - "+str(amount)+"\n")
  f.write("\n\n")
cnx.close()
Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49589278 (excluded)
net , 4228216 (excluded)
uk , 1642140 
fr , 1288946 
edu , 1234619 (excluded)
br , 1201881 
it , 959101 
in , 900793 
org , 886365 (excluded)
nl , 731467 
ca , 709957 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 4.0  - Sum of Positions: 4
Attacker B: Results: 1 (100.0%)  - Avg. Position: 3.0  - Sum of Positions: 3  (TLD True Hits/Total Hits: 1/1)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 3633431.0  - Sum of Positions: 25434017
Attacker B: Results: 7 (70.0%)  - Avg. Position: 3633430.86  - Sum of Positions: 25434016  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 4770123.93  - Sum of Positions: 267126940
Attacker B: Results: 56 (56.0%)  - Avg. Position: 4768392.21  - Sum of Positions: 267029964  (TLD True Hits/Total Hits: 3/7)

After 1000 entries:
Attacker A: Results: 511 (51.1%)  - Avg. Position: 4850541.68  - Sum of Positions: 2478626796
Attacker B: Results: 511 (51.1%)  - Avg. Position: 4787169.46  - Sum of Positions: 2446243596  (TLD True Hits/Total Hits: 55/115)

After 10000 entries:
Attacker A: Results: 4975 (49.75%)  - Avg. Position: 3949997.39  - Sum of Positions: 19651237015
Attacker B: Results: 4975 (49.75%)  - Avg. Position: 3877297.67  - Sum of Positions: 19289555905  (TLD True Hits/Total Hits: 411/1068)

After 100000 entries:
Attacker A: Results: 49861 (49.86%)  - Avg. Position: 3849043.02  - Sum of Positions: 191917134005
Attacker B: Results: 49861 (49.86%)  - Avg. Position: 3766847.06  - Sum of Positions: 187818761494  (TLD True Hits/Total Hits: 3892/10603)

After 1000000 entries:
Attacker A: Results: 498846 (49.88%)  - Avg. Position: 3853836.92  - Sum of Positions: 1922471134409
Attacker B: Results: 498846 (49.88%)  - Avg. Position: 3775958.16  - Sum of Positions: 1883621624066  (TLD True Hits/Total Hits: 38830/106756)

After 5000000 entries:
Attacker A: Results: 2494826 (49.9%)  - Avg. Position: 3856211.98  - Sum of Positions: 9620577904559
Attacker B: Results: 2494826 (49.9%)  - Avg. Position: 3778836.44  - Sum of Positions: 9427539399036  (TLD True Hits/Total Hits: 193558/530490)

After 10000000 entries:
Attacker A: Results: 4990248 (49.9%)  - Avg. Position: 3857919.28  - Sum of Positions: 19251973988425
Attacker B: Results: 4990248 (49.9%)  - Avg. Position: 3780887.79  - Sum of Positions: 18867567731098  (TLD True Hits/Total Hits: 387514/1061554)


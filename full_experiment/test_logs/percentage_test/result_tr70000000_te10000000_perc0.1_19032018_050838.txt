Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49591820 (excluded)
net , 4228353 (excluded)
uk , 1640498 
fr , 1287802 
edu , 1235534 (excluded)
br , 1202176 
it , 958932 
in , 899784 
org , 886426 (excluded)
nl , 731989 
ca , 709287 
au , 583460 
de , 456306 
es , 417871 
za , 368051 
ru , 341799 
ar , 249132 
mx , 224538 
us , 223948 
dk , 204774 
be , 198978 
gov , 184755 (excluded)
se , 167478 
pl , 157693 
nz , 142610 
cn , 141639 
ch , 126148 
jp , 116874 
pt , 113166 
no , 112678 
mil , 102946 (excluded)
id , 102273 
cl , 102262 
sg , 81763 
gr , 74432 
ie , 74302 
il , 71391 
cz , 70218 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 21735.67  - Sum of Positions: 65207
Attacker B: Results: 3 (30.0%)  - Avg. Position: 253869.33  - Sum of Positions: 761608  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 42 (42.0%)  - Avg. Position: 5394168.55  - Sum of Positions: 226555079
Attacker B: Results: 42 (42.0%)  - Avg. Position: 5460429.83  - Sum of Positions: 229338053  (TLD True Hits/Total Hits: 4/20)

After 1000 entries:
Attacker A: Results: 478 (47.8%)  - Avg. Position: 4661572.15  - Sum of Positions: 2228231490
Attacker B: Results: 478 (47.8%)  - Avg. Position: 4540300.87  - Sum of Positions: 2170263815  (TLD True Hits/Total Hits: 53/186)

After 10000 entries:
Attacker A: Results: 5005 (50.05%)  - Avg. Position: 3797660.01  - Sum of Positions: 19007288360
Attacker B: Results: 5005 (50.05%)  - Avg. Position: 3685201.44  - Sum of Positions: 18444433206  (TLD True Hits/Total Hits: 525/1711)

After 100000 entries:
Attacker A: Results: 49744 (49.74%)  - Avg. Position: 3862245.56  - Sum of Positions: 192123543208
Attacker B: Results: 49744 (49.74%)  - Avg. Position: 3746431.86  - Sum of Positions: 186362506425  (TLD True Hits/Total Hits: 5530/17686)

After 1000000 entries:
Attacker A: Results: 497970 (49.8%)  - Avg. Position: 3827643.12  - Sum of Positions: 1906051444797
Attacker B: Results: 497970 (49.8%)  - Avg. Position: 3697509.68  - Sum of Positions: 1841248896403  (TLD True Hits/Total Hits: 55657/176393)

After 5000000 entries:
Attacker A: Results: 2495524 (49.91%)  - Avg. Position: 3845360.17  - Sum of Positions: 9596188603925
Attacker B: Results: 2495524 (49.91%)  - Avg. Position: 3713382.66  - Sum of Positions: 9266835540053  (TLD True Hits/Total Hits: 278494/880737)

After 10000000 entries:
Attacker A: Results: 4991198 (49.91%)  - Avg. Position: 3851484.03  - Sum of Positions: 19223519400721
Attacker B: Results: 4991198 (49.91%)  - Avg. Position: 3719420.77  - Sum of Positions: 18564365506125  (TLD True Hits/Total Hits: 557713/1763713)


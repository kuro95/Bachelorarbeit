Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.5 TLDs:

com , 49585233 (excluded)
net , 4229686 (excluded)
uk , 1641707 
fr , 1289124 
edu , 1233115 (excluded)
br , 1202394 
it , 960176 
in , 899860 
org , 886415 (excluded)
nl , 731911 
ca , 709686 
au , 583778 
de , 456584 
es , 418169 
za , 368543 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 655.0  - Sum of Positions: 655
Attacker B: Results: 1 (100.0%)  - Avg. Position: 655.0  - Sum of Positions: 655  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 1039907.14  - Sum of Positions: 7279350
Attacker B: Results: 7 (70.0%)  - Avg. Position: 1039907.14  - Sum of Positions: 7279350  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 2016243.3  - Sum of Positions: 114925868
Attacker B: Results: 57 (57.0%)  - Avg. Position: 2028858.93  - Sum of Positions: 115644959  (TLD True Hits/Total Hits: 8/17)

After 1000 entries:
Attacker A: Results: 491 (49.1%)  - Avg. Position: 3860814.02  - Sum of Positions: 1895659684
Attacker B: Results: 491 (49.1%)  - Avg. Position: 3622187.35  - Sum of Positions: 1778493987  (TLD True Hits/Total Hits: 52/143)

After 10000 entries:
Attacker A: Results: 5006 (50.06%)  - Avg. Position: 3665384.15  - Sum of Positions: 18348913038
Attacker B: Results: 5006 (50.06%)  - Avg. Position: 3556142.94  - Sum of Positions: 17802051533  (TLD True Hits/Total Hits: 456/1346)

After 100000 entries:
Attacker A: Results: 49904 (49.9%)  - Avg. Position: 3777665.61  - Sum of Positions: 188520624644
Attacker B: Results: 49904 (49.9%)  - Avg. Position: 3682933.33  - Sum of Positions: 183793105043  (TLD True Hits/Total Hits: 4557/13254)

After 1000000 entries:
Attacker A: Results: 499527 (49.95%)  - Avg. Position: 3840601.21  - Sum of Positions: 1918484002967
Attacker B: Results: 499527 (49.95%)  - Avg. Position: 3744085.57  - Sum of Positions: 1870271834466  (TLD True Hits/Total Hits: 46204/132074)

After 5000000 entries:
Attacker A: Results: 2493196 (49.86%)  - Avg. Position: 3850947.45  - Sum of Positions: 9601166769341
Attacker B: Results: 2493196 (49.86%)  - Avg. Position: 3755696.47  - Sum of Positions: 9363687418870  (TLD True Hits/Total Hits: 230325/660043)

After 10000000 entries:
Attacker A: Results: 4984776 (49.85%)  - Avg. Position: 3855073.67  - Sum of Positions: 19216678721901
Attacker B: Results: 4984776 (49.85%)  - Avg. Position: 3759636.38  - Sum of Positions: 18740945184825  (TLD True Hits/Total Hits: 461306/1321454)


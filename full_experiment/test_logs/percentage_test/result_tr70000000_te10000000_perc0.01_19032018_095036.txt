Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.01 TLDs:

com , 49589525 (excluded)
net , 4229937 (excluded)
uk , 1641543 
fr , 1289528 
edu , 1234352 (excluded)
br , 1202221 
it , 959541 
in , 899532 
org , 885876 (excluded)
nl , 731360 
ca , 709928 
au , 583444 
de , 456665 
es , 418023 
za , 367970 
ru , 341615 
ar , 248656 
mx , 224737 
us , 223497 
dk , 204843 
be , 199510 
gov , 184739 (excluded)
se , 166873 
pl , 158027 
nz , 142246 
cn , 141663 
ch , 125872 
jp , 117043 
pt , 113432 
no , 112794 
mil , 102683 (excluded)
id , 102640 
cl , 102126 
sg , 81826 
ie , 74346 
gr , 74315 
il , 71724 
cz , 70367 
fi , 70120 
tr , 58554 
co , 56776 
my , 52430 
eu , 50593 (excluded)
hk , 49379 
at , 45891 
bg , 45081 
tw , 43307 
ph , 40980 
hu , 40879 
biz , 39639 (excluded)
ro , 37825 
ae , 36651 
hr , 28585 
pe , 27661 
vn , 26987 
kr , 26886 
info , 25098 (excluded)
ua , 22556 
lv , 21481 
pk , 20920 
sk , 19599 
uy , 17143 
sa , 15285 
tv , 14051 
rs , 13292 
th , 13012 
lt , 12699 
cc , 11610 
cat , 10997 
ma , 10678 
ee , 10525 
fm , 10410 
ke , 10272 
si , 10265 
zw , 10043 
is , 9905 
ve , 9725 
cr , 9263 
om , 9182 
nu , 8903 
lu , 8684 
lk , 8598 
tn , 8394 
ec , 7814 
con , 7189 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 241843.75  - Sum of Positions: 967375
Attacker B: Results: 4 (40.0%)  - Avg. Position: 241843.75  - Sum of Positions: 967375  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 1625363.37  - Sum of Positions: 92645712
Attacker B: Results: 57 (57.0%)  - Avg. Position: 1640326.42  - Sum of Positions: 93498606  (TLD True Hits/Total Hits: 6/16)

After 1000 entries:
Attacker A: Results: 497 (49.7%)  - Avg. Position: 4062725.3  - Sum of Positions: 2019174476
Attacker B: Results: 497 (49.7%)  - Avg. Position: 4029565.01  - Sum of Positions: 2002693808  (TLD True Hits/Total Hits: 71/211)

After 10000 entries:
Attacker A: Results: 4971 (49.71%)  - Avg. Position: 3855460.84  - Sum of Positions: 19165495851
Attacker B: Results: 4971 (49.71%)  - Avg. Position: 3729597.42  - Sum of Positions: 18539828779  (TLD True Hits/Total Hits: 546/1913)

After 100000 entries:
Attacker A: Results: 49931 (49.93%)  - Avg. Position: 3820665.45  - Sum of Positions: 190769646448
Attacker B: Results: 49931 (49.93%)  - Avg. Position: 3677541.84  - Sum of Positions: 183623341797  (TLD True Hits/Total Hits: 5823/19164)

After 1000000 entries:
Attacker A: Results: 499115 (49.91%)  - Avg. Position: 3850195.51  - Sum of Positions: 1921690332962
Attacker B: Results: 499115 (49.91%)  - Avg. Position: 3710231.89  - Sum of Positions: 1851832388074  (TLD True Hits/Total Hits: 57826/191394)

After 5000000 entries:
Attacker A: Results: 2494250 (49.89%)  - Avg. Position: 3853933.76  - Sum of Positions: 9612674288207
Attacker B: Results: 2494250 (49.89%)  - Avg. Position: 3709828.58  - Sum of Positions: 9253239934819  (TLD True Hits/Total Hits: 289461/955443)

After 10000000 entries:
Attacker A: Results: 4989168 (49.89%)  - Avg. Position: 3854721.14  - Sum of Positions: 19231851377147
Attacker B: Results: 4989168 (49.89%)  - Avg. Position: 3710694.48  - Sum of Positions: 18513278152724  (TLD True Hits/Total Hits: 578666/1911681)


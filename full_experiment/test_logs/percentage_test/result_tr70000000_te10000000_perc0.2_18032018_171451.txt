Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.2 TLDs:

com , 49595662 (excluded)
net , 4227711 (excluded)
uk , 1641192 
fr , 1287126 
edu , 1232958 (excluded)
br , 1203309 
it , 959510 
in , 900032 
org , 886931 (excluded)
nl , 731386 
ca , 710042 
au , 583617 
de , 455832 
es , 417676 
za , 368621 
ru , 340998 
ar , 248800 
mx , 224373 
us , 223761 
dk , 204276 
be , 199871 
gov , 184768 (excluded)
se , 167057 
pl , 157903 
nz , 142409 
cn , 141906 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 9190644.0  - Sum of Positions: 9190644
Attacker B: Results: 1 (100.0%)  - Avg. Position: 9190644.0  - Sum of Positions: 9190644  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 1728735.33  - Sum of Positions: 10372412
Attacker B: Results: 6 (60.0%)  - Avg. Position: 1728735.33  - Sum of Positions: 10372412  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 2580939.9  - Sum of Positions: 126466055
Attacker B: Results: 49 (49.0%)  - Avg. Position: 2616546.29  - Sum of Positions: 128210768  (TLD True Hits/Total Hits: 6/14)

After 1000 entries:
Attacker A: Results: 510 (51.0%)  - Avg. Position: 4331633.73  - Sum of Positions: 2209133201
Attacker B: Results: 510 (51.0%)  - Avg. Position: 4203644.44  - Sum of Positions: 2143858662  (TLD True Hits/Total Hits: 46/142)

After 10000 entries:
Attacker A: Results: 4997 (49.97%)  - Avg. Position: 3824660.35  - Sum of Positions: 19111827754
Attacker B: Results: 4997 (49.97%)  - Avg. Position: 3693975.06  - Sum of Positions: 18458793381  (TLD True Hits/Total Hits: 544/1576)

After 100000 entries:
Attacker A: Results: 50054 (50.05%)  - Avg. Position: 3771792.89  - Sum of Positions: 188793321240
Attacker B: Results: 50054 (50.05%)  - Avg. Position: 3647991.37  - Sum of Positions: 182596560002  (TLD True Hits/Total Hits: 5296/16029)

After 1000000 entries:
Attacker A: Results: 498882 (49.89%)  - Avg. Position: 3838535.61  - Sum of Positions: 1914976323568
Attacker B: Results: 498882 (49.89%)  - Avg. Position: 3717795.23  - Sum of Positions: 1854741119141  (TLD True Hits/Total Hits: 52946/161604)

After 5000000 entries:
Attacker A: Results: 2494795 (49.9%)  - Avg. Position: 3852011.38  - Sum of Positions: 9609978737572
Attacker B: Results: 2494795 (49.9%)  - Avg. Position: 3730674.15  - Sum of Positions: 9307267216435  (TLD True Hits/Total Hits: 264297/808211)

After 10000000 entries:
Attacker A: Results: 4990385 (49.9%)  - Avg. Position: 3846405.76  - Sum of Positions: 19195045632668
Attacker B: Results: 4990385 (49.9%)  - Avg. Position: 3725821.18  - Sum of Positions: 18593282104712  (TLD True Hits/Total Hits: 528920/1616586)


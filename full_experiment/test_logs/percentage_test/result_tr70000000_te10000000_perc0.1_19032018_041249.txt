Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49585081 (excluded)
net , 4229658 (excluded)
uk , 1642086 
fr , 1288403 
edu , 1234070 (excluded)
br , 1201508 
it , 959502 
in , 899856 
org , 887387 (excluded)
nl , 731748 
ca , 709201 
au , 583427 
de , 457365 
es , 418937 
za , 367739 
ru , 342346 
ar , 248671 
mx , 224916 
us , 224059 
dk , 204565 
be , 199810 
gov , 184857 (excluded)
se , 167564 
pl , 157982 
nz , 142379 
cn , 141917 
ch , 126095 
jp , 116960 
pt , 113430 
no , 112576 
id , 102555 
mil , 102513 (excluded)
cl , 101919 
sg , 82071 
gr , 74252 
ie , 73944 
il , 71533 
cz , 70343 
fi , 70101 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 169256.0  - Sum of Positions: 169256
Attacker B: Results: 1 (100.0%)  - Avg. Position: 169256.0  - Sum of Positions: 169256  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 7994897.6  - Sum of Positions: 39974488
Attacker B: Results: 5 (50.0%)  - Avg. Position: 7994897.6  - Sum of Positions: 39974488  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 58 (58.0%)  - Avg. Position: 4596731.26  - Sum of Positions: 266610413
Attacker B: Results: 58 (58.0%)  - Avg. Position: 4604987.29  - Sum of Positions: 267089263  (TLD True Hits/Total Hits: 3/18)

After 1000 entries:
Attacker A: Results: 513 (51.3%)  - Avg. Position: 3516665.51  - Sum of Positions: 1804049407
Attacker B: Results: 513 (51.3%)  - Avg. Position: 3485396.28  - Sum of Positions: 1788008291  (TLD True Hits/Total Hits: 58/174)

After 10000 entries:
Attacker A: Results: 5052 (50.52%)  - Avg. Position: 3738743.63  - Sum of Positions: 18888132805
Attacker B: Results: 5052 (50.52%)  - Avg. Position: 3647017.42  - Sum of Positions: 18424732013  (TLD True Hits/Total Hits: 540/1738)

After 100000 entries:
Attacker A: Results: 50056 (50.06%)  - Avg. Position: 3869952.27  - Sum of Positions: 193714330842
Attacker B: Results: 50056 (50.06%)  - Avg. Position: 3729302.23  - Sum of Positions: 186673952675  (TLD True Hits/Total Hits: 5646/17710)

After 1000000 entries:
Attacker A: Results: 499568 (49.96%)  - Avg. Position: 3874971.76  - Sum of Positions: 1935811891524
Attacker B: Results: 499568 (49.96%)  - Avg. Position: 3741292.54  - Sum of Positions: 1869030030472  (TLD True Hits/Total Hits: 56056/177211)

After 5000000 entries:
Attacker A: Results: 2495629 (49.91%)  - Avg. Position: 3859810.38  - Sum of Positions: 9632654707868
Attacker B: Results: 2495629 (49.91%)  - Avg. Position: 3723459.79  - Sum of Positions: 9292374228996  (TLD True Hits/Total Hits: 279749/886754)

After 10000000 entries:
Attacker A: Results: 4989352 (49.89%)  - Avg. Position: 3857589.61  - Sum of Positions: 19246872433897
Attacker B: Results: 4989352 (49.89%)  - Avg. Position: 3721603.96  - Sum of Positions: 18568392172144  (TLD True Hits/Total Hits: 560033/1774164)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.01 TLDs:

com , 49589888 (excluded)
net , 4230367 (excluded)
uk , 1642461 
fr , 1289209 
edu , 1233738 (excluded)
br , 1201720 
it , 959144 
in , 899399 
org , 885741 (excluded)
nl , 731780 
ca , 709546 
au , 584551 
de , 456002 
es , 418403 
za , 367121 
ru , 341785 
ar , 249147 
mx , 224816 
us , 224269 
dk , 204304 
be , 199191 
gov , 184935 (excluded)
se , 167292 
pl , 157929 
nz , 142463 
cn , 141840 
ch , 126147 
jp , 116671 
pt , 113304 
no , 112024 
mil , 102897 (excluded)
id , 102536 
cl , 101995 
sg , 81716 
gr , 74396 
ie , 73795 
il , 71702 
cz , 70172 
fi , 70169 
tr , 58753 
co , 56759 
my , 52387 
eu , 50331 (excluded)
hk , 49506 
at , 46197 
bg , 45049 
tw , 43283 
hu , 41052 
ph , 40733 
biz , 40040 (excluded)
ro , 38056 
ae , 36555 
hr , 28329 
pe , 27666 
vn , 27121 
kr , 26911 
info , 24610 (excluded)
ua , 22721 
lv , 21377 
pk , 20876 
sk , 19536 
uy , 17158 
sa , 15352 
tv , 14178 
rs , 13341 
th , 13058 
lt , 12545 
cc , 11677 
cat , 10831 
ma , 10626 
ee , 10429 
fm , 10369 
ke , 10353 
si , 10237 
is , 10045 
zw , 10020 
ve , 9819 
cr , 9150 
nu , 8992 
om , 8982 
lu , 8707 
lk , 8544 
tn , 8332 
ec , 7860 
con , 7181 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 635000.0  - Sum of Positions: 635000
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1169115.0  - Sum of Positions: 1169115  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 1056264.0  - Sum of Positions: 8450112
Attacker B: Results: 8 (80.0%)  - Avg. Position: 1128075.38  - Sum of Positions: 9024603  (TLD True Hits/Total Hits: 1/3)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 3581676.7  - Sum of Positions: 200573895
Attacker B: Results: 56 (56.0%)  - Avg. Position: 3481951.07  - Sum of Positions: 194989260  (TLD True Hits/Total Hits: 11/21)

After 1000 entries:
Attacker A: Results: 505 (50.5%)  - Avg. Position: 3140799.08  - Sum of Positions: 1586103533
Attacker B: Results: 505 (50.5%)  - Avg. Position: 2988338.32  - Sum of Positions: 1509110850  (TLD True Hits/Total Hits: 68/218)

After 10000 entries:
Attacker A: Results: 4870 (48.7%)  - Avg. Position: 3714289.15  - Sum of Positions: 18088588172
Attacker B: Results: 4870 (48.7%)  - Avg. Position: 3592700.93  - Sum of Positions: 17496453537  (TLD True Hits/Total Hits: 584/1920)

After 100000 entries:
Attacker A: Results: 49879 (49.88%)  - Avg. Position: 3796159.22  - Sum of Positions: 189348625493
Attacker B: Results: 49879 (49.88%)  - Avg. Position: 3656465.59  - Sum of Positions: 182380846952  (TLD True Hits/Total Hits: 5868/19084)

After 1000000 entries:
Attacker A: Results: 499256 (49.93%)  - Avg. Position: 3850855.88  - Sum of Positions: 1922562904007
Attacker B: Results: 499256 (49.93%)  - Avg. Position: 3709507.6  - Sum of Positions: 1851993926671  (TLD True Hits/Total Hits: 57981/190337)

After 5000000 entries:
Attacker A: Results: 2496899 (49.94%)  - Avg. Position: 3848007.9  - Sum of Positions: 9608087072109
Attacker B: Results: 2496899 (49.94%)  - Avg. Position: 3704894.51  - Sum of Positions: 9250747392293  (TLD True Hits/Total Hits: 290260/955642)

After 10000000 entries:
Attacker A: Results: 4989906 (49.9%)  - Avg. Position: 3856849.96  - Sum of Positions: 19245318779317
Attacker B: Results: 4989906 (49.9%)  - Avg. Position: 3713612.71  - Sum of Positions: 18530578363824  (TLD True Hits/Total Hits: 579319/1911073)


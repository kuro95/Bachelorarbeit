Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.01 TLDs:

com , 49593038 (excluded)
net , 4227947 (excluded)
uk , 1640462 
fr , 1287827 
edu , 1235356 (excluded)
br , 1201695 
it , 959853 
in , 899273 
org , 885212 (excluded)
nl , 732442 
ca , 709816 
au , 583957 
de , 456263 
es , 417628 
za , 367696 
ru , 341745 
ar , 249091 
mx , 224755 
us , 223938 
dk , 204320 
be , 199993 
gov , 185150 (excluded)
se , 167011 
pl , 157414 
nz , 142255 
cn , 141878 
ch , 125936 
jp , 116767 
pt , 113161 
no , 112281 
id , 102487 
mil , 102373 (excluded)
cl , 102082 
sg , 82077 
gr , 74252 
ie , 74223 
il , 71483 
fi , 70177 
cz , 70073 
tr , 58770 
co , 56702 
my , 52343 
eu , 50317 (excluded)
hk , 49550 
at , 46277 
bg , 44837 
tw , 43104 
hu , 41094 
ph , 40824 
biz , 39772 (excluded)
ro , 38124 
ae , 36715 
hr , 28526 
pe , 27953 
vn , 27246 
kr , 26969 
info , 24832 (excluded)
ua , 22723 
lv , 21482 
pk , 20930 
sk , 19385 
uy , 17348 
sa , 15347 
tv , 14162 
rs , 13407 
th , 12761 
lt , 12645 
cc , 11682 
cat , 10932 
ma , 10585 
ee , 10559 
fm , 10450 
ke , 10324 
si , 10202 
zw , 10086 
is , 9859 
ve , 9763 
cr , 9162 
om , 9146 
nu , 9063 
lu , 8748 
tn , 8482 
lk , 8446 
ec , 7857 
con , 7226 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 7631922.5  - Sum of Positions: 45791535
Attacker B: Results: 6 (60.0%)  - Avg. Position: 7693429.0  - Sum of Positions: 46160574  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 42 (42.0%)  - Avg. Position: 3321366.31  - Sum of Positions: 139497385
Attacker B: Results: 42 (42.0%)  - Avg. Position: 2545067.57  - Sum of Positions: 106892838  (TLD True Hits/Total Hits: 3/21)

After 1000 entries:
Attacker A: Results: 492 (49.2%)  - Avg. Position: 3817797.51  - Sum of Positions: 1878356377
Attacker B: Results: 492 (49.2%)  - Avg. Position: 3749836.88  - Sum of Positions: 1844919745  (TLD True Hits/Total Hits: 40/167)

After 10000 entries:
Attacker A: Results: 5057 (50.57%)  - Avg. Position: 3906291.37  - Sum of Positions: 19754115478
Attacker B: Results: 5057 (50.57%)  - Avg. Position: 3818486.33  - Sum of Positions: 19310085392  (TLD True Hits/Total Hits: 560/1849)

After 100000 entries:
Attacker A: Results: 50151 (50.15%)  - Avg. Position: 3876512.3  - Sum of Positions: 194410968484
Attacker B: Results: 50151 (50.15%)  - Avg. Position: 3740037.39  - Sum of Positions: 187566614934  (TLD True Hits/Total Hits: 5788/18889)

After 1000000 entries:
Attacker A: Results: 499194 (49.92%)  - Avg. Position: 3843851.75  - Sum of Positions: 1918827731570
Attacker B: Results: 499194 (49.92%)  - Avg. Position: 3704622.6  - Sum of Positions: 1849325374655  (TLD True Hits/Total Hits: 57927/191354)

After 5000000 entries:
Attacker A: Results: 2495638 (49.91%)  - Avg. Position: 3848336.43  - Sum of Positions: 9604054623598
Attacker B: Results: 2495638 (49.91%)  - Avg. Position: 3706258.75  - Sum of Positions: 9249480179485  (TLD True Hits/Total Hits: 289716/958060)

After 10000000 entries:
Attacker A: Results: 4990964 (49.91%)  - Avg. Position: 3848351.5  - Sum of Positions: 19206983796282
Attacker B: Results: 4990964 (49.91%)  - Avg. Position: 3705119.43  - Sum of Positions: 18492117667157  (TLD True Hits/Total Hits: 579777/1913468)


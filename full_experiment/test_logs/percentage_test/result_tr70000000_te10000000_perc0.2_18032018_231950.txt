Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.2 TLDs:

com , 49591230 (excluded)
net , 4230666 (excluded)
uk , 1642155 
fr , 1289702 
edu , 1233154 (excluded)
br , 1201648 
it , 958718 
in , 899859 
org , 886119 (excluded)
nl , 731420 
ca , 709784 
au , 583594 
de , 456230 
es , 418375 
za , 367535 
ru , 341478 
ar , 248750 
mx , 224482 
us , 223603 
dk , 204693 
be , 199560 
gov , 184835 (excluded)
se , 167093 
pl , 157113 
nz , 142084 
cn , 141920 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 693200.0  - Sum of Positions: 693200
Attacker B: Results: 1 (100.0%)  - Avg. Position: 693200.0  - Sum of Positions: 693200  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 835903.83  - Sum of Positions: 5015423
Attacker B: Results: 6 (60.0%)  - Avg. Position: 835903.83  - Sum of Positions: 5015423  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 2800771.05  - Sum of Positions: 159643950
Attacker B: Results: 57 (57.0%)  - Avg. Position: 2803735.72  - Sum of Positions: 159812936  (TLD True Hits/Total Hits: 3/6)

After 1000 entries:
Attacker A: Results: 532 (53.2%)  - Avg. Position: 4145684.08  - Sum of Positions: 2205503928
Attacker B: Results: 532 (53.2%)  - Avg. Position: 3772891.65  - Sum of Positions: 2007178360  (TLD True Hits/Total Hits: 54/149)

After 10000 entries:
Attacker A: Results: 5018 (50.18%)  - Avg. Position: 3862901.85  - Sum of Positions: 19384041494
Attacker B: Results: 5018 (50.18%)  - Avg. Position: 3711660.08  - Sum of Positions: 18625110265  (TLD True Hits/Total Hits: 519/1593)

After 100000 entries:
Attacker A: Results: 49863 (49.86%)  - Avg. Position: 3983597.95  - Sum of Positions: 198634144460
Attacker B: Results: 49863 (49.86%)  - Avg. Position: 3839520.03  - Sum of Positions: 191449987446  (TLD True Hits/Total Hits: 5230/16148)

After 1000000 entries:
Attacker A: Results: 498410 (49.84%)  - Avg. Position: 3850600.46  - Sum of Positions: 1919177775861
Attacker B: Results: 498410 (49.84%)  - Avg. Position: 3725284.3  - Sum of Positions: 1856718950404  (TLD True Hits/Total Hits: 52897/161675)

After 5000000 entries:
Attacker A: Results: 2492964 (49.86%)  - Avg. Position: 3863010.37  - Sum of Positions: 9630345778630
Attacker B: Results: 2492964 (49.86%)  - Avg. Position: 3739993.6  - Sum of Positions: 9323669413269  (TLD True Hits/Total Hits: 264975/808274)

After 10000000 entries:
Attacker A: Results: 4987149 (49.87%)  - Avg. Position: 3860868.73  - Sum of Positions: 19254727636007
Attacker B: Results: 4987149 (49.87%)  - Avg. Position: 3737474.22  - Sum of Positions: 18639340835820  (TLD True Hits/Total Hits: 530700/1618304)


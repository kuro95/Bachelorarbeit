Attacker A: Default List - Attacker B: TLD-Lists

Results with 10 Training Entries and 50000 Random Entries:

br , 1
my , 1
th , 1
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 8 (8.0%)  - Avg. Position: 6.0  - Sum of Positions: 48
Attacker B: Results: 8 (8.0%)  - Avg. Position: 5.0  - Sum of Positions: 40  (TLD True Hits/Total Hits: 2/7)

After 1000 entries:
Attacker A: Results: 12 (1.2%)  - Avg. Position: 4.92  - Sum of Positions: 59
Attacker B: Results: 12 (1.2%)  - Avg. Position: 4.25  - Sum of Positions: 51  (TLD True Hits/Total Hits: 3/18)

After 10000 entries:
Attacker A: Results: 20 (0.2%)  - Avg. Position: 4.1  - Sum of Positions: 82
Attacker B: Results: 20 (0.2%)  - Avg. Position: 3.8  - Sum of Positions: 76  (TLD True Hits/Total Hits: 3/192)

After 50000 entries:
Attacker A: Results: 57 (0.11%)  - Avg. Position: 2.98  - Sum of Positions: 170
Attacker B: Results: 57 (0.11%)  - Avg. Position: 2.88  - Sum of Positions: 164  (TLD True Hits/Total Hits: 3/525)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 6000000 Training Entries and 500000 Random Entries:

com , 4246961 (excluded)
net , 260261 (excluded)
br , 148647 
uk , 140333 
fr , 117435 
it , 105190 
edu , 103870 (excluded)
in , 82323 
org , 60166 (excluded)
nl , 55790 
es , 49048 
ca , 48732 
au , 45034 
ru , 44399 
de , 35955 
za , 34343 
ar , 28615 
mx , 27918 
COM , 21585 
pl , 17494 
pt , 16054 
se , 13878 
dk , 13593 
id , 12241 
us , 11770 
ch , 10780 
be , 9839 
nz , 9789 
cl , 9606 
cn , 9277 
gov , 9237 (excluded)
gr , 7064 
jp , 7052 
ro , 6866 
no , 6678 
il , 6676 
co , 6338 
ie , 6116 
ae , 6089 
sg , 5775 
tr , 5407 
my , 5114 
mil , 5103 (excluded)
eu , 4713 
pk , 4351 
sa , 4230 
at , 4142 
tw , 4092 
cz , 3986 
hk , 3756 
ph , 3405 
pe , 3156 
NET , 2957 
biz , 2909 (excluded)
ua , 2866 
lv , 2742 
hu , 2567 
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 1539735.0  - Sum of Positions: 4619205
Attacker B: Results: 3 (30.0%)  - Avg. Position: 1539735.0  - Sum of Positions: 4619205  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 30 (30.0%)  - Avg. Position: 895818.53  - Sum of Positions: 26874556
Attacker B: Results: 30 (30.0%)  - Avg. Position: 758846.27  - Sum of Positions: 22765388  (TLD True Hits/Total Hits: 3/18)

After 1000 entries:
Attacker A: Results: 378 (37.8%)  - Avg. Position: 543467.87  - Sum of Positions: 205430853
Attacker B: Results: 378 (37.8%)  - Avg. Position: 518699.58  - Sum of Positions: 196068441  (TLD True Hits/Total Hits: 49/232)

After 10000 entries:
Attacker A: Results: 3554 (35.54%)  - Avg. Position: 508274.66  - Sum of Positions: 1806408124
Attacker B: Results: 3554 (35.54%)  - Avg. Position: 496745.9  - Sum of Positions: 1765434927  (TLD True Hits/Total Hits: 500/2583)

After 100000 entries:
Attacker A: Results: 35127 (35.13%)  - Avg. Position: 521369.61  - Sum of Positions: 18314150454
Attacker B: Results: 35127 (35.13%)  - Avg. Position: 510082.14  - Sum of Positions: 17917655300  (TLD True Hits/Total Hits: 4176/23456)

After 500000 entries:
Attacker A: Results: 175720 (35.14%)  - Avg. Position: 496896.91  - Sum of Positions: 87314724861
Attacker B: Results: 175720 (35.14%)  - Avg. Position: 487793.2  - Sum of Positions: 85715020884  (TLD True Hits/Total Hits: 19416/108502)


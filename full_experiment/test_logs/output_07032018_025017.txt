Attacker A: Default List - Attacker B: TLD-Lists

Results with 6000000 Training Entries and 500000 Random Entries:

c , [] 
o , [] 
m , [] 
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 1178259.33  - Sum of Positions: 3534778
Attacker B: Results: 3 (30.0%)  - Avg. Position: 1178259.33  - Sum of Positions: 3534778  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 30 (30.0%)  - Avg. Position: 818170.5  - Sum of Positions: 24545115
Attacker B: Results: 30 (30.0%)  - Avg. Position: 818170.5  - Sum of Positions: 24545115  (TLD True Hits/Total Hits: 0/0)

After 1000 entries:
Attacker A: Results: 378 (37.8%)  - Avg. Position: 525467.56  - Sum of Positions: 198626737
Attacker B: Results: 378 (37.8%)  - Avg. Position: 525467.56  - Sum of Positions: 198626737  (TLD True Hits/Total Hits: 0/0)

After 10000 entries:
Attacker A: Results: 3554 (35.54%)  - Avg. Position: 483191.84  - Sum of Positions: 1717263803
Attacker B: Results: 3554 (35.54%)  - Avg. Position: 483191.84  - Sum of Positions: 1717263803  (TLD True Hits/Total Hits: 0/0)

After 100000 entries:
Attacker A: Results: 35127 (35.13%)  - Avg. Position: 520087.83  - Sum of Positions: 18269125270
Attacker B: Results: 35127 (35.13%)  - Avg. Position: 520087.83  - Sum of Positions: 18269125270  (TLD True Hits/Total Hits: 0/0)

After 500000 entries:
Attacker A: Results: 175720 (35.14%)  - Avg. Position: 494739.39  - Sum of Positions: 86935606435
Attacker B: Results: 175720 (35.14%)  - Avg. Position: 494739.39  - Sum of Positions: 86935606435  (TLD True Hits/Total Hits: 0/0)


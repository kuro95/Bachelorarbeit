Attacker A: Default List - Attacker B: TLD-Lists

Results with 360000 Training Entries and 50000 Random Entries:

za , 12543
ru , 8792
uk , 5185
cn , 4222
nl , 3598
it , 3347
fr , 3071
ca , 2939
br , 2615
es , 2452
de , 2345
COM , 1576
dk , 1493
jp , 1385
au , 1336
in , 1264
ie , 1141
tw , 1121
pk , 1075
pt , 857
se , 843
pl , 824
mx , 784
NET , 682
cz , 662
hk , 524
info , 501
no , 440
cat , 433
pe , 372
sa , 338
be , 318
user , 314
ch , 308
ar , 293
us , 292
nz , 245
il , 226
vn , 221
ZA , 221
gr , 214
sk , 213
kr , 207
id , 199
ua , 190
sg , 177
at , 176
im , 171
ae , 165
lv , 122
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/7)

After 100 entries:
Attacker A: Results: 16 (16.0%)  - Avg. Position: 32309.06  - Sum of Positions: 516945
Attacker B: Results: 16 (16.0%)  - Avg. Position: 33303.81  - Sum of Positions: 532861  (TLD True Hits/Total Hits: 1/28)

After 1000 entries:
Attacker A: Results: 144 (14.4%)  - Avg. Position: 49492.81  - Sum of Positions: 7126964
Attacker B: Results: 144 (14.4%)  - Avg. Position: 44865.93  - Sum of Positions: 6460694  (TLD True Hits/Total Hits: 20/320)

After 10000 entries:
Attacker A: Results: 1485 (14.85%)  - Avg. Position: 47750.58  - Sum of Positions: 70909608
Attacker B: Results: 1485 (14.85%)  - Avg. Position: 45577.63  - Sum of Positions: 67682784  (TLD True Hits/Total Hits: 158/3224)

After 50000 entries:
Attacker A: Results: 6940 (13.88%)  - Avg. Position: 52142.26  - Sum of Positions: 361867312
Attacker B: Results: 6940 (13.88%)  - Avg. Position: 48821.01  - Sum of Positions: 338817813  (TLD True Hits/Total Hits: 1021/22178)


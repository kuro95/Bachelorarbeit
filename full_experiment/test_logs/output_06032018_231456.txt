Attacker A: Default List - Attacker B: TLD-Lists

Results with 360000 Training Entries and 50000 Random Entries:

za , 12543
ru , 8792
uk , 5185
cn , 4222
nl , 3598
it , 3347
fr , 3071
ca , 2939
br , 2615
es , 2452
de , 2345
COM , 1576
dk , 1493
jp , 1385
au , 1336
in , 1264
ie , 1141
tw , 1121
pk , 1075
pt , 857
se , 843
pl , 824
mx , 784
NET , 682
cz , 662
hk , 524
info , 501
no , 440
cat , 433
pe , 372
sa , 338
be , 318
user , 314
ch , 308
ar , 293
us , 292
nz , 245
il , 226
vn , 221
ZA , 221
gr , 214
sk , 213
kr , 207
id , 199
ua , 190
sg , 177
at , 176
im , 171
ae , 165
lv , 122
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/7)

After 100 entries:
Attacker A: Results: 16 (16.0%)  - Avg. Position: 42933.56  - Sum of Positions: 686937
Attacker B: Results: 16 (16.0%)  - Avg. Position: 43928.31  - Sum of Positions: 702853  (TLD True Hits/Total Hits: 1/28)

After 1000 entries:
Attacker A: Results: 144 (14.4%)  - Avg. Position: 45177.42  - Sum of Positions: 6505548
Attacker B: Results: 144 (14.4%)  - Avg. Position: 42401.8  - Sum of Positions: 6105859  (TLD True Hits/Total Hits: 20/320)

After 10000 entries:
Attacker A: Results: 1485 (14.85%)  - Avg. Position: 49820.8  - Sum of Positions: 73983886
Attacker B: Results: 1485 (14.85%)  - Avg. Position: 47822.59  - Sum of Positions: 71016548  (TLD True Hits/Total Hits: 158/3224)

After 50000 entries:
Attacker A: Results: 6940 (13.88%)  - Avg. Position: 52573.77  - Sum of Positions: 364861960
Attacker B: Results: 6940 (13.88%)  - Avg. Position: 49362.31  - Sum of Positions: 342574410  (TLD True Hits/Total Hits: 1021/22178)


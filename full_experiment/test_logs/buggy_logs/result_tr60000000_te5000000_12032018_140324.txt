Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 70 TLDs:

com , 42446385 (excluded)
net , 3639416 (excluded)
uk , 1435418 
fr , 1115223 
edu , 1057014 (excluded)
br , 1042858 
it , 838948 
org , 771713 (excluded)
in , 764500 
nl , 628688 
ca , 615679 
au , 507377 
de , 389203 
es , 364229 
za , 314383 
ru , 280093 
ar , 219076 
mx , 196350 
us , 194357 
dk , 178645 
be , 169521 
gov , 159736 (excluded)
se , 146496 
pl , 133990 
nz , 123479 
ch , 108853 
pt , 100589 
cn , 97950 
no , 94734 
jp , 91180 
cl , 89456 
mil , 89355 (excluded)
id , 84531 
sg , 67252 
ie , 65183 
gr , 62575 
fi , 60177 
il , 58391 
cz , 57278 
co , 49626 
tr , 49268 
eu , 43983 (excluded)
my , 43425 
hk , 40386 
at , 39829 
bg , 36378 
ph , 35338 
tw , 34756 
biz , 34665 (excluded)
hu , 33890 
ro , 33100 
ae , 31590 
pe , 24031 
hr , 23106 
kr , 21824 
vn , 21234 
info , 21137 (excluded)
ua , 18084 
lv , 17944 
pk , 17794 
sk , 16306 
uy , 15137 
sa , 13426 
tv , 12230 
rs , 10756 
th , 10592 
lt , 10206 
cc , 9879 
cat , 9731 
ma , 9141 
ke , 9023 
fm , 8936 
ee , 8812 
is , 8655 
zw , 8552 
si , 8521 
ve , 8284 
cr , 8031 
nu , 7841 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 24075605.0  - Sum of Positions: 24075605
Attacker B: Results: 1 (100.0%)  - Avg. Position: 24075605.0  - Sum of Positions: 24075605  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 9 (90.0%)  - Avg. Position: 13326582.67  - Sum of Positions: 119939244
Attacker B: Results: 9 (90.0%)  - Avg. Position: 13333334.22  - Sum of Positions: 120000008  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 4762571.88  - Sum of Positions: 266704025
Attacker B: Results: 56 (56.0%)  - Avg. Position: 4779339.39  - Sum of Positions: 267643006  (TLD True Hits/Total Hits: 4/16)

After 1000 entries:
Attacker A: Results: 527 (52.7%)  - Avg. Position: 2773568.83  - Sum of Positions: 1461670774
Attacker B: Results: 527 (52.7%)  - Avg. Position: 2763194.22  - Sum of Positions: 1456203354  (TLD True Hits/Total Hits: 31/120)

After 10000 entries:
Attacker A: Results: 5233 (52.33%)  - Avg. Position: 2948175.75  - Sum of Positions: 15427803698
Attacker B: Results: 5233 (52.33%)  - Avg. Position: 2904814.13  - Sum of Positions: 15200892319  (TLD True Hits/Total Hits: 417/1316)

After 100000 entries:
Attacker A: Results: 47749 (47.75%)  - Avg. Position: 3021112.63  - Sum of Positions: 144255106815
Attacker B: Results: 47749 (47.75%)  - Avg. Position: 2929991.04  - Sum of Positions: 139904142031  (TLD True Hits/Total Hits: 5264/17852)

After 1000000 entries:
Attacker A: Results: 484528 (48.45%)  - Avg. Position: 2988066.32  - Sum of Positions: 1447801797781
Attacker B: Results: 484528 (48.45%)  - Avg. Position: 2920737.17  - Sum of Positions: 1415178937389  (TLD True Hits/Total Hits: 44813/166370)

After 5000000 entries:
Attacker A: Results: 2289915 (45.8%)  - Avg. Position: 3007096.44  - Sum of Positions: 6885995238620
Attacker B: Results: 2289915 (45.8%)  - Avg. Position: 2937439.86  - Sum of Positions: 6726487598378  (TLD True Hits/Total Hits: 243194/923879)


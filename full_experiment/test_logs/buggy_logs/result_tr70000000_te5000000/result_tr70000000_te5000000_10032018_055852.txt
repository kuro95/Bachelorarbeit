Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 5000000 Random Entries:

com , 49516267 (excluded)
net , 4264330 (excluded)
uk , 1674773 
fr , 1297300 
edu , 1233697 (excluded)
br , 1209180 
it , 977328 
org , 900390 (excluded)
in , 885150 
nl , 732198 
ca , 718017 
au , 592578 
de , 453458 
es , 423222 
za , 365365 
ru , 325604 
ar , 253863 
mx , 228228 
us , 227634 
dk , 210865 
be , 198219 
gov , 186962 (excluded)
se , 172144 
pl , 157189 
nz , 144625 
ch , 126012 
pt , 116606 
cn , 115021 
no , 114535 
jp , 108801 
mil , 104444 (excluded)
cl , 103729 
id , 98554 
sg , 78621 
ie , 76663 
gr , 72608 
fi , 71130 
il , 68091 
cz , 67503 
co , 57364 
tr , 56888 
eu , 51421 (excluded)
my , 50688 
hk , 47126 
at , 45909 
bg , 42679 
ph , 41256 
tw , 40837 
biz , 40801 (excluded)
hu , 39646 
ro , 38408 
ae , 36580 
pe , 28004 
hr , 27207 
kr , 25820 
info , 24840 (excluded)
vn , 24780 
ua , 21078 
lv , 20897 
pk , 20535 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 136415.0  - Sum of Positions: 136415
Attacker B: Results: 1 (100.0%)  - Avg. Position: 136415.0  - Sum of Positions: 136415  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 9 (90.0%)  - Avg. Position: 3831528.22  - Sum of Positions: 34483754
Attacker B: Results: 9 (90.0%)  - Avg. Position: 3822173.78  - Sum of Positions: 34399564  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 2442365.39  - Sum of Positions: 136772462
Attacker B: Results: 56 (56.0%)  - Avg. Position: 2466217.86  - Sum of Positions: 138108200  (TLD True Hits/Total Hits: 2/8)

After 1000 entries:
Attacker A: Results: 535 (53.5%)  - Avg. Position: 3108563.6  - Sum of Positions: 1663081527
Attacker B: Results: 535 (53.5%)  - Avg. Position: 3022939.35  - Sum of Positions: 1617272552  (TLD True Hits/Total Hits: 38/110)

After 10000 entries:
Attacker A: Results: 5056 (50.56%)  - Avg. Position: 3269166.38  - Sum of Positions: 16528905216
Attacker B: Results: 5056 (50.56%)  - Avg. Position: 3202159.87  - Sum of Positions: 16190120302  (TLD True Hits/Total Hits: 523/1702)

After 100000 entries:
Attacker A: Results: 52527 (52.53%)  - Avg. Position: 3300386.4  - Sum of Positions: 173359396383
Attacker B: Results: 52527 (52.53%)  - Avg. Position: 3235594.9  - Sum of Positions: 169956093529  (TLD True Hits/Total Hits: 5295/16149)

After 1000000 entries:
Attacker A: Results: 484235 (48.42%)  - Avg. Position: 3490948.91  - Sum of Positions: 1690439643862
Attacker B: Results: 484235 (48.42%)  - Avg. Position: 3407507.79  - Sum of Positions: 1650034534523  (TLD True Hits/Total Hits: 46455/167785)

After 5000000 entries:
Attacker A: Results: 2323259 (46.47%)  - Avg. Position: 3446868.32  - Sum of Positions: 8007967850696
Attacker B: Results: 2323259 (46.47%)  - Avg. Position: 3361659.26  - Sum of Positions: 7810005130573  (TLD True Hits/Total Hits: 252490/915532)


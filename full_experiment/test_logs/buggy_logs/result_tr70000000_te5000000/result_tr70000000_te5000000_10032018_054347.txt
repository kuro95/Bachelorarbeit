Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 5000000 Random Entries:

com , 49520784 (excluded)
net , 4263601 (excluded)
uk , 1675194 
fr , 1296222 
edu , 1232413 (excluded)
br , 1208518 
it , 977953 
org , 901363 (excluded)
in , 884786 
nl , 733453 
ca , 717393 
au , 593232 
de , 453204 
es , 423783 
za , 365270 
ru , 325314 
ar , 253704 
mx , 227540 
us , 227515 
dk , 210468 
be , 197795 
gov , 186685 (excluded)
se , 172525 
pl , 157297 
nz , 144171 
ch , 126260 
pt , 116867 
cn , 114701 
no , 114230 
jp , 109260 
mil , 104094 (excluded)
cl , 103393 
id , 98391 
sg , 78848 
ie , 76351 
gr , 72565 
fi , 70835 
il , 67968 
cz , 67708 
co , 57295 
tr , 56920 
eu , 51138 (excluded)
my , 50546 
hk , 47359 
at , 46019 
bg , 42600 
ph , 41219 
tw , 40823 
biz , 40626 (excluded)
hu , 39877 
ro , 38671 
ae , 36576 
pe , 27959 
hr , 27245 
kr , 25568 
info , 24958 (excluded)
vn , 24838 
ua , 21035 
lv , 20902 
pk , 20573 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 872973.0  - Sum of Positions: 872973
Attacker B: Results: 1 (100.0%)  - Avg. Position: 872973.0  - Sum of Positions: 872973  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 7259069.83  - Sum of Positions: 43554419
Attacker B: Results: 6 (60.0%)  - Avg. Position: 7258669.0  - Sum of Positions: 43552014  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 3405541.35  - Sum of Positions: 173682609
Attacker B: Results: 51 (51.0%)  - Avg. Position: 3407376.29  - Sum of Positions: 173776191  (TLD True Hits/Total Hits: 2/11)

After 1000 entries:
Attacker A: Results: 492 (49.2%)  - Avg. Position: 3428326.31  - Sum of Positions: 1686736544
Attacker B: Results: 492 (49.2%)  - Avg. Position: 3441008.76  - Sum of Positions: 1692976310  (TLD True Hits/Total Hits: 36/120)

After 10000 entries:
Attacker A: Results: 5083 (50.83%)  - Avg. Position: 3248582.02  - Sum of Positions: 16512542433
Attacker B: Results: 5083 (50.83%)  - Avg. Position: 3180880.28  - Sum of Positions: 16168414488  (TLD True Hits/Total Hits: 570/1844)

After 100000 entries:
Attacker A: Results: 52463 (52.46%)  - Avg. Position: 3336630.79  - Sum of Positions: 175049661106
Attacker B: Results: 52463 (52.46%)  - Avg. Position: 3273480.5  - Sum of Positions: 171736607532  (TLD True Hits/Total Hits: 5184/16123)

After 1000000 entries:
Attacker A: Results: 483918 (48.39%)  - Avg. Position: 3500362.65  - Sum of Positions: 1693888492899
Attacker B: Results: 483918 (48.39%)  - Avg. Position: 3413539.14  - Sum of Positions: 1651873035048  (TLD True Hits/Total Hits: 46561/168130)

After 5000000 entries:
Attacker A: Results: 2322487 (46.45%)  - Avg. Position: 3448785.78  - Sum of Positions: 8009760131001
Attacker B: Results: 2322487 (46.45%)  - Avg. Position: 3363947.82  - Sum of Positions: 7812725070282  (TLD True Hits/Total Hits: 252083/915339)


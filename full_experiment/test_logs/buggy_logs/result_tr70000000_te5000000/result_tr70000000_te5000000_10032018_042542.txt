Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 5000000 Random Entries:

com , 49517049 (excluded)
net , 4265901 (excluded)
uk , 1675348 
fr , 1297344 
edu , 1233372 (excluded)
br , 1207990 
it , 977293 
org , 901706 (excluded)
in , 885502 
nl , 731832 
ca , 717681 
au , 592518 
de , 453129 
es , 423039 
za , 365517 
ru , 325411 
ar , 253544 
us , 227999 
mx , 227651 
dk , 210819 
be , 198119 
gov , 186458 (excluded)
se , 172632 
pl , 156981 
nz , 144097 
ch , 126304 
pt , 116627 
cn , 114719 
no , 114370 
jp , 109337 
mil , 103951 (excluded)
cl , 103335 
id , 98691 
sg , 78870 
ie , 76866 
gr , 72646 
fi , 70979 
il , 68033 
cz , 67816 
co , 57268 
tr , 56833 
eu , 51279 (excluded)
my , 50577 
hk , 47311 
at , 45973 
bg , 42770 
ph , 41444 
tw , 40946 
biz , 40551 (excluded)
hu , 39898 
ro , 38504 
ae , 36409 
pe , 27845 
hr , 27163 
kr , 25729 
info , 24773 (excluded)
vn , 24713 
ua , 21158 
lv , 20907 
pk , 20746 
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 7356349.8  - Sum of Positions: 36781749
Attacker B: Results: 5 (50.0%)  - Avg. Position: 7546478.0  - Sum of Positions: 37732390  (TLD True Hits/Total Hits: 2/2)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 4672163.22  - Sum of Positions: 233608161
Attacker B: Results: 50 (50.0%)  - Avg. Position: 3966735.6  - Sum of Positions: 198336780  (TLD True Hits/Total Hits: 5/14)

After 1000 entries:
Attacker A: Results: 487 (48.7%)  - Avg. Position: 3390307.72  - Sum of Positions: 1651079859
Attacker B: Results: 487 (48.7%)  - Avg. Position: 3223669.12  - Sum of Positions: 1569926860  (TLD True Hits/Total Hits: 36/115)

After 10000 entries:
Attacker A: Results: 4943 (49.43%)  - Avg. Position: 3152835.75  - Sum of Positions: 15584467120
Attacker B: Results: 4943 (49.43%)  - Avg. Position: 3106946.0  - Sum of Positions: 15357634070  (TLD True Hits/Total Hits: 409/1499)

After 100000 entries:
Attacker A: Results: 52389 (52.39%)  - Avg. Position: 3288507.47  - Sum of Positions: 172281617760
Attacker B: Results: 52389 (52.39%)  - Avg. Position: 3222222.85  - Sum of Positions: 168809032804  (TLD True Hits/Total Hits: 5247/16211)

After 1000000 entries:
Attacker A: Results: 484017 (48.4%)  - Avg. Position: 3482650.18  - Sum of Positions: 1685661893221
Attacker B: Results: 484017 (48.4%)  - Avg. Position: 3394752.62  - Sum of Positions: 1643117978554  (TLD True Hits/Total Hits: 46512/167631)

After 5000000 entries:
Attacker A: Results: 2322029 (46.44%)  - Avg. Position: 3439422.97  - Sum of Positions: 7986439886546
Attacker B: Results: 2322029 (46.44%)  - Avg. Position: 3353809.04  - Sum of Positions: 7787641859910  (TLD True Hits/Total Hits: 252270/916125)


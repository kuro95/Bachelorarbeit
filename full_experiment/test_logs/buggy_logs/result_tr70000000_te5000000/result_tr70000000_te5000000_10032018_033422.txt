Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 5000000 Random Entries:

com , 49520272 (excluded)
net , 4263903 (excluded)
uk , 1676636 
fr , 1296868 
edu , 1232009 (excluded)
br , 1208108 
it , 978265 
org , 900933 (excluded)
in , 885390 
nl , 732490 
ca , 717196 
au , 592854 
de , 453550 
es , 423431 
za , 364738 
ru , 325363 
ar , 253149 
us , 228348 
mx , 227425 
dk , 210482 
be , 198422 
gov , 186235 (excluded)
se , 172290 
pl , 157251 
nz , 144616 
ch , 125805 
pt , 116685 
cn , 114744 
no , 114173 
jp , 108746 
mil , 104057 (excluded)
cl , 103496 
id , 98360 
sg , 78796 
ie , 76618 
gr , 72711 
fi , 71066 
il , 68034 
cz , 67662 
co , 57624 
tr , 56937 
eu , 51160 (excluded)
my , 50665 
hk , 47400 
at , 45619 
bg , 42558 
ph , 41157 
biz , 40859 (excluded)
tw , 40767 
hu , 39755 
ro , 38665 
ae , 36519 
pe , 27829 
hr , 27179 
kr , 25621 
vn , 24957 
info , 24682 (excluded)
ua , 20897 
lv , 20851 
pk , 20616 
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 2750385.4  - Sum of Positions: 13751927
Attacker B: Results: 5 (50.0%)  - Avg. Position: 324190.2  - Sum of Positions: 1620951  (TLD True Hits/Total Hits: 3/7)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 4746809.94  - Sum of Positions: 242087307
Attacker B: Results: 51 (51.0%)  - Avg. Position: 4518388.59  - Sum of Positions: 230437818  (TLD True Hits/Total Hits: 8/21)

After 1000 entries:
Attacker A: Results: 489 (48.9%)  - Avg. Position: 3467950.46  - Sum of Positions: 1695827775
Attacker B: Results: 489 (48.9%)  - Avg. Position: 3384816.27  - Sum of Positions: 1655175154  (TLD True Hits/Total Hits: 24/100)

After 10000 entries:
Attacker A: Results: 4897 (48.97%)  - Avg. Position: 3228225.16  - Sum of Positions: 15808618599
Attacker B: Results: 4897 (48.97%)  - Avg. Position: 3171475.86  - Sum of Positions: 15530717277  (TLD True Hits/Total Hits: 433/1565)

After 100000 entries:
Attacker A: Results: 52494 (52.49%)  - Avg. Position: 3285696.34  - Sum of Positions: 172479343425
Attacker B: Results: 52494 (52.49%)  - Avg. Position: 3230875.83  - Sum of Positions: 169601595663  (TLD True Hits/Total Hits: 5290/16364)

After 1000000 entries:
Attacker A: Results: 484067 (48.41%)  - Avg. Position: 3477612.6  - Sum of Positions: 1683397499147
Attacker B: Results: 484067 (48.41%)  - Avg. Position: 3396143.15  - Sum of Positions: 1643960824384  (TLD True Hits/Total Hits: 46240/167659)

After 5000000 entries:
Attacker A: Results: 2322725 (46.45%)  - Avg. Position: 3434931.6  - Sum of Positions: 7978401497167
Attacker B: Results: 2322725 (46.45%)  - Avg. Position: 3351711.99  - Sum of Positions: 7785105233096  (TLD True Hits/Total Hits: 252337/915672)


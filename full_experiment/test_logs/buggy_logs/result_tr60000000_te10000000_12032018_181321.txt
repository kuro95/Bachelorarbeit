Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 42366855 (excluded)
net , 3626642 (excluded)
uk , 1432554 
fr , 1131548 
br , 1073429 
edu , 1057823 (excluded)
it , 861994 
org , 771026 (excluded)
in , 737386 
nl , 649818 
ca , 616575 
au , 508637 
de , 384748 
es , 379487 
za , 313055 
ru , 276980 
ar , 227821 
mx , 204724 
us , 193479 
dk , 176962 
be , 172094 
gov , 159218 (excluded)
se , 146571 
pl , 136239 
nz , 123213 
ch , 109042 
pt , 102967 
cn , 97388 
cl , 93504 
no , 92509 
mil , 89026 (excluded)
jp , 87444 
id , 83558 
ie , 67195 
sg , 66189 
gr , 61071 
il , 59729 
fi , 58724 
cz , 58138 
co , 51347 
tr , 50052 
eu , 44729 (excluded)
my , 43189 
hk , 40125 
at , 39697 
ph , 36952 
bg , 35749 
biz , 34847 (excluded)
tw , 34689 
ro , 34296 
hu , 32894 
ae , 31540 
pe , 25160 
hr , 22811 
kr , 21333 
info , 21206 (excluded)
vn , 19710 
lv , 18400 
pk , 17831 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 2 (20.0%)  - Avg. Position: 121639.5  - Sum of Positions: 243279
Attacker B: Results: 2 (20.0%)  - Avg. Position: 121639.5  - Sum of Positions: 243279  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 3006538.0  - Sum of Positions: 138300748
Attacker B: Results: 46 (46.0%)  - Avg. Position: 3024316.87  - Sum of Positions: 139118576  (TLD True Hits/Total Hits: 6/8)

After 1000 entries:
Attacker A: Results: 470 (47.0%)  - Avg. Position: 3575624.01  - Sum of Positions: 1680543284
Attacker B: Results: 470 (47.0%)  - Avg. Position: 3528277.82  - Sum of Positions: 1658290575  (TLD True Hits/Total Hits: 39/116)

After 10000 entries:
Attacker A: Results: 4668 (46.68%)  - Avg. Position: 3228499.41  - Sum of Positions: 15070635232
Attacker B: Results: 4668 (46.68%)  - Avg. Position: 3213792.13  - Sum of Positions: 15001981656  (TLD True Hits/Total Hits: 406/1248)

After 100000 entries:
Attacker A: Results: 45732 (45.73%)  - Avg. Position: 3137849.6  - Sum of Positions: 143500138086
Attacker B: Results: 45732 (45.73%)  - Avg. Position: 3071718.97  - Sum of Positions: 140475851873  (TLD True Hits/Total Hits: 5089/18086)

After 1000000 entries:
Attacker A: Results: 475700 (47.57%)  - Avg. Position: 3013908.78  - Sum of Positions: 1433716405564
Attacker B: Results: 475700 (47.57%)  - Avg. Position: 2952264.02  - Sum of Positions: 1404391993504  (TLD True Hits/Total Hits: 47550/164313)

After 5000000 entries:
Attacker A: Results: 2390858 (47.82%)  - Avg. Position: 2932164.09  - Sum of Positions: 7010387964893
Attacker B: Results: 2390858 (47.82%)  - Avg. Position: 2875520.96  - Sum of Positions: 6874962297310  (TLD True Hits/Total Hits: 238124/832774)

After 10000000 entries:
Attacker A: Results: 4664211 (46.64%)  - Avg. Position: 2962880.37  - Sum of Positions: 13819499215065
Attacker B: Results: 4664211 (46.64%)  - Avg. Position: 2899673.57  - Sum of Positions: 13524689371825  (TLD True Hits/Total Hits: 482000/1749470)


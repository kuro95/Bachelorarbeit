Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 40 TLDs:

com , 42445860 (excluded)
net , 3642098 (excluded)
uk , 1434296 
fr , 1116325 
edu , 1056123 (excluded)
br , 1041339 
it , 841118 
org , 771740 (excluded)
in , 764096 
nl , 628775 
ca , 615785 
au , 506800 
de , 388447 
es , 364580 
za , 314143 
ru , 279463 
ar , 218964 
mx , 195927 
us , 194402 
dk , 178714 
be , 169773 
gov , 159689 (excluded)
se , 146077 
pl , 133548 
nz , 123770 
ch , 108696 
pt , 100116 
cn , 98355 
no , 94842 
jp , 91338 
cl , 89330 
mil , 88810 (excluded)
id , 84272 
sg , 67764 
ie , 65478 
gr , 62389 
fi , 59965 
il , 58770 
cz , 57429 
co , 49495 
tr , 49276 
eu , 43878 (excluded)
my , 43487 
hk , 40316 
at , 39528 
bg , 36663 
ph , 35682 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 23488.0  - Sum of Positions: 23488
Attacker B: Results: 1 (100.0%)  - Avg. Position: 23488.0  - Sum of Positions: 23488  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 330530.25  - Sum of Positions: 2644242
Attacker B: Results: 8 (80.0%)  - Avg. Position: 330530.25  - Sum of Positions: 2644242  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 62 (62.0%)  - Avg. Position: 3194074.31  - Sum of Positions: 198032607
Attacker B: Results: 62 (62.0%)  - Avg. Position: 3210472.76  - Sum of Positions: 199049311  (TLD True Hits/Total Hits: 4/7)

After 1000 entries:
Attacker A: Results: 564 (56.4%)  - Avg. Position: 2574772.15  - Sum of Positions: 1452171491
Attacker B: Results: 564 (56.4%)  - Avg. Position: 2592529.89  - Sum of Positions: 1462186857  (TLD True Hits/Total Hits: 35/125)

After 10000 entries:
Attacker A: Results: 5041 (50.41%)  - Avg. Position: 2849562.03  - Sum of Positions: 14364642218
Attacker B: Results: 5041 (50.41%)  - Avg. Position: 2798822.58  - Sum of Positions: 14108864610  (TLD True Hits/Total Hits: 409/1409)

After 100000 entries:
Attacker A: Results: 47571 (47.57%)  - Avg. Position: 2954207.16  - Sum of Positions: 140534588874
Attacker B: Results: 47571 (47.57%)  - Avg. Position: 2864733.68  - Sum of Positions: 136278246110  (TLD True Hits/Total Hits: 5043/17037)

After 1000000 entries:
Attacker A: Results: 483954 (48.4%)  - Avg. Position: 2968916.18  - Sum of Positions: 1436818860867
Attacker B: Results: 483954 (48.4%)  - Avg. Position: 2902442.94  - Sum of Positions: 1404648871501  (TLD True Hits/Total Hits: 43834/158260)

After 5000000 entries:
Attacker A: Results: 2289644 (45.79%)  - Avg. Position: 3001822.97  - Sum of Positions: 6873105953007
Attacker B: Results: 2289644 (45.79%)  - Avg. Position: 2932801.88  - Sum of Positions: 6715072235783  (TLD True Hits/Total Hits: 237313/874811)


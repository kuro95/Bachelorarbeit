Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 60 TLDs:

com , 42446295 (excluded)
net , 3643045 (excluded)
uk , 1434999 
fr , 1116182 
edu , 1056726 (excluded)
br , 1042294 
it , 839101 
org , 771512 (excluded)
in , 763789 
nl , 628947 
ca , 615509 
au , 507686 
de , 388342 
es , 363900 
za , 313347 
ru , 279876 
ar , 218870 
mx , 195993 
us , 194235 
dk , 179290 
be , 169716 
gov , 160051 (excluded)
se , 145753 
pl , 133466 
nz , 123695 
ch , 108383 
pt , 100483 
cn , 98367 
no , 94502 
jp , 91562 
cl , 89662 
mil , 89085 (excluded)
id , 84554 
sg , 67383 
ie , 65316 
gr , 62337 
fi , 60027 
il , 58587 
cz , 57172 
co , 49501 
tr , 49170 
eu , 44243 (excluded)
my , 43249 
hk , 40573 
at , 39400 
bg , 36527 
ph , 35533 
biz , 34874 (excluded)
tw , 34717 
hu , 33850 
ro , 33364 
ae , 31451 
pe , 23948 
hr , 22927 
kr , 21962 
info , 21592 (excluded)
vn , 21101 
ua , 18027 
pk , 17967 
lv , 17955 
sk , 16442 
uy , 15152 
sa , 13424 
tv , 12069 
rs , 10725 
th , 10679 
lt , 10282 
cc , 9785 
cat , 9594 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 133059.0  - Sum of Positions: 133059
Attacker B: Results: 1 (100.0%)  - Avg. Position: 133059.0  - Sum of Positions: 133059  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 139041.33  - Sum of Positions: 417124
Attacker B: Results: 3 (30.0%)  - Avg. Position: 139041.33  - Sum of Positions: 417124  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 2344810.79  - Sum of Positions: 121930161
Attacker B: Results: 52 (52.0%)  - Avg. Position: 2352936.46  - Sum of Positions: 122352696  (TLD True Hits/Total Hits: 1/6)

After 1000 entries:
Attacker A: Results: 516 (51.6%)  - Avg. Position: 3074638.57  - Sum of Positions: 1586513501
Attacker B: Results: 516 (51.6%)  - Avg. Position: 3054755.73  - Sum of Positions: 1576253958  (TLD True Hits/Total Hits: 40/116)

After 10000 entries:
Attacker A: Results: 4835 (48.35%)  - Avg. Position: 3089677.9  - Sum of Positions: 14938592642
Attacker B: Results: 4835 (48.35%)  - Avg. Position: 3000112.89  - Sum of Positions: 14505545822  (TLD True Hits/Total Hits: 574/2072)

After 100000 entries:
Attacker A: Results: 47355 (47.36%)  - Avg. Position: 2998337.94  - Sum of Positions: 141986293207
Attacker B: Results: 47355 (47.36%)  - Avg. Position: 2919941.83  - Sum of Positions: 138273845325  (TLD True Hits/Total Hits: 5110/17524)

After 1000000 entries:
Attacker A: Results: 483130 (48.31%)  - Avg. Position: 2972637.46  - Sum of Positions: 1436170336680
Attacker B: Results: 483130 (48.31%)  - Avg. Position: 2908692.83  - Sum of Positions: 1405276767231  (TLD True Hits/Total Hits: 44526/164865)

After 5000000 entries:
Attacker A: Results: 2288311 (45.77%)  - Avg. Position: 3003154.61  - Sum of Positions: 6872151721181
Attacker B: Results: 2288311 (45.77%)  - Avg. Position: 2933563.85  - Sum of Positions: 6712906428731  (TLD True Hits/Total Hits: 242310/916671)


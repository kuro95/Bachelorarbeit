Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 70 TLDs:

com , 42444354 (excluded)
net , 3642184 (excluded)
uk , 1436295 
fr , 1115993 
edu , 1057237 (excluded)
br , 1042886 
it , 838898 
org , 771587 (excluded)
in , 763522 
nl , 627662 
ca , 616154 
au , 508125 
de , 388410 
es , 363952 
za , 314100 
ru , 279486 
ar , 218871 
mx , 196077 
us , 194517 
dk , 178301 
be , 169773 
gov , 160208 (excluded)
se , 146260 
pl , 133753 
nz , 123969 
ch , 108625 
pt , 100577 
cn , 98358 
no , 94226 
jp , 91335 
cl , 89563 
mil , 89391 (excluded)
id , 84185 
sg , 67506 
ie , 65723 
gr , 61997 
fi , 60376 
il , 58631 
cz , 57360 
co , 49503 
tr , 49038 
eu , 43769 (excluded)
my , 43613 
hk , 40445 
at , 39602 
bg , 36610 
ph , 35540 
tw , 34859 
biz , 34763 (excluded)
hu , 34017 
ro , 33268 
ae , 31540 
pe , 24035 
hr , 22878 
kr , 21836 
vn , 21154 
info , 21142 (excluded)
ua , 18197 
lv , 17885 
pk , 17813 
sk , 16200 
uy , 15190 
sa , 13456 
tv , 12206 
rs , 10847 
th , 10531 
lt , 10241 
cc , 9930 
cat , 9634 
ma , 9284 
ke , 8876 
ee , 8848 
fm , 8801 
is , 8713 
si , 8615 
zw , 8520 
ve , 8319 
cr , 7908 
om , 7675 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1534.0  - Sum of Positions: 1534
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1534.0  - Sum of Positions: 1534  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 2924144.67  - Sum of Positions: 17544868
Attacker B: Results: 6 (60.0%)  - Avg. Position: 2943203.17  - Sum of Positions: 17659219  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 2496378.8  - Sum of Positions: 127315319
Attacker B: Results: 51 (51.0%)  - Avg. Position: 2484576.88  - Sum of Positions: 126713421  (TLD True Hits/Total Hits: 7/14)

After 1000 entries:
Attacker A: Results: 519 (51.9%)  - Avg. Position: 2645998.08  - Sum of Positions: 1373273001
Attacker B: Results: 519 (51.9%)  - Avg. Position: 2593680.63  - Sum of Positions: 1346120247  (TLD True Hits/Total Hits: 39/127)

After 10000 entries:
Attacker A: Results: 4760 (47.6%)  - Avg. Position: 3144674.57  - Sum of Positions: 14968650958
Attacker B: Results: 4760 (47.6%)  - Avg. Position: 3047841.64  - Sum of Positions: 14507726191  (TLD True Hits/Total Hits: 616/2213)

After 100000 entries:
Attacker A: Results: 47238 (47.24%)  - Avg. Position: 2966237.27  - Sum of Positions: 140119116103
Attacker B: Results: 47238 (47.24%)  - Avg. Position: 2878936.41  - Sum of Positions: 135995198278  (TLD True Hits/Total Hits: 5100/17488)

After 1000000 entries:
Attacker A: Results: 483930 (48.39%)  - Avg. Position: 2984463.67  - Sum of Positions: 1444271505203
Attacker B: Results: 483930 (48.39%)  - Avg. Position: 2918222.57  - Sum of Positions: 1412215448789  (TLD True Hits/Total Hits: 44825/166854)

After 5000000 entries:
Attacker A: Results: 2289449 (45.79%)  - Avg. Position: 3005555.3  - Sum of Positions: 6881065573816
Attacker B: Results: 2289449 (45.79%)  - Avg. Position: 2936432.52  - Sum of Positions: 6722812488198  (TLD True Hits/Total Hits: 243241/924918)


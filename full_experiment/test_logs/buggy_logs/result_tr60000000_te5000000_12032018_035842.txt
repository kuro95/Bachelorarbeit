Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 10 TLDs:

com , 42450917 (excluded)
net , 3641259 (excluded)
uk , 1435311 
fr , 1115527 
edu , 1056804 (excluded)
br , 1042617 
it , 839988 
org , 770021 (excluded)
in , 763795 
nl , 629045 
ca , 614913 
au , 507278 
de , 388609 
es , 363907 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 1 (10.0%)  - Avg. Position: 1742027.0  - Sum of Positions: 1742027
Attacker B: Results: 1 (10.0%)  - Avg. Position: 1742027.0  - Sum of Positions: 1742027  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 1870255.35  - Sum of Positions: 89772257
Attacker B: Results: 48 (48.0%)  - Avg. Position: 1807470.0  - Sum of Positions: 86758560  (TLD True Hits/Total Hits: 5/13)

After 1000 entries:
Attacker A: Results: 530 (53.0%)  - Avg. Position: 2943772.8  - Sum of Positions: 1560199584
Attacker B: Results: 530 (53.0%)  - Avg. Position: 2950211.58  - Sum of Positions: 1563612135  (TLD True Hits/Total Hits: 29/89)

After 10000 entries:
Attacker A: Results: 4977 (49.77%)  - Avg. Position: 2859974.22  - Sum of Positions: 14234091670
Attacker B: Results: 4977 (49.77%)  - Avg. Position: 2842842.61  - Sum of Positions: 14148827693  (TLD True Hits/Total Hits: 317/948)

After 100000 entries:
Attacker A: Results: 47440 (47.44%)  - Avg. Position: 2967796.58  - Sum of Positions: 140792269769
Attacker B: Results: 47440 (47.44%)  - Avg. Position: 2907082.25  - Sum of Positions: 137911981938  (TLD True Hits/Total Hits: 4127/12137)

After 1000000 entries:
Attacker A: Results: 484030 (48.4%)  - Avg. Position: 2967580.05  - Sum of Positions: 1436397772075
Attacker B: Results: 484030 (48.4%)  - Avg. Position: 2933575.41  - Sum of Positions: 1419938503438  (TLD True Hits/Total Hits: 32187/97232)

After 5000000 entries:
Attacker A: Results: 2289305 (45.79%)  - Avg. Position: 3006460.67  - Sum of Positions: 6882705437197
Attacker B: Results: 2289305 (45.79%)  - Avg. Position: 2965540.14  - Sum of Positions: 6789025870619  (TLD True Hits/Total Hits: 176759/552340)


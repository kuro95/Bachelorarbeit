Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 70 TLDs:

com , 42444205 (excluded)
net , 3643114 (excluded)
uk , 1433396 
fr , 1115787 
edu , 1056706 (excluded)
br , 1043040 
it , 840667 
org , 771562 (excluded)
in , 764305 
nl , 628696 
ca , 615330 
au , 508034 
de , 388208 
es , 364800 
za , 313683 
ru , 279240 
ar , 218900 
mx , 196209 
us , 194402 
dk , 179141 
be , 169251 
gov , 159895 (excluded)
se , 146181 
pl , 133347 
nz , 123590 
ch , 108768 
pt , 100292 
cn , 98069 
no , 95137 
jp , 91362 
mil , 89760 (excluded)
cl , 89374 
id , 84567 
sg , 67397 
ie , 65478 
gr , 62473 
fi , 60177 
il , 58647 
cz , 57542 
co , 49386 
tr , 48916 
eu , 43885 (excluded)
my , 43692 
hk , 40355 
at , 39344 
bg , 36361 
ph , 35387 
tw , 34988 
biz , 34644 (excluded)
hu , 33793 
ro , 33317 
ae , 31735 
pe , 23967 
hr , 22821 
kr , 21970 
info , 21295 (excluded)
vn , 21134 
ua , 18066 
lv , 17948 
pk , 17759 
sk , 16280 
uy , 15140 
sa , 13346 
tv , 12110 
rs , 10851 
th , 10699 
lt , 10159 
cc , 10080 
cat , 9588 
ma , 9219 
ke , 9030 
ee , 8840 
fm , 8806 
is , 8743 
si , 8648 
zw , 8548 
ve , 8379 
cr , 7968 
nu , 7953 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 25255.6  - Sum of Positions: 126278
Attacker B: Results: 5 (50.0%)  - Avg. Position: 45683.0  - Sum of Positions: 228415  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 60 (60.0%)  - Avg. Position: 2903329.72  - Sum of Positions: 174199783
Attacker B: Results: 60 (60.0%)  - Avg. Position: 2907690.62  - Sum of Positions: 174461437  (TLD True Hits/Total Hits: 3/10)

After 1000 entries:
Attacker A: Results: 528 (52.8%)  - Avg. Position: 2949621.13  - Sum of Positions: 1557399957
Attacker B: Results: 528 (52.8%)  - Avg. Position: 2968403.87  - Sum of Positions: 1567317241  (TLD True Hits/Total Hits: 35/129)

After 10000 entries:
Attacker A: Results: 5126 (51.26%)  - Avg. Position: 2874196.5  - Sum of Positions: 14733131234
Attacker B: Results: 5126 (51.26%)  - Avg. Position: 2842821.98  - Sum of Positions: 14572305465  (TLD True Hits/Total Hits: 396/1313)

After 100000 entries:
Attacker A: Results: 47716 (47.72%)  - Avg. Position: 3000441.23  - Sum of Positions: 143169053897
Attacker B: Results: 47716 (47.72%)  - Avg. Position: 2905911.57  - Sum of Positions: 138658476653  (TLD True Hits/Total Hits: 5167/17767)

After 1000000 entries:
Attacker A: Results: 483939 (48.39%)  - Avg. Position: 2983178.4  - Sum of Positions: 1443676372056
Attacker B: Results: 483939 (48.39%)  - Avg. Position: 2916642.39  - Sum of Positions: 1411477000486  (TLD True Hits/Total Hits: 44612/166243)

After 5000000 entries:
Attacker A: Results: 2288107 (45.76%)  - Avg. Position: 3000616.04  - Sum of Positions: 6865730569426
Attacker B: Results: 2288107 (45.76%)  - Avg. Position: 2930392.81  - Sum of Positions: 6705052309836  (TLD True Hits/Total Hits: 242314/924016)


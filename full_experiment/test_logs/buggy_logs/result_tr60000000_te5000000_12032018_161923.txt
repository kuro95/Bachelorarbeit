Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 80 TLDs:

com , 42446570 (excluded)
net , 3641142 (excluded)
uk , 1434687 
fr , 1116019 
edu , 1056658 (excluded)
br , 1043087 
it , 840034 
org , 771024 (excluded)
in , 764281 
nl , 628433 
ca , 615455 
au , 507519 
de , 387712 
es , 364279 
za , 313183 
ru , 279421 
ar , 218459 
mx , 196643 
us , 194712 
dk , 179341 
be , 169364 
gov , 159934 (excluded)
se , 146314 
pl , 133841 
nz , 123272 
ch , 108345 
pt , 100648 
cn , 98249 
no , 94620 
jp , 91058 
cl , 89254 
mil , 89224 (excluded)
id , 84242 
sg , 67869 
ie , 65377 
gr , 62682 
fi , 60100 
il , 58662 
cz , 57487 
co , 49500 
tr , 49054 
eu , 43839 (excluded)
my , 43694 
hk , 40592 
at , 39559 
bg , 36495 
ph , 35460 
tw , 35032 
biz , 34855 (excluded)
hu , 33996 
ro , 33164 
ae , 31577 
pe , 24224 
hr , 22928 
kr , 21961 
vn , 21294 
info , 21276 (excluded)
ua , 18061 
lv , 18046 
pk , 17938 
sk , 16484 
uy , 15215 
sa , 13460 
tv , 12282 
rs , 10823 
th , 10579 
lt , 10283 
cc , 9775 
cat , 9702 
ma , 9124 
ke , 8974 
fm , 8897 
ee , 8808 
is , 8725 
si , 8538 
zw , 8484 
ve , 8205 
cr , 8099 
nu , 7806 
om , 7754 
lu , 7722 
tn , 7348 
lk , 7311 
ec , 6793 
con , 6156 
cy , 5333 
int , 5126 
jo , 4899 
lb , 4749 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 241669.0  - Sum of Positions: 241669
Attacker B: Results: 1 (100.0%)  - Avg. Position: 241669.0  - Sum of Positions: 241669  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 516609.33  - Sum of Positions: 3099656
Attacker B: Results: 6 (60.0%)  - Avg. Position: 516609.33  - Sum of Positions: 3099656  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 58 (58.0%)  - Avg. Position: 2077131.43  - Sum of Positions: 120473623
Attacker B: Results: 58 (58.0%)  - Avg. Position: 2103746.79  - Sum of Positions: 122017314  (TLD True Hits/Total Hits: 4/12)

After 1000 entries:
Attacker A: Results: 532 (53.2%)  - Avg. Position: 2476855.78  - Sum of Positions: 1317687277
Attacker B: Results: 532 (53.2%)  - Avg. Position: 2482590.96  - Sum of Positions: 1320738393  (TLD True Hits/Total Hits: 47/150)

After 10000 entries:
Attacker A: Results: 4949 (49.49%)  - Avg. Position: 2818676.98  - Sum of Positions: 13949632373
Attacker B: Results: 4949 (49.49%)  - Avg. Position: 2792979.28  - Sum of Positions: 13822454448  (TLD True Hits/Total Hits: 463/1624)

After 100000 entries:
Attacker A: Results: 47452 (47.45%)  - Avg. Position: 2974347.86  - Sum of Positions: 141138754554
Attacker B: Results: 47452 (47.45%)  - Avg. Position: 2884312.22  - Sum of Positions: 136866383247  (TLD True Hits/Total Hits: 5169/17793)

After 1000000 entries:
Attacker A: Results: 484081 (48.41%)  - Avg. Position: 2985343.89  - Sum of Positions: 1445148254753
Attacker B: Results: 484081 (48.41%)  - Avg. Position: 2920273.12  - Sum of Positions: 1413648731565  (TLD True Hits/Total Hits: 44907/167228)

After 5000000 entries:
Attacker A: Results: 2288950 (45.78%)  - Avg. Position: 3007550.51  - Sum of Positions: 6884132747719
Attacker B: Results: 2288950 (45.78%)  - Avg. Position: 2936937.42  - Sum of Positions: 6722502899606  (TLD True Hits/Total Hits: 243470/928815)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 100000000 Training Entries and 5000000 Random Entries:

com , 70763648 (excluded)
net , 6082476 (excluded)
uk , 2381275 
fr , 1843918 
edu , 1759343 (excluded)
br , 1726641 
it , 1400606 
org , 1283241 (excluded)
in , 1274476 
nl , 1040664 
ca , 1022249 
au , 844572 
de , 652244 
es , 600595 
za , 522849 
ru , 472781 
ar , 360119 
us , 324076 
mx , 323549 
dk , 301160 
be , 283182 
gov , 265331 (excluded)
se , 246549 
pl , 223157 
nz , 205710 
ch , 180964 
pt , 165204 
no , 164752 
cn , 163130 
jp , 157342 
mil , 147738 (excluded)
cl , 146441 
id , 141600 
sg , 112642 
ie , 108588 
gr , 105669 
fi , 102736 
il , 97511 
cz , 97311 
tr , 83222 
co , 81060 
eu , 72833 (excluded)
my , 71792 
hk , 67232 
at , 65434 
bg , 62668 
tw , 58764 
ph , 58601 
biz , 57869 (excluded)
hu , 57461 
ro , 55158 
ae , 52226 
pe , 39612 
hr , 39103 
vn , 38553 
kr , 36529 
info , 35680 (excluded)
ua , 30752 
lv , 30143 
pk , 29537 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 26978631.0  - Sum of Positions: 26978631
Attacker B: Results: 1 (100.0%)  - Avg. Position: 26978631.0  - Sum of Positions: 26978631  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 4859310.57  - Sum of Positions: 34015174
Attacker B: Results: 7 (70.0%)  - Avg. Position: 4851364.57  - Sum of Positions: 33959552  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 2522192.16  - Sum of Positions: 123587416
Attacker B: Results: 49 (49.0%)  - Avg. Position: 2270284.71  - Sum of Positions: 111243951  (TLD True Hits/Total Hits: 6/17)

After 1000 entries:
Attacker A: Results: 492 (49.2%)  - Avg. Position: 3952018.46  - Sum of Positions: 1944393083
Attacker B: Results: 492 (49.2%)  - Avg. Position: 3810096.99  - Sum of Positions: 1874567721  (TLD True Hits/Total Hits: 59/177)

After 10000 entries:
Attacker A: Results: 5006 (50.06%)  - Avg. Position: 4694427.33  - Sum of Positions: 23500303214
Attacker B: Results: 5006 (50.06%)  - Avg. Position: 4628580.68  - Sum of Positions: 23170674869  (TLD True Hits/Total Hits: 594/1808)

After 100000 entries:
Attacker A: Results: 50431 (50.43%)  - Avg. Position: 4862571.71  - Sum of Positions: 245224353827
Attacker B: Results: 50431 (50.43%)  - Avg. Position: 4649960.68  - Sum of Positions: 234502167083  (TLD True Hits/Total Hits: 8206/23558)

After 1000000 entries:
Attacker A: Results: 494392 (49.44%)  - Avg. Position: 4608603.61  - Sum of Positions: 2278456755552
Attacker B: Results: 494392 (49.44%)  - Avg. Position: 4479039.01  - Sum of Positions: 2214401053217  (TLD True Hits/Total Hits: 64970/197335)

After 5000000 entries:
Attacker A: Results: 2404598 (48.09%)  - Avg. Position: 4766689.85  - Sum of Positions: 11461972878262
Attacker B: Results: 2404598 (48.09%)  - Avg. Position: 4636424.59  - Sum of Positions: 11148737284332  (TLD True Hits/Total Hits: 266056/911753)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 100000000 Training Entries and 5000000 Random Entries:

com , 70764055 (excluded)
net , 6082620 (excluded)
uk , 2381881 
fr , 1842817 
edu , 1758773 (excluded)
br , 1726434 
it , 1400319 
org , 1283277 (excluded)
in , 1274835 
nl , 1040476 
ca , 1022201 
au , 844624 
de , 652225 
es , 601009 
za , 523097 
ru , 473200 
ar , 360240 
us , 324404 
mx , 323407 
dk , 301130 
be , 283115 
gov , 265154 (excluded)
se , 246814 
pl , 223179 
nz , 205586 
ch , 180965 
pt , 165405 
no , 164627 
cn , 162791 
jp , 157560 
mil , 147648 (excluded)
cl , 146456 
id , 141605 
sg , 112478 
ie , 108547 
gr , 105596 
fi , 102516 
il , 97459 
cz , 97237 
tr , 83121 
co , 81186 
eu , 72819 (excluded)
my , 71653 
hk , 67223 
at , 65338 
bg , 62712 
tw , 58859 
ph , 58591 
biz , 57776 (excluded)
hu , 57533 
ro , 55213 
ae , 52176 
pe , 39712 
hr , 38979 
vn , 38563 
kr , 36383 
info , 35719 (excluded)
ua , 30757 
lv , 30228 
pk , 29660 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 2111826.0  - Sum of Positions: 2111826
Attacker B: Results: 1 (100.0%)  - Avg. Position: 2111826.0  - Sum of Positions: 2111826  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 919959.6  - Sum of Positions: 4599798
Attacker B: Results: 5 (50.0%)  - Avg. Position: 919959.6  - Sum of Positions: 4599798  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 3705829.36  - Sum of Positions: 185291468
Attacker B: Results: 50 (50.0%)  - Avg. Position: 3187698.5  - Sum of Positions: 159384925  (TLD True Hits/Total Hits: 11/25)

After 1000 entries:
Attacker A: Results: 514 (51.4%)  - Avg. Position: 5626941.73  - Sum of Positions: 2892248050
Attacker B: Results: 514 (51.4%)  - Avg. Position: 5442725.27  - Sum of Positions: 2797560789  (TLD True Hits/Total Hits: 73/204)

After 10000 entries:
Attacker A: Results: 4983 (49.83%)  - Avg. Position: 4738410.39  - Sum of Positions: 23611498960
Attacker B: Results: 4983 (49.83%)  - Avg. Position: 4641525.74  - Sum of Positions: 23128722774  (TLD True Hits/Total Hits: 603/1800)

After 100000 entries:
Attacker A: Results: 50571 (50.57%)  - Avg. Position: 4913094.19  - Sum of Positions: 248460086107
Attacker B: Results: 50571 (50.57%)  - Avg. Position: 4682592.49  - Sum of Positions: 236803384586  (TLD True Hits/Total Hits: 8336/23704)

After 1000000 entries:
Attacker A: Results: 494304 (49.43%)  - Avg. Position: 4591293.61  - Sum of Positions: 2269494795859
Attacker B: Results: 494304 (49.43%)  - Avg. Position: 4461879.83  - Sum of Positions: 2205525045194  (TLD True Hits/Total Hits: 65122/197540)

After 5000000 entries:
Attacker A: Results: 2404671 (48.09%)  - Avg. Position: 4762254.32  - Sum of Positions: 11451654864140
Attacker B: Results: 2404671 (48.09%)  - Avg. Position: 4631649.45  - Sum of Positions: 11137593125950  (TLD True Hits/Total Hits: 266451/912080)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 30 TLDs:

com , 42444070 (excluded)
net , 3642717 (excluded)
uk , 1434336 
fr , 1116097 
edu , 1057892 (excluded)
br , 1042919 
it , 840502 
org , 771891 (excluded)
in , 763103 
nl , 629119 
ca , 615511 
au , 508242 
de , 389310 
es , 364202 
za , 312975 
ru , 279195 
ar , 218711 
mx , 196259 
us , 194653 
dk , 178920 
be , 169797 
gov , 160022 (excluded)
se , 146310 
pl , 133705 
nz , 123409 
ch , 108232 
pt , 100477 
cn , 97961 
no , 94759 
jp , 91218 
cl , 89316 
mil , 88931 (excluded)
id , 84110 
sg , 67290 
ie , 65264 
gr , 62259 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 17150423.0  - Sum of Positions: 17150423
Attacker B: Results: 1 (100.0%)  - Avg. Position: 17150423.0  - Sum of Positions: 17150423  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 2708508.86  - Sum of Positions: 18959562
Attacker B: Results: 7 (70.0%)  - Avg. Position: 2708508.86  - Sum of Positions: 18959562  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 53 (53.0%)  - Avg. Position: 2239939.08  - Sum of Positions: 118716771
Attacker B: Results: 53 (53.0%)  - Avg. Position: 2267474.62  - Sum of Positions: 120176155  (TLD True Hits/Total Hits: 0/7)

After 1000 entries:
Attacker A: Results: 513 (51.3%)  - Avg. Position: 2704610.7  - Sum of Positions: 1387465290
Attacker B: Results: 513 (51.3%)  - Avg. Position: 2711907.1  - Sum of Positions: 1391208340  (TLD True Hits/Total Hits: 30/115)

After 10000 entries:
Attacker A: Results: 4867 (48.67%)  - Avg. Position: 2909807.16  - Sum of Positions: 14162031460
Attacker B: Results: 4867 (48.67%)  - Avg. Position: 2851681.93  - Sum of Positions: 13879135941  (TLD True Hits/Total Hits: 436/1593)

After 100000 entries:
Attacker A: Results: 47541 (47.54%)  - Avg. Position: 2966726.84  - Sum of Positions: 141041160597
Attacker B: Results: 47541 (47.54%)  - Avg. Position: 2882512.18  - Sum of Positions: 137037511706  (TLD True Hits/Total Hits: 4983/16415)

After 1000000 entries:
Attacker A: Results: 483377 (48.34%)  - Avg. Position: 2964375.01  - Sum of Positions: 1432910699147
Attacker B: Results: 483377 (48.34%)  - Avg. Position: 2904397.21  - Sum of Positions: 1403918811996  (TLD True Hits/Total Hits: 42562/150554)

After 5000000 entries:
Attacker A: Results: 2287885 (45.76%)  - Avg. Position: 2997039.47  - Sum of Positions: 6856881648960
Attacker B: Results: 2287885 (45.76%)  - Avg. Position: 2933570.01  - Sum of Positions: 6711670816443  (TLD True Hits/Total Hits: 229903/823029)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 10 TLDs:

com , 42447741 (excluded)
net , 3641771 (excluded)
uk , 1434843 
fr , 1114764 
edu , 1055302 (excluded)
br , 1043151 
it , 839602 
org , 771923 (excluded)
in , 764295 
nl , 629795 
ca , 615725 
au , 507231 
de , 388177 
es , 364294 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 121.0  - Sum of Positions: 121
Attacker B: Results: 1 (100.0%)  - Avg. Position: 121.0  - Sum of Positions: 121  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 5404596.6  - Sum of Positions: 27022983
Attacker B: Results: 5 (50.0%)  - Avg. Position: 5558296.6  - Sum of Positions: 27791483  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 54 (54.0%)  - Avg. Position: 4024437.0  - Sum of Positions: 217319598
Attacker B: Results: 54 (54.0%)  - Avg. Position: 4034383.04  - Sum of Positions: 217856684  (TLD True Hits/Total Hits: 2/4)

After 1000 entries:
Attacker A: Results: 534 (53.4%)  - Avg. Position: 2590855.46  - Sum of Positions: 1383516818
Attacker B: Results: 534 (53.4%)  - Avg. Position: 2568527.39  - Sum of Positions: 1371593625  (TLD True Hits/Total Hits: 35/86)

After 10000 entries:
Attacker A: Results: 5017 (50.17%)  - Avg. Position: 2904042.19  - Sum of Positions: 14569579683
Attacker B: Results: 5017 (50.17%)  - Avg. Position: 2881520.13  - Sum of Positions: 14456586498  (TLD True Hits/Total Hits: 328/925)

After 100000 entries:
Attacker A: Results: 47470 (47.47%)  - Avg. Position: 2996404.6  - Sum of Positions: 142239326524
Attacker B: Results: 47470 (47.47%)  - Avg. Position: 2933262.84  - Sum of Positions: 139241986808  (TLD True Hits/Total Hits: 4084/12063)

After 1000000 entries:
Attacker A: Results: 484492 (48.45%)  - Avg. Position: 2985076.62  - Sum of Positions: 1446245742408
Attacker B: Results: 484492 (48.45%)  - Avg. Position: 2954017.75  - Sum of Positions: 1431197965960  (TLD True Hits/Total Hits: 32443/97460)

After 5000000 entries:
Attacker A: Results: 2289246 (45.78%)  - Avg. Position: 3000299.77  - Sum of Positions: 6868424254158
Attacker B: Results: 2289246 (45.78%)  - Avg. Position: 2961424.31  - Sum of Positions: 6779428767296  (TLD True Hits/Total Hits: 177472/552799)


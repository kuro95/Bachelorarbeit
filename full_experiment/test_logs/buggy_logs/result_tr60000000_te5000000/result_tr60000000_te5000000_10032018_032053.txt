Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42446681 (excluded)
net , 3640661 (excluded)
uk , 1434663 
fr , 1116332 
edu , 1056842 (excluded)
br , 1043309 
it , 839517 
org , 771322 (excluded)
in , 765507 
nl , 628403 
ca , 615667 
au , 508084 
de , 389078 
es , 363751 
za , 312740 
ru , 279360 
ar , 218783 
mx , 196412 
us , 194625 
dk , 179013 
be , 169519 
gov , 159225 (excluded)
se , 146724 
pl , 133651 
nz , 123267 
ch , 108398 
pt , 100343 
cn , 98089 
no , 94706 
jp , 91305 
cl , 89438 
mil , 89237 (excluded)
id , 84557 
sg , 67541 
ie , 65257 
gr , 62554 
fi , 60103 
il , 58644 
cz , 57127 
co , 49382 
tr , 49014 
eu , 43988 (excluded)
my , 43701 
hk , 40628 
at , 39527 
bg , 36477 
ph , 35434 
tw , 34823 
biz , 34691 (excluded)
hu , 33842 
ro , 33307 
ae , 31547 
pe , 23934 
hr , 22976 
kr , 21826 
info , 21226 (excluded)
vn , 21092 
lv , 18101 
ua , 17992 
pk , 17920 
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 10919880.0  - Sum of Positions: 32759640
Attacker B: Results: 3 (30.0%)  - Avg. Position: 10828768.33  - Sum of Positions: 32486305  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 2420942.14  - Sum of Positions: 118626165
Attacker B: Results: 49 (49.0%)  - Avg. Position: 2416004.69  - Sum of Positions: 118384230  (TLD True Hits/Total Hits: 2/7)

After 1000 entries:
Attacker A: Results: 561 (56.1%)  - Avg. Position: 2526093.38  - Sum of Positions: 1417138384
Attacker B: Results: 561 (56.1%)  - Avg. Position: 2482394.24  - Sum of Positions: 1392623167  (TLD True Hits/Total Hits: 30/100)

After 10000 entries:
Attacker A: Results: 5178 (51.78%)  - Avg. Position: 2792286.38  - Sum of Positions: 14458458890
Attacker B: Results: 5178 (51.78%)  - Avg. Position: 2768246.85  - Sum of Positions: 14333982203  (TLD True Hits/Total Hits: 404/1326)

After 100000 entries:
Attacker A: Results: 47620 (47.62%)  - Avg. Position: 2967116.24  - Sum of Positions: 141294075463
Attacker B: Results: 47620 (47.62%)  - Avg. Position: 2873872.43  - Sum of Positions: 136853805017  (TLD True Hits/Total Hits: 5162/17421)

After 1000000 entries:
Attacker A: Results: 483350 (48.34%)  - Avg. Position: 2974485.74  - Sum of Positions: 1437717683537
Attacker B: Results: 483350 (48.34%)  - Avg. Position: 2909157.42  - Sum of Positions: 1406141240483  (TLD True Hits/Total Hits: 44465/163148)

After 5000000 entries:
Attacker A: Results: 2288492 (45.77%)  - Avg. Position: 3012042.09  - Sum of Positions: 6893034215304
Attacker B: Results: 2288492 (45.77%)  - Avg. Position: 2941813.96  - Sum of Positions: 6732317714721  (TLD True Hits/Total Hits: 241135/906420)


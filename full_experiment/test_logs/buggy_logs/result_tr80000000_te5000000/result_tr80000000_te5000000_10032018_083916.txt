Attacker A: Default List - Attacker B: TLD-Lists

Results with 80000000 Training Entries and 5000000 Random Entries:

com , 56603702 (excluded)
net , 4884272 (excluded)
uk , 1913712 
fr , 1476103 
edu , 1412972 (excluded)
br , 1373475 
it , 1111107 
org , 1031922 (excluded)
in , 1009383 
nl , 834305 
ca , 820772 
au , 680161 
de , 516315 
es , 481757 
za , 418825 
ru , 372064 
ar , 288733 
us , 261378 
mx , 259269 
dk , 241277 
be , 225152 
gov , 213469 (excluded)
se , 195934 
pl , 178902 
nz , 165429 
ch , 143556 
pt , 132481 
no , 132008 
cn , 131145 
jp , 124910 
mil , 118897 (excluded)
cl , 117483 
id , 113154 
sg , 90039 
ie , 87506 
gr , 83422 
fi , 82126 
cz , 77238 
il , 77233 
tr , 65537 
co , 65258 
eu , 58093 (excluded)
my , 57401 
hk , 54169 
at , 51896 
bg , 49146 
tw , 47043 
ph , 46807 
biz , 46247 (excluded)
hu , 45412 
ro , 44021 
ae , 41626 
pe , 31900 
hr , 30966 
vn , 30753 
kr , 29320 
info , 28396 (excluded)
ua , 23964 
lv , 23741 
pk , 23487 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 91.0  - Sum of Positions: 91
Attacker B: Results: 1 (100.0%)  - Avg. Position: 91.0  - Sum of Positions: 91  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 8954.67  - Sum of Positions: 26864
Attacker B: Results: 3 (30.0%)  - Avg. Position: 8954.67  - Sum of Positions: 26864  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 1980483.16  - Sum of Positions: 89121742
Attacker B: Results: 45 (45.0%)  - Avg. Position: 1998013.31  - Sum of Positions: 89910599  (TLD True Hits/Total Hits: 3/9)

After 1000 entries:
Attacker A: Results: 461 (46.1%)  - Avg. Position: 3949892.9  - Sum of Positions: 1820900626
Attacker B: Results: 461 (46.1%)  - Avg. Position: 3837834.75  - Sum of Positions: 1769241821  (TLD True Hits/Total Hits: 17/77)

After 10000 entries:
Attacker A: Results: 4905 (49.05%)  - Avg. Position: 4338139.43  - Sum of Positions: 21278573918
Attacker B: Results: 4905 (49.05%)  - Avg. Position: 4235200.16  - Sum of Positions: 20773656769  (TLD True Hits/Total Hits: 315/1135)

After 100000 entries:
Attacker A: Results: 48067 (48.07%)  - Avg. Position: 4089324.82  - Sum of Positions: 196561576246
Attacker B: Results: 48067 (48.07%)  - Avg. Position: 3995654.35  - Sum of Positions: 192059117713  (TLD True Hits/Total Hits: 4251/16816)

After 1000000 entries:
Attacker A: Results: 479184 (47.92%)  - Avg. Position: 3841297.58  - Sum of Positions: 1840688340737
Attacker B: Results: 479184 (47.92%)  - Avg. Position: 3711638.5  - Sum of Positions: 1778557782786  (TLD True Hits/Total Hits: 64033/211490)

After 5000000 entries:
Attacker A: Results: 2341408 (46.83%)  - Avg. Position: 3885831.06  - Sum of Positions: 9098315938774
Attacker B: Results: 2341408 (46.83%)  - Avg. Position: 3782618.27  - Sum of Positions: 8856652686789  (TLD True Hits/Total Hits: 264988/939952)


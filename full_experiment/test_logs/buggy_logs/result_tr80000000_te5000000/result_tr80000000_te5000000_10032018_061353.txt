Attacker A: Default List - Attacker B: TLD-Lists

Results with 80000000 Training Entries and 5000000 Random Entries:

com , 56601755 (excluded)
net , 4883854 (excluded)
uk , 1915003 
fr , 1476140 
edu , 1414568 (excluded)
br , 1374024 
it , 1112638 
org , 1031947 (excluded)
in , 1008516 
nl , 832689 
ca , 821120 
au , 679146 
de , 516138 
es , 481486 
za , 418927 
ru , 372364 
ar , 288275 
us , 261208 
mx , 259030 
dk , 241158 
be , 225514 
gov , 213304 (excluded)
se , 195840 
pl , 178954 
nz , 165237 
ch , 143610 
pt , 132769 
no , 132112 
cn , 131199 
jp , 125472 
mil , 118836 (excluded)
cl , 117491 
id , 112793 
sg , 89983 
ie , 87696 
gr , 83421 
fi , 81892 
il , 77448 
cz , 77264 
tr , 65927 
co , 65059 
eu , 58388 (excluded)
my , 57773 
hk , 54082 
at , 52109 
bg , 48972 
ph , 47012 
tw , 46848 
biz , 46515 (excluded)
hu , 45591 
ro , 44012 
ae , 41673 
pe , 31789 
vn , 30928 
hr , 30908 
kr , 29277 
info , 28317 (excluded)
ua , 23869 
lv , 23736 
pk , 23537 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 27949634.0  - Sum of Positions: 27949634
Attacker B: Results: 1 (100.0%)  - Avg. Position: 27949634.0  - Sum of Positions: 27949634  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 3816894.0  - Sum of Positions: 30535152
Attacker B: Results: 8 (80.0%)  - Avg. Position: 3903229.38  - Sum of Positions: 31225835  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 55 (55.0%)  - Avg. Position: 7340288.02  - Sum of Positions: 403715841
Attacker B: Results: 55 (55.0%)  - Avg. Position: 7368316.24  - Sum of Positions: 405257393  (TLD True Hits/Total Hits: 7/19)

After 1000 entries:
Attacker A: Results: 486 (48.6%)  - Avg. Position: 4518711.91  - Sum of Positions: 2196093990
Attacker B: Results: 486 (48.6%)  - Avg. Position: 4349357.74  - Sum of Positions: 2113787863  (TLD True Hits/Total Hits: 79/174)

After 10000 entries:
Attacker A: Results: 4847 (48.47%)  - Avg. Position: 3902158.38  - Sum of Positions: 18913761647
Attacker B: Results: 4847 (48.47%)  - Avg. Position: 3800961.04  - Sum of Positions: 18423258141  (TLD True Hits/Total Hits: 317/1238)

After 100000 entries:
Attacker A: Results: 47989 (47.99%)  - Avg. Position: 3992688.7  - Sum of Positions: 191605138114
Attacker B: Results: 47989 (47.99%)  - Avg. Position: 3888289.55  - Sum of Positions: 186595127034  (TLD True Hits/Total Hits: 4307/17196)

After 1000000 entries:
Attacker A: Results: 479518 (47.95%)  - Avg. Position: 3823845.16  - Sum of Positions: 1833602584976
Attacker B: Results: 479518 (47.95%)  - Avg. Position: 3699766.11  - Sum of Positions: 1774104447601  (TLD True Hits/Total Hits: 64337/211975)

After 5000000 entries:
Attacker A: Results: 2341698 (46.83%)  - Avg. Position: 3866668.47  - Sum of Positions: 9054569828729
Attacker B: Results: 2341698 (46.83%)  - Avg. Position: 3763750.36  - Sum of Positions: 8813566701926  (TLD True Hits/Total Hits: 265812/941355)


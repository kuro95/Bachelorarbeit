Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 10 TLDs:

com , 42447140 (excluded)
net , 3639599 (excluded)
uk , 1434480 
fr , 1115550 
edu , 1056844 (excluded)
br , 1042574 
it , 840292 
org , 770689 (excluded)
in , 764254 
nl , 629573 
ca , 616344 
au , 507068 
de , 388903 
es , 363327 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 5692.0  - Sum of Positions: 5692
Attacker B: Results: 1 (100.0%)  - Avg. Position: 6216.0  - Sum of Positions: 6216  (TLD True Hits/Total Hits: 1/1)

After 10 entries:
Attacker A: Results: 9 (90.0%)  - Avg. Position: 5788469.67  - Sum of Positions: 52096227
Attacker B: Results: 9 (90.0%)  - Avg. Position: 5898712.11  - Sum of Positions: 53088409  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 2510115.65  - Sum of Positions: 128015898
Attacker B: Results: 51 (51.0%)  - Avg. Position: 2549014.47  - Sum of Positions: 129999738  (TLD True Hits/Total Hits: 1/3)

After 1000 entries:
Attacker A: Results: 509 (50.9%)  - Avg. Position: 2878474.25  - Sum of Positions: 1465143394
Attacker B: Results: 509 (50.9%)  - Avg. Position: 2887999.15  - Sum of Positions: 1469991568  (TLD True Hits/Total Hits: 17/64)

After 10000 entries:
Attacker A: Results: 5172 (51.72%)  - Avg. Position: 2867294.17  - Sum of Positions: 14829645468
Attacker B: Results: 5172 (51.72%)  - Avg. Position: 2848914.71  - Sum of Positions: 14734586887  (TLD True Hits/Total Hits: 314/861)

After 100000 entries:
Attacker A: Results: 47789 (47.79%)  - Avg. Position: 3023059.92  - Sum of Positions: 144469010331
Attacker B: Results: 47789 (47.79%)  - Avg. Position: 2960108.13  - Sum of Positions: 141460607273  (TLD True Hits/Total Hits: 4055/12061)

After 1000000 entries:
Attacker A: Results: 483766 (48.38%)  - Avg. Position: 2975695.16  - Sum of Positions: 1439540146588
Attacker B: Results: 483766 (48.38%)  - Avg. Position: 2939121.2  - Sum of Positions: 1421846905677  (TLD True Hits/Total Hits: 32398/97384)

After 5000000 entries:
Attacker A: Results: 2287856 (45.76%)  - Avg. Position: 3008629.04  - Sum of Positions: 6883309996724
Attacker B: Results: 2287856 (45.76%)  - Avg. Position: 2967328.92  - Sum of Positions: 6788821269760  (TLD True Hits/Total Hits: 177200/552427)


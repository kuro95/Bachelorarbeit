Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 30 TLDs:

com , 42447132 (excluded)
net , 3638559 (excluded)
uk , 1434941 
fr , 1116233 
edu , 1056337 (excluded)
br , 1042763 
it , 839860 
org , 772465 (excluded)
in , 763914 
nl , 628496 
ca , 615545 
au , 507261 
de , 388801 
es , 364822 
za , 313689 
ru , 279543 
ar , 218669 
mx , 196108 
us , 194987 
dk , 178854 
be , 169169 
gov , 159959 (excluded)
se , 146281 
pl , 133440 
nz , 123387 
ch , 108960 
pt , 100389 
cn , 98295 
no , 94693 
jp , 91658 
mil , 89419 (excluded)
cl , 89335 
id , 84468 
sg , 67163 
ie , 65413 
gr , 62371 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 30153.0  - Sum of Positions: 30153
Attacker B: Results: 1 (100.0%)  - Avg. Position: 30153.0  - Sum of Positions: 30153  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 36278.12  - Sum of Positions: 290225
Attacker B: Results: 8 (80.0%)  - Avg. Position: 36278.12  - Sum of Positions: 290225  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 59 (59.0%)  - Avg. Position: 1959264.61  - Sum of Positions: 115596612
Attacker B: Results: 59 (59.0%)  - Avg. Position: 1956843.44  - Sum of Positions: 115453763  (TLD True Hits/Total Hits: 4/10)

After 1000 entries:
Attacker A: Results: 534 (53.4%)  - Avg. Position: 3089141.96  - Sum of Positions: 1649601804
Attacker B: Results: 534 (53.4%)  - Avg. Position: 3011660.39  - Sum of Positions: 1608226650  (TLD True Hits/Total Hits: 44/116)

After 10000 entries:
Attacker A: Results: 5014 (50.14%)  - Avg. Position: 2901736.22  - Sum of Positions: 14549305431
Attacker B: Results: 5014 (50.14%)  - Avg. Position: 2872809.85  - Sum of Positions: 14404268576  (TLD True Hits/Total Hits: 452/1482)

After 100000 entries:
Attacker A: Results: 47503 (47.5%)  - Avg. Position: 2902317.69  - Sum of Positions: 137868797191
Attacker B: Results: 47503 (47.5%)  - Avg. Position: 2827399.43  - Sum of Positions: 134309955295  (TLD True Hits/Total Hits: 5105/16641)

After 1000000 entries:
Attacker A: Results: 484419 (48.44%)  - Avg. Position: 2973873.75  - Sum of Positions: 1440600947928
Attacker B: Results: 484419 (48.44%)  - Avg. Position: 2915346.74  - Sum of Positions: 1412249352340  (TLD True Hits/Total Hits: 42788/151202)

After 5000000 entries:
Attacker A: Results: 2289557 (45.79%)  - Avg. Position: 3004004.17  - Sum of Positions: 6877838784280
Attacker B: Results: 2289557 (45.79%)  - Avg. Position: 2940915.53  - Sum of Positions: 6733393737859  (TLD True Hits/Total Hits: 230279/822956)


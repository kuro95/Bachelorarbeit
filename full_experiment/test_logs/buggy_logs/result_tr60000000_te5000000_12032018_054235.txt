Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 20 TLDs:

com , 42443058 (excluded)
net , 3642372 (excluded)
uk , 1435136 
fr , 1116451 
edu , 1056311 (excluded)
br , 1043739 
it , 839912 
org , 771879 (excluded)
in , 764948 
nl , 628933 
ca , 615787 
au , 507865 
de , 388664 
es , 364568 
za , 313220 
ru , 279723 
ar , 218824 
mx , 195711 
us , 194630 
dk , 179020 
be , 169843 
gov , 159439 (excluded)
se , 146160 
pl , 133183 
nz , 123846 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 580662.67  - Sum of Positions: 1741988
Attacker B: Results: 3 (30.0%)  - Avg. Position: 911383.67  - Sum of Positions: 2734151  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 3999208.0  - Sum of Positions: 195961192
Attacker B: Results: 49 (49.0%)  - Avg. Position: 4032240.0  - Sum of Positions: 197579760  (TLD True Hits/Total Hits: 2/9)

After 1000 entries:
Attacker A: Results: 546 (54.6%)  - Avg. Position: 2812215.66  - Sum of Positions: 1535469749
Attacker B: Results: 546 (54.6%)  - Avg. Position: 2820305.4  - Sum of Positions: 1539886748  (TLD True Hits/Total Hits: 43/113)

After 10000 entries:
Attacker A: Results: 5119 (51.19%)  - Avg. Position: 2895943.08  - Sum of Positions: 14824332615
Attacker B: Results: 5119 (51.19%)  - Avg. Position: 2857262.74  - Sum of Positions: 14626327961  (TLD True Hits/Total Hits: 375/1177)

After 100000 entries:
Attacker A: Results: 47591 (47.59%)  - Avg. Position: 2966260.47  - Sum of Positions: 141167302123
Attacker B: Results: 47591 (47.59%)  - Avg. Position: 2891624.16  - Sum of Positions: 137615285535  (TLD True Hits/Total Hits: 4806/15284)

After 1000000 entries:
Attacker A: Results: 483473 (48.35%)  - Avg. Position: 2983701.33  - Sum of Positions: 1442539033613
Attacker B: Results: 483473 (48.35%)  - Avg. Position: 2932643.99  - Sum of Positions: 1417854189202  (TLD True Hits/Total Hits: 39769/132199)

After 5000000 entries:
Attacker A: Results: 2288524 (45.77%)  - Avg. Position: 3002415.47  - Sum of Positions: 6871099853926
Attacker B: Results: 2288524 (45.77%)  - Avg. Position: 2946486.07  - Sum of Positions: 6743104076182  (TLD True Hits/Total Hits: 212587/714882)


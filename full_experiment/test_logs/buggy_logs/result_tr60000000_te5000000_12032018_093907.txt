Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 40 TLDs:

com , 42442614 (excluded)
net , 3643291 (excluded)
uk , 1433786 
fr , 1116771 
edu , 1057710 (excluded)
br , 1043334 
it , 838895 
org , 772690 (excluded)
in , 764566 
nl , 629327 
ca , 615447 
au , 507667 
de , 389144 
es , 364141 
za , 312975 
ru , 279801 
ar , 218829 
mx , 196611 
us , 194099 
dk , 179082 
be , 169416 
gov , 159576 (excluded)
se , 145948 
pl , 133286 
nz , 123700 
ch , 108519 
pt , 100612 
cn , 98491 
no , 94616 
jp , 91299 
cl , 89810 
mil , 89441 (excluded)
id , 84601 
sg , 67573 
ie , 65055 
gr , 62520 
fi , 60259 
il , 58530 
cz , 57362 
co , 49458 
tr , 48757 
eu , 43692 (excluded)
my , 43341 
hk , 40379 
at , 39652 
bg , 36414 
ph , 35469 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 2944651.5  - Sum of Positions: 17667909
Attacker B: Results: 6 (60.0%)  - Avg. Position: 2944651.5  - Sum of Positions: 17667909  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 47 (47.0%)  - Avg. Position: 2170864.68  - Sum of Positions: 102030640
Attacker B: Results: 47 (47.0%)  - Avg. Position: 2197462.77  - Sum of Positions: 103280750  (TLD True Hits/Total Hits: 2/10)

After 1000 entries:
Attacker A: Results: 541 (54.1%)  - Avg. Position: 2617402.99  - Sum of Positions: 1416015019
Attacker B: Results: 541 (54.1%)  - Avg. Position: 2595781.97  - Sum of Positions: 1404318048  (TLD True Hits/Total Hits: 47/121)

After 10000 entries:
Attacker A: Results: 5033 (50.33%)  - Avg. Position: 2870315.48  - Sum of Positions: 14446297798
Attacker B: Results: 5033 (50.33%)  - Avg. Position: 2816541.43  - Sum of Positions: 14175653034  (TLD True Hits/Total Hits: 430/1339)

After 100000 entries:
Attacker A: Results: 47378 (47.38%)  - Avg. Position: 2954410.32  - Sum of Positions: 139974052216
Attacker B: Results: 47378 (47.38%)  - Avg. Position: 2866469.73  - Sum of Positions: 135807603028  (TLD True Hits/Total Hits: 5115/17102)

After 1000000 entries:
Attacker A: Results: 483513 (48.35%)  - Avg. Position: 2984066.82  - Sum of Positions: 1442835098908
Attacker B: Results: 483513 (48.35%)  - Avg. Position: 2920278.93  - Sum of Positions: 1411992825122  (TLD True Hits/Total Hits: 43826/158316)

After 5000000 entries:
Attacker A: Results: 2289246 (45.78%)  - Avg. Position: 3006545.37  - Sum of Positions: 6882721965674
Attacker B: Results: 2289246 (45.78%)  - Avg. Position: 2939222.67  - Sum of Positions: 6728603739168  (TLD True Hits/Total Hits: 237571/874089)


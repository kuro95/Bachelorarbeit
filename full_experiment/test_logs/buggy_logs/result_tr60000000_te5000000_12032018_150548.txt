Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries and top 70 TLDs:

com , 42444460 (excluded)
net , 3642862 (excluded)
uk , 1435140 
fr , 1116422 
edu , 1056473 (excluded)
br , 1043222 
it , 839650 
org , 771800 (excluded)
in , 764378 
nl , 628749 
ca , 615367 
au , 507311 
de , 388204 
es , 364500 
za , 313641 
ru , 279257 
ar , 218594 
mx , 196006 
us , 194658 
dk , 178733 
be , 169394 
gov , 159488 (excluded)
se , 146166 
pl , 132946 
nz , 123719 
ch , 108167 
pt , 100648 
cn , 98712 
no , 94660 
jp , 91285 
mil , 89420 (excluded)
cl , 89263 
id , 85035 
sg , 67260 
ie , 65398 
gr , 62192 
fi , 60124 
il , 58745 
cz , 57404 
co , 49603 
tr , 48824 
eu , 43906 (excluded)
my , 43688 
hk , 40145 
at , 39481 
bg , 36643 
ph , 35778 
tw , 34975 
biz , 34913 (excluded)
hu , 34044 
ro , 33442 
ae , 31566 
pe , 23961 
hr , 22997 
kr , 22199 
info , 21258 (excluded)
vn , 21153 
lv , 18023 
ua , 17951 
pk , 17817 
sk , 16335 
uy , 15090 
sa , 13373 
tv , 12231 
rs , 10815 
th , 10582 
lt , 10201 
cc , 10031 
cat , 9554 
ma , 9276 
ke , 8928 
ee , 8864 
fm , 8857 
is , 8775 
zw , 8435 
si , 8413 
ve , 8332 
cr , 7937 
om , 7801 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 85.0  - Sum of Positions: 85
Attacker B: Results: 1 (100.0%)  - Avg. Position: 85.0  - Sum of Positions: 85  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 2 (20.0%)  - Avg. Position: 2012.5  - Sum of Positions: 4025
Attacker B: Results: 2 (20.0%)  - Avg. Position: 2012.5  - Sum of Positions: 4025  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 2391397.67  - Sum of Positions: 107612895
Attacker B: Results: 45 (45.0%)  - Avg. Position: 2429692.84  - Sum of Positions: 109336178  (TLD True Hits/Total Hits: 2/11)

After 1000 entries:
Attacker A: Results: 573 (57.3%)  - Avg. Position: 2552649.86  - Sum of Positions: 1462668369
Attacker B: Results: 573 (57.3%)  - Avg. Position: 2479011.76  - Sum of Positions: 1420473739  (TLD True Hits/Total Hits: 45/115)

After 10000 entries:
Attacker A: Results: 5207 (52.07%)  - Avg. Position: 2747142.27  - Sum of Positions: 14304369801
Attacker B: Results: 5207 (52.07%)  - Avg. Position: 2715795.09  - Sum of Positions: 14141145010  (TLD True Hits/Total Hits: 418/1339)

After 100000 entries:
Attacker A: Results: 47773 (47.77%)  - Avg. Position: 3000267.38  - Sum of Positions: 143331773411
Attacker B: Results: 47773 (47.77%)  - Avg. Position: 2913531.67  - Sum of Positions: 139188148348  (TLD True Hits/Total Hits: 5112/17700)

After 1000000 entries:
Attacker A: Results: 483928 (48.39%)  - Avg. Position: 2971468.92  - Sum of Positions: 1437977010499
Attacker B: Results: 483928 (48.39%)  - Avg. Position: 2906557.6  - Sum of Positions: 1406564606611  (TLD True Hits/Total Hits: 44490/166483)

After 5000000 entries:
Attacker A: Results: 2288882 (45.78%)  - Avg. Position: 3008854.4  - Sum of Positions: 6886912670595
Attacker B: Results: 2288882 (45.78%)  - Avg. Position: 2940553.51  - Sum of Positions: 6730579997688  (TLD True Hits/Total Hits: 242696/924245)


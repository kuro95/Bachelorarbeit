Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42448336 (excluded)
net , 3641660 (excluded)
uk , 1433767 
fr , 1116809 
edu , 1056131 (excluded)
br , 1042381 
it , 839062 
org , 770847 (excluded)
in , 764827 
nl , 628105 
ca , 614676 
au , 507972 
de , 388715 
es , 364398 
za , 313859 
ru , 279373 
ar , 218988 
mx , 196336 
us , 194522 
dk , 179157 
be , 169990 
gov , 159209 (excluded)
se , 146273 
pl , 133462 
nz , 123406 
ch , 108705 
pt , 100568 
cn , 98141 
no , 94751 
jp , 91561 
mil , 89586 (excluded)
cl , 89578 
id , 84480 
sg , 67355 
ie , 65311 
gr , 62433 
fi , 60462 
il , 58270 
cz , 57332 
co , 49708 
tr , 49063 
eu , 43980 (excluded)
my , 43518 
hk , 40200 
at , 39853 
bg , 36427 
ph , 35473 
tw , 34915 
biz , 34671 (excluded)
hu , 33928 
ro , 33098 
ae , 31530 
pe , 24056 
hr , 22952 
kr , 22070 
info , 21217 (excluded)
vn , 21210 
ua , 18060 
lv , 18055 
pk , 17893 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 136983.0  - Sum of Positions: 136983
Attacker B: Results: 1 (100.0%)  - Avg. Position: 136983.0  - Sum of Positions: 136983  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 906277.5  - Sum of Positions: 3625110
Attacker B: Results: 4 (40.0%)  - Avg. Position: 165584.75  - Sum of Positions: 662339  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 53 (53.0%)  - Avg. Position: 3787114.19  - Sum of Positions: 200717052
Attacker B: Results: 53 (53.0%)  - Avg. Position: 3625110.6  - Sum of Positions: 192130862  (TLD True Hits/Total Hits: 7/23)

After 1000 entries:
Attacker A: Results: 526 (52.6%)  - Avg. Position: 2823639.69  - Sum of Positions: 1485234475
Attacker B: Results: 526 (52.6%)  - Avg. Position: 2792352.73  - Sum of Positions: 1468777535  (TLD True Hits/Total Hits: 45/146)

After 10000 entries:
Attacker A: Results: 4979 (49.79%)  - Avg. Position: 2934652.34  - Sum of Positions: 14611634009
Attacker B: Results: 4979 (49.79%)  - Avg. Position: 2892742.18  - Sum of Positions: 14402963315  (TLD True Hits/Total Hits: 449/1612)

After 100000 entries:
Attacker A: Results: 47604 (47.6%)  - Avg. Position: 2929466.29  - Sum of Positions: 139454313262
Attacker B: Results: 47604 (47.6%)  - Avg. Position: 2849736.68  - Sum of Positions: 135658864793  (TLD True Hits/Total Hits: 5114/17406)

After 1000000 entries:
Attacker A: Results: 483584 (48.36%)  - Avg. Position: 2970475.03  - Sum of Positions: 1436474194606
Attacker B: Results: 483584 (48.36%)  - Avg. Position: 2905604.7  - Sum of Positions: 1405103942694  (TLD True Hits/Total Hits: 44155/162355)

After 5000000 entries:
Attacker A: Results: 2288614 (45.77%)  - Avg. Position: 2999734.31  - Sum of Positions: 6865233932157
Attacker B: Results: 2288614 (45.77%)  - Avg. Position: 2931383.26  - Sum of Positions: 6708804760373  (TLD True Hits/Total Hits: 241392/907019)


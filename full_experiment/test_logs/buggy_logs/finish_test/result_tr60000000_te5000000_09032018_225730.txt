Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42447141 (excluded)
net , 3642718 (excluded)
uk , 1436031 
fr , 1115813 
edu , 1055606 (excluded)
br , 1042527 
it , 839873 
org , 770668 (excluded)
in , 763394 
nl , 628227 
ca , 615688 
au , 507259 
de , 388935 
es , 363965 
za , 312869 
ru , 279184 
ar , 219224 
mx , 195806 
us , 194967 
dk , 178758 
be , 169805 
gov , 159977 (excluded)
se , 145972 
pl , 133404 
nz , 123721 
ch , 108426 
pt , 100494 
cn , 98415 
no , 94908 
jp , 91361 
mil , 89501 (excluded)
cl , 89427 
id , 84456 
sg , 67246 
ie , 65392 
gr , 62671 
fi , 60170 
il , 58440 
cz , 57300 
co , 49677 
tr , 48798 
eu , 44308 (excluded)
my , 43443 
hk , 40391 
at , 39579 
bg , 36323 
ph , 35521 
biz , 34846 (excluded)
tw , 34777 
hu , 33828 
ro , 33371 
ae , 31485 
pe , 23770 
hr , 22979 
kr , 22089 
info , 21314 (excluded)
vn , 21158 
ua , 18144 
lv , 18038 
pk , 17841 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 372606.0  - Sum of Positions: 372606
Attacker B: Results: 1 (100.0%)  - Avg. Position: 372606.0  - Sum of Positions: 372606  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 1298286.6  - Sum of Positions: 6491433
Attacker B: Results: 5 (50.0%)  - Avg. Position: 1298286.6  - Sum of Positions: 6491433  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 47 (47.0%)  - Avg. Position: 2876546.17  - Sum of Positions: 135197670
Attacker B: Results: 47 (47.0%)  - Avg. Position: 2897154.09  - Sum of Positions: 136166242  (TLD True Hits/Total Hits: 2/8)

After 1000 entries:
Attacker A: Results: 519 (51.9%)  - Avg. Position: 3576273.08  - Sum of Positions: 1856085731
Attacker B: Results: 519 (51.9%)  - Avg. Position: 3586414.85  - Sum of Positions: 1861349309  (TLD True Hits/Total Hits: 39/106)

After 10000 entries:
Attacker A: Results: 5049 (50.49%)  - Avg. Position: 2872820.75  - Sum of Positions: 14504871976
Attacker B: Results: 5049 (50.49%)  - Avg. Position: 2845545.02  - Sum of Positions: 14367156793  (TLD True Hits/Total Hits: 420/1376)

After 100000 entries:
Attacker A: Results: 47395 (47.39%)  - Avg. Position: 2981763.61  - Sum of Positions: 141320686404
Attacker B: Results: 47395 (47.39%)  - Avg. Position: 2895577.83  - Sum of Positions: 137235911482  (TLD True Hits/Total Hits: 5156/17595)

After 1000000 entries:
Attacker A: Results: 484473 (48.45%)  - Avg. Position: 2989734.45  - Sum of Positions: 1448445616691
Attacker B: Results: 484473 (48.45%)  - Avg. Position: 2924755.29  - Sum of Positions: 1416964971299  (TLD True Hits/Total Hits: 44311/163265)

After 5000000 entries:
Attacker A: Results: 2290456 (45.81%)  - Avg. Position: 3009960.11  - Sum of Positions: 6894181189421
Attacker B: Results: 2290456 (45.81%)  - Avg. Position: 2940686.76  - Sum of Positions: 6735513626886  (TLD True Hits/Total Hits: 240948/906898)


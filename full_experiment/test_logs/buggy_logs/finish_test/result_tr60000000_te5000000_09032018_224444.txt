Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42446508 (excluded)
net , 3638605 (excluded)
uk , 1435895 
fr , 1115890 
edu , 1057647 (excluded)
br , 1043125 
it , 839537 
org , 771227 (excluded)
in , 764722 
nl , 628770 
ca , 615601 
au , 507296 
de , 388971 
es , 364358 
za , 313372 
ru , 278983 
ar , 219141 
mx , 196541 
us , 194496 
dk , 178680 
be , 169436 
gov , 159938 (excluded)
se , 146138 
pl , 133633 
nz , 123726 
ch , 108345 
pt , 100519 
cn , 98424 
no , 94534 
jp , 91358 
mil , 89355 (excluded)
cl , 89059 
id , 84695 
sg , 67592 
ie , 65129 
gr , 62165 
fi , 60033 
il , 58504 
cz , 57456 
co , 49350 
tr , 48943 
eu , 43801 (excluded)
my , 43568 
hk , 40330 
at , 39547 
bg , 36552 
ph , 35567 
biz , 35112 (excluded)
tw , 34874 
hu , 33878 
ro , 33343 
ae , 31530 
pe , 24152 
hr , 22890 
kr , 22007 
info , 21444 (excluded)
vn , 21171 
lv , 18231 
pk , 17926 
ua , 17889 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 85766.0  - Sum of Positions: 85766
Attacker B: Results: 1 (100.0%)  - Avg. Position: 85766.0  - Sum of Positions: 85766  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 125286.4  - Sum of Positions: 626432
Attacker B: Results: 5 (50.0%)  - Avg. Position: 104392.8  - Sum of Positions: 521964  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 55 (55.0%)  - Avg. Position: 4213408.42  - Sum of Positions: 231737463
Attacker B: Results: 55 (55.0%)  - Avg. Position: 4218555.56  - Sum of Positions: 232020556  (TLD True Hits/Total Hits: 5/15)

After 1000 entries:
Attacker A: Results: 552 (55.2%)  - Avg. Position: 2899511.94  - Sum of Positions: 1600530590
Attacker B: Results: 552 (55.2%)  - Avg. Position: 2923370.5  - Sum of Positions: 1613700515  (TLD True Hits/Total Hits: 38/97)

After 10000 entries:
Attacker A: Results: 5150 (51.5%)  - Avg. Position: 2849109.26  - Sum of Positions: 14672912697
Attacker B: Results: 5150 (51.5%)  - Avg. Position: 2812272.99  - Sum of Positions: 14483205902  (TLD True Hits/Total Hits: 440/1345)

After 100000 entries:
Attacker A: Results: 47692 (47.69%)  - Avg. Position: 3002368.88  - Sum of Positions: 143188976403
Attacker B: Results: 47692 (47.69%)  - Avg. Position: 2912286.04  - Sum of Positions: 138892745961  (TLD True Hits/Total Hits: 5230/17649)

After 1000000 entries:
Attacker A: Results: 483747 (48.37%)  - Avg. Position: 2978316.13  - Sum of Positions: 1440751494982
Attacker B: Results: 483747 (48.37%)  - Avg. Position: 2914729.57  - Sum of Positions: 1409991686020  (TLD True Hits/Total Hits: 44609/163165)

After 5000000 entries:
Attacker A: Results: 2289436 (45.79%)  - Avg. Position: 3006567.38  - Sum of Positions: 6883343588569
Attacker B: Results: 2289436 (45.79%)  - Avg. Position: 2936538.23  - Sum of Positions: 6723016338016  (TLD True Hits/Total Hits: 240993/906539)


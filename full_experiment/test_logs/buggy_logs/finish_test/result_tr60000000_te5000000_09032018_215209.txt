Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42450973 (excluded)
net , 3639921 (excluded)
uk , 1435323 
fr , 1115523 
edu , 1055442 (excluded)
br , 1043377 
it , 838907 
org , 771361 (excluded)
in , 764766 
nl , 628375 
ca , 616103 
au , 507168 
de , 388306 
es , 364081 
za , 312978 
ru , 280330 
ar , 218502 
mx , 196093 
us , 194673 
dk , 178843 
be , 169385 
gov , 159770 (excluded)
se , 145961 
pl , 133793 
nz , 123161 
ch , 108487 
pt , 100748 
cn , 97955 
no , 94904 
jp , 91218 
mil , 89344 (excluded)
cl , 89197 
id , 84468 
sg , 67111 
ie , 65275 
gr , 62607 
fi , 60066 
il , 58324 
cz , 57405 
co , 49516 
tr , 48925 
eu , 43691 (excluded)
my , 43468 
hk , 40441 
at , 39733 
bg , 36440 
ph , 35426 
tw , 34806 
biz , 34640 (excluded)
hu , 33986 
ro , 33185 
ae , 31699 
pe , 24071 
hr , 22853 
kr , 22142 
info , 21256 (excluded)
vn , 21194 
ua , 18161 
lv , 18032 
pk , 17773 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 19430.0  - Sum of Positions: 19430
Attacker B: Results: 1 (100.0%)  - Avg. Position: 19430.0  - Sum of Positions: 19430  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 5115426.2  - Sum of Positions: 25577131
Attacker B: Results: 5 (50.0%)  - Avg. Position: 5110791.4  - Sum of Positions: 25553957  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 2006479.73  - Sum of Positions: 102330466
Attacker B: Results: 51 (51.0%)  - Avg. Position: 1967915.06  - Sum of Positions: 100363668  (TLD True Hits/Total Hits: 6/11)

After 1000 entries:
Attacker A: Results: 528 (52.8%)  - Avg. Position: 3157579.67  - Sum of Positions: 1667202064
Attacker B: Results: 528 (52.8%)  - Avg. Position: 3124666.33  - Sum of Positions: 1649823821  (TLD True Hits/Total Hits: 45/138)

After 10000 entries:
Attacker A: Results: 4904 (49.04%)  - Avg. Position: 3124442.54  - Sum of Positions: 15322266207
Attacker B: Results: 4904 (49.04%)  - Avg. Position: 3034661.78  - Sum of Positions: 14881981366  (TLD True Hits/Total Hits: 544/1869)

After 100000 entries:
Attacker A: Results: 47670 (47.67%)  - Avg. Position: 3004084.75  - Sum of Positions: 143204720265
Attacker B: Results: 47670 (47.67%)  - Avg. Position: 2910620.24  - Sum of Positions: 138749266760  (TLD True Hits/Total Hits: 5228/17376)

After 1000000 entries:
Attacker A: Results: 484659 (48.47%)  - Avg. Position: 2977299.84  - Sum of Positions: 1442975163733
Attacker B: Results: 484659 (48.47%)  - Avg. Position: 2909674.49  - Sum of Positions: 1410199927894  (TLD True Hits/Total Hits: 44739/163269)

After 5000000 entries:
Attacker A: Results: 2288884 (45.78%)  - Avg. Position: 3008286.25  - Sum of Positions: 6885618275704
Attacker B: Results: 2288884 (45.78%)  - Avg. Position: 2938688.5  - Sum of Positions: 6726317088334  (TLD True Hits/Total Hits: 241291/906954)


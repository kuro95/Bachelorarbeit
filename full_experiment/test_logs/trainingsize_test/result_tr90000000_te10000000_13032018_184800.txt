Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63757067 (excluded)
net , 5439096 (excluded)
uk , 2108867 
fr , 1657185 
edu , 1587567 (excluded)
br , 1546370 
it , 1232793 
in , 1156370 
org , 1139971 (excluded)
nl , 940895 
ca , 912793 
au , 750318 
de , 586632 
es , 537750 
za , 473216 
ru , 439413 
ar , 319897 
mx , 288851 
us , 288063 
dk , 263126 
be , 256551 
gov , 237912 (excluded)
se , 215131 
pl , 202644 
nz , 182980 
cn , 182256 
ch , 162453 
jp , 150350 
pt , 145638 
no , 144692 
mil , 131938 (excluded)
id , 131760 
cl , 130972 
sg , 105311 
gr , 95559 
ie , 95049 
il , 92127 
cz , 90423 
fi , 90131 
tr , 75101 
co , 73079 
my , 67505 
eu , 65015 (excluded)
hk , 63444 
at , 59359 
bg , 58089 
tw , 55571 
ph , 52488 
hu , 52454 
biz , 51312 (excluded)
ro , 48602 
ae , 47021 
hr , 36664 
pe , 35753 
vn , 34913 
kr , 34711 
info , 31989 (excluded)
ua , 29083 
lv , 27551 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 2 (20.0%)  - Avg. Position: 2910154.5  - Sum of Positions: 5820309
Attacker B: Results: 2 (20.0%)  - Avg. Position: 2910154.5  - Sum of Positions: 5820309  (TLD True Hits/Total Hits: 0/3)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 5307201.1  - Sum of Positions: 265360055
Attacker B: Results: 50 (50.0%)  - Avg. Position: 5274192.54  - Sum of Positions: 263709627  (TLD True Hits/Total Hits: 4/19)

After 1000 entries:
Attacker A: Results: 526 (52.6%)  - Avg. Position: 4778961.78  - Sum of Positions: 2513733894
Attacker B: Results: 526 (52.6%)  - Avg. Position: 4689072.07  - Sum of Positions: 2466451911  (TLD True Hits/Total Hits: 62/198)

After 10000 entries:
Attacker A: Results: 5164 (51.64%)  - Avg. Position: 4665482.6  - Sum of Positions: 24092552123
Attacker B: Results: 5164 (51.64%)  - Avg. Position: 4554521.76  - Sum of Positions: 23519550371  (TLD True Hits/Total Hits: 602/1889)

After 100000 entries:
Attacker A: Results: 51657 (51.66%)  - Avg. Position: 4900516.8  - Sum of Positions: 253145996250
Attacker B: Results: 51657 (51.66%)  - Avg. Position: 4718033.59  - Sum of Positions: 243719461282  (TLD True Hits/Total Hits: 6084/18620)

After 1000000 entries:
Attacker A: Results: 518511 (51.85%)  - Avg. Position: 4879628.29  - Sum of Positions: 2530140942079
Attacker B: Results: 518511 (51.85%)  - Avg. Position: 4685450.14  - Sum of Positions: 2429457439468  (TLD True Hits/Total Hits: 61026/187144)

After 5000000 entries:
Attacker A: Results: 2592929 (51.86%)  - Avg. Position: 4892146.33  - Sum of Positions: 12684988089294
Attacker B: Results: 2592929 (51.86%)  - Avg. Position: 4693057.96  - Sum of Positions: 12168766089150  (TLD True Hits/Total Hits: 305589/935717)

After 10000000 entries:
Attacker A: Results: 5186980 (51.87%)  - Avg. Position: 4887715.04  - Sum of Positions: 25352480137496
Attacker B: Results: 5186980 (51.87%)  - Avg. Position: 4688912.77  - Sum of Positions: 24321296774197  (TLD True Hits/Total Hits: 610914/1871070)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 100000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 70842294 (excluded)
net , 6042675 (excluded)
uk , 2344745 
fr , 1840220 
edu , 1762942 (excluded)
br , 1716707 
it , 1370417 
in , 1284860 
org , 1267107 (excluded)
nl , 1045458 
ca , 1014043 
au , 833667 
de , 652187 
es , 597402 
za , 525664 
ru , 488386 
ar , 355884 
mx , 321207 
us , 319984 
dk , 292265 
be , 285048 
gov , 264258 (excluded)
se , 238794 
pl , 225295 
nz , 203320 
cn , 202529 
ch , 180059 
jp , 167096 
pt , 161690 
no , 160448 
mil , 146734 (excluded)
id , 146519 
cl , 145521 
sg , 116976 
gr , 106103 
ie , 105874 
il , 102461 
cz , 100229 
fi , 100179 
tr , 83767 
co , 81293 
my , 74873 
eu , 72199 (excluded)
hk , 70522 
at , 65827 
bg , 64344 
tw , 61823 
hu , 58516 
ph , 58278 
biz , 57105 (excluded)
ro , 54217 
ae , 52379 
hr , 40779 
pe , 39620 
vn , 38744 
kr , 38430 
info , 35558 (excluded)
ua , 32495 
lv , 30708 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 13017049.25  - Sum of Positions: 52068197
Attacker B: Results: 4 (40.0%)  - Avg. Position: 13017049.25  - Sum of Positions: 52068197  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 58 (58.0%)  - Avg. Position: 4481506.48  - Sum of Positions: 259927376
Attacker B: Results: 58 (58.0%)  - Avg. Position: 3532264.07  - Sum of Positions: 204871316  (TLD True Hits/Total Hits: 11/20)

After 1000 entries:
Attacker A: Results: 518 (51.8%)  - Avg. Position: 5566577.76  - Sum of Positions: 2883487280
Attacker B: Results: 518 (51.8%)  - Avg. Position: 5109697.01  - Sum of Positions: 2646823052  (TLD True Hits/Total Hits: 75/197)

After 10000 entries:
Attacker A: Results: 5253 (52.53%)  - Avg. Position: 5421487.22  - Sum of Positions: 28479072386
Attacker B: Results: 5253 (52.53%)  - Avg. Position: 5177005.84  - Sum of Positions: 27194811675  (TLD True Hits/Total Hits: 648/1886)

After 100000 entries:
Attacker A: Results: 52742 (52.74%)  - Avg. Position: 5400771.28  - Sum of Positions: 284847478652
Attacker B: Results: 52742 (52.74%)  - Avg. Position: 5168850.84  - Sum of Positions: 272615531053  (TLD True Hits/Total Hits: 6350/18724)

After 1000000 entries:
Attacker A: Results: 526647 (52.66%)  - Avg. Position: 5372707.13  - Sum of Positions: 2829520093812
Attacker B: Results: 526647 (52.66%)  - Avg. Position: 5143384.83  - Sum of Positions: 2708748190332  (TLD True Hits/Total Hits: 62731/186758)

After 5000000 entries:
Attacker A: Results: 2635525 (52.71%)  - Avg. Position: 5389761.79  - Sum of Positions: 14204851938447
Attacker B: Results: 2635525 (52.71%)  - Avg. Position: 5161605.18  - Sum of Positions: 13603539500081  (TLD True Hits/Total Hits: 313138/934849)

After 10000000 entries:
Attacker A: Results: 5271380 (52.71%)  - Avg. Position: 5394115.62  - Sum of Positions: 28434433178167
Attacker B: Results: 5271380 (52.71%)  - Avg. Position: 5165802.73  - Sum of Positions: 27230909213430  (TLD True Hits/Total Hits: 626039/1869347)


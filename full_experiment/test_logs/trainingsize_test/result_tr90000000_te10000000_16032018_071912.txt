Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63760935 (excluded)
net , 5437666 (excluded)
uk , 2111485 
fr , 1657069 
edu , 1586438 (excluded)
br , 1545453 
it , 1233222 
in , 1155961 
org , 1139873 (excluded)
nl , 940864 
ca , 911753 
au , 750341 
de , 586530 
es , 537534 
za , 473320 
ru , 439285 
ar , 319285 
mx , 288920 
us , 287297 
dk , 263190 
be , 256828 
gov , 237422 (excluded)
se , 215150 
pl , 202759 
nz , 183123 
cn , 182299 
ch , 161315 
jp , 150398 
pt , 145471 
no , 144478 
mil , 132069 (excluded)
id , 132014 
cl , 130990 
sg , 105139 
gr , 95405 
ie , 95294 
il , 92340 
cz , 90157 
fi , 90074 
tr , 75438 
co , 72913 
my , 67464 
eu , 64970 (excluded)
hk , 63524 
at , 59469 
bg , 57748 
tw , 55688 
hu , 52710 
ph , 52404 
biz , 51333 (excluded)
ro , 48768 
ae , 47015 
hr , 36643 
pe , 35744 
vn , 34731 
kr , 34506 
info , 32027 (excluded)
ua , 29128 
lv , 27488 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1174505.0  - Sum of Positions: 1174505
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1174505.0  - Sum of Positions: 1174505  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 2336231.0  - Sum of Positions: 16353617
Attacker B: Results: 7 (70.0%)  - Avg. Position: 2336241.43  - Sum of Positions: 16353690  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 43 (43.0%)  - Avg. Position: 2161209.3  - Sum of Positions: 92932000
Attacker B: Results: 43 (43.0%)  - Avg. Position: 2224737.28  - Sum of Positions: 95663703  (TLD True Hits/Total Hits: 3/17)

After 1000 entries:
Attacker A: Results: 511 (51.1%)  - Avg. Position: 4524193.84  - Sum of Positions: 2311863051
Attacker B: Results: 511 (51.1%)  - Avg. Position: 4363014.73  - Sum of Positions: 2229500527  (TLD True Hits/Total Hits: 55/178)

After 10000 entries:
Attacker A: Results: 5198 (51.98%)  - Avg. Position: 4725574.88  - Sum of Positions: 24563538242
Attacker B: Results: 5198 (51.98%)  - Avg. Position: 4568101.55  - Sum of Positions: 23744991878  (TLD True Hits/Total Hits: 572/1851)

After 100000 entries:
Attacker A: Results: 51793 (51.79%)  - Avg. Position: 4870761.85  - Sum of Positions: 252271368691
Attacker B: Results: 51793 (51.79%)  - Avg. Position: 4673843.94  - Sum of Positions: 242072399173  (TLD True Hits/Total Hits: 6002/18659)

After 1000000 entries:
Attacker A: Results: 518580 (51.86%)  - Avg. Position: 4866595.12  - Sum of Positions: 2523718899352
Attacker B: Results: 518580 (51.86%)  - Avg. Position: 4671979.49  - Sum of Positions: 2422795123019  (TLD True Hits/Total Hits: 60815/186932)

After 5000000 entries:
Attacker A: Results: 2593553 (51.87%)  - Avg. Position: 4893527.28  - Sum of Positions: 12691622367413
Attacker B: Results: 2593553 (51.87%)  - Avg. Position: 4694756.35  - Sum of Positions: 12176099411374  (TLD True Hits/Total Hits: 305308/936400)

After 10000000 entries:
Attacker A: Results: 5186626 (51.87%)  - Avg. Position: 4887532.62  - Sum of Positions: 25349803773846
Attacker B: Results: 5186626 (51.87%)  - Avg. Position: 4689842.24  - Sum of Positions: 24324457722862  (TLD True Hits/Total Hits: 609760/1870516)


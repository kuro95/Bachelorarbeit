Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 42507155 (excluded)
net , 3624833 (excluded)
uk , 1408327 
fr , 1103460 
edu , 1057258 (excluded)
br , 1030067 
it , 822287 
in , 771037 
org , 759804 (excluded)
nl , 626977 
ca , 609188 
au , 500713 
de , 391338 
es , 358801 
za , 314876 
ru , 292877 
ar , 213003 
mx , 192301 
us , 191472 
dk , 175454 
be , 170334 
gov , 158724 (excluded)
se , 143379 
pl , 134830 
nz , 122048 
cn , 121468 
ch , 108097 
jp , 100298 
pt , 97145 
no , 96611 
id , 87936 
mil , 87897 (excluded)
cl , 87262 
sg , 70131 
gr , 63678 
ie , 63528 
il , 61475 
cz , 59991 
fi , 59959 
tr , 50261 
co , 48542 
my , 44998 
eu , 43550 (excluded)
hk , 42412 
at , 39673 
bg , 38391 
tw , 37029 
hu , 35080 
ph , 34791 
biz , 34409 (excluded)
ro , 32613 
ae , 31318 
hr , 24490 
pe , 23915 
vn , 23406 
kr , 23233 
info , 21312 (excluded)
ua , 19426 
lv , 18345 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 3229490.0  - Sum of Positions: 3229490
Attacker B: Results: 1 (100.0%)  - Avg. Position: 3229490.0  - Sum of Positions: 3229490  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 869476.75  - Sum of Positions: 3477907
Attacker B: Results: 4 (40.0%)  - Avg. Position: 855243.25  - Sum of Positions: 3420973  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 2301514.47  - Sum of Positions: 103568151
Attacker B: Results: 45 (45.0%)  - Avg. Position: 2318116.24  - Sum of Positions: 104315231  (TLD True Hits/Total Hits: 5/19)

After 1000 entries:
Attacker A: Results: 481 (48.1%)  - Avg. Position: 3616196.93  - Sum of Positions: 1739390722
Attacker B: Results: 481 (48.1%)  - Avg. Position: 3480087.34  - Sum of Positions: 1673922012  (TLD True Hits/Total Hits: 57/192)

After 10000 entries:
Attacker A: Results: 4940 (49.4%)  - Avg. Position: 3294613.15  - Sum of Positions: 16275388937
Attacker B: Results: 4940 (49.4%)  - Avg. Position: 3183651.85  - Sum of Positions: 15727240155  (TLD True Hits/Total Hits: 581/1904)

After 100000 entries:
Attacker A: Results: 48833 (48.83%)  - Avg. Position: 3364755.49  - Sum of Positions: 164311104702
Attacker B: Results: 48833 (48.83%)  - Avg. Position: 3239406.8  - Sum of Positions: 158189952502  (TLD True Hits/Total Hits: 5542/18806)

After 1000000 entries:
Attacker A: Results: 487314 (48.73%)  - Avg. Position: 3344926.72  - Sum of Positions: 1630029621799
Attacker B: Results: 487314 (48.73%)  - Avg. Position: 3226145.82  - Sum of Positions: 1572146023108  (TLD True Hits/Total Hits: 55371/187475)

After 5000000 entries:
Attacker A: Results: 2436592 (48.73%)  - Avg. Position: 3332536.33  - Sum of Positions: 8120031367418
Attacker B: Results: 2436592 (48.73%)  - Avg. Position: 3216916.72  - Sum of Positions: 7838313549729  (TLD True Hits/Total Hits: 277110/935210)

After 10000000 entries:
Attacker A: Results: 4872557 (48.73%)  - Avg. Position: 3331346.18  - Sum of Positions: 16232174139514
Attacker B: Results: 4872557 (48.73%)  - Avg. Position: 3216258.13  - Sum of Positions: 15671401071695  (TLD True Hits/Total Hits: 552923/1868756)


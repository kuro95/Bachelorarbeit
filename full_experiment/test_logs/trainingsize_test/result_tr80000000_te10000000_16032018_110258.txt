Attacker A: Default List - Attacker B: TLD-Lists

Results with 80000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 56674703 (excluded)
net , 4833810 (excluded)
uk , 1876254 
fr , 1472606 
edu , 1410169 (excluded)
br , 1372994 
it , 1096514 
in , 1027607 
org , 1013425 (excluded)
nl , 836527 
ca , 809817 
au , 667629 
de , 521335 
es , 478262 
za , 420456 
ru , 391141 
ar , 284033 
mx , 256291 
us , 255604 
dk , 233832 
be , 228310 
gov , 211444 (excluded)
se , 191445 
pl , 180296 
cn , 162404 
nz , 162214 
ch , 144199 
jp , 133445 
pt , 129450 
no , 128545 
mil , 117530 (excluded)
id , 117466 
cl , 116520 
sg , 93762 
gr , 85070 
ie , 84561 
il , 81939 
fi , 80349 
cz , 80083 
tr , 67094 
co , 64964 
my , 59810 
eu , 57876 (excluded)
hk , 56610 
at , 53078 
bg , 51334 
tw , 49404 
hu , 46785 
ph , 46539 
biz , 45697 (excluded)
ro , 43143 
ae , 41658 
hr , 32586 
pe , 31745 
vn , 30977 
kr , 30898 
info , 28168 (excluded)
ua , 25954 
lv , 24609 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1123.0  - Sum of Positions: 1123
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1123.0  - Sum of Positions: 1123  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 755603.5  - Sum of Positions: 3022414
Attacker B: Results: 4 (40.0%)  - Avg. Position: 755603.5  - Sum of Positions: 3022414  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 2124848.76  - Sum of Positions: 95618194
Attacker B: Results: 45 (45.0%)  - Avg. Position: 1953427.56  - Sum of Positions: 87904240  (TLD True Hits/Total Hits: 5/19)

After 1000 entries:
Attacker A: Results: 523 (52.3%)  - Avg. Position: 3624277.5  - Sum of Positions: 1895497135
Attacker B: Results: 523 (52.3%)  - Avg. Position: 3521595.66  - Sum of Positions: 1841794531  (TLD True Hits/Total Hits: 55/172)

After 10000 entries:
Attacker A: Results: 5027 (50.27%)  - Avg. Position: 4414311.5  - Sum of Positions: 22190743902
Attacker B: Results: 5027 (50.27%)  - Avg. Position: 4251852.63  - Sum of Positions: 21374063189  (TLD True Hits/Total Hits: 548/1805)

After 100000 entries:
Attacker A: Results: 50623 (50.62%)  - Avg. Position: 4305030.93  - Sum of Positions: 217933580912
Attacker B: Results: 50623 (50.62%)  - Avg. Position: 4147758.55  - Sum of Positions: 209971980973  (TLD True Hits/Total Hits: 5855/18695)

After 1000000 entries:
Attacker A: Results: 508818 (50.88%)  - Avg. Position: 4354484.68  - Sum of Positions: 2215640185900
Attacker B: Results: 508818 (50.88%)  - Avg. Position: 4184148.09  - Sum of Positions: 2128969864997  (TLD True Hits/Total Hits: 59200/187302)

After 5000000 entries:
Attacker A: Results: 2546208 (50.92%)  - Avg. Position: 4369409.28  - Sum of Positions: 11125424863494
Attacker B: Results: 2546208 (50.92%)  - Avg. Position: 4201172.42  - Sum of Positions: 10697058825857  (TLD True Hits/Total Hits: 295597/934364)

After 10000000 entries:
Attacker A: Results: 5093384 (50.93%)  - Avg. Position: 4372530.07  - Sum of Positions: 22270974679114
Attacker B: Results: 5093384 (50.93%)  - Avg. Position: 4204868.53  - Sum of Positions: 21417010105063  (TLD True Hits/Total Hits: 591696/1869033)


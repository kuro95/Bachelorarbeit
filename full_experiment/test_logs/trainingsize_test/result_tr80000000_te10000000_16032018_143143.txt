Attacker A: Default List - Attacker B: TLD-Lists

Results with 80000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 56674985 (excluded)
net , 4833059 (excluded)
uk , 1874632 
fr , 1472403 
edu , 1411200 (excluded)
br , 1372950 
it , 1096262 
in , 1028718 
org , 1013490 (excluded)
nl , 836625 
ca , 811057 
au , 666894 
de , 521611 
es , 477170 
za , 420925 
ru , 390074 
ar , 283817 
mx , 256967 
us , 255857 
dk , 233868 
be , 228107 
gov , 211445 (excluded)
se , 191346 
pl , 180063 
nz , 162575 
cn , 162143 
ch , 144080 
jp , 134009 
pt , 129197 
no , 128678 
mil , 117505 (excluded)
id , 117139 
cl , 116347 
sg , 93622 
gr , 84997 
ie , 84733 
il , 82032 
cz , 80452 
fi , 79915 
tr , 66938 
co , 64971 
my , 59959 
eu , 57718 (excluded)
hk , 56779 
at , 52627 
bg , 51478 
tw , 49597 
ph , 46815 
hu , 46540 
biz , 45598 (excluded)
ro , 43139 
ae , 41868 
hr , 32640 
pe , 31846 
vn , 31015 
kr , 30793 
info , 28473 (excluded)
ua , 25871 
lv , 24522 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 275.0  - Sum of Positions: 275
Attacker B: Results: 1 (100.0%)  - Avg. Position: 275.0  - Sum of Positions: 275  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 5346690.33  - Sum of Positions: 32080142
Attacker B: Results: 6 (60.0%)  - Avg. Position: 5363902.5  - Sum of Positions: 32183415  (TLD True Hits/Total Hits: 2/4)

After 100 entries:
Attacker A: Results: 55 (55.0%)  - Avg. Position: 2445124.2  - Sum of Positions: 134481831
Attacker B: Results: 55 (55.0%)  - Avg. Position: 2392427.31  - Sum of Positions: 131583502  (TLD True Hits/Total Hits: 7/22)

After 1000 entries:
Attacker A: Results: 512 (51.2%)  - Avg. Position: 3917667.4  - Sum of Positions: 2005845708
Attacker B: Results: 512 (51.2%)  - Avg. Position: 3745771.6  - Sum of Positions: 1917835060  (TLD True Hits/Total Hits: 60/200)

After 10000 entries:
Attacker A: Results: 5109 (51.09%)  - Avg. Position: 4281520.81  - Sum of Positions: 21874289812
Attacker B: Results: 5109 (51.09%)  - Avg. Position: 4052672.53  - Sum of Positions: 20705103942  (TLD True Hits/Total Hits: 639/1925)

After 100000 entries:
Attacker A: Results: 51045 (51.04%)  - Avg. Position: 4371235.47  - Sum of Positions: 223129714531
Attacker B: Results: 51045 (51.04%)  - Avg. Position: 4212270.56  - Sum of Positions: 215015350925  (TLD True Hits/Total Hits: 5998/18732)

After 1000000 entries:
Attacker A: Results: 509763 (50.98%)  - Avg. Position: 4342668.35  - Sum of Positions: 2213731647113
Attacker B: Results: 509763 (50.98%)  - Avg. Position: 4176959.31  - Sum of Positions: 2129259310444  (TLD True Hits/Total Hits: 59530/187349)

After 5000000 entries:
Attacker A: Results: 2547782 (50.96%)  - Avg. Position: 4364813.85  - Sum of Positions: 11120594153536
Attacker B: Results: 2547782 (50.96%)  - Avg. Position: 4194323.13  - Sum of Positions: 10686220972518  (TLD True Hits/Total Hits: 297291/936532)

After 10000000 entries:
Attacker A: Results: 5092141 (50.92%)  - Avg. Position: 4368764.02  - Sum of Positions: 22246362390345
Attacker B: Results: 5092141 (50.92%)  - Avg. Position: 4199176.33  - Sum of Positions: 21382797961959  (TLD True Hits/Total Hits: 593937/1872476)


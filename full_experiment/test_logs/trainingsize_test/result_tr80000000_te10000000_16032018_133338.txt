Attacker A: Default List - Attacker B: TLD-Lists

Results with 80000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 56672630 (excluded)
net , 4834264 (excluded)
uk , 1875235 
fr , 1472064 
edu , 1411111 (excluded)
br , 1373705 
it , 1096957 
in , 1028156 
org , 1013895 (excluded)
nl , 835379 
ca , 810993 
au , 666779 
de , 521448 
es , 477853 
za , 420098 
ru , 390686 
ar , 284302 
mx , 256780 
us , 255554 
dk , 234525 
be , 228207 
gov , 211213 (excluded)
se , 191222 
pl , 180531 
nz , 162605 
cn , 162063 
ch , 144250 
jp , 133750 
pt , 129169 
no , 128586 
mil , 117334 (excluded)
id , 116984 
cl , 116513 
sg , 93812 
gr , 84854 
ie , 84358 
il , 82066 
cz , 80487 
fi , 80208 
tr , 67143 
co , 65178 
my , 59932 
eu , 57760 (excluded)
hk , 56530 
at , 52910 
bg , 51523 
tw , 49458 
hu , 46872 
ph , 46688 
biz , 45637 (excluded)
ro , 43272 
ae , 41848 
hr , 32519 
pe , 31845 
vn , 30970 
kr , 30813 
info , 28328 (excluded)
ua , 25922 
lv , 24454 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 6812.0  - Sum of Positions: 6812
Attacker B: Results: 1 (100.0%)  - Avg. Position: 6812.0  - Sum of Positions: 6812  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 14795983.6  - Sum of Positions: 73979918
Attacker B: Results: 5 (50.0%)  - Avg. Position: 14795983.6  - Sum of Positions: 73979918  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 7175447.66  - Sum of Positions: 401825069
Attacker B: Results: 56 (56.0%)  - Avg. Position: 6650006.95  - Sum of Positions: 372400389  (TLD True Hits/Total Hits: 5/14)

After 1000 entries:
Attacker A: Results: 517 (51.7%)  - Avg. Position: 4626705.36  - Sum of Positions: 2392006669
Attacker B: Results: 517 (51.7%)  - Avg. Position: 4533210.47  - Sum of Positions: 2343669815  (TLD True Hits/Total Hits: 47/156)

After 10000 entries:
Attacker A: Results: 5145 (51.45%)  - Avg. Position: 4346868.38  - Sum of Positions: 22364637825
Attacker B: Results: 5145 (51.45%)  - Avg. Position: 4211177.19  - Sum of Positions: 21666506660  (TLD True Hits/Total Hits: 581/1868)

After 100000 entries:
Attacker A: Results: 50960 (50.96%)  - Avg. Position: 4342176.38  - Sum of Positions: 221277308488
Attacker B: Results: 50960 (50.96%)  - Avg. Position: 4185956.24  - Sum of Positions: 213316329976  (TLD True Hits/Total Hits: 5969/18806)

After 1000000 entries:
Attacker A: Results: 509708 (50.97%)  - Avg. Position: 4372498.09  - Sum of Positions: 2228697255921
Attacker B: Results: 509708 (50.97%)  - Avg. Position: 4208659.89  - Sum of Positions: 2145187615591  (TLD True Hits/Total Hits: 59119/187133)

After 5000000 entries:
Attacker A: Results: 2545693 (50.91%)  - Avg. Position: 4363116.84  - Sum of Positions: 11107155992424
Attacker B: Results: 2545693 (50.91%)  - Avg. Position: 4196514.44  - Sum of Positions: 10683037434620  (TLD True Hits/Total Hits: 296249/933779)

After 10000000 entries:
Attacker A: Results: 5091197 (50.91%)  - Avg. Position: 4361806.71  - Sum of Positions: 22206817234006
Attacker B: Results: 5091197 (50.91%)  - Avg. Position: 4195419.76  - Sum of Positions: 21359708514175  (TLD True Hits/Total Hits: 593496/1870575)


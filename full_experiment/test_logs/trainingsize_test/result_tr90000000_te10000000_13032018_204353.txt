Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63760588 (excluded)
net , 5436137 (excluded)
uk , 2111136 
fr , 1657358 
edu , 1586088 (excluded)
br , 1545693 
it , 1233139 
in , 1157148 
org , 1139525 (excluded)
nl , 940862 
ca , 911838 
au , 750441 
de , 586781 
es , 537105 
za , 473640 
ru , 439059 
ar , 320155 
mx , 288354 
us , 288030 
dk , 262736 
be , 256548 
gov , 237582 (excluded)
se , 215021 
pl , 203003 
nz , 183300 
cn , 182235 
ch , 162263 
jp , 150384 
pt , 145864 
no , 144341 
mil , 132007 (excluded)
id , 131674 
cl , 130881 
sg , 105422 
gr , 95642 
ie , 95220 
il , 92332 
fi , 90214 
cz , 90087 
tr , 75266 
co , 73111 
my , 67431 
eu , 65071 (excluded)
hk , 63650 
at , 59193 
bg , 57925 
tw , 55654 
hu , 52725 
ph , 52383 
biz , 51318 (excluded)
ro , 48624 
ae , 47038 
hr , 36573 
pe , 35773 
vn , 35061 
kr , 34672 
info , 31911 (excluded)
ua , 29231 
lv , 27641 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 184979.0  - Sum of Positions: 184979
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1239525.0  - Sum of Positions: 1239525  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 241334.86  - Sum of Positions: 1689344
Attacker B: Results: 7 (70.0%)  - Avg. Position: 435221.0  - Sum of Positions: 3046547  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 1716050.85  - Sum of Positions: 82370441
Attacker B: Results: 48 (48.0%)  - Avg. Position: 1731231.17  - Sum of Positions: 83099096  (TLD True Hits/Total Hits: 7/19)

After 1000 entries:
Attacker A: Results: 513 (51.3%)  - Avg. Position: 4477068.42  - Sum of Positions: 2296736099
Attacker B: Results: 513 (51.3%)  - Avg. Position: 4243785.43  - Sum of Positions: 2177061928  (TLD True Hits/Total Hits: 79/201)

After 10000 entries:
Attacker A: Results: 5223 (52.23%)  - Avg. Position: 5000312.32  - Sum of Positions: 26116631255
Attacker B: Results: 5223 (52.23%)  - Avg. Position: 4749590.43  - Sum of Positions: 24807110795  (TLD True Hits/Total Hits: 663/1927)

After 100000 entries:
Attacker A: Results: 51597 (51.6%)  - Avg. Position: 4834913.72  - Sum of Positions: 249467043323
Attacker B: Results: 51597 (51.6%)  - Avg. Position: 4630849.8  - Sum of Positions: 238937957181  (TLD True Hits/Total Hits: 6155/18814)

After 1000000 entries:
Attacker A: Results: 518503 (51.85%)  - Avg. Position: 4891475.91  - Sum of Positions: 2536244933024
Attacker B: Results: 518503 (51.85%)  - Avg. Position: 4694522.2  - Sum of Positions: 2434123843742  (TLD True Hits/Total Hits: 61083/186839)

After 5000000 entries:
Attacker A: Results: 2591869 (51.84%)  - Avg. Position: 4883310.31  - Sum of Positions: 12656900615140
Attacker B: Results: 2591869 (51.84%)  - Avg. Position: 4688827.02  - Sum of Positions: 12152825396783  (TLD True Hits/Total Hits: 303617/932815)

After 10000000 entries:
Attacker A: Results: 5184410 (51.84%)  - Avg. Position: 4879191.81  - Sum of Positions: 25295730786837
Attacker B: Results: 5184410 (51.84%)  - Avg. Position: 4685429.85  - Sum of Positions: 24291189361057  (TLD True Hits/Total Hits: 608183/1866972)


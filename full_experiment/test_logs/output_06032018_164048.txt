Attacker A: Default List - Attacker B: TLD-Lists

Results with 50000 random entries:

uk , 14071
fr , 11190
br , 10410
it , 8359
in , 7692
ca , 6265
nl , 6105
au , 5155
de , 3811
es , 3688
za , 3228
ru , 2805
ar , 2160
mx , 1982
us , 1944
dk , 1745
be , 1688
gov , 1559
se , 1522
pl , 1354
nz , 1215
ch , 1091
pt , 1000
cn , 972
jp , 946
cl , 939
no , 895
id , 854
mil , 785
sg , 699
gr , 674
ie , 625
fi , 618
il , 585
cz , 578
co , 522
tr , 466
eu , 463
my , 461
at , 404
hk , 395
biz , 358
bg , 356
hu , 353
ro , 334
ph , 329
tw , 326
ae , 325
kr , 232
pe , 229
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 29924.0  - Sum of Positions: 89772
Attacker B: Results: 3 (30.0%)  - Avg. Position: 29924.0  - Sum of Positions: 89772  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 15 (15.0%)  - Avg. Position: 52914.87  - Sum of Positions: 793723
Attacker B: Results: 15 (15.0%)  - Avg. Position: 52889.93  - Sum of Positions: 793349  (TLD True Hits/Total Hits: 1/13)

After 1000 entries:
Attacker A: Results: 184 (18.4%)  - Avg. Position: 71422.88  - Sum of Positions: 13141809
Attacker B: Results: 184 (18.4%)  - Avg. Position: 68736.16  - Sum of Positions: 12647454  (TLD True Hits/Total Hits: 19/179)

After 10000 entries:
Attacker A: Results: 2039 (20.39%)  - Avg. Position: 73593.09  - Sum of Positions: 150056314
Attacker B: Results: 2039 (20.39%)  - Avg. Position: 73242.21  - Sum of Positions: 149340871  (TLD True Hits/Total Hits: 108/1669)

After 50000 entries:
Attacker A: Results: 9531 (19.06%)  - Avg. Position: 72929.04  - Sum of Positions: 695086722
Attacker B: Results: 9531 (19.06%)  - Avg. Position: 72463.92  - Sum of Positions: 690653585  (TLD True Hits/Total Hits: 529/9074)


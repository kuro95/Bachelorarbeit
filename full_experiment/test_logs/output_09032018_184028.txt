Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 5000000 Random Entries:

com , 42231002 (excluded)
net , 3616994 (excluded)
uk , 1427846 
fr , 1108052 
edu , 1047088 (excluded)
br , 1038811 
it , 835897 
org , 764669 (excluded)
in , 761081 
nl , 627162 
ca , 612882 
au , 506746 
de , 388272 
es , 362115 
za , 311954 
ru , 278551 
ar , 217807 
COM , 201675 (excluded)
mx , 195713 
us , 192956 
dk , 177846 
be , 167893 
gov , 156824 (excluded)
se , 146571 
pl , 133206 
nz , 123548 
ch , 108337 
pt , 100150 
cn , 98136 
no , 94620 
jp , 91342 
cl , 89173 
mil , 87669 (excluded)
id , 84123 
sg , 66984 
ie , 65261 
gr , 61812 
fi , 59690 
il , 58050 
cz , 57229 
tr , 48912 
co , 48759 
eu , 43698 
my , 43475 
hk , 40365 
at , 39968 
bg , 36518 
ph , 35497 
tw , 34832 
biz , 34641 (excluded)
hu , 33830 
ro , 33053 
ae , 31397 
pe , 23874 
hr , 23065 
NET , 22501 (excluded)
kr , 22124 
info , 21213 (excluded)
vn , 21149 
ua , 17981 
lv , 17807 
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 13224.0  - Sum of Positions: 13224
Attacker B: Results: 1 (100.0%)  - Avg. Position: 13224.0  - Sum of Positions: 13224  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 7090310.33  - Sum of Positions: 42541862
Attacker B: Results: 6 (60.0%)  - Avg. Position: 7090310.33  - Sum of Positions: 42541862  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 4586886.23  - Sum of Positions: 220170539
Attacker B: Results: 48 (48.0%)  - Avg. Position: 4600282.27  - Sum of Positions: 220813549  (TLD True Hits/Total Hits: 2/10)

After 1000 entries:
Attacker A: Results: 549 (54.9%)  - Avg. Position: 3140416.39  - Sum of Positions: 1724088598
Attacker B: Results: 549 (54.9%)  - Avg. Position: 3153088.81  - Sum of Positions: 1731045756  (TLD True Hits/Total Hits: 42/126)

After 10000 entries:
Attacker A: Results: 5049 (50.49%)  - Avg. Position: 2963825.49  - Sum of Positions: 14964354905
Attacker B: Results: 5049 (50.49%)  - Avg. Position: 2944882.75  - Sum of Positions: 14868712982  (TLD True Hits/Total Hits: 391/1365)

After 100000 entries:
Attacker A: Results: 47552 (47.55%)  - Avg. Position: 2999362.6  - Sum of Positions: 142625690224
Attacker B: Results: 47552 (47.55%)  - Avg. Position: 2918523.08  - Sum of Positions: 138781609264  (TLD True Hits/Total Hits: 5092/17416)

After 1000000 entries:
Attacker A: Results: 483919 (48.39%)  - Avg. Position: 2979452.28  - Sum of Positions: 1441813570181
Attacker B: Results: 483919 (48.39%)  - Avg. Position: 2917456.08  - Sum of Positions: 1411812427605  (TLD True Hits/Total Hits: 44181/162871)

After 5000000 entries:
Attacker A: Results: 2289205 (45.78%)  - Avg. Position: 3001557.93  - Sum of Positions: 6871181416121
Attacker B: Results: 2289205 (45.78%)  - Avg. Position: 2932953.94  - Sum of Positions: 6714132832107  (TLD True Hits/Total Hits: 239745/902903)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 49592840 (excluded)
net , 4229897 (excluded)
uk , 1640594 
fr , 1288008 
edu , 1234386 (excluded)
br , 1201966 
it , 959045 
in , 899503 
org , 886104 (excluded)
nl , 731206 
ca , 709382 
au , 583576 
de , 455717 
es , 417910 
za , 367758 
ru , 342192 
ar , 248481 
mx , 225215 
us , 224158 
dk , 204312 
be , 199319 
gov , 184823 (excluded)
se , 167383 
pl , 157539 
nz , 142466 
cn , 142098 
ch , 126149 
jp , 117122 
pt , 112905 
no , 112145 
mil , 102869 (excluded)
id , 102859 
cl , 101772 
sg , 81781 
gr , 74292 
ie , 73878 
il , 71726 
fi , 69898 
cz , 69890 
tr , 58527 
co , 56835 
my , 52200 
eu , 50589 (excluded)
hk , 49436 
at , 46298 
bg , 45051 
tw , 43262 
ph , 41102 
hu , 41003 
biz , 40082 (excluded)
ro , 37761 
ae , 36603 
hr , 28581 
pe , 27905 
kr , 26944 
vn , 26943 
info , 25063 (excluded)
ua , 22838 
lv , 21456 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 2317097.29  - Sum of Positions: 16219681
Attacker B: Results: 7 (70.0%)  - Avg. Position: 2332617.86  - Sum of Positions: 16328325  (TLD True Hits/Total Hits: 2/4)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 3609096.78  - Sum of Positions: 166018452
Attacker B: Results: 46 (46.0%)  - Avg. Position: 3462615.57  - Sum of Positions: 159280316  (TLD True Hits/Total Hits: 5/21)

After 1000 entries:
Attacker A: Results: 518 (51.8%)  - Avg. Position: 3635262.05  - Sum of Positions: 1883065742
Attacker B: Results: 518 (51.8%)  - Avg. Position: 3478191.68  - Sum of Positions: 1801703290  (TLD True Hits/Total Hits: 65/183)

After 10000 entries:
Attacker A: Results: 5005 (50.05%)  - Avg. Position: 3785336.38  - Sum of Positions: 18945608593
Attacker B: Results: 5005 (50.05%)  - Avg. Position: 3659170.13  - Sum of Positions: 18314146499  (TLD True Hits/Total Hits: 582/1872)

After 100000 entries:
Attacker A: Results: 49626 (49.63%)  - Avg. Position: 3856953.25  - Sum of Positions: 191405161958
Attacker B: Results: 49626 (49.63%)  - Avg. Position: 3729915.71  - Sum of Positions: 185100797058  (TLD True Hits/Total Hits: 5698/18751)

After 1000000 entries:
Attacker A: Results: 499228 (49.92%)  - Avg. Position: 3862826.84  - Sum of Positions: 1928431316632
Attacker B: Results: 499228 (49.92%)  - Avg. Position: 3719740.23  - Sum of Positions: 1856998476900  (TLD True Hits/Total Hits: 57384/186752)

After 5000000 entries:
Attacker A: Results: 2494451 (49.89%)  - Avg. Position: 3854228.23  - Sum of Positions: 9614183466194
Attacker B: Results: 2494451 (49.89%)  - Avg. Position: 3713254.51  - Sum of Positions: 9262531431698  (TLD True Hits/Total Hits: 286878/934783)

After 10000000 entries:
Attacker A: Results: 4988149 (49.88%)  - Avg. Position: 3853357.24  - Sum of Positions: 19221120085212
Attacker B: Results: 4988149 (49.88%)  - Avg. Position: 3712866.47  - Sum of Positions: 18520331144600  (TLD True Hits/Total Hits: 573513/1870042)


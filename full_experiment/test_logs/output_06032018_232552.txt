Attacker A: Default List - Attacker B: TLD-Lists

Results with 360000 Training Entries and 50000 Random Entries:

za , 12543
ru , 8792
uk , 5185
cn , 4222
nl , 3598
it , 3347
fr , 3071
ca , 2939
br , 2615
es , 2452
de , 2345
COM , 1576
dk , 1493
jp , 1385
au , 1336
in , 1264
ie , 1141
tw , 1121
pk , 1075
pt , 857
se , 843
pl , 824
mx , 784
NET , 682
cz , 662
hk , 524
info , 501
no , 440
cat , 433
pe , 372
sa , 338
be , 318
user , 314
ch , 308
ar , 293
us , 292
nz , 245
il , 226
ZA , 221
vn , 221
gr , 214
sk , 213
kr , 207
id , 199
ua , 190
sg , 177
at , 176
im , 171
ae , 165
lv , 122
After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/7)

After 100 entries:
Attacker A: Results: 16 (16.0%)  - Avg. Position: 56756.69  - Sum of Positions: 908107
Attacker B: Results: 16 (16.0%)  - Avg. Position: 57751.44  - Sum of Positions: 924023  (TLD True Hits/Total Hits: 1/28)

After 1000 entries:
Attacker A: Results: 144 (14.4%)  - Avg. Position: 50203.57  - Sum of Positions: 7229314
Attacker B: Results: 144 (14.4%)  - Avg. Position: 47171.65  - Sum of Positions: 6792718  (TLD True Hits/Total Hits: 20/320)

After 10000 entries:
Attacker A: Results: 1485 (14.85%)  - Avg. Position: 47091.31  - Sum of Positions: 69930599
Attacker B: Results: 1485 (14.85%)  - Avg. Position: 44524.4  - Sum of Positions: 66118731  (TLD True Hits/Total Hits: 158/3224)

After 50000 entries:
Attacker A: Results: 6940 (13.88%)  - Avg. Position: 51139.82  - Sum of Positions: 354910335
Attacker B: Results: 6940 (13.88%)  - Avg. Position: 48095.96  - Sum of Positions: 333785972  (TLD True Hits/Total Hits: 1021/22178)


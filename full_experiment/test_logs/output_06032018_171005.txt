Attacker A: Default List - Attacker B: TLD-Lists

Results with 5000000 random entries:

uk , 426407
fr , 347495
br , 321920
it , 258109
in , 206145
nl , 199332
ca , 186036
au , 151064
de , 116160
es , 115540
za , 94154
ru , 84093
ar , 69247
mx , 62604
us , 58457
dk , 56025
be , 53561
gov , 47463
se , 46178
pl , 43001
nz , 36429
ch , 33047
pt , 31423
cn , 29379
cl , 28740
no , 28467
mil , 26451
jp , 25396
id , 25128
ie , 20301
sg , 20121
gr , 19413
cz , 18246
fi , 17920
il , 17227
co , 15847
tr , 14822
eu , 13944
my , 12526
hk , 12306
at , 12104
ph , 11038
bg , 11010
ro , 10720
biz , 10619
tw , 10418
hu , 10415
ae , 9157
pe , 7625
hr , 7110
After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 9136942.0  - Sum of Positions: 9136942
Attacker B: Results: 1 (100.0%)  - Avg. Position: 9136942.0  - Sum of Positions: 9136942  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 3716522.2  - Sum of Positions: 18582611
Attacker B: Results: 5 (50.0%)  - Avg. Position: 3588516.2  - Sum of Positions: 17942581  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 43 (43.0%)  - Avg. Position: 1302849.3  - Sum of Positions: 56022520
Attacker B: Results: 43 (43.0%)  - Avg. Position: 1293869.56  - Sum of Positions: 55636391  (TLD True Hits/Total Hits: 1/13)

After 1000 entries:
Attacker A: Results: 430 (43.0%)  - Avg. Position: 1096970.08  - Sum of Positions: 471697133
Attacker B: Results: 430 (43.0%)  - Avg. Position: 1099640.07  - Sum of Positions: 472845228  (TLD True Hits/Total Hits: 20/108)

After 10000 entries:
Attacker A: Results: 4057 (40.57%)  - Avg. Position: 1112757.48  - Sum of Positions: 4514457078
Attacker B: Results: 4057 (40.57%)  - Avg. Position: 1112294.25  - Sum of Positions: 4512577756  (TLD True Hits/Total Hits: 211/1077)

After 100000 entries:
Attacker A: Results: 41063 (41.06%)  - Avg. Position: 1109462.04  - Sum of Positions: 45557839851
Attacker B: Results: 41063 (41.06%)  - Avg. Position: 1096353.48  - Sum of Positions: 45019562969  (TLD True Hits/Total Hits: 4088/19467)

After 1000000 entries:
Attacker A: Results: 399732 (39.97%)  - Avg. Position: 1090618.33  - Sum of Positions: 435955048234
Attacker B: Results: 399732 (39.97%)  - Avg. Position: 1076236.59  - Sum of Positions: 430206203366  (TLD True Hits/Total Hits: 42607/194109)

After 5000000 entries:
Attacker A: Results: 1979868 (39.6%)  - Avg. Position: 1091784.43  - Sum of Positions: 2161589057247
Attacker B: Results: 1979868 (39.6%)  - Avg. Position: 1077411.69  - Sum of Positions: 2133132932833  (TLD True Hits/Total Hits: 188539/900685)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 49590554 (excluded)
net , 4230827 (excluded)
uk , 1642800 
fr , 1287317 
edu , 1234260 (excluded)
br , 1201597 
it , 959161 
in , 899791 
org , 886596 (excluded)
nl , 731627 
ca , 709527 
au , 583269 
de , 456316 
es , 418043 
za , 367959 
ru , 341524 
ar , 248615 
mx , 224834 
us , 224110 
dk , 204352 
be , 199177 
gov , 184299 (excluded)
se , 167411 
pl , 157710 
nz , 142226 
cn , 141436 
ch , 125564 
jp , 116900 
pt , 113115 
no , 112615 
mil , 102729 (excluded)
id , 102468 
cl , 102121 
sg , 81893 
gr , 74343 
ie , 74300 
il , 71917 
cz , 70385 
fi , 70049 
tr , 58687 
co , 56823 
my , 52365 
eu , 50496 (excluded)
hk , 49361 
at , 46256 
bg , 45023 
tw , 43319 
ph , 40971 
hu , 40813 
biz , 39995 (excluded)
ro , 37673 
ae , 36658 
hr , 28562 
pe , 27678 
vn , 27043 
kr , 26983 
info , 24820 (excluded)
ua , 22657 
lv , 21382 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 824685.0  - Sum of Positions: 824685
Attacker B: Results: 1 (100.0%)  - Avg. Position: 824685.0  - Sum of Positions: 824685  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 900387.25  - Sum of Positions: 7203098
Attacker B: Results: 8 (80.0%)  - Avg. Position: 1054088.0  - Sum of Positions: 8432704  (TLD True Hits/Total Hits: 0/3)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 3713274.38  - Sum of Positions: 193090268
Attacker B: Results: 52 (52.0%)  - Avg. Position: 3734979.73  - Sum of Positions: 194218946  (TLD True Hits/Total Hits: 4/14)

After 1000 entries:
Attacker A: Results: 507 (50.7%)  - Avg. Position: 2894296.41  - Sum of Positions: 1467408282
Attacker B: Results: 507 (50.7%)  - Avg. Position: 2869866.03  - Sum of Positions: 1455022078  (TLD True Hits/Total Hits: 58/183)

After 10000 entries:
Attacker A: Results: 5026 (50.26%)  - Avg. Position: 3714087.29  - Sum of Positions: 18667002710
Attacker B: Results: 5026 (50.26%)  - Avg. Position: 3571111.58  - Sum of Positions: 17948406822  (TLD True Hits/Total Hits: 572/1862)

After 100000 entries:
Attacker A: Results: 50083 (50.08%)  - Avg. Position: 3870138.17  - Sum of Positions: 193828130062
Attacker B: Results: 50083 (50.08%)  - Avg. Position: 3725477.45  - Sum of Positions: 186583086910  (TLD True Hits/Total Hits: 5751/18740)

After 1000000 entries:
Attacker A: Results: 499355 (49.94%)  - Avg. Position: 3849461.4  - Sum of Positions: 1922247795937
Attacker B: Results: 499355 (49.94%)  - Avg. Position: 3710709.08  - Sum of Positions: 1852961134854  (TLD True Hits/Total Hits: 57172/186440)

After 5000000 entries:
Attacker A: Results: 2495023 (49.9%)  - Avg. Position: 3858527.86  - Sum of Positions: 9627115760664
Attacker B: Results: 2495023 (49.9%)  - Avg. Position: 3717329.21  - Sum of Positions: 9274821884471  (TLD True Hits/Total Hits: 287210/935544)

After 10000000 entries:
Attacker A: Results: 4990204 (49.9%)  - Avg. Position: 3847922.76  - Sum of Positions: 19201919569253
Attacker B: Results: 4990204 (49.9%)  - Avg. Position: 3706471.06  - Sum of Positions: 18496046690688  (TLD True Hits/Total Hits: 574897/1870669)

Attacker A (with miss): Results: 4990204 (49.9%)  - Avg. Position: 21888065.02  - Sum of Positions: 218880650177225
Attacker B (with miss): Results: 4990204 (49.9%)  - Avg. Position: 21865330.42  - Sum of Positions: 218653304214991  (TLD True Hits/Total Hits: 574897/1870669)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 60000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 42507606 (excluded)
net , 3627546 (excluded)
uk , 1405951 
fr , 1104884 
edu , 1058347 (excluded)
br , 1030382 
it , 822743 
in , 770532 
org , 759463 (excluded)
nl , 626840 
ca , 607527 
au , 500432 
de , 390890 
es , 357924 
za , 315877 
ru , 293095 
ar , 213228 
mx , 192535 
us , 191931 
dk , 175005 
be , 171368 
gov , 158320 (excluded)
se , 143315 
pl , 135117 
cn , 121510 
nz , 121470 
ch , 108194 
jp , 100253 
no , 96751 
pt , 96616 
mil , 88327 (excluded)
id , 87447 
cl , 87382 
sg , 70095 
gr , 63722 
ie , 63615 
il , 61495 
cz , 60084 
fi , 60005 
tr , 49852 
co , 48306 
my , 44704 
eu , 43197 (excluded)
hk , 42394 
at , 39374 
bg , 38754 
tw , 36944 
hu , 35171 
ph , 35107 
biz , 34272 (excluded)
ro , 32505 
ae , 31336 
hr , 24351 
pe , 23683 
kr , 23238 
vn , 23137 
info , 21186 (excluded)
ua , 19465 
lv , 18434 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 213603.25  - Sum of Positions: 854413
Attacker B: Results: 4 (40.0%)  - Avg. Position: 213603.25  - Sum of Positions: 854413  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 4144636.48  - Sum of Positions: 190653278
Attacker B: Results: 46 (46.0%)  - Avg. Position: 4207967.52  - Sum of Positions: 193566506  (TLD True Hits/Total Hits: 2/21)

After 1000 entries:
Attacker A: Results: 480 (48.0%)  - Avg. Position: 3427936.05  - Sum of Positions: 1645409303
Attacker B: Results: 480 (48.0%)  - Avg. Position: 3450979.5  - Sum of Positions: 1656470160  (TLD True Hits/Total Hits: 48/196)

After 10000 entries:
Attacker A: Results: 4897 (48.97%)  - Avg. Position: 3228004.94  - Sum of Positions: 15807540197
Attacker B: Results: 4897 (48.97%)  - Avg. Position: 3164579.95  - Sum of Positions: 15496947997  (TLD True Hits/Total Hits: 559/1886)

After 100000 entries:
Attacker A: Results: 48634 (48.63%)  - Avg. Position: 3313873.44  - Sum of Positions: 161166920674
Attacker B: Results: 48634 (48.63%)  - Avg. Position: 3193691.46  - Sum of Positions: 155321990705  (TLD True Hits/Total Hits: 5647/18910)

After 1000000 entries:
Attacker A: Results: 487774 (48.78%)  - Avg. Position: 3342561.54  - Sum of Positions: 1630414611664
Attacker B: Results: 487774 (48.78%)  - Avg. Position: 3223502.05  - Sum of Positions: 1572340488129  (TLD True Hits/Total Hits: 55667/187260)

After 5000000 entries:
Attacker A: Results: 2436962 (48.74%)  - Avg. Position: 3331654.52  - Sum of Positions: 8119115458064
Attacker B: Results: 2436962 (48.74%)  - Avg. Position: 3217309.66  - Sum of Positions: 7840461371921  (TLD True Hits/Total Hits: 276472/934285)

After 10000000 entries:
Attacker A: Results: 4873726 (48.74%)  - Avg. Position: 3328638.83  - Sum of Positions: 16222873620955
Attacker B: Results: 4873726 (48.74%)  - Avg. Position: 3215355.16  - Sum of Positions: 15670760050542  (TLD True Hits/Total Hits: 553407/1869327)

Attacker A (with miss): Results: 4873726 (48.74%)  - Avg. Position: 19457919.24  - Sum of Positions: 194579192372971
Attacker B (with miss): Results: 4873726 (48.74%)  - Avg. Position: 19445271.75  - Sum of Positions: 194452717452203  (TLD True Hits/Total Hits: 553407/1869327)


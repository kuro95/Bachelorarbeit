Attacker A: Default List - Attacker B: TLD-Lists

Results with 100000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 70846968 (excluded)
net , 6040802 (excluded)
uk , 2343637 
fr , 1840890 
edu , 1762811 (excluded)
br , 1717280 
it , 1369864 
in , 1285154 
org , 1266312 (excluded)
nl , 1045236 
ca , 1012693 
au , 834088 
de , 651808 
es , 597431 
za , 525934 
ru , 488175 
ar , 355136 
mx , 321225 
us , 319861 
dk , 292823 
be , 284773 
gov , 263869 (excluded)
se , 238871 
pl , 225626 
nz , 203261 
cn , 202526 
ch , 179963 
jp , 166918 
pt , 161835 
no , 160622 
mil , 146850 (excluded)
id , 146330 
cl , 145879 
sg , 117028 
gr , 106118 
ie , 105770 
il , 102514 
cz , 100317 
fi , 100184 
tr , 83704 
co , 81004 
my , 74870 
eu , 72116 (excluded)
hk , 70599 
at , 66166 
bg , 64152 
tw , 61809 
hu , 58428 
ph , 58337 
biz , 57000 (excluded)
ro , 54165 
ae , 52291 
hr , 40675 
pe , 39803 
vn , 38909 
kr , 38483 
info , 35566 (excluded)
ua , 32292 
lv , 30573 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 56291.0  - Sum of Positions: 56291
Attacker B: Results: 1 (100.0%)  - Avg. Position: 56291.0  - Sum of Positions: 56291  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 7195035.6  - Sum of Positions: 35975178
Attacker B: Results: 5 (50.0%)  - Avg. Position: 7195035.6  - Sum of Positions: 35975178  (TLD True Hits/Total Hits: 0/3)

After 100 entries:
Attacker A: Results: 43 (43.0%)  - Avg. Position: 7706243.07  - Sum of Positions: 331368452
Attacker B: Results: 43 (43.0%)  - Avg. Position: 6063889.65  - Sum of Positions: 260747255  (TLD True Hits/Total Hits: 3/20)

After 1000 entries:
Attacker A: Results: 499 (49.9%)  - Avg. Position: 5888957.86  - Sum of Positions: 2938589970
Attacker B: Results: 499 (49.9%)  - Avg. Position: 5734938.32  - Sum of Positions: 2861734221  (TLD True Hits/Total Hits: 50/165)

After 10000 entries:
Attacker A: Results: 5272 (52.72%)  - Avg. Position: 5679658.72  - Sum of Positions: 29943160752
Attacker B: Results: 5272 (52.72%)  - Avg. Position: 5475194.74  - Sum of Positions: 28865226661  (TLD True Hits/Total Hits: 608/1864)

After 100000 entries:
Attacker A: Results: 52844 (52.84%)  - Avg. Position: 5396677.04  - Sum of Positions: 285182001634
Attacker B: Results: 52844 (52.84%)  - Avg. Position: 5182116.38  - Sum of Positions: 273843757948  (TLD True Hits/Total Hits: 6322/18856)

After 1000000 entries:
Attacker A: Results: 527912 (52.79%)  - Avg. Position: 5400515.45  - Sum of Positions: 2850996911444
Attacker B: Results: 527912 (52.79%)  - Avg. Position: 5178578.89  - Sum of Positions: 2733833938871  (TLD True Hits/Total Hits: 62746/186854)

After 5000000 entries:
Attacker A: Results: 2634997 (52.7%)  - Avg. Position: 5408374.94  - Sum of Positions: 14251051753689
Attacker B: Results: 2634997 (52.7%)  - Avg. Position: 5184175.73  - Sum of Positions: 13660287507609  (TLD True Hits/Total Hits: 313346/936848)

After 10000000 entries:
Attacker A: Results: 5270511 (52.71%)  - Avg. Position: 5400838.38  - Sum of Positions: 28465178114394
Attacker B: Results: 5270511 (52.71%)  - Avg. Position: 5175990.81  - Sum of Positions: 27280116521693  (TLD True Hits/Total Hits: 625661/1871468)

Attacker A (with miss): Results: 5270511 (52.71%)  - Avg. Position: 28597778.57  - Sum of Positions: 285977785656160
Attacker B (with miss): Results: 5270511 (52.71%)  - Avg. Position: 28541489.64  - Sum of Positions: 285414896438042  (TLD True Hits/Total Hits: 625661/1871468)


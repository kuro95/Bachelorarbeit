Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 49592577 (excluded)
net , 4228444 (excluded)
uk , 1641543 
fr , 1288497 
edu , 1233596 (excluded)
br , 1201347 
it , 959010 
in , 900128 
org , 886951 (excluded)
nl , 731127 
ca , 709676 
au , 584503 
de , 456528 
es , 418078 
za , 368013 
ru , 341099 
ar , 249034 
mx , 225029 
us , 224028 
dk , 204994 
be , 199273 
gov , 184573 (excluded)
se , 167364 
pl , 157753 
nz , 142276 
cn , 141519 
ch , 126066 
jp , 117182 
pt , 112611 
no , 112485 
mil , 102416 (excluded)
id , 102334 
cl , 101589 
sg , 82204 
ie , 73951 
gr , 73754 
il , 71922 
cz , 70267 
fi , 70113 
tr , 58511 
co , 56842 
my , 52386 
eu , 50807 (excluded)
hk , 49248 
at , 46463 
bg , 45024 
tw , 43229 
ph , 40830 
hu , 40824 
biz , 39947 (excluded)
ro , 37945 
ae , 36551 
hr , 28438 
pe , 27616 
vn , 27204 
kr , 27020 
info , 24927 (excluded)
ua , 22674 
lv , 21429 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 144.0  - Sum of Positions: 144
Attacker B: Results: 1 (100.0%)  - Avg. Position: 144.0  - Sum of Positions: 144  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 6410755.83  - Sum of Positions: 38464535
Attacker B: Results: 6 (60.0%)  - Avg. Position: 6430616.0  - Sum of Positions: 38583696  (TLD True Hits/Total Hits: 1/3)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 4496319.08  - Sum of Positions: 229312273
Attacker B: Results: 51 (51.0%)  - Avg. Position: 4491702.29  - Sum of Positions: 229076817  (TLD True Hits/Total Hits: 6/19)

After 1000 entries:
Attacker A: Results: 496 (49.6%)  - Avg. Position: 4046268.38  - Sum of Positions: 2006949114
Attacker B: Results: 496 (49.6%)  - Avg. Position: 3853654.49  - Sum of Positions: 1911412627  (TLD True Hits/Total Hits: 60/183)

After 10000 entries:
Attacker A: Results: 4935 (49.35%)  - Avg. Position: 3890528.88  - Sum of Positions: 19199759999
Attacker B: Results: 4935 (49.35%)  - Avg. Position: 3747992.73  - Sum of Positions: 18496344130  (TLD True Hits/Total Hits: 567/1826)

After 100000 entries:
Attacker A: Results: 49760 (49.76%)  - Avg. Position: 3831623.69  - Sum of Positions: 190661594710
Attacker B: Results: 49760 (49.76%)  - Avg. Position: 3680950.66  - Sum of Positions: 183164105068  (TLD True Hits/Total Hits: 5789/18879)

After 1000000 entries:
Attacker A: Results: 498964 (49.9%)  - Avg. Position: 3834794.73  - Sum of Positions: 1913424516146
Attacker B: Results: 498964 (49.9%)  - Avg. Position: 3699736.76  - Sum of Positions: 1846035453021  (TLD True Hits/Total Hits: 57715/187235)

After 5000000 entries:
Attacker A: Results: 2495654 (49.91%)  - Avg. Position: 3852880.58  - Sum of Positions: 9615456825262
Attacker B: Results: 2495654 (49.91%)  - Avg. Position: 3713615.7  - Sum of Positions: 9267899864368  (TLD True Hits/Total Hits: 287387/934880)

After 10000000 entries:
Attacker A: Results: 4989891 (49.9%)  - Avg. Position: 3849496.32  - Sum of Positions: 19208567049095
Attacker B: Results: 4989891 (49.9%)  - Avg. Position: 3708153.28  - Sum of Positions: 18503280679453  (TLD True Hits/Total Hits: 574623/1870554)

Attacker A (with miss): Results: 4989891 (49.9%)  - Avg. Position: 21891908.21  - Sum of Positions: 218919082063794
Attacker B (with miss): Results: 4989891 (49.9%)  - Avg. Position: 21869297.67  - Sum of Positions: 218692976722990  (TLD True Hits/Total Hits: 574623/1870554)


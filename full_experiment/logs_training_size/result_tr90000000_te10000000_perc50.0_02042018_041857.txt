Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63758664 (excluded)
net , 5437902 (excluded)
uk , 2110293 
fr , 1657266 
edu , 1586951 (excluded)
br , 1545817 
it , 1233738 
in , 1156832 
org , 1140430 (excluded)
nl , 941008 
ca , 912221 
au , 750317 
de , 586618 
es , 537436 
za , 472960 
ru , 439878 
ar , 319498 
mx , 288753 
us , 287502 
dk , 263366 
be , 256658 
gov , 237437 (excluded)
se , 214754 
pl , 202469 
nz , 182974 
cn , 182265 
ch , 162091 
jp , 150431 
pt , 145276 
no , 144487 
mil , 132016 (excluded)
id , 131705 
cl , 131010 
sg , 105171 
gr , 95472 
ie , 95234 
il , 92050 
fi , 90205 
cz , 90182 
tr , 75325 
co , 73187 
my , 67313 
eu , 64915 (excluded)
hk , 63511 
at , 59273 
bg , 57945 
tw , 55633 
ph , 52576 
hu , 52534 
biz , 51301 (excluded)
ro , 48642 
ae , 47041 
hr , 36727 
pe , 35742 
vn , 34890 
kr , 34609 
info , 31951 (excluded)
ua , 29242 
lv , 27681 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 6213.0  - Sum of Positions: 6213
Attacker B: Results: 1 (100.0%)  - Avg. Position: 6213.0  - Sum of Positions: 6213  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 988518.0  - Sum of Positions: 3954072
Attacker B: Results: 4 (40.0%)  - Avg. Position: 268494.5  - Sum of Positions: 1073978  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 2432321.08  - Sum of Positions: 121616054
Attacker B: Results: 50 (50.0%)  - Avg. Position: 2358445.4  - Sum of Positions: 117922270  (TLD True Hits/Total Hits: 4/16)

After 1000 entries:
Attacker A: Results: 510 (51.0%)  - Avg. Position: 3951018.05  - Sum of Positions: 2015019207
Attacker B: Results: 510 (51.0%)  - Avg. Position: 3831774.11  - Sum of Positions: 1954204798  (TLD True Hits/Total Hits: 54/185)

After 10000 entries:
Attacker A: Results: 5160 (51.6%)  - Avg. Position: 4569604.35  - Sum of Positions: 23579158468
Attacker B: Results: 5160 (51.6%)  - Avg. Position: 4407844.3  - Sum of Positions: 22744476593  (TLD True Hits/Total Hits: 567/1833)

After 100000 entries:
Attacker A: Results: 51660 (51.66%)  - Avg. Position: 4839932.81  - Sum of Positions: 250030928924
Attacker B: Results: 51660 (51.66%)  - Avg. Position: 4647873.84  - Sum of Positions: 240109162747  (TLD True Hits/Total Hits: 6105/18798)

After 1000000 entries:
Attacker A: Results: 518247 (51.82%)  - Avg. Position: 4857334.48  - Sum of Positions: 2517299024452
Attacker B: Results: 518247 (51.82%)  - Avg. Position: 4664777.71  - Sum of Positions: 2417507052529  (TLD True Hits/Total Hits: 60859/187055)

After 5000000 entries:
Attacker A: Results: 2593422 (51.87%)  - Avg. Position: 4878973.0  - Sum of Positions: 12653235915925
Attacker B: Results: 2593422 (51.87%)  - Avg. Position: 4685225.39  - Sum of Positions: 12150766613741  (TLD True Hits/Total Hits: 304815/934608)

After 10000000 entries:
Attacker A: Results: 5187139 (51.87%)  - Avg. Position: 4880983.19  - Sum of Positions: 25318338239758
Attacker B: Results: 5187139 (51.87%)  - Avg. Position: 4684993.91  - Sum of Positions: 24301714635713  (TLD True Hits/Total Hits: 610374/1869899)

Attacker A (with miss): Results: 5187139 (51.87%)  - Avg. Position: 26441032.83  - Sum of Positions: 264410328273844
Attacker B (with miss): Results: 5187139 (51.87%)  - Avg. Position: 26396903.5  - Sum of Positions: 263969034970414  (TLD True Hits/Total Hits: 610374/1869899)


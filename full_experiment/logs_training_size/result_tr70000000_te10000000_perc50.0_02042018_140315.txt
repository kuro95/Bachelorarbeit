Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 49589070 (excluded)
net , 4229878 (excluded)
uk , 1641658 
fr , 1286975 
edu , 1234064 (excluded)
br , 1201995 
it , 960002 
in , 899338 
org , 887147 (excluded)
nl , 731904 
ca , 709458 
au , 583670 
de , 456171 
es , 417937 
za , 368533 
ru , 341687 
ar , 248598 
mx , 225306 
us , 224086 
dk , 204532 
be , 199716 
gov , 185098 (excluded)
se , 167550 
pl , 157706 
cn , 142274 
nz , 142100 
ch , 126269 
jp , 116808 
pt , 113264 
no , 112599 
id , 102651 
mil , 102434 (excluded)
cl , 101568 
sg , 81896 
gr , 74314 
ie , 73776 
il , 71767 
cz , 70236 
fi , 70192 
tr , 58744 
co , 57144 
my , 52343 
eu , 50528 (excluded)
hk , 49265 
at , 45866 
bg , 44831 
tw , 43375 
hu , 40939 
ph , 40731 
biz , 39858 (excluded)
ro , 37781 
ae , 36607 
hr , 28422 
pe , 27811 
vn , 27319 
kr , 26965 
info , 24808 (excluded)
ua , 22594 
lv , 21419 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 17151.0  - Sum of Positions: 17151
Attacker B: Results: 1 (100.0%)  - Avg. Position: 22092.0  - Sum of Positions: 22092  (TLD True Hits/Total Hits: 1/1)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 561947.2  - Sum of Positions: 2809736
Attacker B: Results: 5 (50.0%)  - Avg. Position: 562935.4  - Sum of Positions: 2814677  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 4451811.57  - Sum of Positions: 204783332
Attacker B: Results: 46 (46.0%)  - Avg. Position: 4457341.37  - Sum of Positions: 205037703  (TLD True Hits/Total Hits: 4/14)

After 1000 entries:
Attacker A: Results: 485 (48.5%)  - Avg. Position: 4806664.86  - Sum of Positions: 2331232456
Attacker B: Results: 485 (48.5%)  - Avg. Position: 4679572.9  - Sum of Positions: 2269592856  (TLD True Hits/Total Hits: 57/196)

After 10000 entries:
Attacker A: Results: 5033 (50.33%)  - Avg. Position: 4009648.99  - Sum of Positions: 20180563350
Attacker B: Results: 5033 (50.33%)  - Avg. Position: 3894032.74  - Sum of Positions: 19598666790  (TLD True Hits/Total Hits: 587/1916)

After 100000 entries:
Attacker A: Results: 50141 (50.14%)  - Avg. Position: 3896974.19  - Sum of Positions: 195398183020
Attacker B: Results: 50141 (50.14%)  - Avg. Position: 3765010.14  - Sum of Positions: 188781373237  (TLD True Hits/Total Hits: 5885/18931)

After 1000000 entries:
Attacker A: Results: 498936 (49.89%)  - Avg. Position: 3853876.05  - Sum of Positions: 1922837501206
Attacker B: Results: 498936 (49.89%)  - Avg. Position: 3713974.36  - Sum of Positions: 1853035512565  (TLD True Hits/Total Hits: 57564/187353)

After 5000000 entries:
Attacker A: Results: 2494562 (49.89%)  - Avg. Position: 3848335.56  - Sum of Positions: 9599911648250
Attacker B: Results: 2494562 (49.89%)  - Avg. Position: 3704884.93  - Sum of Positions: 9242065148343  (TLD True Hits/Total Hits: 287667/936253)

After 10000000 entries:
Attacker A: Results: 4989644 (49.9%)  - Avg. Position: 3849334.98  - Sum of Positions: 19206811211746
Attacker B: Results: 4989644 (49.9%)  - Avg. Position: 3707741.59  - Sum of Positions: 18500310601949  (TLD True Hits/Total Hits: 574795/1870958)

Attacker A (with miss): Results: 4989644 (49.9%)  - Avg. Position: 21890865.37  - Sum of Positions: 218908653743886
Attacker B (with miss): Results: 4989644 (49.9%)  - Avg. Position: 21868076.27  - Sum of Positions: 218680762748598  (TLD True Hits/Total Hits: 574795/1870958)


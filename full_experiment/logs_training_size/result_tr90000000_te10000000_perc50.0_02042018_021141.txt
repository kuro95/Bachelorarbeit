Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63758285 (excluded)
net , 5437485 (excluded)
uk , 2109970 
fr , 1656858 
edu , 1586921 (excluded)
br , 1545662 
it , 1233002 
in , 1157332 
org , 1139758 (excluded)
nl , 941173 
ca , 911962 
au , 750436 
de , 586835 
es , 537709 
za , 472917 
ru , 439020 
ar , 319789 
mx , 288765 
us , 287224 
dk , 263693 
be , 256591 
gov , 238108 (excluded)
se , 215209 
pl , 202829 
nz , 183294 
cn , 182110 
ch , 162460 
jp , 150432 
pt , 145701 
no , 144585 
id , 131916 
mil , 131757 (excluded)
cl , 131035 
sg , 105213 
gr , 95541 
ie , 95353 
il , 91875 
fi , 90331 
cz , 90214 
tr , 75438 
co , 73017 
my , 67426 
eu , 65085 (excluded)
hk , 63557 
at , 59406 
bg , 57864 
tw , 55660 
hu , 52619 
ph , 52365 
biz , 51245 (excluded)
ro , 48655 
ae , 47123 
hr , 36643 
pe , 35650 
vn , 34847 
kr , 34588 
info , 31930 (excluded)
ua , 29197 
lv , 27720 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 126461.0  - Sum of Positions: 126461
Attacker B: Results: 1 (100.0%)  - Avg. Position: 126461.0  - Sum of Positions: 126461  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 20761574.0  - Sum of Positions: 83046296
Attacker B: Results: 4 (40.0%)  - Avg. Position: 16779016.0  - Sum of Positions: 67116064  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 6257200.96  - Sum of Positions: 287831244
Attacker B: Results: 46 (46.0%)  - Avg. Position: 5870449.46  - Sum of Positions: 270040675  (TLD True Hits/Total Hits: 5/15)

After 1000 entries:
Attacker A: Results: 475 (47.5%)  - Avg. Position: 5436857.73  - Sum of Positions: 2582507424
Attacker B: Results: 475 (47.5%)  - Avg. Position: 5174998.3  - Sum of Positions: 2458124193  (TLD True Hits/Total Hits: 58/182)

After 10000 entries:
Attacker A: Results: 5110 (51.1%)  - Avg. Position: 4838473.37  - Sum of Positions: 24724598927
Attacker B: Results: 5110 (51.1%)  - Avg. Position: 4688544.3  - Sum of Positions: 23958461390  (TLD True Hits/Total Hits: 569/1860)

After 100000 entries:
Attacker A: Results: 51802 (51.8%)  - Avg. Position: 4846207.66  - Sum of Positions: 251043249062
Attacker B: Results: 51802 (51.8%)  - Avg. Position: 4660114.72  - Sum of Positions: 241403262568  (TLD True Hits/Total Hits: 6098/18759)

After 1000000 entries:
Attacker A: Results: 517449 (51.74%)  - Avg. Position: 4895469.92  - Sum of Positions: 2533156013101
Attacker B: Results: 517449 (51.74%)  - Avg. Position: 4696523.03  - Sum of Positions: 2430211142779  (TLD True Hits/Total Hits: 61006/187137)

After 5000000 entries:
Attacker A: Results: 2592149 (51.84%)  - Avg. Position: 4889080.18  - Sum of Positions: 12673224309569
Attacker B: Results: 2592149 (51.84%)  - Avg. Position: 4690883.23  - Sum of Positions: 12159468262164  (TLD True Hits/Total Hits: 304899/934510)

After 10000000 entries:
Attacker A: Results: 5186111 (51.86%)  - Avg. Position: 4889446.29  - Sum of Positions: 25357211209354
Attacker B: Results: 5186111 (51.86%)  - Avg. Position: 4690915.46  - Sum of Positions: 24327608245595  (TLD True Hits/Total Hits: 610176/1868656)

Attacker A (with miss): Results: 5186111 (51.86%)  - Avg. Position: 26449173.01  - Sum of Positions: 264491730106682
Attacker B (with miss): Results: 5186111 (51.86%)  - Avg. Position: 26403754.01  - Sum of Positions: 264037540113161  (TLD True Hits/Total Hits: 610176/1868656)


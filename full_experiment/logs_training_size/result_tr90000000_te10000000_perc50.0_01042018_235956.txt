Attacker A: Default List - Attacker B: TLD-Lists

Results with 90000000 Training Entries and 10000000 Random Entries and top 50 TLDs:

com , 63760417 (excluded)
net , 5436354 (excluded)
uk , 2110881 
fr , 1656236 
edu , 1586486 (excluded)
br , 1544525 
it , 1233625 
in , 1156811 
org , 1140162 (excluded)
nl , 940393 
ca , 912197 
au , 750796 
de , 586330 
es , 537099 
za , 472841 
ru , 439157 
ar , 320277 
mx , 289162 
us , 288047 
dk , 262986 
be , 256634 
gov , 237372 (excluded)
se , 215268 
pl , 202659 
nz , 183135 
cn , 182259 
ch , 162275 
jp , 150742 
pt , 145538 
no , 144530 
mil , 132120 (excluded)
id , 131543 
cl , 131380 
sg , 105204 
gr , 95662 
ie , 95298 
il , 92294 
fi , 90223 
cz , 90122 
tr , 75306 
co , 73092 
my , 67454 
eu , 65066 (excluded)
hk , 63519 
at , 59312 
bg , 57855 
tw , 55657 
hu , 52672 
ph , 52506 
biz , 51212 (excluded)
ro , 48648 
ae , 46936 
hr , 36636 
pe , 35663 
vn , 34874 
kr , 34525 
info , 32030 (excluded)
ua , 29158 
lv , 27624 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 264276.0  - Sum of Positions: 264276
Attacker B: Results: 1 (100.0%)  - Avg. Position: 264276.0  - Sum of Positions: 264276  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 7922647.67  - Sum of Positions: 47535886
Attacker B: Results: 6 (60.0%)  - Avg. Position: 7922647.67  - Sum of Positions: 47535886  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 4902280.28  - Sum of Positions: 245114014
Attacker B: Results: 50 (50.0%)  - Avg. Position: 4956891.88  - Sum of Positions: 247844594  (TLD True Hits/Total Hits: 11/25)

After 1000 entries:
Attacker A: Results: 530 (53.0%)  - Avg. Position: 4981229.3  - Sum of Positions: 2640051527
Attacker B: Results: 530 (53.0%)  - Avg. Position: 4862631.87  - Sum of Positions: 2577194889  (TLD True Hits/Total Hits: 63/181)

After 10000 entries:
Attacker A: Results: 5154 (51.54%)  - Avg. Position: 4975235.47  - Sum of Positions: 25642363616
Attacker B: Results: 5154 (51.54%)  - Avg. Position: 4767149.62  - Sum of Positions: 24569889127  (TLD True Hits/Total Hits: 581/1883)

After 100000 entries:
Attacker A: Results: 51452 (51.45%)  - Avg. Position: 4902395.86  - Sum of Positions: 252238071640
Attacker B: Results: 51452 (51.45%)  - Avg. Position: 4704034.68  - Sum of Positions: 242031992329  (TLD True Hits/Total Hits: 6091/18623)

After 1000000 entries:
Attacker A: Results: 518526 (51.85%)  - Avg. Position: 4899252.79  - Sum of Positions: 2540389950571
Attacker B: Results: 518526 (51.85%)  - Avg. Position: 4702067.72  - Sum of Positions: 2438144369147  (TLD True Hits/Total Hits: 61291/187560)

After 5000000 entries:
Attacker A: Results: 2593344 (51.87%)  - Avg. Position: 4897326.04  - Sum of Positions: 12700451093644
Attacker B: Results: 2593344 (51.87%)  - Avg. Position: 4700931.46  - Sum of Positions: 12191132403495  (TLD True Hits/Total Hits: 305584/935778)

After 10000000 entries:
Attacker A: Results: 5188907 (51.89%)  - Avg. Position: 4889853.81  - Sum of Positions: 25372996637763
Attacker B: Results: 5188907 (51.89%)  - Avg. Position: 4693274.82  - Sum of Positions: 24352966541164  (TLD True Hits/Total Hits: 611346/1871013)

Attacker A (with miss): Results: 5188907 (51.89%)  - Avg. Position: 26439342.28  - Sum of Positions: 264393422757714
Attacker B (with miss): Results: 5188907 (51.89%)  - Avg. Position: 26394834.8  - Sum of Positions: 263948347958835  (TLD True Hits/Total Hits: 611346/1871013)


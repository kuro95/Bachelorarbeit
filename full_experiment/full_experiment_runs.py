# Experiment for bachelor thesis. Attacker A -> One big default list ; Attacker B -> TLD lists and a default list

import time
import collections
import random
import MySQLdb
import MySQLdb.cursors
import gc
import sys
import csv

# Start values

position_a_sum = 0 # Sum of the positions in the test for Attacker A
position_a_miss_sum = 0
position_b_sum = 0 # Sum of the positions in the test for Attacker B
position_b_miss_sum = 0
training_value = int(sys.argv[1]) # Amount of pairs used for training the attackers
test_value = int(sys.argv[2]) # Amount of pairs used to test the attackers
path_default = "G:\\Bachelorarbeit\\full_experiment\\" # Local path to the output folder
tld_cap = float(sys.argv[3]) # Only the TOP x TLDs will be considered for Attacker B
tld_threshold = float(sys.argv[3]) # TLDs under this threshold percentage will be dismissed 
tld_perc_flag = int(sys.argv[4]) # 0 -> Hard cap will be used, 1 -> Percentage cap will be used
break_value = [1,10,100,1000,10000,100000,1000000,5000000,10000000,20000000] # Output the temp results at these values
tld_hit = 0 # Counter for having a tld-specific list for this pair
tld_miss = 0 # Counter for missing hashes in these tld-specific lists
tld_list_size = [] # Amount of entries in each List
list_tld = [] # Chooses which TLD gets a list
blacklist = ['com','net','edu','org','gov','mil','biz','COM','NET','info','eu','coop','con','int','aero','name','coom','cpm','ocm','coom','asia','cim','vom','travel','ccom','pro'] # Excluding TLDs which are NOT country-related, as this thesis is focused on countries

if(tld_perc_flag == 0):
	tld_cap = int(sys.argv[3])

# Training part. Top TLDs will be calcuted. Attacker lists will be sorted and created.

def dict_init(cursor,file_output):
	
	training_data_counter = collections.defaultdict(list) # List with the amount of each TLD processed
	hash_only = [] # List with only the hashes in it (Used to calculate the lists)
	tld_sum_list = collections.defaultdict(list) # List with only the hashes in it, but separated by TLD
	
	# Fetching our training set and distribute to our lists	

	for (tld,hash_string) in cursor.fetchmany(size=training_value):
		if(tld not in training_data_counter.keys()):
			training_data_counter[tld] = 0
		training_data_counter[tld] += 1 
		hash_only.append(hash_string)
		tld_sum_list[tld].append(hash_string)

	print(time.strftime("%H:%M:%S")+" - Fetched! Begin with Training")

	# Deciding which TLDs should be used for Attacker B, method depends on hard/soft cap

	tld_count = 0
	if(tld_perc_flag == 0):
		while (tld_count < tld_cap):
			tld = max(training_data_counter.keys(),  key=(lambda key: training_data_counter[key]))
			file_output.write(tld+" , "+str(training_data_counter[tld])+" ")
			if(tld not in blacklist):
				list_tld.append(tld)
				file_output.write("\n")
				tld_count += 1
			else:
				file_output.write("(excluded)\n")
			training_data_counter.pop(tld, None)
		file_output.write("\n")
	else:
		while True:
			tld = max(training_data_counter.keys(),  key=(lambda key: training_data_counter[key]))
			if(training_data_counter[tld] <= (training_value*(tld_threshold/100))):
				break
			file_output.write(tld+" , "+str(training_data_counter[tld])+" ")
			if(tld not in blacklist):
				list_tld.append(tld)
				file_output.write("\n")
				tld_count += 1
			else:
				file_output.write("(excluded)\n")
			training_data_counter.pop(tld, None)
		file_output.write("\n")

	# May be useless (trying to save memory)

	training_data_counter = None
	gc.collect()

	print(time.strftime("%H:%M:%S")+" - Top TLD fetched!")
	print(list_tld)

	# Creating the TLD-lists for Attacker B

	for tld in list_tld:
		dict[tld] = {}
		position = 1
		for (hash,amount) in (collections.Counter(tld_sum_list[tld])).most_common():
			dict[tld][hash] = position
			position += 1
		tld_list_size.append((tld,position))

	# May be useless (trying to save memory)

	tld_sum_list = None
	gc.collect()

	print(time.strftime("%H:%M:%S")+" - Attacker B lists made")

	# Creating the default list for both Attacker A and B

	dict['attacker'] = {}
	position = 1
	for (hash,amount) in (collections.Counter(hash_only)).most_common():
		dict['attacker'][hash] = position
		position += 1
	tld_list_size.append(('attacker',position))

	# May be useless (trying to save memory)

	hash_only = None
	gc.collect()

	print(time.strftime("%H:%M:%S")+" - Attacker A lists made")

# Attacker A

def attacker_normal(hash_string):
	global position_a_sum
	global position_a_miss_sum
	if hash_string in dict['attacker']:
		position_a_sum += dict['attacker'][hash_string]
		position_a_miss_sum += dict['attacker'][hash_string]
		return 1
	for item in tld_list_size:
		if item[0] == 'attacker':
			position_a_miss_sum += item[1]
			break
	return 0

# Attacker B	

def attacker_tld(tld,hash_string):
	global position_b_sum
	global position_b_miss_sum
	global tld_hit
	tld_hit += 1
	if not tld in list_tld:
		tld = 'attacker'
		tld_hit -= 1
	if tld == 'attacker':	
		if hash_string in dict[tld]:
			position_b_sum += dict[tld][hash_string]
			position_b_miss_sum += dict[tld][hash_string]
			return 1
		for item in tld_list_size:
			if item[0] == tld:
				position_b_miss_sum += item[1]
				break
		return 0
	else:
		if hash_string in dict[tld]:
			position_b_sum += dict[tld][hash_string]
			position_b_miss_sum += dict[tld][hash_string]
			return 1
		size_tld = 0
		global tld_miss
		tld_miss += 1
		for item in tld_list_size:
			if item[0] == tld:
				size_tld = item[1]
				break
		tld = 'attacker'
		if hash_string in dict[tld]:
			position_b_sum += (dict[tld][hash_string] + size_tld)
			position_b_miss_sum += (dict[tld][hash_string] + size_tld)
			return 1
		for item in tld_list_size:
			if item[0] == tld:
				position_b_miss_sum += (item[1] + size_tld)
				break
		return 0

# Helper function for calculating percentage

def save_div_perc(result,value):
	if value == 0:
		return 0
	else:
		return round((result/value)*100,2)

# Helper function for calculating the average of the position in the list

def save_div_pos(result,position_sum):
	if result == 0:
		return 0
	else:
		return round((position_sum/result),2)

# Establish MySQL Connection

try:
  cnx = MySQLdb.connect(user='root',password='root',host='localhost',
    database='bachelorarbeit',cursorclass = MySQLdb.cursors.SSCursor)
  cursor = cnx.cursor()
except MySQLdb.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)

# Start the Query and send to the Training when query is completed

print("Test with "+str(training_value)+" Training - "+str(test_value)+" Test - "+str(tld_cap)+" TLDs")
print(time.strftime("%H:%M:%S")+" - Begin querying SQL database")
dict = {}
time_string = time.strftime("%d%m%Y_%H%M%S")
csv_writer = csv.writer(open(time_string+'_output.csv', 'w'), delimiter=',', dialect='excel')
file_output = open(path_default+"result_tr"+str(training_value)+"_te"+str(test_value)+'_perc'+str(float(sys.argv[3]))+'_'+time_string+'.txt','w')
file_output.write("Attacker A: Default List - Attacker B: TLD-Lists\n\n")
file_output.write("Results with "+str(training_value)+" Training Entries and "+str(test_value)+" Random Entries and top "+str(tld_cap)+" TLDs:\n\n")
result_a = 0
result_b = 0
query = ("SELECT LOWER(SUBSTRING_INDEX(SUBSTRING_INDEX(MEMBER_PRIMARY_EMAIL, '@', -1), '.', -1)),MEMBER_HASH FROM bachelorarbeit.fulllist ORDER BY RAND() LIMIT "+str(training_value+test_value+10)+"")
cursor.execute(query)
print(time.strftime("%H:%M:%S")+" - Query executed! Begin with Fetching")
dict_init(cursor,file_output)
print(time.strftime("%H:%M:%S")+" - Training done! Start testing")
start = time.time()
i = 0

# Testing stage, both Attacker will attack the same pair

for (tld,hash_string) in cursor.fetchmany(size=test_value):
	i += 1
	result_a += attacker_normal(hash_string)
	result_b += attacker_tld(tld,hash_string)
	
	if i in break_value and i != test_value:
		print("Break value: "+str(i))
		file_output.write("After "+str(i)+" entries:\n")
		file_output.write("Attacker A: Results: "+str(result_a)+" ("+str(save_div_perc(result_a,i))+"%)  - Avg. Position: "+str(save_div_pos(result_a,position_a_sum))+"  - Sum of Positions: "+str(position_a_sum)+"\n")
		file_output.write("Attacker B: Results: "+str(result_b)+" ("+str(save_div_perc(result_b,i))+"%)  - Avg. Position: "+str(save_div_pos(result_b,position_b_sum))+"  - Sum of Positions: "+str(position_b_sum)+"  (TLD True Hits/Total Hits: "+str(tld_hit-tld_miss)+"/"+str(tld_hit)+")\n\n")

end = time.time()

file_output.write("After "+str(test_value)+" entries:\n")
file_output.write("Attacker A: Results: "+str(result_a)+" ("+str(save_div_perc(result_a,test_value))+"%)  - Avg. Position: "+str(save_div_pos(result_a,position_a_sum))+"  - Sum of Positions: "+str(position_a_sum)+"\n")
file_output.write("Attacker B: Results: "+str(result_b)+" ("+str(save_div_perc(result_b,test_value))+"%)  - Avg. Position: "+str(save_div_pos(result_b,position_b_sum))+"  - Sum of Positions: "+str(position_b_sum)+"  (TLD True Hits/Total Hits: "+str(tld_hit-tld_miss)+"/"+str(tld_hit)+")\n\n")
file_output.write("Attacker A (with miss): Results: "+str(result_a)+" ("+str(save_div_perc(result_a,test_value))+"%)  - Avg. Position: "+str(save_div_pos(test_value,position_a_miss_sum))+"  - Sum of Positions: "+str(position_a_miss_sum)+"\n")
file_output.write("Attacker B (with miss): Results: "+str(result_b)+" ("+str(save_div_perc(result_b,test_value))+"%)  - Avg. Position: "+str(save_div_pos(test_value,position_b_miss_sum))+"  - Sum of Positions: "+str(position_b_miss_sum)+"  (TLD True Hits/Total Hits: "+str(tld_hit-tld_miss)+"/"+str(tld_hit)+")\n\n")
if(tld_perc_flag == 0):
	csv_writer.writerow([time_string,training_value,test_value,tld_cap,'',result_a,'',save_div_pos(result_a,position_a_sum),result_b,'',save_div_pos(result_b,position_b_sum),tld_hit-tld_miss,tld_hit,'','','',save_div_pos(test_value,position_a_miss_sum),save_div_pos(test_value,position_b_miss_sum),''])
else:
	csv_writer.writerow([time_string,training_value,test_value,'',tld_threshold,result_a,'',save_div_pos(result_a,position_a_sum),result_b,'',save_div_pos(result_b,position_b_sum),tld_hit-tld_miss,tld_hit,'','','',save_div_pos(test_value,position_a_miss_sum),save_div_pos(test_value,position_b_miss_sum),''])

print(time.strftime("%H:%M:%S")+" - Execution time of loop: "+str((end - start))+" sec")

cursor.close()
cnx.close()
Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.001 TLDs:

com , 49591365 (excluded)
net , 4229597 (excluded)
uk , 1640105 
fr , 1288971 
edu , 1233519 (excluded)
br , 1201189 
it , 959492 
in , 900263 
org , 887147 (excluded)
nl , 731411 
ca , 708906 
au , 583880 
de , 456670 
es , 418309 
za , 368187 
ru , 341337 
ar , 248865 
mx , 224955 
us , 223455 
dk , 204875 
be , 200052 
gov , 184715 (excluded)
se , 166860 
pl , 158075 
nz , 142715 
cn , 141635 
ch , 126128 
jp , 116764 
pt , 113369 
no , 112333 
mil , 102692 (excluded)
id , 102577 
cl , 101942 
sg , 81684 
gr , 74023 
ie , 74014 
il , 71665 
cz , 70206 
fi , 69887 
tr , 58626 
co , 56720 
my , 52561 
eu , 50601 (excluded)
hk , 49296 
at , 46218 
bg , 44835 
tw , 43403 
ph , 40996 
hu , 40811 
biz , 39866 (excluded)
ro , 37924 
ae , 36554 
hr , 28482 
pe , 28030 
vn , 27034 
kr , 26916 
info , 24953 (excluded)
ua , 22614 
lv , 21450 
pk , 21051 
sk , 19548 
uy , 17302 
sa , 15409 
tv , 14097 
rs , 13197 
th , 13092 
lt , 12666 
cc , 11661 
cat , 11060 
ma , 10684 
ee , 10567 
fm , 10490 
ke , 10270 
si , 10260 
zw , 10072 
is , 9889 
ve , 9640 
cr , 9261 
nu , 8978 
om , 8973 
lu , 8717 
lk , 8441 
tn , 8397 
ec , 7833 
con , 7244 (excluded)
cy , 6074 
int , 5875 (excluded)
jo , 5748 
lb , 5433 
mk , 5364 
me , 5294 
na , 5271 
qa , 5242 
eg , 5145 
ws , 5090 
bw , 4985 
cm , 4903 
do , 4582 
mu , 4538 
coop , 4364 (excluded)
by , 4156 
yu , 3930 
gt , 3883 
zm , 3752 
py , 3641 
kz , 3633 
ug , 3628 
aero , 3608 (excluded)
ge , 3584 
mt , 3298 
bm , 3276 
np , 3242 
ba , 3139 
pg , 3053 
tt , 3000 
ir , 2960 
mz , 2862 
bh , 2793 
name , 2780 (excluded)
bo , 2688 
fj , 2482 
sv , 2470 
ni , 2408 
tz , 2310 
pa , 2240 
cu , 2078 
md , 1943 
bd , 1871 
kw , 1786 
az , 1713 
as , 1706 
gh , 1655 
bz , 1628 
ng , 1531 
am , 1524 
dz , 1523 
sn , 1452 
cpm , 1414 (excluded)
coom , 1413 (excluded)
kh , 1397 
nc , 1380 
al , 1371 
pf , 1347 
ky , 1261 
hn , 1260 
ao , 1243 
et , 1206 
ocm , 1203 (excluded)
jm , 1191 
ci , 1180 
mg , 1175 
mn , 1132 
comm , 1117 
ms , 1116 
to , 1110 
asia , 1108 (excluded)
fo , 1102 
cim , 1088 (excluded)
im , 1059 
li , 1002 
sz , 967 
vom , 910 (excluded)
an , 904 
mo , 895 
travel , 876 (excluded)
gl , 874 
sy , 872 
mc , 867 
mw , 832 
ccom , 825 (excluded)
ps , 815 
ad , 777 
ls , 769 
ag , 742 
pr , 737 
pro , 713 (excluded)

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 16438154.0  - Sum of Positions: 16438154
Attacker B: Results: 1 (100.0%)  - Avg. Position: 206588.0  - Sum of Positions: 206588  (TLD True Hits/Total Hits: 1/1)

After 10 entries:
Attacker A: Results: 9 (90.0%)  - Avg. Position: 2656842.67  - Sum of Positions: 23911584
Attacker B: Results: 9 (90.0%)  - Avg. Position: 978264.78  - Sum of Positions: 8804383  (TLD True Hits/Total Hits: 4/6)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 4683090.46  - Sum of Positions: 266936156
Attacker B: Results: 57 (57.0%)  - Avg. Position: 4466491.23  - Sum of Positions: 254590000  (TLD True Hits/Total Hits: 9/24)

After 1000 entries:
Attacker A: Results: 479 (47.9%)  - Avg. Position: 4036893.84  - Sum of Positions: 1933672151
Attacker B: Results: 479 (47.9%)  - Avg. Position: 3872509.99  - Sum of Positions: 1854932284  (TLD True Hits/Total Hits: 62/206)

After 10000 entries:
Attacker A: Results: 4915 (49.15%)  - Avg. Position: 4095744.83  - Sum of Positions: 20130585845
Attacker B: Results: 4915 (49.15%)  - Avg. Position: 3961740.98  - Sum of Positions: 19471956907  (TLD True Hits/Total Hits: 554/1958)

After 100000 entries:
Attacker A: Results: 49799 (49.8%)  - Avg. Position: 3890463.79  - Sum of Positions: 193741206190
Attacker B: Results: 49799 (49.8%)  - Avg. Position: 3741138.06  - Sum of Positions: 186304934356  (TLD True Hits/Total Hits: 5759/19351)

After 1000000 entries:
Attacker A: Results: 498911 (49.89%)  - Avg. Position: 3858894.98  - Sum of Positions: 1925245151065
Attacker B: Results: 498911 (49.89%)  - Avg. Position: 3710260.75  - Sum of Positions: 1851089903203  (TLD True Hits/Total Hits: 58282/194535)

After 5000000 entries:
Attacker A: Results: 2493983 (49.88%)  - Avg. Position: 3844817.22  - Sum of Positions: 9588908787966
Attacker B: Results: 2493983 (49.88%)  - Avg. Position: 3699577.78  - Sum of Positions: 9226684095336  (TLD True Hits/Total Hits: 290715/970140)

After 10000000 entries:
Attacker A: Results: 4988718 (49.89%)  - Avg. Position: 3846136.13  - Sum of Positions: 19187288557581
Attacker B: Results: 4988718 (49.89%)  - Avg. Position: 3703024.4  - Sum of Positions: 18473344481943  (TLD True Hits/Total Hits: 580757/1939447)

Attacker A (with miss): Results: 4988718 (49.89%)  - Avg. Position: 21891982.54  - Sum of Positions: 218919825436731
Attacker B (with miss): Results: 4988718 (49.89%)  - Avg. Position: 21868486.09  - Sum of Positions: 218684860909019  (TLD True Hits/Total Hits: 580757/1939447)


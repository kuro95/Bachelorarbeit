Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49589040 (excluded)
net , 4230489 (excluded)
uk , 1641993 
fr , 1289230 
edu , 1232838 (excluded)
br , 1201982 
it , 959948 
in , 900323 
org , 886319 (excluded)
nl , 731753 
ca , 709403 
au , 583342 
de , 456411 
es , 418051 
za , 367613 
ru , 341730 
ar , 249228 
mx , 224471 
us , 223788 
dk , 204879 
be , 199350 
gov , 184500 (excluded)
se , 167739 
pl , 157974 
nz , 142253 
cn , 141538 
ch , 125782 
jp , 117151 
pt , 112926 
no , 112641 
mil , 102574 (excluded)
id , 102411 
cl , 102101 
sg , 81880 
ie , 74215 
gr , 74151 
il , 71690 
fi , 70308 
cz , 70099 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 34588655.0  - Sum of Positions: 34588655
Attacker B: Results: 1 (100.0%)  - Avg. Position: 34588655.0  - Sum of Positions: 34588655  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 8423847.4  - Sum of Positions: 42119237
Attacker B: Results: 5 (50.0%)  - Avg. Position: 8465665.8  - Sum of Positions: 42328329  (TLD True Hits/Total Hits: 1/4)

After 100 entries:
Attacker A: Results: 54 (54.0%)  - Avg. Position: 5812386.48  - Sum of Positions: 313868870
Attacker B: Results: 54 (54.0%)  - Avg. Position: 5832009.56  - Sum of Positions: 314928516  (TLD True Hits/Total Hits: 6/17)

After 1000 entries:
Attacker A: Results: 503 (50.3%)  - Avg. Position: 4557648.46  - Sum of Positions: 2292497177
Attacker B: Results: 503 (50.3%)  - Avg. Position: 4521210.92  - Sum of Positions: 2274169094  (TLD True Hits/Total Hits: 54/179)

After 10000 entries:
Attacker A: Results: 5034 (50.34%)  - Avg. Position: 3927567.26  - Sum of Positions: 19771373601
Attacker B: Results: 5034 (50.34%)  - Avg. Position: 3775637.72  - Sum of Positions: 19006560300  (TLD True Hits/Total Hits: 554/1752)

After 100000 entries:
Attacker A: Results: 49836 (49.84%)  - Avg. Position: 3863048.48  - Sum of Positions: 192518883957
Attacker B: Results: 49836 (49.84%)  - Avg. Position: 3717791.27  - Sum of Positions: 185279845598  (TLD True Hits/Total Hits: 5563/17663)

After 1000000 entries:
Attacker A: Results: 499849 (49.98%)  - Avg. Position: 3872050.19  - Sum of Positions: 1935440414332
Attacker B: Results: 499849 (49.98%)  - Avg. Position: 3733486.2  - Sum of Positions: 1866179344840  (TLD True Hits/Total Hits: 55924/177188)

After 5000000 entries:
Attacker A: Results: 2495070 (49.9%)  - Avg. Position: 3855105.79  - Sum of Positions: 9618758811996
Attacker B: Results: 2495070 (49.9%)  - Avg. Position: 3720355.98  - Sum of Positions: 9282548604385  (TLD True Hits/Total Hits: 278767/886328)

After 10000000 entries:
Attacker A: Results: 4990121 (49.9%)  - Avg. Position: 3852594.18  - Sum of Positions: 19224911119793
Attacker B: Results: 4990121 (49.9%)  - Avg. Position: 3717995.81  - Sum of Positions: 18553248959343  (TLD True Hits/Total Hits: 558988/1773853)

Attacker A (with miss): Results: 4990121 (49.9%)  - Avg. Position: 21889417.97  - Sum of Positions: 218894179731725
Attacker B (with miss): Results: 4990121 (49.9%)  - Avg. Position: 21869828.07  - Sum of Positions: 218698280657809  (TLD True Hits/Total Hits: 558988/1773853)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.2 TLDs:

com , 49593346 (excluded)
net , 4228117 (excluded)
uk , 1641584 
fr , 1287833 
edu , 1234499 (excluded)
br , 1202490 
it , 958969 
in , 899651 
org , 886288 (excluded)
nl , 731850 
ca , 709702 
au , 583209 
de , 456720 
es , 418306 
za , 368506 
ru , 341468 
ar , 248976 
mx , 225130 
us , 223507 
dk , 204390 
be , 199425 
gov , 184723 (excluded)
se , 167065 
pl , 157647 
nz , 142487 
cn , 141560 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 5712428.0  - Sum of Positions: 28562140
Attacker B: Results: 5 (50.0%)  - Avg. Position: 612777.8  - Sum of Positions: 3063889  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 4913234.96  - Sum of Positions: 240748513
Attacker B: Results: 49 (49.0%)  - Avg. Position: 3657957.06  - Sum of Positions: 179239896  (TLD True Hits/Total Hits: 7/13)

After 1000 entries:
Attacker A: Results: 482 (48.2%)  - Avg. Position: 4035215.64  - Sum of Positions: 1944973938
Attacker B: Results: 482 (48.2%)  - Avg. Position: 3617760.33  - Sum of Positions: 1743760481  (TLD True Hits/Total Hits: 65/165)

After 10000 entries:
Attacker A: Results: 4944 (49.44%)  - Avg. Position: 3811084.22  - Sum of Positions: 18842000398
Attacker B: Results: 4944 (49.44%)  - Avg. Position: 3677432.83  - Sum of Positions: 18181227930  (TLD True Hits/Total Hits: 540/1667)

After 100000 entries:
Attacker A: Results: 49924 (49.92%)  - Avg. Position: 3903613.35  - Sum of Positions: 194883992979
Attacker B: Results: 49924 (49.92%)  - Avg. Position: 3775545.26  - Sum of Positions: 188490321720  (TLD True Hits/Total Hits: 5235/16104)

After 1000000 entries:
Attacker A: Results: 499292 (49.93%)  - Avg. Position: 3873945.44  - Sum of Positions: 1934229968279
Attacker B: Results: 499292 (49.93%)  - Avg. Position: 3752488.23  - Sum of Positions: 1873587353354  (TLD True Hits/Total Hits: 52686/161108)

After 5000000 entries:
Attacker A: Results: 2493815 (49.88%)  - Avg. Position: 3865639.5  - Sum of Positions: 9640189772860
Attacker B: Results: 2493815 (49.88%)  - Avg. Position: 3742351.92  - Sum of Positions: 9332733360325  (TLD True Hits/Total Hits: 265153/808845)

After 10000000 entries:
Attacker A: Results: 4989926 (49.9%)  - Avg. Position: 3853119.49  - Sum of Positions: 19226781114046
Attacker B: Results: 4989926 (49.9%)  - Avg. Position: 3730558.58  - Sum of Positions: 18615211266416  (TLD True Hits/Total Hits: 529926/1617783)

Attacker A (with miss): Results: 4989926 (49.9%)  - Avg. Position: 21892479.36  - Sum of Positions: 218924793641802
Attacker B (with miss): Results: 4989926 (49.9%)  - Avg. Position: 21878223.51  - Sum of Positions: 218782235103105  (TLD True Hits/Total Hits: 529926/1617783)


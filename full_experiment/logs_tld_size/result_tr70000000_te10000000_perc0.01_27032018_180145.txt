Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.01 TLDs:

com , 49590155 (excluded)
net , 4229301 (excluded)
uk , 1642791 
fr , 1289003 
edu , 1234496 (excluded)
br , 1201077 
it , 960058 
in , 899363 
org , 886544 (excluded)
nl , 731289 
ca , 709833 
au , 582951 
de , 456677 
es , 418097 
za , 368198 
ru , 342196 
ar , 248688 
mx , 224517 
us , 223824 
dk , 205451 
be , 199262 
gov , 184926 (excluded)
se , 167414 
pl , 157494 
nz , 142397 
cn , 141798 
ch , 126204 
jp , 116769 
pt , 112677 
no , 112598 
mil , 102615 (excluded)
id , 102299 
cl , 102173 
sg , 81447 
gr , 74219 
ie , 73804 
il , 71688 
fi , 70251 
cz , 70022 
tr , 58641 
co , 56734 
my , 52263 
eu , 50441 (excluded)
hk , 49333 
at , 46423 
bg , 45135 
tw , 43352 
hu , 40880 
ph , 40744 
biz , 40040 (excluded)
ro , 37751 
ae , 36429 
hr , 28441 
pe , 27774 
vn , 27120 
kr , 26842 
info , 24765 (excluded)
ua , 22573 
lv , 21587 
pk , 20855 
sk , 19520 
uy , 17285 
sa , 15339 
tv , 14055 
rs , 13292 
th , 12997 
lt , 12624 
cc , 11574 
cat , 10954 
ee , 10568 
ma , 10501 
fm , 10438 
ke , 10335 
si , 10240 
zw , 10137 
is , 9930 
ve , 9814 
cr , 9210 
om , 9145 
nu , 9043 
lu , 8749 
lk , 8498 
tn , 8495 
ec , 7894 
con , 7254 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 190359.83  - Sum of Positions: 1142159
Attacker B: Results: 6 (60.0%)  - Avg. Position: 190359.83  - Sum of Positions: 1142159  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 54 (54.0%)  - Avg. Position: 2880647.35  - Sum of Positions: 155554957
Attacker B: Results: 54 (54.0%)  - Avg. Position: 2717250.63  - Sum of Positions: 146731534  (TLD True Hits/Total Hits: 5/17)

After 1000 entries:
Attacker A: Results: 497 (49.7%)  - Avg. Position: 3536981.59  - Sum of Positions: 1757879851
Attacker B: Results: 497 (49.7%)  - Avg. Position: 3530959.64  - Sum of Positions: 1754886940  (TLD True Hits/Total Hits: 60/190)

After 10000 entries:
Attacker A: Results: 5037 (50.37%)  - Avg. Position: 4087993.39  - Sum of Positions: 20591222716
Attacker B: Results: 5037 (50.37%)  - Avg. Position: 3967139.85  - Sum of Positions: 19982483406  (TLD True Hits/Total Hits: 613/1956)

After 100000 entries:
Attacker A: Results: 50107 (50.11%)  - Avg. Position: 3838746.31  - Sum of Positions: 192348061350
Attacker B: Results: 50107 (50.11%)  - Avg. Position: 3716488.47  - Sum of Positions: 186222087928  (TLD True Hits/Total Hits: 5871/19260)

After 1000000 entries:
Attacker A: Results: 498471 (49.85%)  - Avg. Position: 3843873.1  - Sum of Positions: 1916059267434
Attacker B: Results: 498471 (49.85%)  - Avg. Position: 3706482.33  - Sum of Positions: 1847573955718  (TLD True Hits/Total Hits: 57532/191597)

After 5000000 entries:
Attacker A: Results: 2494870 (49.9%)  - Avg. Position: 3856920.44  - Sum of Positions: 9622515107443
Attacker B: Results: 2494870 (49.9%)  - Avg. Position: 3715884.43  - Sum of Positions: 9270648583762  (TLD True Hits/Total Hits: 289217/956883)

After 10000000 entries:
Attacker A: Results: 4989493 (49.89%)  - Avg. Position: 3854216.58  - Sum of Positions: 19230586654264
Attacker B: Results: 4989493 (49.89%)  - Avg. Position: 3712576.04  - Sum of Positions: 18523872183553  (TLD True Hits/Total Hits: 579043/1913341)

Attacker A (with miss): Results: 4989493 (49.89%)  - Avg. Position: 21895150.01  - Sum of Positions: 218951500087204
Attacker B (with miss): Results: 4989493 (49.89%)  - Avg. Position: 21872342.51  - Sum of Positions: 218723425142029  (TLD True Hits/Total Hits: 579043/1913341)


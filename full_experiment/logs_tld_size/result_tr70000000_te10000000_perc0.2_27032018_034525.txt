Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.2 TLDs:

com , 49590242 (excluded)
net , 4229120 (excluded)
uk , 1641216 
fr , 1288966 
edu , 1236057 (excluded)
br , 1201421 
it , 958411 
in , 899546 
org , 887330 (excluded)
nl , 731328 
ca , 709314 
au , 582774 
de , 455676 
es , 418719 
za , 368034 
ru , 341952 
ar , 248263 
mx , 224668 
us , 224317 
dk , 204491 
be , 199699 
gov , 185187 (excluded)
se , 167722 
pl , 158103 
nz , 142413 
cn , 141987 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 144590.0  - Sum of Positions: 144590
Attacker B: Results: 1 (100.0%)  - Avg. Position: 144590.0  - Sum of Positions: 144590  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 8 (80.0%)  - Avg. Position: 3590248.5  - Sum of Positions: 28721988
Attacker B: Results: 8 (80.0%)  - Avg. Position: 3587283.25  - Sum of Positions: 28698266  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 47 (47.0%)  - Avg. Position: 3985400.6  - Sum of Positions: 187313828
Attacker B: Results: 47 (47.0%)  - Avg. Position: 3963300.51  - Sum of Positions: 186275124  (TLD True Hits/Total Hits: 7/19)

After 1000 entries:
Attacker A: Results: 511 (51.1%)  - Avg. Position: 3763471.14  - Sum of Positions: 1923133755
Attacker B: Results: 511 (51.1%)  - Avg. Position: 3764679.28  - Sum of Positions: 1923751110  (TLD True Hits/Total Hits: 59/181)

After 10000 entries:
Attacker A: Results: 5002 (50.02%)  - Avg. Position: 3918696.26  - Sum of Positions: 19601318717
Attacker B: Results: 5002 (50.02%)  - Avg. Position: 3803880.13  - Sum of Positions: 19027008408  (TLD True Hits/Total Hits: 569/1670)

After 100000 entries:
Attacker A: Results: 49730 (49.73%)  - Avg. Position: 3868362.82  - Sum of Positions: 192373683225
Attacker B: Results: 49730 (49.73%)  - Avg. Position: 3753683.17  - Sum of Positions: 186670664026  (TLD True Hits/Total Hits: 5370/16365)

After 1000000 entries:
Attacker A: Results: 499004 (49.9%)  - Avg. Position: 3863449.35  - Sum of Positions: 1927876677218
Attacker B: Results: 499004 (49.9%)  - Avg. Position: 3747970.68  - Sum of Positions: 1870252363555  (TLD True Hits/Total Hits: 52999/161744)

After 5000000 entries:
Attacker A: Results: 2495083 (49.9%)  - Avg. Position: 3858370.05  - Sum of Positions: 9626953531059
Attacker B: Results: 2495083 (49.9%)  - Avg. Position: 3735460.33  - Sum of Positions: 9320283561517  (TLD True Hits/Total Hits: 265642/808259)

After 10000000 entries:
Attacker A: Results: 4988946 (49.89%)  - Avg. Position: 3852831.25  - Sum of Positions: 19221567037113
Attacker B: Results: 4988946 (49.89%)  - Avg. Position: 3731244.37  - Sum of Positions: 18614976680180  (TLD True Hits/Total Hits: 530446/1615653)

Attacker A (with miss): Results: 4988946 (49.89%)  - Avg. Position: 21896366.27  - Sum of Positions: 218963662749097
Attacker B (with miss): Results: 4988946 (49.89%)  - Avg. Position: 21882614.62  - Sum of Positions: 218826146187888  (TLD True Hits/Total Hits: 530446/1615653)


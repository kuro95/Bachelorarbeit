Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.001 TLDs:

com , 49589783 (excluded)
net , 4227607 (excluded)
uk , 1641638 
fr , 1288498 
edu , 1233769 (excluded)
br , 1201724 
it , 959235 
in , 899841 
org , 886616 (excluded)
nl , 732110 
ca , 710044 
au , 584175 
de , 456389 
es , 418087 
za , 367854 
ru , 341832 
ar , 248845 
mx , 224852 
us , 224017 
dk , 205161 
be , 199504 
gov , 185039 (excluded)
se , 167366 
pl , 157786 
nz , 142661 
cn , 141849 
ch , 126360 
jp , 117317 
pt , 112840 
no , 112396 
id , 102671 
mil , 102503 (excluded)
cl , 101945 
sg , 82094 
gr , 74300 
ie , 73918 
il , 71795 
fi , 70000 
cz , 69893 
tr , 58793 
co , 56906 
my , 52432 
eu , 50608 (excluded)
hk , 49138 
at , 46123 
bg , 44882 
tw , 43277 
hu , 41009 
ph , 40818 
biz , 40139 (excluded)
ro , 37879 
ae , 36464 
hr , 28496 
pe , 27717 
vn , 27098 
kr , 26939 
info , 24887 (excluded)
ua , 22594 
lv , 21241 
pk , 20944 
sk , 19570 
uy , 17291 
sa , 15368 
tv , 13968 
rs , 13188 
th , 12898 
lt , 12756 
cc , 11696 
cat , 10981 
ee , 10533 
ma , 10481 
fm , 10462 
ke , 10196 
si , 10096 
zw , 10021 
is , 9981 
ve , 9760 
cr , 9183 
om , 9042 
nu , 8858 
lu , 8765 
lk , 8579 
tn , 8484 
ec , 7912 
con , 7160 (excluded)
cy , 6109 
int , 5850 (excluded)
jo , 5663 
lb , 5360 
me , 5334 
mk , 5324 
eg , 5301 
na , 5301 
qa , 5194 
ws , 5141 
bw , 4896 
cm , 4896 
do , 4627 
mu , 4549 
coop , 4288 (excluded)
by , 4123 
gt , 3808 
yu , 3802 
zm , 3706 
kz , 3691 
py , 3682 
ug , 3682 
aero , 3662 (excluded)
ge , 3589 
np , 3304 
mt , 3299 
bm , 3275 
ba , 3156 
pg , 3049 
tt , 3005 
ir , 2974 
name , 2842 (excluded)
mz , 2837 
bo , 2725 
bh , 2695 
sv , 2472 
ni , 2466 
fj , 2461 
tz , 2287 
pa , 2200 
cu , 2066 
md , 1961 
bd , 1860 
kw , 1778 
az , 1765 
as , 1695 
gh , 1692 
bz , 1584 
am , 1569 
dz , 1558 
ng , 1557 
cpm , 1430 (excluded)
nc , 1413 
al , 1410 
sn , 1406 
kh , 1384 
pf , 1376 
coom , 1373 (excluded)
mg , 1269 
ao , 1263 
hn , 1259 
ky , 1228 
jm , 1221 
ci , 1211 
ocm , 1194 (excluded)
et , 1184 
to , 1151 
comm , 1146 
ms , 1138 
mn , 1117 
im , 1112 
cim , 1110 (excluded)
fo , 1109 
asia , 1083 (excluded)
li , 1038 
sz , 1016 
an , 907 
vom , 891 (excluded)
mo , 881 
sy , 873 
travel , 864 (excluded)
mw , 855 
mc , 849 
gl , 840 
ccom , 829 (excluded)
ps , 820 
ls , 802 
ad , 776 
pr , 745 
pro , 744 (excluded)
ag , 713 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 4393120.8  - Sum of Positions: 21965604
Attacker B: Results: 5 (50.0%)  - Avg. Position: 442603.2  - Sum of Positions: 2213016  (TLD True Hits/Total Hits: 2/3)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 3639946.79  - Sum of Positions: 174717446
Attacker B: Results: 48 (48.0%)  - Avg. Position: 3251968.5  - Sum of Positions: 156094488  (TLD True Hits/Total Hits: 6/18)

After 1000 entries:
Attacker A: Results: 508 (50.8%)  - Avg. Position: 3903331.71  - Sum of Positions: 1982892508
Attacker B: Results: 508 (50.8%)  - Avg. Position: 3730474.52  - Sum of Positions: 1895081056  (TLD True Hits/Total Hits: 63/192)

After 10000 entries:
Attacker A: Results: 5009 (50.09%)  - Avg. Position: 3752223.63  - Sum of Positions: 18794888155
Attacker B: Results: 5009 (50.09%)  - Avg. Position: 3572840.12  - Sum of Positions: 17896356148  (TLD True Hits/Total Hits: 632/1966)

After 100000 entries:
Attacker A: Results: 49921 (49.92%)  - Avg. Position: 3919647.97  - Sum of Positions: 195672746493
Attacker B: Results: 49921 (49.92%)  - Avg. Position: 3764044.32  - Sum of Positions: 187904856581  (TLD True Hits/Total Hits: 6002/19845)

After 1000000 entries:
Attacker A: Results: 498796 (49.88%)  - Avg. Position: 3852270.06  - Sum of Positions: 1921496895844
Attacker B: Results: 498796 (49.88%)  - Avg. Position: 3709760.7  - Sum of Positions: 1850413799653  (TLD True Hits/Total Hits: 57928/193477)

After 5000000 entries:
Attacker A: Results: 2493668 (49.87%)  - Avg. Position: 3851919.77  - Sum of Positions: 9605409063201
Attacker B: Results: 2493668 (49.87%)  - Avg. Position: 3710225.0  - Sum of Positions: 9252069344673  (TLD True Hits/Total Hits: 290118/967847)

After 10000000 entries:
Attacker A: Results: 4986693 (49.87%)  - Avg. Position: 3851977.93  - Sum of Positions: 19208631388973
Attacker B: Results: 4986693 (49.87%)  - Avg. Position: 3708555.5  - Sum of Positions: 18493427748560  (TLD True Hits/Total Hits: 580565/1936376)

Attacker A (with miss): Results: 4986693 (49.87%)  - Avg. Position: 21904138.46  - Sum of Positions: 219041384610035
Attacker B (with miss): Results: 4986693 (49.87%)  - Avg. Position: 21880340.66  - Sum of Positions: 218803406603282  (TLD True Hits/Total Hits: 580565/1936376)


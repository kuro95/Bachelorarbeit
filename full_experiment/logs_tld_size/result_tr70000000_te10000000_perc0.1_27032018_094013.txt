Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49590068 (excluded)
net , 4229638 (excluded)
uk , 1640590 
fr , 1289723 
edu , 1234555 (excluded)
br , 1202955 
it , 959452 
in , 898969 
org , 885918 (excluded)
nl , 732251 
ca , 710130 
au , 583317 
de , 455642 
es , 417840 
za , 367969 
ru , 341573 
ar , 248802 
mx , 225038 
us , 223595 
dk , 205057 
be , 199287 
gov , 185200 (excluded)
se , 166945 
pl , 157319 
nz , 142571 
cn , 141908 
ch , 126073 
jp , 117050 
pt , 112965 
no , 112223 
mil , 102636 (excluded)
id , 102491 
cl , 101874 
sg , 81918 
gr , 74333 
ie , 73979 
il , 71589 
cz , 70051 
fi , 70018 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 161768.0  - Sum of Positions: 161768
Attacker B: Results: 1 (100.0%)  - Avg. Position: 161768.0  - Sum of Positions: 161768  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 1553700.17  - Sum of Positions: 9322201
Attacker B: Results: 6 (60.0%)  - Avg. Position: 1553700.17  - Sum of Positions: 9322201  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 7704399.08  - Sum of Positions: 369811156
Attacker B: Results: 48 (48.0%)  - Avg. Position: 7420469.69  - Sum of Positions: 356182545  (TLD True Hits/Total Hits: 2/12)

After 1000 entries:
Attacker A: Results: 493 (49.3%)  - Avg. Position: 4413640.77  - Sum of Positions: 2175924902
Attacker B: Results: 493 (49.3%)  - Avg. Position: 4149065.85  - Sum of Positions: 2045489466  (TLD True Hits/Total Hits: 66/176)

After 10000 entries:
Attacker A: Results: 4976 (49.76%)  - Avg. Position: 3576887.04  - Sum of Positions: 17798589910
Attacker B: Results: 4976 (49.76%)  - Avg. Position: 3432224.39  - Sum of Positions: 17078748577  (TLD True Hits/Total Hits: 590/1823)

After 100000 entries:
Attacker A: Results: 49886 (49.89%)  - Avg. Position: 3855020.2  - Sum of Positions: 192311537878
Attacker B: Results: 49886 (49.89%)  - Avg. Position: 3704851.48  - Sum of Positions: 184820220853  (TLD True Hits/Total Hits: 5667/17626)

After 1000000 entries:
Attacker A: Results: 499428 (49.94%)  - Avg. Position: 3853580.08  - Sum of Positions: 1924585791027
Attacker B: Results: 499428 (49.94%)  - Avg. Position: 3720192.81  - Sum of Positions: 1857968452863  (TLD True Hits/Total Hits: 56149/177187)

After 5000000 entries:
Attacker A: Results: 2494719 (49.89%)  - Avg. Position: 3850892.43  - Sum of Positions: 9606894522954
Attacker B: Results: 2494719 (49.89%)  - Avg. Position: 3715023.87  - Sum of Positions: 9267940645564  (TLD True Hits/Total Hits: 279032/886296)

After 10000000 entries:
Attacker A: Results: 4988337 (49.88%)  - Avg. Position: 3848997.02  - Sum of Positions: 19200094236921
Attacker B: Results: 4988337 (49.88%)  - Avg. Position: 3713486.06  - Sum of Positions: 18524119908364  (TLD True Hits/Total Hits: 559265/1773818)

Attacker A (with miss): Results: 4988337 (49.88%)  - Avg. Position: 21896434.49  - Sum of Positions: 218964344935720
Attacker B (with miss): Results: 4988337 (49.88%)  - Avg. Position: 21876379.48  - Sum of Positions: 218763794830590  (TLD True Hits/Total Hits: 559265/1773818)


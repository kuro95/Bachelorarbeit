Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1.0 TLDs:

com , 49588839 (excluded)
net , 4228915 (excluded)
uk , 1640965 
fr , 1288727 
edu , 1233637 (excluded)
br , 1203489 
it , 959600 
in , 899409 
org , 886735 (excluded)
nl , 732169 
ca , 709665 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 2777271.0  - Sum of Positions: 2777271
Attacker B: Results: 1 (100.0%)  - Avg. Position: 2777271.0  - Sum of Positions: 2777271  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 972346.67  - Sum of Positions: 2917040
Attacker B: Results: 3 (30.0%)  - Avg. Position: 1253890.67  - Sum of Positions: 3761672  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 3806481.06  - Sum of Positions: 197937015
Attacker B: Results: 52 (52.0%)  - Avg. Position: 3858698.88  - Sum of Positions: 200652342  (TLD True Hits/Total Hits: 6/18)

After 1000 entries:
Attacker A: Results: 514 (51.4%)  - Avg. Position: 3346743.82  - Sum of Positions: 1720226321
Attacker B: Results: 514 (51.4%)  - Avg. Position: 3345869.88  - Sum of Positions: 1719777116  (TLD True Hits/Total Hits: 54/137)

After 10000 entries:
Attacker A: Results: 4944 (49.44%)  - Avg. Position: 3802773.35  - Sum of Positions: 18800911457
Attacker B: Results: 4944 (49.44%)  - Avg. Position: 3761920.31  - Sum of Positions: 18598933998  (TLD True Hits/Total Hits: 388/1083)

After 100000 entries:
Attacker A: Results: 49906 (49.91%)  - Avg. Position: 3823461.79  - Sum of Positions: 190813684257
Attacker B: Results: 49906 (49.91%)  - Avg. Position: 3750217.01  - Sum of Positions: 187158329924  (TLD True Hits/Total Hits: 3942/10715)

After 1000000 entries:
Attacker A: Results: 499239 (49.92%)  - Avg. Position: 3850692.81  - Sum of Positions: 1922416026319
Attacker B: Results: 499239 (49.92%)  - Avg. Position: 3777161.47  - Sum of Positions: 1885706315753  (TLD True Hits/Total Hits: 38701/106166)

After 5000000 entries:
Attacker A: Results: 2493358 (49.87%)  - Avg. Position: 3847970.13  - Sum of Positions: 9594367111824
Attacker B: Results: 2493358 (49.87%)  - Avg. Position: 3773480.25  - Sum of Positions: 9408637162198  (TLD True Hits/Total Hits: 193361/530627)

After 10000000 entries:
Attacker A: Results: 4988535 (49.89%)  - Avg. Position: 3845291.29  - Sum of Positions: 19182370178335
Attacker B: Results: 4988535 (49.89%)  - Avg. Position: 3770230.91  - Sum of Positions: 18807928868886  (TLD True Hits/Total Hits: 386187/1060575)

Attacker A (with miss): Results: 4988535 (49.89%)  - Avg. Position: 21891407.72  - Sum of Positions: 218914077225945
Attacker B (with miss): Results: 4988535 (49.89%)  - Avg. Position: 21893261.21  - Sum of Positions: 218932612122565  (TLD True Hits/Total Hits: 386187/1060575)


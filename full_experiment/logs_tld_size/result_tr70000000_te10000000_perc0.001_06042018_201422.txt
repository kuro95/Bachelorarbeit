Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.001 TLDs:

com , 49591076 (excluded)
net , 4229892 (excluded)
uk , 1640548 
fr , 1289603 
edu , 1233388 (excluded)
br , 1203635 
it , 959490 
in , 898958 
org , 886493 (excluded)
nl , 731420 
ca , 709250 
au , 583927 
de , 456539 
es , 417408 
za , 367747 
ru , 341427 
ar , 249007 
mx , 224825 
us , 223624 
dk , 204625 
be , 199813 
gov , 185047 (excluded)
se , 167241 
pl , 158088 
nz , 141900 
cn , 141739 
ch , 125950 
jp , 116625 
pt , 113423 
no , 112043 
mil , 103055 (excluded)
id , 102328 
cl , 101992 
sg , 81640 
gr , 74111 
ie , 73985 
il , 71731 
cz , 70131 
fi , 69610 
tr , 58506 
co , 56592 
my , 52355 
eu , 50702 (excluded)
hk , 49293 
at , 46199 
bg , 45014 
tw , 43277 
hu , 41014 
ph , 40750 
biz , 40089 (excluded)
ro , 37717 
ae , 36626 
hr , 28489 
pe , 27780 
vn , 27294 
kr , 27037 
info , 24979 (excluded)
ua , 22768 
lv , 21315 
pk , 21162 
sk , 19495 
uy , 17345 
sa , 15390 
tv , 13878 
rs , 13128 
th , 13092 
lt , 12759 
cc , 11626 
cat , 10980 
ma , 10622 
fm , 10542 
ee , 10534 
ke , 10281 
si , 10253 
zw , 10087 
is , 9898 
ve , 9856 
om , 9194 
cr , 9156 
nu , 8869 
lu , 8724 
lk , 8543 
tn , 8432 
ec , 7805 
con , 7256 (excluded)
cy , 6170 
int , 5928 (excluded)
jo , 5657 
lb , 5491 
mk , 5360 
na , 5358 
eg , 5289 
me , 5221 
qa , 5172 
ws , 5145 
bw , 4862 
cm , 4858 
do , 4612 
mu , 4552 
coop , 4353 (excluded)
by , 4081 
gt , 3830 
yu , 3806 
zm , 3717 
py , 3713 
aero , 3644 (excluded)
kz , 3631 
ug , 3594 
ge , 3542 
np , 3328 
mt , 3307 
bm , 3223 
ba , 3140 
pg , 3029 
tt , 2989 
ir , 2943 
mz , 2893 
name , 2750 (excluded)
bo , 2727 
bh , 2667 
sv , 2458 
fj , 2456 
ni , 2391 
tz , 2329 
pa , 2184 
cu , 2091 
md , 1947 
bd , 1874 
kw , 1827 
az , 1760 
gh , 1699 
as , 1696 
bz , 1666 
am , 1558 
ng , 1530 
dz , 1516 
sn , 1462 
coom , 1427 (excluded)
kh , 1407 
pf , 1399 
cpm , 1380 (excluded)
al , 1372 
nc , 1370 
hn , 1247 
ao , 1241 
jm , 1229 
et , 1213 
mg , 1212 
ky , 1209 
ci , 1201 
to , 1193 
comm , 1192 
ocm , 1191 (excluded)
ms , 1142 
mn , 1134 
im , 1133 
fo , 1125 
cim , 1120 (excluded)
asia , 1098 (excluded)
li , 1009 
sz , 992 
an , 942 
vom , 927 (excluded)
mo , 907 
travel , 880 (excluded)
mc , 865 
gl , 861 
ps , 849 
mw , 837 
sy , 801 
ccom , 793 (excluded)
ag , 780 
ls , 757 
pr , 754 
pro , 740 (excluded)
ad , 726 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 5166074.25  - Sum of Positions: 20664297
Attacker B: Results: 4 (40.0%)  - Avg. Position: 5166074.25  - Sum of Positions: 20664297  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 1712704.38  - Sum of Positions: 77071697
Attacker B: Results: 45 (45.0%)  - Avg. Position: 1763361.98  - Sum of Positions: 79351289  (TLD True Hits/Total Hits: 4/19)

After 1000 entries:
Attacker A: Results: 475 (47.5%)  - Avg. Position: 3864680.76  - Sum of Positions: 1835723363
Attacker B: Results: 475 (47.5%)  - Avg. Position: 3766412.14  - Sum of Positions: 1789045766  (TLD True Hits/Total Hits: 49/169)

After 10000 entries:
Attacker A: Results: 5053 (50.53%)  - Avg. Position: 3832307.3  - Sum of Positions: 19364648794
Attacker B: Results: 5053 (50.53%)  - Avg. Position: 3709834.71  - Sum of Positions: 18745794781  (TLD True Hits/Total Hits: 593/1909)

After 100000 entries:
Attacker A: Results: 49790 (49.79%)  - Avg. Position: 3875254.43  - Sum of Positions: 192948918071
Attacker B: Results: 49790 (49.79%)  - Avg. Position: 3715443.79  - Sum of Positions: 184991946396  (TLD True Hits/Total Hits: 5841/19370)

After 1000000 entries:
Attacker A: Results: 498237 (49.82%)  - Avg. Position: 3861522.93  - Sum of Positions: 1923953598737
Attacker B: Results: 498237 (49.82%)  - Avg. Position: 3718037.67  - Sum of Positions: 1852463935801  (TLD True Hits/Total Hits: 58073/193976)

After 5000000 entries:
Attacker A: Results: 2496500 (49.93%)  - Avg. Position: 3855721.09  - Sum of Positions: 9625807689131
Attacker B: Results: 2496500 (49.93%)  - Avg. Position: 3714105.19  - Sum of Positions: 9272263606500  (TLD True Hits/Total Hits: 290521/968953)

After 10000000 entries:
Attacker A: Results: 4992351 (49.92%)  - Avg. Position: 3851903.81  - Sum of Positions: 19230055862304
Attacker B: Results: 4992351 (49.92%)  - Avg. Position: 3708476.95  - Sum of Positions: 18514018617374  (TLD True Hits/Total Hits: 581490/1939480)

Attacker A (with miss): Results: 4992351 (49.92%)  - Avg. Position: 21881103.35  - Sum of Positions: 218811033478329
Attacker B (with miss): Results: 4992351 (49.92%)  - Avg. Position: 21857341.01  - Sum of Positions: 218573410074579  (TLD True Hits/Total Hits: 581490/1939480)


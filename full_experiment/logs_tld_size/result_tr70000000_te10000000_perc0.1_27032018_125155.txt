Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49589681 (excluded)
net , 4227270 (excluded)
uk , 1642013 
fr , 1288564 
edu , 1232756 (excluded)
br , 1202527 
it , 959593 
in , 900121 
org , 886523 (excluded)
nl , 731784 
ca , 709791 
au , 583608 
de , 456662 
es , 418025 
za , 367996 
ru , 342478 
ar , 248559 
mx , 224722 
us , 223836 
dk , 204468 
be , 199416 
gov , 185326 (excluded)
se , 167306 
pl , 157910 
nz , 142124 
cn , 141981 
ch , 126336 
jp , 117180 
pt , 113103 
no , 112344 
mil , 102791 (excluded)
id , 102463 
cl , 102176 
sg , 81849 
gr , 74548 
ie , 74005 
il , 71789 
cz , 70335 
fi , 70050 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 786751.0  - Sum of Positions: 786751
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1066306.0  - Sum of Positions: 1066306  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 2065256.14  - Sum of Positions: 14456793
Attacker B: Results: 7 (70.0%)  - Avg. Position: 1965350.29  - Sum of Positions: 13757452  (TLD True Hits/Total Hits: 2/4)

After 100 entries:
Attacker A: Results: 62 (62.0%)  - Avg. Position: 5279329.45  - Sum of Positions: 327318426
Attacker B: Results: 62 (62.0%)  - Avg. Position: 5293248.37  - Sum of Positions: 328181399  (TLD True Hits/Total Hits: 10/16)

After 1000 entries:
Attacker A: Results: 516 (51.6%)  - Avg. Position: 4554666.61  - Sum of Positions: 2350207972
Attacker B: Results: 516 (51.6%)  - Avg. Position: 4350145.81  - Sum of Positions: 2244675240  (TLD True Hits/Total Hits: 77/178)

After 10000 entries:
Attacker A: Results: 5025 (50.25%)  - Avg. Position: 3819639.66  - Sum of Positions: 19193689306
Attacker B: Results: 5025 (50.25%)  - Avg. Position: 3685760.92  - Sum of Positions: 18520948616  (TLD True Hits/Total Hits: 573/1764)

After 100000 entries:
Attacker A: Results: 49775 (49.78%)  - Avg. Position: 3952004.96  - Sum of Positions: 196711046670
Attacker B: Results: 49775 (49.78%)  - Avg. Position: 3820308.36  - Sum of Positions: 190155848694  (TLD True Hits/Total Hits: 5641/17824)

After 1000000 entries:
Attacker A: Results: 498353 (49.84%)  - Avg. Position: 3859170.89  - Sum of Positions: 1923229390327
Attacker B: Results: 498353 (49.84%)  - Avg. Position: 3724432.21  - Sum of Positions: 1856081965443  (TLD True Hits/Total Hits: 55706/176896)

After 5000000 entries:
Attacker A: Results: 2492991 (49.86%)  - Avg. Position: 3856100.73  - Sum of Positions: 9613224402929
Attacker B: Results: 2492991 (49.86%)  - Avg. Position: 3722286.03  - Sum of Positions: 9279625565608  (TLD True Hits/Total Hits: 279242/885532)

After 10000000 entries:
Attacker A: Results: 4987560 (49.88%)  - Avg. Position: 3854382.78  - Sum of Positions: 19223965369763
Attacker B: Results: 4987560 (49.88%)  - Avg. Position: 3719422.44  - Sum of Positions: 18550842608482  (TLD True Hits/Total Hits: 559326/1773719)

Attacker A (with miss): Results: 4987560 (49.88%)  - Avg. Position: 21899503.73  - Sum of Positions: 218995037253963
Attacker B (with miss): Results: 4987560 (49.88%)  - Avg. Position: 21879782.27  - Sum of Positions: 218797822675930  (TLD True Hits/Total Hits: 559326/1773719)


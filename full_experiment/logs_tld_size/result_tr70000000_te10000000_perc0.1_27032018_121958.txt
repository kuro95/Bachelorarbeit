Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49588352 (excluded)
net , 4228982 (excluded)
uk , 1641377 
fr , 1289161 
edu , 1234768 (excluded)
br , 1202218 
it , 959838 
in , 899078 
org , 886522 (excluded)
nl , 731122 
ca , 709194 
au , 583417 
de , 456947 
es , 418158 
za , 367741 
ru , 341922 
ar , 248953 
mx , 224753 
us , 224190 
dk , 204857 
be , 199448 
gov , 184814 (excluded)
se , 167314 
pl , 157361 
nz , 142823 
cn , 141986 
ch , 126040 
jp , 116816 
pt , 113344 
no , 112201 
mil , 102804 (excluded)
id , 102660 
cl , 102377 
sg , 81796 
gr , 74371 
ie , 74053 
il , 71584 
cz , 70287 
fi , 70046 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 5986.0  - Sum of Positions: 5986
Attacker B: Results: 1 (100.0%)  - Avg. Position: 5986.0  - Sum of Positions: 5986  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 2 (20.0%)  - Avg. Position: 607563.0  - Sum of Positions: 1215126
Attacker B: Results: 2 (20.0%)  - Avg. Position: 607563.0  - Sum of Positions: 1215126  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 51 (51.0%)  - Avg. Position: 5347414.39  - Sum of Positions: 272718134
Attacker B: Results: 51 (51.0%)  - Avg. Position: 5346728.76  - Sum of Positions: 272683167  (TLD True Hits/Total Hits: 3/15)

After 1000 entries:
Attacker A: Results: 477 (47.7%)  - Avg. Position: 4316087.76  - Sum of Positions: 2058773863
Attacker B: Results: 477 (47.7%)  - Avg. Position: 4244125.88  - Sum of Positions: 2024448044  (TLD True Hits/Total Hits: 54/185)

After 10000 entries:
Attacker A: Results: 5000 (50.0%)  - Avg. Position: 4034739.14  - Sum of Positions: 20173695702
Attacker B: Results: 5000 (50.0%)  - Avg. Position: 3874942.85  - Sum of Positions: 19374714256  (TLD True Hits/Total Hits: 565/1778)

After 100000 entries:
Attacker A: Results: 49820 (49.82%)  - Avg. Position: 3848794.92  - Sum of Positions: 191746963157
Attacker B: Results: 49820 (49.82%)  - Avg. Position: 3706516.48  - Sum of Positions: 184658650850  (TLD True Hits/Total Hits: 5628/17770)

After 1000000 entries:
Attacker A: Results: 498598 (49.86%)  - Avg. Position: 3838106.46  - Sum of Positions: 1913672206866
Attacker B: Results: 498598 (49.86%)  - Avg. Position: 3702662.82  - Sum of Positions: 1846140277529  (TLD True Hits/Total Hits: 55688/176989)

After 5000000 entries:
Attacker A: Results: 2494666 (49.89%)  - Avg. Position: 3851465.37  - Sum of Positions: 9608119708301
Attacker B: Results: 2494666 (49.89%)  - Avg. Position: 3715657.6  - Sum of Positions: 9269324685994  (TLD True Hits/Total Hits: 279743/887367)

After 10000000 entries:
Attacker A: Results: 4989643 (49.9%)  - Avg. Position: 3848987.82  - Sum of Positions: 19205075138017
Attacker B: Results: 4989643 (49.9%)  - Avg. Position: 3713697.58  - Sum of Positions: 18530025125272  (TLD True Hits/Total Hits: 560350/1775810)

Attacker A (with miss): Results: 4989643 (49.9%)  - Avg. Position: 21890564.98  - Sum of Positions: 218905649824795
Attacker B (with miss): Results: 4989643 (49.9%)  - Avg. Position: 21870683.84  - Sum of Positions: 218706838406552  (TLD True Hits/Total Hits: 560350/1775810)


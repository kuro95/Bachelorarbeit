Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1.0 TLDs:

com , 49590740 (excluded)
net , 4230727 (excluded)
uk , 1641337 
fr , 1289414 
edu , 1234367 (excluded)
br , 1201438 
it , 957672 
in , 901066 
org , 886576 (excluded)
nl , 732152 
ca , 708395 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 7613007.33  - Sum of Positions: 22839022
Attacker B: Results: 3 (30.0%)  - Avg. Position: 7613007.33  - Sum of Positions: 22839022  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 55 (55.0%)  - Avg. Position: 1481199.84  - Sum of Positions: 81465991
Attacker B: Results: 55 (55.0%)  - Avg. Position: 1513003.62  - Sum of Positions: 83215199  (TLD True Hits/Total Hits: 2/7)

After 1000 entries:
Attacker A: Results: 488 (48.8%)  - Avg. Position: 3820713.53  - Sum of Positions: 1864508201
Attacker B: Results: 488 (48.8%)  - Avg. Position: 3855016.55  - Sum of Positions: 1881248078  (TLD True Hits/Total Hits: 32/100)

After 10000 entries:
Attacker A: Results: 4975 (49.75%)  - Avg. Position: 3944429.7  - Sum of Positions: 19623537766
Attacker B: Results: 4975 (49.75%)  - Avg. Position: 3878595.76  - Sum of Positions: 19296013920  (TLD True Hits/Total Hits: 386/1012)

After 100000 entries:
Attacker A: Results: 49825 (49.83%)  - Avg. Position: 3850148.96  - Sum of Positions: 191833671901
Attacker B: Results: 49825 (49.83%)  - Avg. Position: 3771122.29  - Sum of Positions: 187896167917  (TLD True Hits/Total Hits: 3891/10636)

After 1000000 entries:
Attacker A: Results: 498548 (49.85%)  - Avg. Position: 3857413.98  - Sum of Positions: 1923106023717
Attacker B: Results: 498548 (49.85%)  - Avg. Position: 3780303.55  - Sum of Positions: 1884662775204  (TLD True Hits/Total Hits: 38987/106372)

After 5000000 entries:
Attacker A: Results: 2496631 (49.93%)  - Avg. Position: 3857969.34  - Sum of Positions: 9631925842174
Attacker B: Results: 2496631 (49.93%)  - Avg. Position: 3781195.68  - Sum of Positions: 9440250361742  (TLD True Hits/Total Hits: 193608/530787)

After 10000000 entries:
Attacker A: Results: 4989568 (49.9%)  - Avg. Position: 3856797.26  - Sum of Positions: 19243752175348
Attacker B: Results: 4989568 (49.9%)  - Avg. Position: 3779835.31  - Sum of Positions: 18859745327177  (TLD True Hits/Total Hits: 386978/1061193)

Attacker A (with miss): Results: 4989568 (49.9%)  - Avg. Position: 21895268.24  - Sum of Positions: 218952682351348
Attacker B (with miss): Results: 4989568 (49.9%)  - Avg. Position: 21896182.65  - Sum of Positions: 218961826487377  (TLD True Hits/Total Hits: 386978/1061193)


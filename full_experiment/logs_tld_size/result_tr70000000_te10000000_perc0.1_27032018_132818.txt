Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.1 TLDs:

com , 49589082 (excluded)
net , 4227596 (excluded)
uk , 1641378 
fr , 1289154 
edu , 1233946 (excluded)
br , 1201720 
it , 959592 
in , 900048 
org , 886558 (excluded)
nl , 731863 
ca , 709209 
au , 583568 
de , 456317 
es , 418600 
za , 368526 
ru , 342082 
ar , 249502 
mx , 224532 
us , 224063 
dk , 204513 
be , 200019 
gov , 184958 (excluded)
se , 166917 
pl , 157320 
nz , 142045 
cn , 141724 
ch , 125911 
jp , 117304 
pt , 113323 
no , 112610 
mil , 102871 (excluded)
id , 102436 
cl , 102033 
sg , 82159 
gr , 74524 
ie , 73852 
il , 71789 
cz , 70148 
fi , 70029 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 1646757.5  - Sum of Positions: 6587030
Attacker B: Results: 4 (40.0%)  - Avg. Position: 1646757.5  - Sum of Positions: 6587030  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 46 (46.0%)  - Avg. Position: 2563300.02  - Sum of Positions: 117911801
Attacker B: Results: 46 (46.0%)  - Avg. Position: 2557287.48  - Sum of Positions: 117635224  (TLD True Hits/Total Hits: 4/13)

After 1000 entries:
Attacker A: Results: 475 (47.5%)  - Avg. Position: 4343377.42  - Sum of Positions: 2063104274
Attacker B: Results: 475 (47.5%)  - Avg. Position: 4197647.69  - Sum of Positions: 1993882653  (TLD True Hits/Total Hits: 45/146)

After 10000 entries:
Attacker A: Results: 4997 (49.97%)  - Avg. Position: 3967667.69  - Sum of Positions: 19826435422
Attacker B: Results: 4997 (49.97%)  - Avg. Position: 3800351.52  - Sum of Positions: 18990356541  (TLD True Hits/Total Hits: 563/1756)

After 100000 entries:
Attacker A: Results: 49922 (49.92%)  - Avg. Position: 3872970.5  - Sum of Positions: 193346433095
Attacker B: Results: 49922 (49.92%)  - Avg. Position: 3737421.94  - Sum of Positions: 186579578153  (TLD True Hits/Total Hits: 5713/17755)

After 1000000 entries:
Attacker A: Results: 499685 (49.97%)  - Avg. Position: 3837582.84  - Sum of Positions: 1917582582934
Attacker B: Results: 499685 (49.97%)  - Avg. Position: 3702001.48  - Sum of Positions: 1849834610237  (TLD True Hits/Total Hits: 56383/177636)

After 5000000 entries:
Attacker A: Results: 2494214 (49.88%)  - Avg. Position: 3842834.26  - Sum of Positions: 9584850998625
Attacker B: Results: 2494214 (49.88%)  - Avg. Position: 3705221.76  - Sum of Positions: 9241615996454  (TLD True Hits/Total Hits: 281218/888030)

After 10000000 entries:
Attacker A: Results: 4986101 (49.86%)  - Avg. Position: 3847861.12  - Sum of Positions: 19185824178313
Attacker B: Results: 4986101 (49.86%)  - Avg. Position: 3710448.11  - Sum of Positions: 18500669047478  (TLD True Hits/Total Hits: 561088/1774771)

Attacker A (with miss): Results: 4986101 (49.86%)  - Avg. Position: 21901395.66  - Sum of Positions: 219013956571675
Attacker B (with miss): Results: 4986101 (49.86%)  - Avg. Position: 21880522.1  - Sum of Positions: 218805220958188  (TLD True Hits/Total Hits: 561088/1774771)


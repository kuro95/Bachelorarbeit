Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.01 TLDs:

com , 49593312 (excluded)
net , 4229972 (excluded)
uk , 1641687 
fr , 1287272 
edu , 1233562 (excluded)
br , 1201784 
it , 959983 
in , 899791 
org , 887231 (excluded)
nl , 731573 
ca , 708863 
au , 583098 
de , 455477 
es , 417797 
za , 367390 
ru , 341870 
ar , 248564 
mx , 224665 
us , 224241 
dk , 204553 
be , 199401 
gov , 184660 (excluded)
se , 167102 
pl , 157907 
nz , 142190 
cn , 141871 
ch , 125825 
jp , 116921 
pt , 113461 
no , 112288 
mil , 102765 (excluded)
id , 102575 
cl , 101944 
sg , 82015 
gr , 74235 
ie , 73941 
il , 71768 
cz , 70262 
fi , 69993 
tr , 58738 
co , 56864 
my , 52409 
eu , 50633 (excluded)
hk , 49265 
at , 45917 
bg , 44761 
tw , 43225 
hu , 40838 
ph , 40646 
biz , 40059 (excluded)
ro , 37797 
ae , 36622 
hr , 28495 
pe , 27685 
vn , 27011 
kr , 26864 
info , 24893 (excluded)
ua , 22746 
lv , 21288 
pk , 21030 
sk , 19589 
uy , 17376 
sa , 15323 
tv , 13972 
rs , 13308 
th , 12985 
lt , 12741 
cc , 11763 
cat , 10994 
ee , 10568 
ma , 10559 
fm , 10474 
ke , 10307 
si , 10142 
zw , 10101 
is , 9874 
ve , 9741 
cr , 9179 
om , 9179 
nu , 9011 
lu , 8838 
lk , 8624 
tn , 8394 
ec , 7908 
con , 7284 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 4011194.0  - Sum of Positions: 16044776
Attacker B: Results: 4 (40.0%)  - Avg. Position: 4011194.0  - Sum of Positions: 16044776  (TLD True Hits/Total Hits: 0/2)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 1869986.25  - Sum of Positions: 89759340
Attacker B: Results: 48 (48.0%)  - Avg. Position: 1895379.25  - Sum of Positions: 90978204  (TLD True Hits/Total Hits: 1/10)

After 1000 entries:
Attacker A: Results: 525 (52.5%)  - Avg. Position: 3618544.1  - Sum of Positions: 1899735650
Attacker B: Results: 525 (52.5%)  - Avg. Position: 3589761.82  - Sum of Positions: 1884624958  (TLD True Hits/Total Hits: 50/180)

After 10000 entries:
Attacker A: Results: 4973 (49.73%)  - Avg. Position: 3776014.17  - Sum of Positions: 18778118460
Attacker B: Results: 4973 (49.73%)  - Avg. Position: 3626295.79  - Sum of Positions: 18033568986  (TLD True Hits/Total Hits: 581/1969)

After 100000 entries:
Attacker A: Results: 49663 (49.66%)  - Avg. Position: 3764643.68  - Sum of Positions: 186963498993
Attacker B: Results: 49663 (49.66%)  - Avg. Position: 3624934.9  - Sum of Positions: 180025141700  (TLD True Hits/Total Hits: 5871/19412)

After 1000000 entries:
Attacker A: Results: 498287 (49.83%)  - Avg. Position: 3838419.59  - Sum of Positions: 1912634582602
Attacker B: Results: 498287 (49.83%)  - Avg. Position: 3689838.46  - Sum of Positions: 1838598534350  (TLD True Hits/Total Hits: 58272/191886)

After 5000000 entries:
Attacker A: Results: 2493903 (49.88%)  - Avg. Position: 3842378.43  - Sum of Positions: 9582519105332
Attacker B: Results: 2493903 (49.88%)  - Avg. Position: 3698958.69  - Sum of Positions: 9224844180775  (TLD True Hits/Total Hits: 290192/957550)

After 10000000 entries:
Attacker A: Results: 4988074 (49.88%)  - Avg. Position: 3846036.27  - Sum of Positions: 19184313508330
Attacker B: Results: 4988074 (49.88%)  - Avg. Position: 3702196.37  - Sum of Positions: 18466829459115  (TLD True Hits/Total Hits: 579410/1912596)

Attacker A (with miss): Results: 4988074 (49.88%)  - Avg. Position: 21894416.69  - Sum of Positions: 218944166945434
Attacker B (with miss): Results: 4988074 (49.88%)  - Avg. Position: 21870546.5  - Sum of Positions: 218705464973656  (TLD True Hits/Total Hits: 579410/1912596)


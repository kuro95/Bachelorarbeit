Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1.0 TLDs:

com , 49589998 (excluded)
net , 4230496 (excluded)
uk , 1641889 
fr , 1289482 
edu , 1235299 (excluded)
br , 1201449 
it , 959312 
in , 898917 
org , 886232 (excluded)
nl , 731240 
ca , 709679 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 5571513.4  - Sum of Positions: 27857567
Attacker B: Results: 5 (50.0%)  - Avg. Position: 5571513.4  - Sum of Positions: 27857567  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 45 (45.0%)  - Avg. Position: 3023019.56  - Sum of Positions: 136035880
Attacker B: Results: 45 (45.0%)  - Avg. Position: 3035323.31  - Sum of Positions: 136589549  (TLD True Hits/Total Hits: 3/7)

After 1000 entries:
Attacker A: Results: 512 (51.2%)  - Avg. Position: 4231249.61  - Sum of Positions: 2166399800
Attacker B: Results: 512 (51.2%)  - Avg. Position: 4028355.23  - Sum of Positions: 2062517879  (TLD True Hits/Total Hits: 30/98)

After 10000 entries:
Attacker A: Results: 5019 (50.19%)  - Avg. Position: 3771729.45  - Sum of Positions: 18930310128
Attacker B: Results: 5019 (50.19%)  - Avg. Position: 3677270.28  - Sum of Positions: 18456219540  (TLD True Hits/Total Hits: 395/1053)

After 100000 entries:
Attacker A: Results: 49901 (49.9%)  - Avg. Position: 3838889.48  - Sum of Positions: 191564424138
Attacker B: Results: 49901 (49.9%)  - Avg. Position: 3769281.44  - Sum of Positions: 188090913218  (TLD True Hits/Total Hits: 3829/10607)

After 1000000 entries:
Attacker A: Results: 498974 (49.9%)  - Avg. Position: 3869159.52  - Sum of Positions: 1930610001473
Attacker B: Results: 498974 (49.9%)  - Avg. Position: 3799089.97  - Sum of Positions: 1895647116634  (TLD True Hits/Total Hits: 38395/105673)

After 5000000 entries:
Attacker A: Results: 2493786 (49.88%)  - Avg. Position: 3855136.7  - Sum of Positions: 9613885926655
Attacker B: Results: 2493786 (49.88%)  - Avg. Position: 3779977.65  - Sum of Positions: 9426455331682  (TLD True Hits/Total Hits: 193309/529811)

After 10000000 entries:
Attacker A: Results: 4987642 (49.88%)  - Avg. Position: 3849497.37  - Sum of Positions: 19199914748200
Attacker B: Results: 4987642 (49.88%)  - Avg. Position: 3773408.52  - Sum of Positions: 18820410821402  (TLD True Hits/Total Hits: 387180/1061027)

Attacker A (with miss): Results: 4987642 (49.88%)  - Avg. Position: 21898268.54  - Sum of Positions: 218982685418878
Attacker B (with miss): Results: 4987642 (49.88%)  - Avg. Position: 21899635.53  - Sum of Positions: 218996355337788  (TLD True Hits/Total Hits: 387180/1061027)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.001 TLDs:

com , 49591789 (excluded)
net , 4228697 (excluded)
uk , 1641601 
fr , 1288385 
edu , 1233767 (excluded)
br , 1201608 
it , 959064 
in , 899758 
org , 886438 (excluded)
nl , 731540 
ca , 709001 
au , 583543 
de , 457045 
es , 418053 
za , 367652 
ru , 342153 
ar , 248808 
mx , 224423 
us , 223826 
dk , 204130 
be , 199628 
gov , 184585 (excluded)
se , 167153 
pl , 157513 
nz , 142462 
cn , 142050 
ch , 126175 
jp , 116626 
pt , 113195 
no , 112357 
mil , 103043 (excluded)
id , 102556 
cl , 102256 
sg , 81983 
gr , 74298 
ie , 74155 
il , 71685 
cz , 70210 
fi , 70200 
tr , 58654 
co , 57024 
my , 52355 
eu , 50868 (excluded)
hk , 49250 
at , 46352 
bg , 45256 
tw , 43344 
hu , 41073 
ph , 40801 
biz , 39937 (excluded)
ro , 37832 
ae , 36725 
hr , 28397 
pe , 27774 
vn , 27310 
kr , 26829 
info , 24750 (excluded)
ua , 22623 
lv , 21520 
pk , 21090 
sk , 19445 
uy , 17286 
sa , 15495 
tv , 13886 
rs , 13289 
th , 12888 
lt , 12701 
cc , 11490 
cat , 10872 
ee , 10602 
fm , 10540 
ma , 10513 
ke , 10270 
si , 10179 
zw , 10102 
is , 9921 
ve , 9790 
cr , 9179 
om , 9125 
nu , 8997 
lu , 8733 
lk , 8576 
tn , 8416 
ec , 7895 
con , 7138 (excluded)
cy , 6241 
int , 5857 (excluded)
jo , 5665 
lb , 5435 
na , 5337 
mk , 5260 
eg , 5250 
qa , 5238 
me , 5195 
ws , 5011 
cm , 4895 
bw , 4860 
do , 4577 
mu , 4558 
coop , 4370 (excluded)
by , 4106 
yu , 3898 
gt , 3830 
zm , 3705 
kz , 3693 
py , 3612 
aero , 3608 (excluded)
ug , 3606 
ge , 3577 
mt , 3322 
np , 3255 
bm , 3182 
ba , 3124 
tt , 3027 
pg , 3011 
ir , 2953 
mz , 2877 
name , 2764 (excluded)
bh , 2761 
bo , 2683 
fj , 2496 
sv , 2467 
ni , 2425 
pa , 2298 
tz , 2263 
cu , 2068 
md , 1928 
bd , 1837 
kw , 1783 
az , 1756 
gh , 1703 
as , 1684 
bz , 1616 
dz , 1551 
am , 1533 
ng , 1520 
sn , 1444 
cpm , 1428 (excluded)
kh , 1402 
pf , 1377 
coom , 1375 (excluded)
nc , 1364 
al , 1344 
hn , 1282 
ky , 1256 
jm , 1255 
mg , 1246 
ao , 1235 
ocm , 1220 (excluded)
et , 1189 
mn , 1184 
ci , 1180 
ms , 1179 
comm , 1162 
to , 1122 
cim , 1118 (excluded)
im , 1113 
asia , 1085 (excluded)
fo , 1080 
li , 1018 
sz , 981 
an , 910 
vom , 898 (excluded)
mo , 876 
mc , 867 
ps , 861 
travel , 852 (excluded)
sy , 851 
gl , 825 
mw , 821 
ccom , 812 (excluded)
ad , 775 
ls , 762 
ag , 757 
pr , 726 
pro , 711 (excluded)

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 4018404.0  - Sum of Positions: 4018404
Attacker B: Results: 1 (100.0%)  - Avg. Position: 56572.0  - Sum of Positions: 56572  (TLD True Hits/Total Hits: 1/1)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 16470269.2  - Sum of Positions: 82351346
Attacker B: Results: 5 (50.0%)  - Avg. Position: 15677902.8  - Sum of Positions: 78389514  (TLD True Hits/Total Hits: 1/2)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 6314150.23  - Sum of Positions: 359906563
Attacker B: Results: 57 (57.0%)  - Avg. Position: 5390021.09  - Sum of Positions: 307231202  (TLD True Hits/Total Hits: 11/31)

After 1000 entries:
Attacker A: Results: 535 (53.5%)  - Avg. Position: 4393563.06  - Sum of Positions: 2350556236
Attacker B: Results: 535 (53.5%)  - Avg. Position: 4171650.12  - Sum of Positions: 2231832813  (TLD True Hits/Total Hits: 78/221)

After 10000 entries:
Attacker A: Results: 4982 (49.82%)  - Avg. Position: 3777826.83  - Sum of Positions: 18821133274
Attacker B: Results: 4982 (49.82%)  - Avg. Position: 3635646.02  - Sum of Positions: 18112788482  (TLD True Hits/Total Hits: 608/1959)

After 100000 entries:
Attacker A: Results: 49930 (49.93%)  - Avg. Position: 3863760.94  - Sum of Positions: 192917583551
Attacker B: Results: 49930 (49.93%)  - Avg. Position: 3720464.96  - Sum of Positions: 185762815537  (TLD True Hits/Total Hits: 5787/19340)

After 1000000 entries:
Attacker A: Results: 499079 (49.91%)  - Avg. Position: 3858462.24  - Sum of Positions: 1925677475298
Attacker B: Results: 499079 (49.91%)  - Avg. Position: 3712480.48  - Sum of Positions: 1852821046913  (TLD True Hits/Total Hits: 58050/193485)

After 5000000 entries:
Attacker A: Results: 2494545 (49.89%)  - Avg. Position: 3848127.5  - Sum of Positions: 9599327212462
Attacker B: Results: 2494545 (49.89%)  - Avg. Position: 3701195.48  - Sum of Positions: 9232798671869  (TLD True Hits/Total Hits: 290835/970026)

After 10000000 entries:
Attacker A: Results: 4988188 (49.88%)  - Avg. Position: 3855385.56  - Sum of Positions: 19231388006922
Attacker B: Results: 4988188 (49.88%)  - Avg. Position: 3709929.66  - Sum of Positions: 18505826603759  (TLD True Hits/Total Hits: 582220/1939807)

Attacker A (with miss): Results: 4988188 (49.88%)  - Avg. Position: 21898905.33  - Sum of Positions: 218989053308610
Attacker B (with miss): Results: 4988188 (49.88%)  - Avg. Position: 21874159.59  - Sum of Positions: 218741595895980  (TLD True Hits/Total Hits: 582220/1939807)


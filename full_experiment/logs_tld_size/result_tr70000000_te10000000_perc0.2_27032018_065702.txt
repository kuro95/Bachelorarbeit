Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 0.2 TLDs:

com , 49591250 (excluded)
net , 4228157 (excluded)
uk , 1641165 
fr , 1288448 
edu , 1234864 (excluded)
br , 1201932 
it , 959169 
in , 900171 
org , 884957 (excluded)
nl , 732406 
ca , 709225 
au , 582789 
de , 456145 
es , 418572 
za , 367797 
ru , 342133 
ar , 248844 
mx , 224805 
us , 223981 
dk , 204765 
be , 199878 
gov , 185025 (excluded)
se , 167145 
pl , 157638 
nz , 142196 
cn , 141914 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 12690236.0  - Sum of Positions: 50760944
Attacker B: Results: 4 (40.0%)  - Avg. Position: 12760033.75  - Sum of Positions: 51040135  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 44 (44.0%)  - Avg. Position: 3644493.32  - Sum of Positions: 160357706
Attacker B: Results: 44 (44.0%)  - Avg. Position: 3707570.25  - Sum of Positions: 163133091  (TLD True Hits/Total Hits: 9/24)

After 1000 entries:
Attacker A: Results: 523 (52.3%)  - Avg. Position: 3936855.51  - Sum of Positions: 2058975431
Attacker B: Results: 523 (52.3%)  - Avg. Position: 3801543.29  - Sum of Positions: 1988207142  (TLD True Hits/Total Hits: 65/178)

After 10000 entries:
Attacker A: Results: 5016 (50.16%)  - Avg. Position: 3871570.39  - Sum of Positions: 19419797054
Attacker B: Results: 5016 (50.16%)  - Avg. Position: 3702089.46  - Sum of Positions: 18569680746  (TLD True Hits/Total Hits: 558/1635)

After 100000 entries:
Attacker A: Results: 49807 (49.81%)  - Avg. Position: 3837990.32  - Sum of Positions: 191158783753
Attacker B: Results: 49807 (49.81%)  - Avg. Position: 3716350.79  - Sum of Positions: 185100283756  (TLD True Hits/Total Hits: 5232/16024)

After 1000000 entries:
Attacker A: Results: 498626 (49.86%)  - Avg. Position: 3835741.36  - Sum of Positions: 1912600373098
Attacker B: Results: 498626 (49.86%)  - Avg. Position: 3713912.44  - Sum of Positions: 1851853301885  (TLD True Hits/Total Hits: 52885/161639)

After 5000000 entries:
Attacker A: Results: 2495576 (49.91%)  - Avg. Position: 3850934.82  - Sum of Positions: 9610300526220
Attacker B: Results: 2495576 (49.91%)  - Avg. Position: 3729660.5  - Sum of Positions: 9307651228805  (TLD True Hits/Total Hits: 265111/809491)

After 10000000 entries:
Attacker A: Results: 4988524 (49.89%)  - Avg. Position: 3849782.96  - Sum of Positions: 19204734693151
Attacker B: Results: 4988524 (49.89%)  - Avg. Position: 3728030.1  - Sum of Positions: 18597367619916  (TLD True Hits/Total Hits: 528779/1615898)

Attacker A (with miss): Results: 4988524 (49.89%)  - Avg. Position: 21896415.26  - Sum of Positions: 218964152597647
Attacker B (with miss): Results: 4988524 (49.89%)  - Avg. Position: 21882565.26  - Sum of Positions: 218825652619309  (TLD True Hits/Total Hits: 528779/1615898)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49589408 (excluded)
net , 4228780 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 5328285.0  - Sum of Positions: 5328285
Attacker B: Results: 1 (100.0%)  - Avg. Position: 5328285.0  - Sum of Positions: 5328285  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 1789006.33  - Sum of Positions: 5367019
Attacker B: Results: 3 (30.0%)  - Avg. Position: 1792395.67  - Sum of Positions: 5377187  (TLD True Hits/Total Hits: 1/3)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 6076411.77  - Sum of Positions: 315973412
Attacker B: Results: 52 (52.0%)  - Avg. Position: 6243696.77  - Sum of Positions: 324672232  (TLD True Hits/Total Hits: 3/13)

After 1000 entries:
Attacker A: Results: 497 (49.7%)  - Avg. Position: 4505958.1  - Sum of Positions: 2239461176
Attacker B: Results: 497 (49.7%)  - Avg. Position: 4593709.89  - Sum of Positions: 2283073814  (TLD True Hits/Total Hits: 21/56)

After 10000 entries:
Attacker A: Results: 4971 (49.71%)  - Avg. Position: 3844559.83  - Sum of Positions: 19111306914
Attacker B: Results: 4971 (49.71%)  - Avg. Position: 3895300.39  - Sum of Positions: 19363538228  (TLD True Hits/Total Hits: 235/595)

After 100000 entries:
Attacker A: Results: 49952 (49.95%)  - Avg. Position: 3882571.12  - Sum of Positions: 193942192618
Attacker B: Results: 49952 (49.95%)  - Avg. Position: 3912712.0  - Sum of Positions: 195447789925  (TLD True Hits/Total Hits: 2266/5963)

After 1000000 entries:
Attacker A: Results: 498426 (49.84%)  - Avg. Position: 3845124.16  - Sum of Positions: 1916509854471
Attacker B: Results: 498426 (49.84%)  - Avg. Position: 3873220.03  - Sum of Positions: 1930513566020  (TLD True Hits/Total Hits: 22895/60639)

After 5000000 entries:
Attacker A: Results: 2492931 (49.86%)  - Avg. Position: 3856779.15  - Sum of Positions: 9614684308930
Attacker B: Results: 2492931 (49.86%)  - Avg. Position: 3884426.97  - Sum of Positions: 9683608402014  (TLD True Hits/Total Hits: 114303/301688)

After 10000000 entries:
Attacker A: Results: 4987521 (49.88%)  - Avg. Position: 3852123.35  - Sum of Positions: 19212546096414
Attacker B: Results: 4987521 (49.88%)  - Avg. Position: 3879660.4  - Sum of Positions: 19349887724790  (TLD True Hits/Total Hits: 228622/603447)

Attacker A (with miss): Results: 4987521 (49.88%)  - Avg. Position: 21898280.14  - Sum of Positions: 218982801425192
Attacker B (with miss): Results: 4987521 (49.88%)  - Avg. Position: 21992306.76  - Sum of Positions: 219923067602044  (TLD True Hits/Total Hits: 228622/603447)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49589589 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 204792.0  - Sum of Positions: 204792
Attacker B: Results: 1 (100.0%)  - Avg. Position: 204792.0  - Sum of Positions: 204792  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 1220405.67  - Sum of Positions: 7322434
Attacker B: Results: 6 (60.0%)  - Avg. Position: 871782.67  - Sum of Positions: 5230696  (TLD True Hits/Total Hits: 2/4)

After 100 entries:
Attacker A: Results: 59 (59.0%)  - Avg. Position: 1674372.88  - Sum of Positions: 98788000
Attacker B: Results: 59 (59.0%)  - Avg. Position: 1194206.56  - Sum of Positions: 70458187  (TLD True Hits/Total Hits: 41/73)

After 1000 entries:
Attacker A: Results: 506 (50.6%)  - Avg. Position: 3831512.88  - Sum of Positions: 1938745515
Attacker B: Results: 506 (50.6%)  - Avg. Position: 4660021.06  - Sum of Positions: 2357970654  (TLD True Hits/Total Hits: 332/701)

After 10000 entries:
Attacker A: Results: 4949 (49.49%)  - Avg. Position: 3864374.07  - Sum of Positions: 19124787262
Attacker B: Results: 4949 (49.49%)  - Avg. Position: 4702975.69  - Sum of Positions: 23275026698  (TLD True Hits/Total Hits: 3241/7032)

After 100000 entries:
Attacker A: Results: 50020 (50.02%)  - Avg. Position: 3796435.56  - Sum of Positions: 189897706763
Attacker B: Results: 50020 (50.02%)  - Avg. Position: 4613466.65  - Sum of Positions: 230765601602  (TLD True Hits/Total Hits: 33194/70562)

After 1000000 entries:
Attacker A: Results: 499235 (49.92%)  - Avg. Position: 3835852.35  - Sum of Positions: 1914991746177
Attacker B: Results: 499235 (49.92%)  - Avg. Position: 4642657.61  - Sum of Positions: 2317777171744  (TLD True Hits/Total Hits: 332979/708361)

After 5000000 entries:
Attacker A: Results: 2496373 (49.93%)  - Avg. Position: 3844071.7  - Sum of Positions: 9596236790111
Attacker B: Results: 2496373 (49.93%)  - Avg. Position: 4654227.11  - Sum of Positions: 11618686895142  (TLD True Hits/Total Hits: 1665503/3541766)

After 10000000 entries:
Attacker A: Results: 4990655 (49.91%)  - Avg. Position: 3851999.56  - Sum of Positions: 19224000855595
Attacker B: Results: 4990655 (49.91%)  - Avg. Position: 4658671.39  - Sum of Positions: 23249821681739  (TLD True Hits/Total Hits: 3329083/7082790)

Attacker A (with miss): Results: 4990655 (49.91%)  - Avg. Position: 21887536.31  - Sum of Positions: 218875363138385
Attacker B (with miss): Results: 4990655 (49.91%)  - Avg. Position: 32906164.63  - Sum of Positions: 329061646277065  (TLD True Hits/Total Hits: 3329083/7082790)


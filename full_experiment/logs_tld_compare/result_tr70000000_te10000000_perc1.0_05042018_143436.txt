Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49591862 (excluded)
net , 4229419 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 1178147.71  - Sum of Positions: 8247034
Attacker B: Results: 7 (70.0%)  - Avg. Position: 1178147.71  - Sum of Positions: 8247034  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 63 (63.0%)  - Avg. Position: 2862455.94  - Sum of Positions: 180334724
Attacker B: Results: 63 (63.0%)  - Avg. Position: 2860957.02  - Sum of Positions: 180240292  (TLD True Hits/Total Hits: 3/6)

After 1000 entries:
Attacker A: Results: 498 (49.8%)  - Avg. Position: 3175997.27  - Sum of Positions: 1581646641
Attacker B: Results: 498 (49.8%)  - Avg. Position: 3222125.61  - Sum of Positions: 1604618552  (TLD True Hits/Total Hits: 23/61)

After 10000 entries:
Attacker A: Results: 5022 (50.22%)  - Avg. Position: 3648960.97  - Sum of Positions: 18325082001
Attacker B: Results: 5022 (50.22%)  - Avg. Position: 3676064.29  - Sum of Positions: 18461194883  (TLD True Hits/Total Hits: 203/552)

After 100000 entries:
Attacker A: Results: 49912 (49.91%)  - Avg. Position: 3815275.01  - Sum of Positions: 190428006505
Attacker B: Results: 49912 (49.91%)  - Avg. Position: 3839476.33  - Sum of Positions: 191635942592  (TLD True Hits/Total Hits: 2189/5942)

After 1000000 entries:
Attacker A: Results: 498626 (49.86%)  - Avg. Position: 3849461.72  - Sum of Positions: 1919441697394
Attacker B: Results: 498626 (49.86%)  - Avg. Position: 3878003.32  - Sum of Positions: 1933673284274  (TLD True Hits/Total Hits: 22829/60409)

After 5000000 entries:
Attacker A: Results: 2495295 (49.91%)  - Avg. Position: 3853789.71  - Sum of Positions: 9616342189620
Attacker B: Results: 2495295 (49.91%)  - Avg. Position: 3882928.97  - Sum of Positions: 9689053238917  (TLD True Hits/Total Hits: 114348/302751)

After 10000000 entries:
Attacker A: Results: 4991508 (49.92%)  - Avg. Position: 3851614.43  - Sum of Positions: 19225364218197
Attacker B: Results: 4991508 (49.92%)  - Avg. Position: 3879913.46  - Sum of Positions: 19366619078655  (TLD True Hits/Total Hits: 228860/604646)

Attacker A (with miss): Results: 4991508 (49.92%)  - Avg. Position: 21887605.1  - Sum of Positions: 218876051016217
Attacker B (with miss): Results: 4991508 (49.92%)  - Avg. Position: 21982232.37  - Sum of Positions: 219822323679859  (TLD True Hits/Total Hits: 228860/604646)


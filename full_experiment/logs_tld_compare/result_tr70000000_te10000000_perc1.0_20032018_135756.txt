Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49591222 (excluded)
net , 4229464 (excluded)
uk , 1641022 (excluded)
fr , 1289537 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 3204041.0  - Sum of Positions: 3204041
Attacker B: Results: 1 (100.0%)  - Avg. Position: 3204041.0  - Sum of Positions: 3204041  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 10975031.83  - Sum of Positions: 65850191
Attacker B: Results: 6 (60.0%)  - Avg. Position: 10975031.83  - Sum of Positions: 65850191  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 44 (44.0%)  - Avg. Position: 3372711.59  - Sum of Positions: 148399310
Attacker B: Results: 44 (44.0%)  - Avg. Position: 3386059.66  - Sum of Positions: 148986625  (TLD True Hits/Total Hits: 1/1)

After 1000 entries:
Attacker A: Results: 485 (48.5%)  - Avg. Position: 3675195.73  - Sum of Positions: 1782469931
Attacker B: Results: 485 (48.5%)  - Avg. Position: 3680919.89  - Sum of Positions: 1785246145  (TLD True Hits/Total Hits: 4/14)

After 10000 entries:
Attacker A: Results: 4980 (49.8%)  - Avg. Position: 3899533.76  - Sum of Positions: 19419678145
Attacker B: Results: 4980 (49.8%)  - Avg. Position: 3882887.08  - Sum of Positions: 19336777651  (TLD True Hits/Total Hits: 69/173)

After 100000 entries:
Attacker A: Results: 49831 (49.83%)  - Avg. Position: 3878085.13  - Sum of Positions: 193248860050
Attacker B: Results: 49831 (49.83%)  - Avg. Position: 3863922.52  - Sum of Positions: 192543122956  (TLD True Hits/Total Hits: 711/1810)

After 1000000 entries:
Attacker A: Results: 498690 (49.87%)  - Avg. Position: 3860261.6  - Sum of Positions: 1925073857954
Attacker B: Results: 498690 (49.87%)  - Avg. Position: 3846792.2  - Sum of Positions: 1918356803737  (TLD True Hits/Total Hits: 7084/18408)

After 5000000 entries:
Attacker A: Results: 2493522 (49.87%)  - Avg. Position: 3856282.11  - Sum of Positions: 9615724269352
Attacker B: Results: 2493522 (49.87%)  - Avg. Position: 3842034.98  - Sum of Positions: 9580198736971  (TLD True Hits/Total Hits: 35631/91878)

After 10000000 entries:
Attacker A: Results: 4988905 (49.89%)  - Avg. Position: 3858929.7  - Sum of Positions: 19251833672209
Attacker B: Results: 4988905 (49.89%)  - Avg. Position: 3844499.16  - Sum of Positions: 19179841063587  (TLD True Hits/Total Hits: 71353/183743)

Attacker A (with miss): Results: 4988905 (49.89%)  - Avg. Position: 21899268.23  - Sum of Positions: 218992682276704
Attacker B (with miss): Results: 4988905 (49.89%)  - Avg. Position: 21899695.71  - Sum of Positions: 218996957113530  (TLD True Hits/Total Hits: 71353/183743)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 3 TLDs:

com , 49588340 (excluded)
net , 4228797 (excluded)
uk , 1642493 (excluded)
fr , 1288368 (excluded)
edu , 1234146 (excluded)
br , 1202962 (excluded)
it , 959457 (excluded)
in , 898947 (excluded)
org , 886102 (excluded)
nl , 731662 (excluded)
ca , 709708 (excluded)
au , 584129 (excluded)
de , 456776 
es , 418127 (excluded)
za , 368436 (excluded)
ru , 341285 (excluded)
ar , 248881 (excluded)
mx , 225020 (excluded)
us , 223894 (excluded)
dk , 204489 (excluded)
be , 199288 (excluded)
gov , 184892 (excluded)
se , 167170 (excluded)
pl , 157681 (excluded)
nz , 142531 (excluded)
cn , 141494 (excluded)
ch , 125780 
jp , 116934 (excluded)
pt , 113169 (excluded)
no , 112392 (excluded)
mil , 102922 (excluded)
id , 102790 (excluded)
cl , 101842 (excluded)
sg , 81884 (excluded)
gr , 74470 (excluded)
ie , 73794 (excluded)
il , 71676 (excluded)
fi , 70230 (excluded)
cz , 70170 (excluded)
tr , 58713 (excluded)
co , 56834 (excluded)
my , 52265 (excluded)
eu , 50489 (excluded)
hk , 49297 (excluded)
at , 46186 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 802419.4  - Sum of Positions: 4012097
Attacker B: Results: 5 (50.0%)  - Avg. Position: 802419.4  - Sum of Positions: 4012097  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 56 (56.0%)  - Avg. Position: 4509832.75  - Sum of Positions: 252550634
Attacker B: Results: 56 (56.0%)  - Avg. Position: 4509832.75  - Sum of Positions: 252550634  (TLD True Hits/Total Hits: 0/0)

After 1000 entries:
Attacker A: Results: 521 (52.1%)  - Avg. Position: 4034966.9  - Sum of Positions: 2102217756
Attacker B: Results: 521 (52.1%)  - Avg. Position: 4036391.86  - Sum of Positions: 2102960161  (TLD True Hits/Total Hits: 2/9)

After 10000 entries:
Attacker A: Results: 5122 (51.22%)  - Avg. Position: 3981858.35  - Sum of Positions: 20395078455
Attacker B: Results: 5122 (51.22%)  - Avg. Position: 3977713.74  - Sum of Positions: 20373849764  (TLD True Hits/Total Hits: 20/79)

After 100000 entries:
Attacker A: Results: 50228 (50.23%)  - Avg. Position: 3839199.79  - Sum of Positions: 192835327091
Attacker B: Results: 50228 (50.23%)  - Avg. Position: 3832897.81  - Sum of Positions: 192518791424  (TLD True Hits/Total Hits: 200/842)

After 1000000 entries:
Attacker A: Results: 499500 (49.95%)  - Avg. Position: 3848294.29  - Sum of Positions: 1922222998394
Attacker B: Results: 499500 (49.95%)  - Avg. Position: 3841668.39  - Sum of Positions: 1918913360690  (TLD True Hits/Total Hits: 2088/9040)

After 5000000 entries:
Attacker A: Results: 2494948 (49.9%)  - Avg. Position: 3853345.45  - Sum of Positions: 9613896512316
Attacker B: Results: 2494948 (49.9%)  - Avg. Position: 3846329.6  - Sum of Positions: 9596392337432  (TLD True Hits/Total Hits: 10016/44779)

After 10000000 entries:
Attacker A: Results: 4988528 (49.89%)  - Avg. Position: 3854662.18  - Sum of Positions: 19229090220692
Attacker B: Results: 4988528 (49.89%)  - Avg. Position: 3847402.74  - Sum of Positions: 19192876311815  (TLD True Hits/Total Hits: 20132/90142)

Attacker A (with miss): Results: 4988528 (49.89%)  - Avg. Position: 21896520.57  - Sum of Positions: 218965205705908
Attacker B (with miss): Results: 4988528 (49.89%)  - Avg. Position: 21894378.78  - Sum of Positions: 218943787846184  (TLD True Hits/Total Hits: 20132/90142)


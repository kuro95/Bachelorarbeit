Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49586657 (excluded)
net , 4230850 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 127150.0  - Sum of Positions: 762900
Attacker B: Results: 6 (60.0%)  - Avg. Position: 127150.0  - Sum of Positions: 762900  (TLD True Hits/Total Hits: 0/1)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 4417748.14  - Sum of Positions: 251811644
Attacker B: Results: 57 (57.0%)  - Avg. Position: 4417122.84  - Sum of Positions: 251776002  (TLD True Hits/Total Hits: 1/5)

After 1000 entries:
Attacker A: Results: 509 (50.9%)  - Avg. Position: 3419070.29  - Sum of Positions: 1740306776
Attacker B: Results: 509 (50.9%)  - Avg. Position: 3499941.0  - Sum of Positions: 1781469969  (TLD True Hits/Total Hits: 20/59)

After 10000 entries:
Attacker A: Results: 4992 (49.92%)  - Avg. Position: 3899125.11  - Sum of Positions: 19464432563
Attacker B: Results: 4992 (49.92%)  - Avg. Position: 3920628.69  - Sum of Positions: 19571778416  (TLD True Hits/Total Hits: 207/588)

After 100000 entries:
Attacker A: Results: 49947 (49.95%)  - Avg. Position: 3897194.16  - Sum of Positions: 194653156783
Attacker B: Results: 49947 (49.95%)  - Avg. Position: 3928624.29  - Sum of Positions: 196222997557  (TLD True Hits/Total Hits: 2235/5977)

After 1000000 entries:
Attacker A: Results: 498887 (49.89%)  - Avg. Position: 3853970.09  - Sum of Positions: 1922695576997
Attacker B: Results: 498887 (49.89%)  - Avg. Position: 3884065.51  - Sum of Positions: 1937709788074  (TLD True Hits/Total Hits: 23036/60698)

After 5000000 entries:
Attacker A: Results: 2494102 (49.88%)  - Avg. Position: 3853357.49  - Sum of Positions: 9610666615023
Attacker B: Results: 2494102 (49.88%)  - Avg. Position: 3882144.06  - Sum of Positions: 9682463272510  (TLD True Hits/Total Hits: 114361/301717)

After 10000000 entries:
Attacker A: Results: 4987994 (49.88%)  - Avg. Position: 3851176.24  - Sum of Positions: 19209643989672
Attacker B: Results: 4987994 (49.88%)  - Avg. Position: 3880095.05  - Sum of Positions: 19353890823376  (TLD True Hits/Total Hits: 229470/604112)

Attacker A (with miss): Results: 4987994 (49.88%)  - Avg. Position: 21898263.48  - Sum of Positions: 218982634811006
Attacker B (with miss): Results: 4987994 (49.88%)  - Avg. Position: 21992982.94  - Sum of Positions: 219929829387209  (TLD True Hits/Total Hits: 229470/604112)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49592445 (excluded)
net , 4228743 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 5257698.17  - Sum of Positions: 31546189
Attacker B: Results: 6 (60.0%)  - Avg. Position: 5257698.17  - Sum of Positions: 31546189  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 42 (42.0%)  - Avg. Position: 3582473.24  - Sum of Positions: 150463876
Attacker B: Results: 42 (42.0%)  - Avg. Position: 3582473.24  - Sum of Positions: 150463876  (TLD True Hits/Total Hits: 0/1)

After 1000 entries:
Attacker A: Results: 500 (50.0%)  - Avg. Position: 4381122.93  - Sum of Positions: 2190561463
Attacker B: Results: 500 (50.0%)  - Avg. Position: 4455256.19  - Sum of Positions: 2227628093  (TLD True Hits/Total Hits: 28/55)

After 10000 entries:
Attacker A: Results: 4906 (49.06%)  - Avg. Position: 3806462.4  - Sum of Positions: 18674504556
Attacker B: Results: 4906 (49.06%)  - Avg. Position: 3841663.93  - Sum of Positions: 18847203222  (TLD True Hits/Total Hits: 222/604)

After 100000 entries:
Attacker A: Results: 49774 (49.77%)  - Avg. Position: 3796411.18  - Sum of Positions: 188962569867
Attacker B: Results: 49774 (49.77%)  - Avg. Position: 3822698.73  - Sum of Positions: 190271006772  (TLD True Hits/Total Hits: 2297/5939)

After 1000000 entries:
Attacker A: Results: 498577 (49.86%)  - Avg. Position: 3837970.08  - Sum of Positions: 1913523610460
Attacker B: Results: 498577 (49.86%)  - Avg. Position: 3866485.59  - Sum of Positions: 1927740787207  (TLD True Hits/Total Hits: 22902/60476)

After 5000000 entries:
Attacker A: Results: 2494567 (49.89%)  - Avg. Position: 3844783.86  - Sum of Positions: 9591070943635
Attacker B: Results: 2494567 (49.89%)  - Avg. Position: 3873474.44  - Sum of Positions: 9662641522334  (TLD True Hits/Total Hits: 114192/301645)

After 10000000 entries:
Attacker A: Results: 4990173 (49.9%)  - Avg. Position: 3848130.17  - Sum of Positions: 19202835266967
Attacker B: Results: 4990173 (49.9%)  - Avg. Position: 3876488.85  - Sum of Positions: 19344349983497  (TLD True Hits/Total Hits: 229024/604053)

Attacker A (with miss): Results: 4990173 (49.9%)  - Avg. Position: 21888163.42  - Sum of Positions: 218881634172615
Attacker B (with miss): Results: 4990173 (49.9%)  - Avg. Position: 21982614.45  - Sum of Positions: 219826144463329  (TLD True Hits/Total Hits: 229024/604053)


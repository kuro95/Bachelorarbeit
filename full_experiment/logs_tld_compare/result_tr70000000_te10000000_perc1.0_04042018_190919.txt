Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49587083 (excluded)
net , 4228242 (excluded)
uk , 1642374 (excluded)
fr , 1289555 (excluded)
edu , 1233931 (excluded)
br , 1202594 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 113620.0  - Sum of Positions: 113620
Attacker B: Results: 1 (100.0%)  - Avg. Position: 113620.0  - Sum of Positions: 113620  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 195436.0  - Sum of Positions: 1368052
Attacker B: Results: 7 (70.0%)  - Avg. Position: 195436.0  - Sum of Positions: 1368052  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 2083049.65  - Sum of Positions: 118733830
Attacker B: Results: 57 (57.0%)  - Avg. Position: 2097860.0  - Sum of Positions: 119578020  (TLD True Hits/Total Hits: 0/3)

After 1000 entries:
Attacker A: Results: 514 (51.4%)  - Avg. Position: 3333377.84  - Sum of Positions: 1713356211
Attacker B: Results: 514 (51.4%)  - Avg. Position: 3236696.22  - Sum of Positions: 1663661858  (TLD True Hits/Total Hits: 7/25)

After 10000 entries:
Attacker A: Results: 5021 (50.21%)  - Avg. Position: 3899136.64  - Sum of Positions: 19577565090
Attacker B: Results: 5021 (50.21%)  - Avg. Position: 3882858.07  - Sum of Positions: 19495830391  (TLD True Hits/Total Hits: 66/180)

After 100000 entries:
Attacker A: Results: 49934 (49.93%)  - Avg. Position: 3850834.85  - Sum of Positions: 192287587470
Attacker B: Results: 49934 (49.93%)  - Avg. Position: 3838325.81  - Sum of Positions: 191662960898  (TLD True Hits/Total Hits: 638/1694)

After 1000000 entries:
Attacker A: Results: 498633 (49.86%)  - Avg. Position: 3854697.78  - Sum of Positions: 1922079517966
Attacker B: Results: 498633 (49.86%)  - Avg. Position: 3842020.47  - Sum of Positions: 1915758195286  (TLD True Hits/Total Hits: 6308/17049)

After 5000000 entries:
Attacker A: Results: 2493074 (49.86%)  - Avg. Position: 3853521.47  - Sum of Positions: 9607114176851
Attacker B: Results: 2493074 (49.86%)  - Avg. Position: 3840908.42  - Sum of Positions: 9575668926420  (TLD True Hits/Total Hits: 32048/85591)

After 10000000 entries:
Attacker A: Results: 4989722 (49.9%)  - Avg. Position: 3846040.84  - Sum of Positions: 19190674595185
Attacker B: Results: 4989722 (49.9%)  - Avg. Position: 3833204.3  - Sum of Positions: 19126623826129  (TLD True Hits/Total Hits: 64370/171504)

Attacker A (with miss): Results: 4989722 (49.9%)  - Avg. Position: 21889286.53  - Sum of Positions: 218892865309575
Attacker B (with miss): Results: 4989722 (49.9%)  - Avg. Position: 21889577.91  - Sum of Positions: 218895779068079  (TLD True Hits/Total Hits: 64370/171504)


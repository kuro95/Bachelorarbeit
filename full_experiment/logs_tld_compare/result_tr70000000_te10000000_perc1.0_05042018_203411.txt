Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49595732 (excluded)
net , 4227560 (excluded)
uk , 1640320 (excluded)
fr , 1288378 (excluded)
edu , 1233803 (excluded)
br , 1202577 (excluded)
it , 959156 (excluded)
in , 899439 (excluded)
org , 887353 (excluded)
nl , 730919 (excluded)
ca , 709230 (excluded)
au , 582821 (excluded)
de , 457193 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 6225451.83  - Sum of Positions: 37352711
Attacker B: Results: 6 (60.0%)  - Avg. Position: 6225451.83  - Sum of Positions: 37352711  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 5525496.88  - Sum of Positions: 270749347
Attacker B: Results: 49 (49.0%)  - Avg. Position: 5525496.88  - Sum of Positions: 270749347  (TLD True Hits/Total Hits: 0/0)

After 1000 entries:
Attacker A: Results: 501 (50.1%)  - Avg. Position: 3682692.41  - Sum of Positions: 1845028898
Attacker B: Results: 501 (50.1%)  - Avg. Position: 3682978.64  - Sum of Positions: 1845172297  (TLD True Hits/Total Hits: 1/6)

After 10000 entries:
Attacker A: Results: 4974 (49.74%)  - Avg. Position: 3951827.33  - Sum of Positions: 19656389131
Attacker B: Results: 4974 (49.74%)  - Avg. Position: 3938265.87  - Sum of Positions: 19588934427  (TLD True Hits/Total Hits: 21/66)

After 100000 entries:
Attacker A: Results: 49758 (49.76%)  - Avg. Position: 3840372.79  - Sum of Positions: 191089269290
Attacker B: Results: 49758 (49.76%)  - Avg. Position: 3834872.4  - Sum of Positions: 190815581001  (TLD True Hits/Total Hits: 185/686)

After 1000000 entries:
Attacker A: Results: 498133 (49.81%)  - Avg. Position: 3854760.49  - Sum of Positions: 1920183408210
Attacker B: Results: 498133 (49.81%)  - Avg. Position: 3849992.69  - Sum of Positions: 1917808408884  (TLD True Hits/Total Hits: 1627/6664)

After 5000000 entries:
Attacker A: Results: 2492530 (49.85%)  - Avg. Position: 3849041.77  - Sum of Positions: 9593852078980
Attacker B: Results: 2492530 (49.85%)  - Avg. Position: 3843312.7  - Sum of Positions: 9579572198243  (TLD True Hits/Total Hits: 7952/32592)

After 10000000 entries:
Attacker A: Results: 4987509 (49.88%)  - Avg. Position: 3848491.97  - Sum of Positions: 19194388323223
Attacker B: Results: 4987509 (49.88%)  - Avg. Position: 3843016.07  - Sum of Positions: 19167077259166  (TLD True Hits/Total Hits: 15744/64936)

Attacker A (with miss): Results: 4987509 (49.88%)  - Avg. Position: 21897314.69  - Sum of Positions: 218973146905076
Attacker B (with miss): Results: 4987509 (49.88%)  - Avg. Position: 21895928.23  - Sum of Positions: 218959282252704  (TLD True Hits/Total Hits: 15744/64936)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49587935 (excluded)
net , 4229492 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1.0  - Sum of Positions: 1
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1.0  - Sum of Positions: 1  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 16130562.33  - Sum of Positions: 96783374
Attacker B: Results: 6 (60.0%)  - Avg. Position: 16130562.33  - Sum of Positions: 96783374  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 5990937.15  - Sum of Positions: 311528732
Attacker B: Results: 52 (52.0%)  - Avg. Position: 6114874.69  - Sum of Positions: 317973484  (TLD True Hits/Total Hits: 2/6)

After 1000 entries:
Attacker A: Results: 497 (49.7%)  - Avg. Position: 3956038.73  - Sum of Positions: 1966151250
Attacker B: Results: 497 (49.7%)  - Avg. Position: 4015897.53  - Sum of Positions: 1995901071  (TLD True Hits/Total Hits: 24/66)

After 10000 entries:
Attacker A: Results: 4961 (49.61%)  - Avg. Position: 4145792.33  - Sum of Positions: 20567275770
Attacker B: Results: 4961 (49.61%)  - Avg. Position: 4177557.47  - Sum of Positions: 20724862629  (TLD True Hits/Total Hits: 233/600)

After 100000 entries:
Attacker A: Results: 50004 (50.0%)  - Avg. Position: 3883013.05  - Sum of Positions: 194166184661
Attacker B: Results: 50004 (50.0%)  - Avg. Position: 3914467.85  - Sum of Positions: 195739050203  (TLD True Hits/Total Hits: 2291/6003)

After 1000000 entries:
Attacker A: Results: 498773 (49.88%)  - Avg. Position: 3844381.05  - Sum of Positions: 1917473470257
Attacker B: Results: 498773 (49.88%)  - Avg. Position: 3874181.88  - Sum of Positions: 1932337318081  (TLD True Hits/Total Hits: 22765/60344)

After 5000000 entries:
Attacker A: Results: 2494667 (49.89%)  - Avg. Position: 3852987.1  - Sum of Positions: 9611919775471
Attacker B: Results: 2494667 (49.89%)  - Avg. Position: 3881289.0  - Sum of Positions: 9682523577688  (TLD True Hits/Total Hits: 114042/302114)

After 10000000 entries:
Attacker A: Results: 4987951 (49.88%)  - Avg. Position: 3849586.63  - Sum of Positions: 19201549497807
Attacker B: Results: 4987951 (49.88%)  - Avg. Position: 3877949.95  - Sum of Positions: 19343024315422  (TLD True Hits/Total Hits: 228864/604537)

Attacker A (with miss): Results: 4987951 (49.88%)  - Avg. Position: 21896903.69  - Sum of Positions: 218969036900808
Attacker B (with miss): Results: 4987951 (49.88%)  - Avg. Position: 21991518.76  - Sum of Positions: 219915187590856  (TLD True Hits/Total Hits: 228864/604537)


Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49592299 (excluded)
net , 4228122 (excluded)
uk , 1640855 (excluded)
fr , 1287553 (excluded)
edu , 1233671 (excluded)
br , 1203530 (excluded)
it , 959317 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 3 (30.0%)  - Avg. Position: 304847.67  - Sum of Positions: 914543
Attacker B: Results: 3 (30.0%)  - Avg. Position: 304847.67  - Sum of Positions: 914543  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 52 (52.0%)  - Avg. Position: 3921052.25  - Sum of Positions: 203894717
Attacker B: Results: 52 (52.0%)  - Avg. Position: 3917739.63  - Sum of Positions: 203722461  (TLD True Hits/Total Hits: 1/3)

After 1000 entries:
Attacker A: Results: 523 (52.3%)  - Avg. Position: 3817820.13  - Sum of Positions: 1996719927
Attacker B: Results: 523 (52.3%)  - Avg. Position: 3819384.42  - Sum of Positions: 1997538053  (TLD True Hits/Total Hits: 5/14)

After 10000 entries:
Attacker A: Results: 5063 (50.63%)  - Avg. Position: 4115054.99  - Sum of Positions: 20834523434
Attacker B: Results: 5063 (50.63%)  - Avg. Position: 4106056.07  - Sum of Positions: 20788961902  (TLD True Hits/Total Hits: 48/142)

After 100000 entries:
Attacker A: Results: 49869 (49.87%)  - Avg. Position: 3850785.15  - Sum of Positions: 192034804782
Attacker B: Results: 49869 (49.87%)  - Avg. Position: 3838629.25  - Sum of Positions: 191428601873  (TLD True Hits/Total Hits: 528/1409)

After 1000000 entries:
Attacker A: Results: 498975 (49.9%)  - Avg. Position: 3865354.03  - Sum of Positions: 1928715024976
Attacker B: Results: 498975 (49.9%)  - Avg. Position: 3849573.38  - Sum of Positions: 1920840875014  (TLD True Hits/Total Hits: 5165/13788)

After 5000000 entries:
Attacker A: Results: 2495565 (49.91%)  - Avg. Position: 3852218.88  - Sum of Positions: 9613462609264
Attacker B: Results: 2495565 (49.91%)  - Avg. Position: 3836161.23  - Sum of Positions: 9573389705820  (TLD True Hits/Total Hits: 26152/68697)

After 10000000 entries:
Attacker A: Results: 4990031 (49.9%)  - Avg. Position: 3854867.3  - Sum of Positions: 19235907335707
Attacker B: Results: 4990031 (49.9%)  - Avg. Position: 3838907.3  - Sum of Positions: 19156266439961  (TLD True Hits/Total Hits: 52435/137537)

Attacker A (with miss): Results: 4990031 (49.9%)  - Avg. Position: 21892254.53  - Sum of Positions: 218922545332078
Attacker B (with miss): Results: 4990031 (49.9%)  - Avg. Position: 21888674.56  - Sum of Positions: 218886745554440  (TLD True Hits/Total Hits: 52435/137537)


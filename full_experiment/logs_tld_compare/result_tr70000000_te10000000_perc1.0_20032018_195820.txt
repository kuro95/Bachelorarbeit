Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49588558 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/1)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 583099.25  - Sum of Positions: 2332397
Attacker B: Results: 4 (40.0%)  - Avg. Position: 563717.25  - Sum of Positions: 2254869  (TLD True Hits/Total Hits: 3/7)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 3953342.44  - Sum of Positions: 225340519
Attacker B: Results: 57 (57.0%)  - Avg. Position: 5720697.23  - Sum of Positions: 326079742  (TLD True Hits/Total Hits: 33/69)

After 1000 entries:
Attacker A: Results: 519 (51.9%)  - Avg. Position: 4046930.13  - Sum of Positions: 2100356738
Attacker B: Results: 519 (51.9%)  - Avg. Position: 5143637.46  - Sum of Positions: 2669547840  (TLD True Hits/Total Hits: 348/727)

After 10000 entries:
Attacker A: Results: 4963 (49.63%)  - Avg. Position: 3938801.17  - Sum of Positions: 19548270204
Attacker B: Results: 4963 (49.63%)  - Avg. Position: 4585063.72  - Sum of Positions: 22755671267  (TLD True Hits/Total Hits: 3317/7113)

After 100000 entries:
Attacker A: Results: 49577 (49.58%)  - Avg. Position: 3913207.67  - Sum of Positions: 194005096809
Attacker B: Results: 49577 (49.58%)  - Avg. Position: 4679599.12  - Sum of Positions: 232000485442  (TLD True Hits/Total Hits: 33040/70917)

After 1000000 entries:
Attacker A: Results: 498129 (49.81%)  - Avg. Position: 3865338.11  - Sum of Positions: 1925437004971
Attacker B: Results: 498129 (49.81%)  - Avg. Position: 4669284.23  - Sum of Positions: 2325905885184  (TLD True Hits/Total Hits: 332254/708492)

After 5000000 entries:
Attacker A: Results: 2493449 (49.87%)  - Avg. Position: 3851002.54  - Sum of Positions: 9602278439065
Attacker B: Results: 2493449 (49.87%)  - Avg. Position: 4655058.51  - Sum of Positions: 11607150991186  (TLD True Hits/Total Hits: 1663798/3542734)

After 10000000 entries:
Attacker A: Results: 4987583 (49.88%)  - Avg. Position: 3852852.56  - Sum of Positions: 19216421928456
Attacker B: Results: 4987583 (49.88%)  - Avg. Position: 4658370.32  - Sum of Positions: 23234008623383  (TLD True Hits/Total Hits: 3328323/7085470)

Attacker A (with miss): Results: 4987583 (49.88%)  - Avg. Position: 21900200.54  - Sum of Positions: 219002005365917
Attacker B (with miss): Results: 4987583 (49.88%)  - Avg. Position: 32929056.53  - Sum of Positions: 329290565298886  (TLD True Hits/Total Hits: 3328323/7085470)


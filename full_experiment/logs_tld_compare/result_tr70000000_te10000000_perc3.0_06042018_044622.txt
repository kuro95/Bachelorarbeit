Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 3 TLDs:

com , 49589510 (excluded)
net , 4227798 (excluded)
uk , 1641553 (excluded)
fr , 1289867 (excluded)
edu , 1234018 (excluded)
br , 1201637 (excluded)
it , 958324 (excluded)
in , 900292 (excluded)
org , 886919 (excluded)
nl , 731555 (excluded)
ca , 709327 (excluded)
au , 583697 (excluded)
de , 456625 
es , 418019 (excluded)
za , 367839 (excluded)
ru , 342499 (excluded)
ar , 249550 (excluded)
mx , 224333 (excluded)
us , 223726 (excluded)
dk , 204720 (excluded)
be , 199555 (excluded)
gov , 184781 (excluded)
se , 166984 (excluded)
pl , 157727 (excluded)
nz , 142290 (excluded)
cn , 141823 (excluded)
ch , 125727 
jp , 117233 (excluded)
pt , 112903 (excluded)
no , 112310 (excluded)
mil , 102924 (excluded)
id , 102555 (excluded)
cl , 102267 (excluded)
sg , 82033 (excluded)
gr , 74348 (excluded)
ie , 73630 (excluded)
il , 71669 (excluded)
cz , 70244 (excluded)
fi , 70055 (excluded)
tr , 58453 (excluded)
co , 56961 (excluded)
my , 52476 (excluded)
eu , 50559 (excluded)
hk , 49379 (excluded)
at , 46350 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 4772057.0  - Sum of Positions: 4772057
Attacker B: Results: 1 (100.0%)  - Avg. Position: 4772057.0  - Sum of Positions: 4772057  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 1691899.6  - Sum of Positions: 8459498
Attacker B: Results: 5 (50.0%)  - Avg. Position: 1691899.6  - Sum of Positions: 8459498  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 53 (53.0%)  - Avg. Position: 3659260.3  - Sum of Positions: 193940796
Attacker B: Results: 53 (53.0%)  - Avg. Position: 3659260.3  - Sum of Positions: 193940796  (TLD True Hits/Total Hits: 0/0)

After 1000 entries:
Attacker A: Results: 526 (52.6%)  - Avg. Position: 4363529.04  - Sum of Positions: 2295216277
Attacker B: Results: 526 (52.6%)  - Avg. Position: 4364900.13  - Sum of Positions: 2295937468  (TLD True Hits/Total Hits: 3/9)

After 10000 entries:
Attacker A: Results: 4944 (49.44%)  - Avg. Position: 3879184.29  - Sum of Positions: 19178687146
Attacker B: Results: 4944 (49.44%)  - Avg. Position: 3865615.67  - Sum of Positions: 19111603856  (TLD True Hits/Total Hits: 26/84)

After 100000 entries:
Attacker A: Results: 49789 (49.79%)  - Avg. Position: 3928372.15  - Sum of Positions: 195589721106
Attacker B: Results: 49789 (49.79%)  - Avg. Position: 3922357.04  - Sum of Positions: 195290234737  (TLD True Hits/Total Hits: 197/900)

After 1000000 entries:
Attacker A: Results: 498988 (49.9%)  - Avg. Position: 3861929.96  - Sum of Positions: 1927056705149
Attacker B: Results: 498988 (49.9%)  - Avg. Position: 3853955.41  - Sum of Positions: 1923077504367  (TLD True Hits/Total Hits: 1979/8913)

After 5000000 entries:
Attacker A: Results: 2493293 (49.87%)  - Avg. Position: 3863477.07  - Sum of Positions: 9632780338220
Attacker B: Results: 2493293 (49.87%)  - Avg. Position: 3855641.85  - Sum of Positions: 9613244838627  (TLD True Hits/Total Hits: 10070/44947)

After 10000000 entries:
Attacker A: Results: 4986903 (49.87%)  - Avg. Position: 3857025.1  - Sum of Positions: 19234610061775
Attacker B: Results: 4986903 (49.87%)  - Avg. Position: 3849484.75  - Sum of Positions: 19197007065605  (TLD True Hits/Total Hits: 20172/89708)

Attacker A (with miss): Results: 4986903 (49.87%)  - Avg. Position: 21904190.8  - Sum of Positions: 219041907950401
Attacker B (with miss): Results: 4986903 (49.87%)  - Avg. Position: 21901899.08  - Sum of Positions: 219018990799130  (TLD True Hits/Total Hits: 20172/89708)


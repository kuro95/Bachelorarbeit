Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49594258 (excluded)
net , 4228606 (excluded)
uk , 1640602 (excluded)
fr , 1289194 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 26918242.0  - Sum of Positions: 26918242
Attacker B: Results: 1 (100.0%)  - Avg. Position: 26918242.0  - Sum of Positions: 26918242  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 10203205.0  - Sum of Positions: 61219230
Attacker B: Results: 6 (60.0%)  - Avg. Position: 10196800.83  - Sum of Positions: 61180805  (TLD True Hits/Total Hits: 1/1)

After 100 entries:
Attacker A: Results: 57 (57.0%)  - Avg. Position: 3508400.28  - Sum of Positions: 199978816
Attacker B: Results: 57 (57.0%)  - Avg. Position: 3523121.26  - Sum of Positions: 200817912  (TLD True Hits/Total Hits: 1/2)

After 1000 entries:
Attacker A: Results: 519 (51.9%)  - Avg. Position: 3795509.22  - Sum of Positions: 1969869287
Attacker B: Results: 519 (51.9%)  - Avg. Position: 3798820.03  - Sum of Positions: 1971587593  (TLD True Hits/Total Hits: 10/20)

After 10000 entries:
Attacker A: Results: 4998 (49.98%)  - Avg. Position: 3957862.76  - Sum of Positions: 19781398059
Attacker B: Results: 4998 (49.98%)  - Avg. Position: 3958297.67  - Sum of Positions: 19783571777  (TLD True Hits/Total Hits: 83/193)

After 100000 entries:
Attacker A: Results: 50016 (50.02%)  - Avg. Position: 3918167.24  - Sum of Positions: 195971052554
Attacker B: Results: 50016 (50.02%)  - Avg. Position: 3901338.73  - Sum of Positions: 195129357812  (TLD True Hits/Total Hits: 730/1886)

After 1000000 entries:
Attacker A: Results: 498848 (49.88%)  - Avg. Position: 3866594.92  - Sum of Positions: 1928843144744
Attacker B: Results: 498848 (49.88%)  - Avg. Position: 3852716.58  - Sum of Positions: 1921919959047  (TLD True Hits/Total Hits: 7097/18385)

After 5000000 entries:
Attacker A: Results: 2494990 (49.9%)  - Avg. Position: 3858486.74  - Sum of Positions: 9626885827759
Attacker B: Results: 2494990 (49.9%)  - Avg. Position: 3843592.34  - Sum of Positions: 9589724461720  (TLD True Hits/Total Hits: 35655/91854)

After 10000000 entries:
Attacker A: Results: 4991298 (49.91%)  - Avg. Position: 3849900.97  - Sum of Positions: 19216003007786
Attacker B: Results: 4991298 (49.91%)  - Avg. Position: 3835153.81  - Sum of Positions: 19142395517171  (TLD True Hits/Total Hits: 71627/183838)

Attacker A (with miss): Results: 4991298 (49.91%)  - Avg. Position: 21885332.82  - Sum of Positions: 218853328159178
Attacker B (with miss): Results: 4991298 (49.91%)  - Avg. Position: 21885569.91  - Sum of Positions: 218855699069306  (TLD True Hits/Total Hits: 71627/183838)


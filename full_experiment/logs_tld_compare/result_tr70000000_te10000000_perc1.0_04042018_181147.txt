Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49590371 (excluded)
net , 4229170 (excluded)
uk , 1641212 (excluded)
fr , 1289382 (excluded)
edu , 1234075 (excluded)
br , 1200513 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1244219.0  - Sum of Positions: 1244219
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1244219.0  - Sum of Positions: 1244219  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 6813546.5  - Sum of Positions: 40881279
Attacker B: Results: 6 (60.0%)  - Avg. Position: 6813546.5  - Sum of Positions: 40881279  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 5463479.54  - Sum of Positions: 273173977
Attacker B: Results: 50 (50.0%)  - Avg. Position: 5461888.84  - Sum of Positions: 273094442  (TLD True Hits/Total Hits: 1/2)

After 1000 entries:
Attacker A: Results: 506 (50.6%)  - Avg. Position: 3983089.15  - Sum of Positions: 2015443108
Attacker B: Results: 506 (50.6%)  - Avg. Position: 3977239.09  - Sum of Positions: 2012482980  (TLD True Hits/Total Hits: 7/13)

After 10000 entries:
Attacker A: Results: 5018 (50.18%)  - Avg. Position: 3658510.83  - Sum of Positions: 18358407326
Attacker B: Results: 5018 (50.18%)  - Avg. Position: 3648382.11  - Sum of Positions: 18307581405  (TLD True Hits/Total Hits: 70/164)

After 100000 entries:
Attacker A: Results: 49937 (49.94%)  - Avg. Position: 3832033.19  - Sum of Positions: 191360241382
Attacker B: Results: 49937 (49.94%)  - Avg. Position: 3815612.31  - Sum of Positions: 190540232027  (TLD True Hits/Total Hits: 685/1757)

After 1000000 entries:
Attacker A: Results: 498653 (49.87%)  - Avg. Position: 3841193.89  - Sum of Positions: 1915422857304
Attacker B: Results: 498653 (49.87%)  - Avg. Position: 3828606.87  - Sum of Positions: 1909146299854  (TLD True Hits/Total Hits: 6540/17336)

After 5000000 entries:
Attacker A: Results: 2493094 (49.86%)  - Avg. Position: 3854405.53  - Sum of Positions: 9609395309921
Attacker B: Results: 2493094 (49.86%)  - Avg. Position: 3841492.04  - Sum of Positions: 9577200752162  (TLD True Hits/Total Hits: 32312/85996)

After 10000000 entries:
Attacker A: Results: 4987101 (49.87%)  - Avg. Position: 3856490.17  - Sum of Positions: 19232706003727
Attacker B: Results: 4987101 (49.87%)  - Avg. Position: 3843357.15  - Sum of Positions: 19167210295882  (TLD True Hits/Total Hits: 64621/172366)

Attacker A (with miss): Results: 4987101 (49.87%)  - Avg. Position: 21904358.67  - Sum of Positions: 219043586720680
Attacker B (with miss): Results: 4987101 (49.87%)  - Avg. Position: 21904529.57  - Sum of Positions: 219045295748755  (TLD True Hits/Total Hits: 64621/172366)


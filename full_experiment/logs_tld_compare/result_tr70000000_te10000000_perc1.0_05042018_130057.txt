Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49589165 (excluded)
net , 4228633 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 55754.0  - Sum of Positions: 55754
Attacker B: Results: 1 (100.0%)  - Avg. Position: 55754.0  - Sum of Positions: 55754  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 4 (40.0%)  - Avg. Position: 447205.5  - Sum of Positions: 1788822
Attacker B: Results: 4 (40.0%)  - Avg. Position: 447205.5  - Sum of Positions: 1788822  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 39 (39.0%)  - Avg. Position: 2739714.05  - Sum of Positions: 106848848
Attacker B: Results: 39 (39.0%)  - Avg. Position: 2882893.08  - Sum of Positions: 112432830  (TLD True Hits/Total Hits: 2/4)

After 1000 entries:
Attacker A: Results: 520 (52.0%)  - Avg. Position: 3584883.32  - Sum of Positions: 1864139327
Attacker B: Results: 520 (52.0%)  - Avg. Position: 3662043.63  - Sum of Positions: 1904262686  (TLD True Hits/Total Hits: 27/59)

After 10000 entries:
Attacker A: Results: 4976 (49.76%)  - Avg. Position: 4008515.83  - Sum of Positions: 19946374756
Attacker B: Results: 4976 (49.76%)  - Avg. Position: 4036488.18  - Sum of Positions: 20085565180  (TLD True Hits/Total Hits: 220/578)

After 100000 entries:
Attacker A: Results: 49887 (49.89%)  - Avg. Position: 3864754.08  - Sum of Positions: 192800986869
Attacker B: Results: 49887 (49.89%)  - Avg. Position: 3893374.64  - Sum of Positions: 194228780438  (TLD True Hits/Total Hits: 2339/6059)

After 1000000 entries:
Attacker A: Results: 499571 (49.96%)  - Avg. Position: 3875493.89  - Sum of Positions: 1936084358757
Attacker B: Results: 499571 (49.96%)  - Avg. Position: 3901321.59  - Sum of Positions: 1948987128328  (TLD True Hits/Total Hits: 22992/60325)

After 5000000 entries:
Attacker A: Results: 2495034 (49.9%)  - Avg. Position: 3867822.33  - Sum of Positions: 9650348229999
Attacker B: Results: 2495034 (49.9%)  - Avg. Position: 3895255.65  - Sum of Positions: 9718795275488  (TLD True Hits/Total Hits: 114410/301846)

After 10000000 entries:
Attacker A: Results: 4990838 (49.91%)  - Avg. Position: 3858530.97  - Sum of Positions: 19257302972342
Attacker B: Results: 4990838 (49.91%)  - Avg. Position: 3887082.33  - Sum of Positions: 19399798226106  (TLD True Hits/Total Hits: 229340/604485)

Attacker A (with miss): Results: 4990838 (49.91%)  - Avg. Position: 21889917.76  - Sum of Positions: 218899177634070
Attacker B (with miss): Results: 4990838 (49.91%)  - Avg. Position: 21984396.52  - Sum of Positions: 219843965230026  (TLD True Hits/Total Hits: 229340/604485)


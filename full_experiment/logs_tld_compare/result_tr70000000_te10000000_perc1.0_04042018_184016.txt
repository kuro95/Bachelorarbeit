Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49590045 (excluded)
net , 4230274 (excluded)
uk , 1642189 (excluded)
fr , 1290415 (excluded)
edu , 1234171 (excluded)
br , 1202034 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 1520.0  - Sum of Positions: 1520
Attacker B: Results: 1 (100.0%)  - Avg. Position: 1520.0  - Sum of Positions: 1520  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 1954955.8  - Sum of Positions: 9774779
Attacker B: Results: 5 (50.0%)  - Avg. Position: 1954955.8  - Sum of Positions: 9774779  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 4788650.47  - Sum of Positions: 234643873
Attacker B: Results: 49 (49.0%)  - Avg. Position: 4805868.0  - Sum of Positions: 235487532  (TLD True Hits/Total Hits: 1/3)

After 1000 entries:
Attacker A: Results: 488 (48.8%)  - Avg. Position: 3834301.34  - Sum of Positions: 1871139056
Attacker B: Results: 488 (48.8%)  - Avg. Position: 3840240.31  - Sum of Positions: 1874037272  (TLD True Hits/Total Hits: 7/18)

After 10000 entries:
Attacker A: Results: 4967 (49.67%)  - Avg. Position: 3720674.26  - Sum of Positions: 18480589035
Attacker B: Results: 4967 (49.67%)  - Avg. Position: 3717756.88  - Sum of Positions: 18466098429  (TLD True Hits/Total Hits: 67/169)

After 100000 entries:
Attacker A: Results: 49614 (49.61%)  - Avg. Position: 3829457.32  - Sum of Positions: 189994695628
Attacker B: Results: 49614 (49.61%)  - Avg. Position: 3818049.83  - Sum of Positions: 189428724245  (TLD True Hits/Total Hits: 656/1708)

After 1000000 entries:
Attacker A: Results: 498612 (49.86%)  - Avg. Position: 3868870.94  - Sum of Positions: 1929065479239
Attacker B: Results: 498612 (49.86%)  - Avg. Position: 3855340.67  - Sum of Positions: 1922319123322  (TLD True Hits/Total Hits: 6530/17161)

After 5000000 entries:
Attacker A: Results: 2495258 (49.91%)  - Avg. Position: 3863294.87  - Sum of Positions: 9639917425052
Attacker B: Results: 2495258 (49.91%)  - Avg. Position: 3850096.59  - Sum of Positions: 9606984321049  (TLD True Hits/Total Hits: 32370/86095)

After 10000000 entries:
Attacker A: Results: 4989849 (49.9%)  - Avg. Position: 3865632.16  - Sum of Positions: 19288920762642
Attacker B: Results: 4989849 (49.9%)  - Avg. Position: 3852373.43  - Sum of Positions: 19222761692046  (TLD True Hits/Total Hits: 64529/172099)

Attacker A (with miss): Results: 4989849 (49.9%)  - Avg. Position: 21897713.14  - Sum of Positions: 218977131378117
Attacker B (with miss): Results: 4989849 (49.9%)  - Avg. Position: 21897828.44  - Sum of Positions: 218978284437558  (TLD True Hits/Total Hits: 64529/172099)


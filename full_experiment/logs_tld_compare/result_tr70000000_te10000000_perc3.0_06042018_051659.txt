Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 3 TLDs:

com , 49591197 (excluded)
net , 4229859 (excluded)
uk , 1640493 (excluded)
fr , 1288484 (excluded)
edu , 1235306 (excluded)
br , 1202925 (excluded)
it , 959134 (excluded)
in , 899555 (excluded)
org , 886683 (excluded)
nl , 731502 (excluded)
ca , 708940 (excluded)
au , 583717 (excluded)
de , 455376 
es , 418563 (excluded)
za , 367850 (excluded)
ru , 341334 (excluded)
ar , 248780 (excluded)
mx , 224310 (excluded)
us , 222996 (excluded)
dk , 204395 (excluded)
be , 199834 (excluded)
gov , 185031 (excluded)
se , 167364 (excluded)
pl , 157985 (excluded)
nz , 142348 (excluded)
cn , 142059 (excluded)
ch , 125975 
jp , 117029 (excluded)
pt , 112888 (excluded)
no , 112301 (excluded)
mil , 102742 (excluded)
id , 102342 (excluded)
cl , 101875 (excluded)
sg , 81578 (excluded)
ie , 74337 (excluded)
gr , 74299 (excluded)
il , 71832 (excluded)
fi , 70110 (excluded)
cz , 70102 (excluded)
tr , 58668 (excluded)
co , 56844 (excluded)
my , 52500 (excluded)
eu , 50319 (excluded)
hk , 49355 (excluded)
at , 46238 

After 1 entries:
Attacker A: Results: 1 (100.0%)  - Avg. Position: 450146.0  - Sum of Positions: 450146
Attacker B: Results: 1 (100.0%)  - Avg. Position: 450146.0  - Sum of Positions: 450146  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 7 (70.0%)  - Avg. Position: 7113915.29  - Sum of Positions: 49797407
Attacker B: Results: 7 (70.0%)  - Avg. Position: 7113915.29  - Sum of Positions: 49797407  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 49 (49.0%)  - Avg. Position: 5940051.16  - Sum of Positions: 291062507
Attacker B: Results: 49 (49.0%)  - Avg. Position: 5940051.16  - Sum of Positions: 291062507  (TLD True Hits/Total Hits: 0/0)

After 1000 entries:
Attacker A: Results: 486 (48.6%)  - Avg. Position: 3924311.98  - Sum of Positions: 1907215620
Attacker B: Results: 486 (48.6%)  - Avg. Position: 3922676.0  - Sum of Positions: 1906420538  (TLD True Hits/Total Hits: 3/10)

After 10000 entries:
Attacker A: Results: 5027 (50.27%)  - Avg. Position: 3768044.55  - Sum of Positions: 18941959942
Attacker B: Results: 5027 (50.27%)  - Avg. Position: 3762810.36  - Sum of Positions: 18915647700  (TLD True Hits/Total Hits: 24/97)

After 100000 entries:
Attacker A: Results: 49640 (49.64%)  - Avg. Position: 3864825.79  - Sum of Positions: 191849952446
Attacker B: Results: 49640 (49.64%)  - Avg. Position: 3857126.43  - Sum of Positions: 191467755970  (TLD True Hits/Total Hits: 189/917)

After 1000000 entries:
Attacker A: Results: 498637 (49.86%)  - Avg. Position: 3858006.32  - Sum of Positions: 1923744699199
Attacker B: Results: 498637 (49.86%)  - Avg. Position: 3849755.74  - Sum of Positions: 1919630653164  (TLD True Hits/Total Hits: 2058/8986)

After 5000000 entries:
Attacker A: Results: 2493414 (49.87%)  - Avg. Position: 3860101.5  - Sum of Positions: 9624831119077
Attacker B: Results: 2493414 (49.87%)  - Avg. Position: 3852639.94  - Sum of Positions: 9606226366293  (TLD True Hits/Total Hits: 9978/44986)

After 10000000 entries:
Attacker A: Results: 4984524 (49.85%)  - Avg. Position: 3854016.31  - Sum of Positions: 19210436814966
Attacker B: Results: 4984524 (49.85%)  - Avg. Position: 3846681.04  - Sum of Positions: 19173873958477  (TLD True Hits/Total Hits: 20030/90215)

Attacker A (with miss): Results: 4984524 (49.85%)  - Avg. Position: 21910225.79  - Sum of Positions: 219102257872346
Attacker B (with miss): Results: 4984524 (49.85%)  - Avg. Position: 21908048.85  - Sum of Positions: 219080488486805  (TLD True Hits/Total Hits: 20030/90215)


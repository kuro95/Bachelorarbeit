Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49591201 (excluded)
net , 4226196 (excluded)
uk , 1642165 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 6 (60.0%)  - Avg. Position: 5951482.67  - Sum of Positions: 35708896
Attacker B: Results: 6 (60.0%)  - Avg. Position: 5951482.67  - Sum of Positions: 35708896  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 48 (48.0%)  - Avg. Position: 2867689.17  - Sum of Positions: 137649080
Attacker B: Results: 48 (48.0%)  - Avg. Position: 2887612.02  - Sum of Positions: 138605377  (TLD True Hits/Total Hits: 2/3)

After 1000 entries:
Attacker A: Results: 510 (51.0%)  - Avg. Position: 3637413.78  - Sum of Positions: 1855081029
Attacker B: Results: 510 (51.0%)  - Avg. Position: 3655209.2  - Sum of Positions: 1864156694  (TLD True Hits/Total Hits: 8/26)

After 10000 entries:
Attacker A: Results: 5007 (50.07%)  - Avg. Position: 3635772.02  - Sum of Positions: 18204310496
Attacker B: Results: 5007 (50.07%)  - Avg. Position: 3625078.95  - Sum of Positions: 18150770287  (TLD True Hits/Total Hits: 82/230)

After 100000 entries:
Attacker A: Results: 49935 (49.94%)  - Avg. Position: 3811731.04  - Sum of Positions: 190338789263
Attacker B: Results: 49935 (49.94%)  - Avg. Position: 3799833.36  - Sum of Positions: 189744678716  (TLD True Hits/Total Hits: 842/2263)

After 1000000 entries:
Attacker A: Results: 499173 (49.92%)  - Avg. Position: 3859878.78  - Sum of Positions: 1926747271250
Attacker B: Results: 499173 (49.92%)  - Avg. Position: 3852702.81  - Sum of Positions: 1923165218909  (TLD True Hits/Total Hits: 8912/23304)

After 5000000 entries:
Attacker A: Results: 2494632 (49.89%)  - Avg. Position: 3853722.38  - Sum of Positions: 9613619162178
Attacker B: Results: 2494632 (49.89%)  - Avg. Position: 3846722.4  - Sum of Positions: 9596156796873  (TLD True Hits/Total Hits: 44554/117125)

After 10000000 entries:
Attacker A: Results: 4987715 (49.88%)  - Avg. Position: 3850126.9  - Sum of Positions: 19203335666441
Attacker B: Results: 4987715 (49.88%)  - Avg. Position: 3843133.79  - Sum of Positions: 19168456044484  (TLD True Hits/Total Hits: 89408/234429)

Attacker A (with miss): Results: 4987715 (49.88%)  - Avg. Position: 21896731.78  - Sum of Positions: 218967317781246
Attacker B (with miss): Results: 4987715 (49.88%)  - Avg. Position: 21904767.29  - Sum of Positions: 219047672863289  (TLD True Hits/Total Hits: 89408/234429)


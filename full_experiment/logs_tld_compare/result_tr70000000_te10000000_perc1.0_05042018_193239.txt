Attacker A: Default List - Attacker B: TLD-Lists

Results with 70000000 Training Entries and 10000000 Random Entries and top 1 TLDs:

com , 49591832 (excluded)
net , 4230316 (excluded)
uk , 1640634 (excluded)
fr , 1288373 (excluded)
edu , 1233308 (excluded)
br , 1201691 (excluded)
it , 959405 (excluded)
in , 900481 (excluded)
org , 886236 (excluded)
nl , 731798 (excluded)
ca , 709335 (excluded)
au , 583031 (excluded)
de , 456199 

After 1 entries:
Attacker A: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0
Attacker B: Results: 0 (0.0%)  - Avg. Position: 0  - Sum of Positions: 0  (TLD True Hits/Total Hits: 0/0)

After 10 entries:
Attacker A: Results: 5 (50.0%)  - Avg. Position: 6211058.4  - Sum of Positions: 31055292
Attacker B: Results: 5 (50.0%)  - Avg. Position: 6211058.4  - Sum of Positions: 31055292  (TLD True Hits/Total Hits: 0/0)

After 100 entries:
Attacker A: Results: 50 (50.0%)  - Avg. Position: 4199522.32  - Sum of Positions: 209976116
Attacker B: Results: 50 (50.0%)  - Avg. Position: 4199522.32  - Sum of Positions: 209976116  (TLD True Hits/Total Hits: 0/2)

After 1000 entries:
Attacker A: Results: 499 (49.9%)  - Avg. Position: 3724643.77  - Sum of Positions: 1858597239
Attacker B: Results: 499 (49.9%)  - Avg. Position: 3726129.72  - Sum of Positions: 1859338731  (TLD True Hits/Total Hits: 0/5)

After 10000 entries:
Attacker A: Results: 4895 (48.95%)  - Avg. Position: 4068270.63  - Sum of Positions: 19914184745
Attacker B: Results: 4895 (48.95%)  - Avg. Position: 4067001.73  - Sum of Positions: 19907973451  (TLD True Hits/Total Hits: 16/67)

After 100000 entries:
Attacker A: Results: 49478 (49.48%)  - Avg. Position: 3903356.41  - Sum of Positions: 193130268443
Attacker B: Results: 49478 (49.48%)  - Avg. Position: 3899389.25  - Sum of Positions: 192933981326  (TLD True Hits/Total Hits: 129/614)

After 1000000 entries:
Attacker A: Results: 498715 (49.87%)  - Avg. Position: 3850477.17  - Sum of Positions: 1920290723621
Attacker B: Results: 498715 (49.87%)  - Avg. Position: 3844842.47  - Sum of Positions: 1917480611855  (TLD True Hits/Total Hits: 1526/6418)

After 5000000 entries:
Attacker A: Results: 2494106 (49.88%)  - Avg. Position: 3852145.74  - Sum of Positions: 9607659812027
Attacker B: Results: 2494106 (49.88%)  - Avg. Position: 3846096.44  - Sum of Positions: 9592572208037  (TLD True Hits/Total Hits: 7997/32408)

After 10000000 entries:
Attacker A: Results: 4987513 (49.88%)  - Avg. Position: 3851315.01  - Sum of Positions: 19208483703474
Attacker B: Results: 4987513 (49.88%)  - Avg. Position: 3845465.82  - Sum of Positions: 19179310764396  (TLD True Hits/Total Hits: 16074/65409)

Attacker A (with miss): Results: 4987513 (49.88%)  - Avg. Position: 21899967.42  - Sum of Positions: 218999674227939
Attacker B (with miss): Results: 4987513 (49.88%)  - Avg. Position: 21898401.94  - Sum of Positions: 218984019429513  (TLD True Hits/Total Hits: 16074/65409)


list_tld = ['uk', 'fr', 'br', 'it', 'in', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'ua', 'lv']
path_tld = "D:\\Bachelorarbeit\\database\\hashes_per_tld\\"
path_hash = "D:\\Bachelorarbeit\\experiment\\TLD_lists\\"
top_value = 5000
top_value_dumb = 250000

for tld in list_tld:
	file_tld = open(path_tld+tld+'.txt','r')
	file_hash = open(path_hash+tld+'.txt','w')
	i = -2
	for line in file_tld:
	  if(i > 0):
	  	file_hash.write(line.split(' ', 1)[0]+'\n')
	  if(i >= top_value):
	  	break
	  i += 1
	file_tld.close()
	file_hash.close()
	print(tld)

file_default_seek = open("D:\\Bachelorarbeit\\database\\hashes_per_tld\\fulllist.txt",'r')
file_default_create = open("D:\\Bachelorarbeit\\experiment\\TLD_lists\\default.txt",'w')
file_default_attacker_one = open("D:\\Bachelorarbeit\\experiment\\default.txt",'w')
i = -2
for line in file_default_seek:
	if(i > 0):
		file_default_create.write(line.split(' ', 1)[0]+'\n')
	if(i >= top_value):
		break
	i += 1
print("default")
i = -2
for line in file_default_seek:
	if(i > 0):
		file_default_attacker_one.write(line.split(' ', 1)[0]+'\n')
	if(i >= top_value_dumb):
		break
	i += 1
print("attacker")
file_default_seek.close()
file_default_create.close()
file_default_attacker_one.close()
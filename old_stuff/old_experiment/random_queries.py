import mysql.connector
from mysql.connector import errorcode
import time

position_a_list = []
position_b_list = []
path_hash = "D:\\Bachelorarbeit\\experiment\\TLD_lists\\"
path_default = "D:\\Bachelorarbeit\\experiment\\"
query_value = 10000
list_tld = ['default', 'uk', 'fr', 'br', 'it', 'in', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'ua', 'lv']

def dict_init():
	for tld in list_tld:
		file_tld = open(path_hash+tld+'.txt','r')
		dict[tld] = []
		for line in file_tld:	
			dict[tld].append(line.split()[0])
	file_default = open(path_default+'default.txt','r')
	dict['attacker'] = []
	for line in file_default:
		dict['attacker'].append(line.split()[0])

def attacker_normal(hash_string):
	position_a = 0
	for item in dict['attacker']:
		position_a += 1
		if(hash_string == item):
			position_a_list.append(position_a)
			return 1
	return 0

def attacker_tld(tld,hash_string):
	position_b = 0
	if not tld in list_tld:
		tld = 'default'
	for item in dict[tld]:
		position_b += 1
		if(hash_string == item):
			position_b_list.append(position_b)
			return 1
	return 0
try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
    database='bachelorarbeit')
  cursor = cnx.cursor(buffered=True)
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)


dict = {}
dict_init()

result_a = 0
result_b = 0
query = ("SELECT * FROM bachelorarbeit.fulllist ORDER BY RAND() LIMIT "+str(query_value))
cursor.execute(query)
stored_query = cursor.fetchall()
print("Fetched! Time started")
start = time.time()
for (email,hash_string) in stored_query:
	tld = email.split('.')[-1].lower()
	result_a += attacker_normal(hash_string)
	result_b += attacker_tld(tld,hash_string)
end = time.time()
avg_pos_a = round(sum(position_a_list)/result_a,2)
avg_pos_b = round(sum(position_b_list)/result_b,2)
print("Results with "+str(query_value)+" random entries:")
print("Attacker A: Results: "+str(result_a)+" ("+str(round(((result_a/query_value)*100),2))+"%)  \tAvg. Position: "+str(avg_pos_a)+"  \tSum of Positions: "+str(sum(position_a_list)))
print("Attacker B: Results: "+str(result_b)+" ("+str(round(((result_b/query_value)*100),2))+"%)   \tAvg. Position: "+str(avg_pos_b)+"  \tSum of Positions: "+str(sum(position_b_list)))
print("Execution time of loop: "+str((end - start))+" sec")
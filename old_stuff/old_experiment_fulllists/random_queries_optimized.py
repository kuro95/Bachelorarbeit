import mysql.connector
from mysql.connector import errorcode
import time

position_a_list = []
position_b_list = []
path_hash = "G:\\Bachelorarbeit\\database\\hashes_per_tld\\"
path_default = "G:\\Bachelorarbeit\\experiment_fulllists\\"
query_value = 5000000
break_value = [1,10,100,1000,10000,100000,1000000]
tld_hit = 0
tld_list_size = []
list_tld = ['uk', 'fr', 'br', 'it', 'in', 'nl', 'ca', 'au', 'de', 'es', 'za', 'ru', 'ar', 'mx', 'us', 'dk', 'be', 'se', 'pl', 'nz', 'cn', 'ch', 'jp', 'pt', 'no', 'id', 'cl', 'sg', 'gr', 'ie', 'il', 'cz', 'fi', 'tr', 'co', 'my', 'hk', 'at', 'bg', 'tw', 'hu', 'ph', 'ro', 'ae', 'hr', 'pe', 'vn', 'kr', 'ua', 'lv']

def dict_init():
	for tld in list_tld:
		file_tld = open(path_hash+tld+'.txt','r')
		dict[tld] = {}
		position = 0
		i = 0
		for line in file_tld:
			if i == 3:
				position += 1
				dict[tld][line.split()[0]] = position
			else:
				i += 1
		tld_list_size.append((tld,position))
	file_default = open(path_hash+'fulllist.txt','r')
	dict['attacker'] = {}
	position = 0
	i = 0
	for line in file_default:
		if i == 3:
			position += 1
			dict['attacker'][line.split()[0]] = position
		else:
			i += 1
	tld_list_size.append(('attacker',position))

def attacker_normal(hash_string):
	position_a = 0
	if hash_string in dict['attacker']:
		position_a_list.append(dict['attacker'][hash_string])
		return 1
	return 0

def attacker_tld(tld,hash_string):
	position_b = 0
	global tld_hit
	tld_hit += 1
	if not tld in list_tld:
		tld = 'attacker'
		tld_hit -= 1
	if tld == 'attacker':	
		if hash_string in dict[tld]:
			position_b_list.append(dict[tld][hash_string])
			return 1
		return 0
	else:
		if hash_string in dict[tld]:
			position_b_list.append(dict[tld][hash_string])
			return 1
		size_tld = 0
		for item in tld_list_size:
			if item[0] == tld:
				size_tld = item[1]
		tld = 'attacker'
		if hash_string in dict[tld]:
			position_b_list.append(dict[tld][hash_string]+size_tld)
			return 1
		return 0

def save_div_perc(result,value):
	if value == 0:
		return 0
	else:
		return round((result/value)*100,2)

def save_div_pos(result,list):
	if result == 0:
		return 0
	else:
		return round((sum(list)/result),2)

try:
  cnx = mysql.connector.connect(user='root',password='root',host='localhost',
    database='bachelorarbeit')
  cursor = cnx.cursor(buffered=True)
except mysql.connector.Error as err:
  if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
    print("Something is wrong with your user name or password")
  elif err.errno == errorcode.ER_BAD_DB_ERROR:
    print("Database does not exist")
  else:
    print(err)


dict = {}
dict_init()
file_output = open(path_default+'output.txt','w')
file_output.write("Attacker A: Default List - Attacker B: TLD-Lists\n\n")
file_output.write("Results with "+str(query_value)+" random entries:\n\n")
result_a = 0
result_b = 0
query = ("SELECT * FROM bachelorarbeit.fulllist LIMIT "+str(query_value))
cursor.execute(query)
stored_query = cursor.fetchall()
print("Fetched! Time started")
start = time.time()
i = 0
for (email,hash_string) in stored_query:
	i += 1
	tld = email.split('.')[-1].lower()
	result_a += attacker_normal(hash_string)
	result_b += attacker_tld(tld,hash_string)
	if i in break_value:
		file_output.write("After "+str(i)+" entries:\n")
		file_output.write("Attacker A: Results: "+str(result_a)+" ("+str(save_div_perc(result_a,i))+"%)  - Avg. Position: "+str(save_div_pos(result_a,position_a_list))+"  - Sum of Positions: "+str(sum(position_a_list))+"\n")
		file_output.write("Attacker B: Results: "+str(result_b)+" ("+str(save_div_perc(result_b,i))+"%)  - Avg. Position: "+str(save_div_pos(result_b,position_b_list))+"  - Sum of Positions: "+str(sum(position_b_list))+"  (TLD Hits: "+str(tld_hit)+")\n\n")
end = time.time()
file_output.write("After "+str(query_value)+" entries:\n")
file_output.write("Attacker A: Results: "+str(result_a)+" ("+str(save_div_perc(result_a,query_value))+"%)  - Avg. Position: "+str(save_div_pos(result_a,position_a_list))+"  - Sum of Positions: "+str(sum(position_a_list))+"\n")
file_output.write("Attacker B: Results: "+str(result_b)+" ("+str(save_div_perc(result_b,query_value))+"%)  - Avg. Position: "+str(save_div_pos(result_b,position_b_list))+"  - Sum of Positions: "+str(sum(position_b_list))+"  (TLD Hits: "+str(tld_hit)+")\n\n")
print("Execution time of loop: "+str((end - start))+" sec")
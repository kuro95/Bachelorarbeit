\documentclass{beamer}
\usepackage[language=english]{uibkstyle}
\DeclareMathAlphabet{\pazocal}{OMS}{zplm}{m}{n}

\title{Country-dependent analysis of the LinkedIn leak}
\subtitle{Initialpräsentation\\Betreuer: Dr. Pascal Schöttle}
\author{Daniel Linter}

\begin{document}
\begin{frame}[plain]
\maketitle
\end{frame}

\begin{frame}{Überblick}
	\tableofcontents
\end{frame}

\section{Motivation}

\begin{frame}{Motivation}
	\includegraphics[width=1\textwidth]{login_masks.png}
\end{frame}

\section{Common Practices}

\begin{frame}{Common Practices}
	\includegraphics[width=1\textwidth]{5-most-used-passwords.png} \\
	\hspace*{10pt}\hbox{\scriptsize [1] http://wpengine.com/unmasked/assets/images/5-most-used-passwords.png}
\end{frame}

\section{LinkedIn Leak}

\begin{frame}{LinkedIn}
\centering
	\includegraphics[width=0.82\textwidth]{linkedin_countries.png} \\
	\hspace*{10pt}\hbox{\scriptsize [2] https://content.linkedin.com/content/dam/press/images/500M-FINAL.jpg}
\end{frame}

\begin{frame}{LinkedIn Leak}
\begin{itemize}
\item Angriff aus Jahre 2012 \newline
\item Rund 160 Millionen Datensätze \newline
\item Beinhalten E-Mail und gehashte Passwörter\newline
\item Datensatz wurde bereinigt und auf leere Passwörter überprüft \newline
\end{itemize}
\end{frame}

\section{Forschungsfrage}

\begin{frame}{Forschungsfrage}
\centering
{\Large Hat der Angreifer einen Vorteil, \\[2ex] wenn er die Top-Level-Domain des Nutzers kennt?}
\end{frame}

\section{Unterschiede zwischen Ländern}

\begin{frame}{Unterschiede zwischen Ländern}
\begin{columns}
    \begin{column}{0.47\textwidth}
       \begin{table}[H]
\centering
\parbox{.85\linewidth}{
\centering
  \begin{tabular}{ |c r| }
\hline
 Klartext & Anzahl \\
    \hline
123456 & 2723 \\
linkedin & 832 \\
berlin & 402 \\
sommer & 352 \\
passwort & 323 \\
qwertz & 319 \\
123456789 & 301 \\
geheim & 288 \\
hamburg & 283 \\
sunshine & 267 \\
    \hline
  \end{tabular}
\only<2>{\caption{Häufigste Hashes von .de}}
}
\end{table}
    \end{column}
    \begin{column}{0.5\textwidth}
\begin{table}[H]
\centering
\parbox{.85\linewidth}{
\centering
\begin{tabular}{ |c r| }
\hline
 Klartext & Anzahl \\
    \hline
123456 & 8134 \\
linkedin & 1757 \\
francesco & 1712 \\
andrea & 1691 \\
francesca & 1509 \\
alessandro & 1471 \\
juventus & 1302 \\
giovanni & 1202 \\
lorenzo & 1199 \\
giulia & 1196 \\ 
    \hline
  \end{tabular}
\only<2>{\caption{Häufigste Hashes von .it}}
}
\end{table}
    \end{column}
\end{columns}
\end{frame}

\section{Globale vs länderspezifische Listen}
\begin{frame}{Globale vs länderspezifische Listen}
\begin{itemize}
\visible<1->{\item Um wie viel und lohnt es sich? \newline}
\visible<2->{\item Namen und Städte werden sich immer nach Land ändern \newline}
\visible<3->{\item Lokale Listen, die je nach TLD verschieden sind.\newline}
\visible<4->{\item Aufgrund von bestehenden Erkenntnissen, wie z.b. Leaks\newline}
\end{itemize}
\end{frame}

\section{Metriken}

\begin{frame}{Metriken}
\begin{itemize}
\visible<1->{\item $\beta$-success-rate: \newline
\begin{equation*}\lambda_\beta (\pazocal{X}) = \displaystyle\sum_{i=1}^{\beta} p_i {\hspace*{10pt}\hbox{\scriptsize [3]}} \end{equation*} \newline}
\visible<2->{\item  $\alpha$-work-factor: \newline
\begin{equation*} \mu_\alpha (\pazocal{X}) =  min \{  j \mid \displaystyle\sum_{i=1}^{j} p_i \geq \alpha \} {\hspace*{10pt}\hbox{\scriptsize [3]}} \end{equation*} \newline}
\end{itemize}
\end{frame}

\begin{frame}{Passwörter in Österreich}
\includegraphics[width=0.97\textwidth]{pass_austria.png} \\
\hspace*{10pt}\hbox{\scriptsize [4] https://blog.haschek.solutions/2016/06/19/austrian\_password/}
\end{frame}

\begin{frame}{}
\centering
{\LARGE Danke für die Aufmerksamkeit!}
\\ [6em]
{\LARGE Fragen?}
\end{frame}

\begin{frame}{Quellenverzeichnis}
\begin{itemize}
{\small \item [1] -  Unmasked: What 10 million passwords reveal about the people who choose them | http://wpengine.com/unmasked/assets/images/5-most-used-passwords.png\newline
\item [2] - About Us - LinkedIn | https://content.linkedin.com/content/dam/press/images/500M-FINAL.jpg\newline
\item [3] - Joseph Bonneau. Guessing human-chosen secrets. University of Cambridge, 1 edition, 2012.
\newline
\item [4] - Haschek Solutions blog - Austrian password mentality | https://blog.haschek.solutions/2016/06/19/austrian\_passwords/ \newline
\item [5] - XKCD - Password Strength | https://imgs.xkcd.com/comics/password\_strength.png\newline}
\end{itemize}
\end{frame}

\begin{frame}{Best Practices}
\centering
\includegraphics[width=0.7\textwidth]{password_strength.png} \\
\hspace*{10pt}\hbox{\scriptsize [5] https://imgs.xkcd.com/comics/password\_strength.png}
\end{frame}

\begin{frame}{Ethik}
\begin{itemize}
\item Wir versuchen, Personen zu besseren Passwörter zu bewegen \newline
\item Angreifer können aber reagieren \newline
\item Spezifische Vorgaben schlecht -> Angreifer hat gleiche Wissen \newline
\item Personen bewegen, persönliche sichere Passwörter zu überlegen \newline
\end{itemize}
\end{frame}

\end{document}
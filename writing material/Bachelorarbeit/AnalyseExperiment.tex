\subsection{Aufbau des Experiments}
\label{subsec:Aufbau}

Nachdem wir uns die Metriken angesehen haben, wenden wir uns nun dem Experiment zu. Das Experiment soll ein reales Szenario darstellen, um dadurch aussagekräftige Antworten auf unsere Forschungsfragen erhalten zu können. Dafür benötigen wir aber zwei verschiedene Angreifer. Das Szenario in unserem Experiment lautet wie folgt: Ein Angreifer möchte aus einer vorher definierten Anzahl an zufälligen E-Mail-Passwort-Paaren so viele Passwörter wie möglich knacken. \\
\\
Um dies zu ermöglichen, müssen wir nun den Begriff \enquote{Passwortliste} definieren. Jeder Angreifer bekommt eine zufällige Menge an Email-Passwort-Paaren, welche jedoch disjunkt zur der vorher genannten ist. Aus dieser Menge extrahiert der Angreifer die Passwörter und ordnet diese der Häufigkeit absteigend an. Folglich ist das häufigste Passwort an der ersten Stelle, gefolgt von dem zweithäufigsten Passwort an der zweiten Stelle, bis jedes Passwort mittels seiner Wahrscheinlichkeit einsortiert wurde. Damit hat der Angreifer die Reihenfolge der Passwörter in seiner Liste festgelegt. Im Falle von TLD-spezifischen Passwortlisten, welche in Kapitel \ref{subsubsec:Erstellung} genauer erklärt werden, mach der Angreifer einen Zwischenschritt und berechnet zuerst eine Teilmenge des Datensatzes, welche die gesuchten TLDs in deren E-Mail besitzen. Die restlichen Schritte sind wiederum identisch zu den vorher genannten. \\
\\
Mit dieser Passwortliste kann der Angreifer nun versuchen beliebige Passwörter zu knacken, in dem er jedes in seiner Liste befindliches Passwort nacheinander durchprobiert, bis er erstens einen Treffer erzielt hat, oder zweitens seine Liste zu Ende ist. Am Ende jedes Angriffs wird die Position des Passwortes in seiner eigenen Passwortliste gespeichert und später ausgewertet. \\
\\
Wichtig zu erwähnen ist, dass bei fast allen Experimentdurchläufen eine sogenannte \enquote{Blacklist} implementiert wurde, um alle internationalen TLDs ausschließen zu können. Dies ist notwendig, um den länderspezifischen Faktor der Arbeit beibehalten. Das einzige Teilexperiment, in welcher keine Blacklist zur Verwendung kommt, ist der Vergleich zwischen den einzelnen TLDs, aber dazu mehr in Kapitel \ref{subsubsec:einzelne-tld}.\\
\\
Dieses Szenario gilt für beide Angreifer, wobei allerdings der zweite Angreifer noch für jede angegebene TLD eine zusätzliche Passwortliste bekommt. Im folgenden Kapitel \ref{subsubsec:Erstellung} sehen wir uns an, wie genau sich beide Angreifer unterscheiden.

\subsubsection{Erstellung der Passwortliste(n)}
\label{subsubsec:Erstellung}

\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Media/tld_listen.png}
\captionof{figure}{Übersicht der Listen beider Angreifer in einem Beispiel}
\label{tbl:tld_listen} 
~ \\
Wie bereits erwähnt, benötigen wir zwei Angreifer für unser Experiment. Im Folgenden werden sie \enquote{Angreifer A} und \enquote{Angreifer B} genannt. Der Unterschied zwischen beiden Angreifern liegt nur in den Passwortlisten, die sie mittels der zur Verfügung gestellten Trainingsdatensätze erstellen. Während Angreifer A nur eine einzige Standardliste berechnet, erstellt Angreifer B noch zusätzlich zu jener Standardliste, die Angreifer A hat, für jede angegebene TLD eine eigene Passwortliste. Wichtig anzumerken ist, dass die Zusatzlisten aus dem identischen Trainingsdatensatz erstellt werden. Es wird lediglich als Erstes aus dem Trainingsdatensatz eine Teilmenge gebildet, welche nur Einträge berücksichtigt, deren E-Mail mit der ausgewählten TLD übereinstimmt. Dies geschieht so lange, bis eine vorher definierte Anzahl an TLDs erreicht wurde (Hard Cap), oder die nächste TLD einen vorher definierten Prozentanteil (Soft Cap) unterschreitet. \\
\\
Die Implementierung dieses Abschnitts beginnt damit, dass zuerst eine Menge von x zufälligen Benutzern aus der Datenbank geladen wird. Dies ist unser Trainingsdatensatz. Daraus werden nun für die Standardliste die Passwörter extrahiert und deren Häufigkeit aufsummiert. Im folgenden Schritt wird die Liste nach der Häufigkeit der Passwörter absteigend sortiert und somit bekommen wir unsere sortierte Passwortliste, die wir im zweiten Teil für den Angriff benötigen. Die Liste muss deshalb der Häufigkeit nach sortiert werden, da wir den maximalen Erfolg mit minimaler Versuchsanzahl erreichen möchten. Daher bietet es sich an, das häufigste Passwort an erster Stelle zu setzen, da wir zum Beispiel mit einem einzigen Versuch die höchstmögliche Wahrscheinlichkeit haben das Passwort zu knacken. \\
\\
Die Abbildung \ref{tbl:tld_listen} zeigt ein konkretes Beispiel, wie Angreifer B seine zusätzlichen Passwortlisten nutzen kann. In diesem Beispiel arbeitet Angreifer A mit einer einzigen Standardliste. Angreifer B besitzt die identische Standardliste, jedoch zusätzliche Listen für die TLDs .it, .de und .at. Die Passwörter jeder Zusatzliste sind in der Standardliste bereits enthalten, da die Zusatzliste eine echte Teilemenge der Standardliste ist. Falls nun ein Passwort geknackt werden muss, dessen E-Mail die TLD einer unserer drei Listen beinhaltet, kann Angreifer B sofort zu dieser Zusatzliste springen. Falls das Passwort nicht in dieser Liste enthalten ist, kann Angreifer B darauffolgend in der Standardliste suchen.

\subsubsection{Angriffsdurchführung}
\label{subsubsec:Angriff}

Nachdem wir die Passwortlisten für unsere Angreifer vorbereitet haben, können wir nun den \enquote{Angriff} durchführen. Hierbei wird wieder eine Anzahl von y zufälligen E-Mail-Passwort-Paaren aus der Datenbank geladen und unabhängig von beiden Angreifern gleichzeitig attackiert. Dabei ist es wichtig, dass unser Testdatensatz eine zum Trainingsdatensatz disjunkte Menge ist, da wir sonst keine unabhängigen Messungen durchführen können. Da wir nur die Position des Passwortes in der Liste erfahren möchten, müssen wir den Angriff nicht realitätsgetreu darstellen. Stattdessen können wir uns den kostenintensiven Prozess sparen und müssen uns einfach nur merken, an welcher Stelle das Passwort in der unserer Passwortliste liegt. Dazu nehmen wir das zu suchende Passwort und gleichen es mit unserer Liste ab. Falls es vorhanden ist, merken wir uns die Position. Wenn nicht, dann wird keine Position gespeichert und in beiden Fällen fahren wir mit dem nächsten Passwort aus dem Testdatensatz fort. \\
\\
Eine Ausnahme bildet allerdings Angreifer B. Dieser extrahiert sich zuerst die TLD aus der E-Mail und sieht nach, ob er zu dieser TLD eine passende Zusatzliste hat. Falls nicht, führt er den Angriff wie Angreifer A fort. Sollte er allerdings eine Liste zu dieser bestimmten TLD haben, dann überprüft er zuerst die TLD-spezifische Liste nach dem Passwort. Falls er das Passwort in seiner Zusatzliste gefunden hat, speichert er die Position und springt zum nächsten Passwort. Falls er das Passwort allerdings in seiner Zusatzliste nicht finden kann, greift er auf die Standardliste zurück und überprüft diese nach dem Passwort. Er speichert in der Zwischenzeit aber die Größe dieser TLD-spezifischen Liste, da die Kosten der Suche in den Zusatzlisten mitberechnet werden müssen. Falls er das Passwort dann in der Standardliste findet, speichert Angreifer B die Position in der Standardliste addiert mit der Größe der Zusatzliste. Falls er allerdings das Passwort in der Standardliste auch nicht finden kann, speichert er wie Angreifer A nichts. \\
\\

\includegraphics[width=\textwidth,height=\textheight,keepaspectratio]{Media/attack_flowchart.png}
\captionof{figure}{Flowchart der beiden Angreifer}
\label{tbl:flowchart}
~ \\
Das Flussdiagramm in Abbildung \ref{tbl:flowchart} soll den Entscheidungsweg beider Angreifer visuell verdeutlichen.\\
\\
Am Ende werden alle Positionen aufsummiert und durch die Anzahl der gefunden Passwörter dividiert. So erhält man die durchschnittliche Position, an der das Passwort von einem Angreifer in seinen Listen gefunden wurde. Dies ist die Hauptkennzahl dieser Arbeit. Denn mit ihr können wir nun quantifizieren, ob und wie groß der Vorteil eines Angreifers ist.

\subsubsection{Kennzahlen und Durchführungsmethode}
\label{subsubsec:Kennzahlen}

Das Experiment wird für jede eindeutige Kombination von Anfangswerten zehn Mal durchgeführt, um eine gewisse statistische Signifikanz zu erreichen. In Kapitel \ref{subsec:ergebnisse-experiment} werden eigene Kennzahlen angeführt, die wir auf der folgenden Seite einführen und erklären werden. Die Werte in Kapitel \ref{subsec:ergebnisse-experiment} beziehen sich immer auf den Durchschnitt über zehn Durchläufe, außer es wird anders gekennzeichnet.

\begin{changemargin}{-1cm}{-1cm}

\begin{landscape}
\begin{labeling}{(TLD) Hard Cap:}
\item [Trainingsgröße:] Die Anzahl an E-Mail-Passwort-Paaren, die wir verwenden um unsere Passwortlisten zu \enquote{trainieren}
\item [Testgröße:] Die Anzahl an E-Mail-Passwort-Paaren, die wir mit unseren Passwortlisten \enquote{angreifen} (disjunkt zum Training)
\item [(TLD) Hard Cap:] Die vorher festgelegte Anzahl an TLDs, die unser Angreifer B verwenden soll.
\item [(TLD) Soft Cap:] Der Prozentanteil an der ganzen Liste, welche jede TLD überschreiten muss, um berücksichtigt zu werden. 
\item [Results A:] Die Anzahl an gefunden Passwörtern von Angreifer A (Diese sollte immer der Zahl von Angreifer B entsprechen)
\item [Perc A:] Der Prozentanteil an Passwörtern, die von Angreifer A gefunden wurden.
\item [TLD Hits:] Anzahl der Passwörter, die in den TLD-spezifischen Listen gefunden wurden.
\item [TLD Total:] Wie oft insgesamt in die TLD-spezifischen Listen hinein gesprungen wurde (unabhängig ob das Passwort gefunden wurde oder nicht)
\item [Perc Hits:] Prozentanteil aus den zwei vorherigen Kennzahlen. Bezeichnet den Anteil der Passwörter, die in den TLD-spezifischen Listen gefunden wurden. (Nur erlaubte TLDs werden hier berücksichtigt.)
\item [Perc TLD:] Prozentanteil der Paare, zu denen der Angreifer eine passende TLD-spezifische Passwortliste zur Verfügung hatte.
\item [Avg Pos A:] Steht für \enquote{Average Position A} und kennzeichnet die durchschnittliche Position eines gefundenen Passwortes für Angreifer A. 
\item [Avg Pos B:] Steht für \enquote{Average Position B} und kennzeichnet die durchschnittliche Position eines gefundenen Passwortes für Angreifer B.
\item [Advantage B:] Dies entspricht dem prozentualen Vorteil, der Angreifer B mit diesen Werten erlangt. Es ist die Hauptkennzahl dieses Experimentes und lässt uns darüber aussagen, ob Angreifer B einen Vorteil (positive Zahl) oder einen Nachteil (negative Zahl) hat. Sie wird mittels dieser Formel berechnet: $\displaystyle\frac{\text{Angreifer A}}{\text{Angreifer B}}$ - $1$. Das Ergebnis ist dann eine Prozentzahl, welche den Vorteil wiedergibt.
\item [Abs Pos Adv B:] Steht für \enquote{Absolute Position Advantage B} und kennzeichnet die Anzahl an Positionen, die Angreifer B mit jedem gefundenen Passwort gegenüber Angreifer A spart. Beispiel: Angreifer A hat durchschnittlich eine Position von 100.000 und Angreifer B eine durchschnittliche Position von 60.000. So liegt seine \enquote{Absolute Position Advantage B} bei 40.000, da er sich bei jedem gefundenen Passwort im Durchschnitt 40.000 Positionen sparen kann. 
\item [StdDev Adv B:] Steht für \enquote{Standard Derivation Advantage B} und ist auf Deutsch die Standardabweichung des Vorteils von Angreifer B. Diese wird mit angeführt, um die statistische Robustheit des Experimentes zu zeigen.
\end{labeling}
\end{landscape}

\end{changemargin}